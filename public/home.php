<?php
	include '../public/includes/connect.php';

	$consulta = 'SELECT * FROM seccion_homes';
	$resultado= mysqli_query($conexion,$consulta);
	$r01 = mysqli_fetch_assoc($resultado);

?>


	<section class="main-head d-none d-lg-block">
		<div class="main-head__image">
			<img src="/storage/<?php echo $r01['img']; ?>" alt="Madesal">
		</div>
		<div class="main-head__title">
			<?php echo "<h1>".$r01['titulo_principal']."<span>".$r01['titulo_principal2']."</span></h1>"; ?>
		</div>
		<div class="main-head__form">
		<?php
			include '../public/includes/filtro.php';
		?>
		</div>
	</section>   

