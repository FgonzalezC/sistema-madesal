
<?php 
	include 'includes/funciones.php';
	
?>
    <!-- POST VENTA -->
    <section class="simuladores">
        <div class="container">
            <div class="row">
               <div class="col-12">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="hipotecario-tab" data-toggle="tab" href="#hipotecario" role="tab" aria-controls="hipotecario"
                                aria-selected="true">Simulador de dividendo</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="dividendo-tab" data-toggle="tab" href="#dividendo" role="tab" aria-controls="dividendo"
                                aria-selected="false"> Simulador hipotecario </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="endeudamiento-tab" data-toggle="tab" href="#endeudamiento" role="tab" aria-controls="endeudamiento"
                                aria-selected="false">Capacidad de endeudamiento</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="hipotecario" role="tabpanel" aria-labelledby="hipotecario-tab">
							<form  name="form-dividendo"  id="form-dividendo">
								<div class="row">
									<div class="col-lg-6">
										<ul>
											<li>Precio de la propiedad</li>
										<li><input type="number" id="input-with-keypress-0" name="precio_propiedad" min="0" value="" required></li>
										
											<li>
												<?php 
													if($valor=="uf"){
												?>
												<input type="radio" name="valor" id="uf" value="uf" checked>
												<label for="uf">UF</label>
												<input type="radio" name="valor" id="peso" value="peso">
												<label for="peso">$</label>

												<?php 
													}else if($valor=="peso"){
												?>

												<input type="radio" name="valor" id="uf" value="uf">
												<label for="uf">UF</label>
												<input type="radio" name="valor" id="peso" value="peso" checked>
												<label for="peso">$</label>

												<?php 
													}else{
												?>

												<input type="radio" name="valor" id="uf" value="uf">
												<label for="uf">UF</label>
												<input type="radio" name="valor" id="peso" value="peso">
												<label for="peso">$</label>
												<?php 
													}
												?>



											</li>
										</ul>									
										<div id="valor-propiedad" class="noUi-target noUi-ltr noUi-horizontal"></div>									
									</div>
									<div class="col-lg-6">
										<ul>
											<li>Pie (%)</li>
										<li><input type="number" id="input-with-keypress-1" name="pie"  min="0" pattern="\d+" max="100"  onFocus="this.blur()" required></li>
											<li>
												<input type="radio" name="valor_pie" id="porcent-pie"   value="porcent_pie" checked>
												<label for="porcent-pie">%</label>
											</li>
										</ul>									
										<div id="valor-pie" class="noUi-target noUi-ltr noUi-horizontal"></div>
									</div>
									<div class="col-lg-6">
										<ul>
											<li>Plazo (Años)</li>
											<li>
												<div id="plazo" class="noUi-target noUi-ltr noUi-horizontal"></div>
											</li>
											<li>
												<input type="number" id="input-with-keypress-2" id="valor" name="plazo" min="0"value="" step="any" required>
											</li>
										</ul>
									</div>
									<div class="col-lg-6">
										<ul>
											<li>Tasa de interés <span>(% anual)</span></li>
											<li>
												<div id="tasa" class="noUi-target noUi-ltr noUi-horizontal"></div>
											</li>
											<li>
											<input type="number" name="tasa" id="input-with-keypress-3" value="" min="0" max="4" step="any" required>
											</li>
										</ul>
									</div>
									<div class="col-12 text-center">
										<button onclick="calcular_divi();" id="btsubmit">Calcular</button>
									</div>
									<div id="capa-dividendo" class="col-12 text-center result" style="display: none;">
										<p>EL MONTO DEL DIVIDENDO DE TU CRÉDITO ES:</p>
									
										<div class="price" id="respuesta_dividendo"></div>
									
										<p class="small">*Los valores y tasas de interés anteriores son sólo referenciales, y por lo tanto, no compromete a madesal.cl, para más información contáctate con nuestros ejecutivos.</p>
									</div>
								</div>
							</form>
						</div>
                        <div class="tab-pane fade" id="dividendo" role="tabpanel" aria-labelledby="dividendo-tab">
							<form name="form-hipotecario"  id="form-hipotecario">
								<div class="row justify-content-center">
									<div class="col-lg-8 text-center">
										<h3>Simula tu crédito si ya conoces el valor de la vivienda.</h3>
									
										<p>Valor UF al día: <b><?php echo valorUF();?></b></p>
											
									
									</div>
									<div class="col-lg-8">
										<ul class="justify-content-center">
											<li>
												<h4>Monto a financiar</h4>
											</li>
											<li>
												<div id="monto" class="noUi-target noUi-ltr noUi-horizontal" ></div>
											</li>
											<li>
											<input type="number" name="monto" id="input-with-keypress-4" min="0"  value="">
											</li>									
										</ul>
									</div>
									<div class="col-lg-8">
										<ul class="justify-content-center">
											<li>
												<h4>Pie(%)</h4>
											</li>
											<li>
												<div id="div-plazo" class="noUi-target noUi-ltr noUi-horizontal"></div>
											</li>
											<li>
											<input type="number" name="div_plazo" id="input-with-keypress-5"  min="0" max="100"  step="any"  onFocus="this.blur()" value="">
											</li>
										</ul>
									</div>
									<!--<div class="col-lg-8">
										<ul class="justify-content-center">
											<li>
												<h4>Tasa de interés</h4>
											</li>
											<li>
												<div id="div-tasa" class="noUi-target noUi-ltr noUi-horizontal"></div>
											</li>
											<li>
											<input type="number" name="div_tasa" id="input-with-keypress-6"  step="any" value="{{$div_tasa}}">
											</li>
										</ul>
									</div>-->
									<div class="col-12 text-center">
										<button onclick="calcular_hipo();" id="bhsubmit">Calcular</button>
									</div>
									<div id="capa-hipotecario" class="col-12 text-center result" style="display: none;">
										<p>EL MONTO DE TU CRÉDITO A SOLICITAR ES:</p>
										<div class="price" id="respuesta_hipotecario"></div>
										<p class="small">*Los valores anteriores son sólo referenciales, y por lo tanto, no compromete a madesal.cl, para más información contáctate con nuestros ejecutivos.</p>
									</div>
								</div>
							</form>
						</div>
                        <div class="tab-pane fade" id="endeudamiento" role="tabpanel" aria-labelledby="endeudamiento-tab">
							<form class="gnrl-form" id="form-deuda">
								<div class="row align-items-end">
									<div class="col-lg-12 text-center">
										<h3>Responde las siguientes preguntas para revisar tu capacidad de endeudamiento.</h3>
										<p>Valor UF al día: <b><?php echo valorUF();?></b> </p>
									</div>
									<div class="col-lg-6">
										<div class="question">
											<label for="">¿Cuál es tu renta mensual líquida aproximada?</label>
										<input type="text" id="monto-renta" name ="monto_renta" value="" placeholder="ESCRIBE EL MONTO DE LA RENTA DEL TITULAR" required>
										</div>										
									</div>
									<div class="col-lg-6">
										<div class="question">											
										<input type="text" id="co-renta"  name="co_renta" value=""  placeholder="¿COMPLEMENTAS? ESCRIBE LA RENTA DEL CODEUDOR" required>
										</div>	
									</div>
									<div class="col-lg-6">
										<div class="question">
											<label for="">¿Tienes otros ingresos?</label>
											
											<select  id="otro_ingreso" name="otro_ingreso" required>
												<option style="font-weight:bold"  value=""></option>
												<option  name="otro_ingreso"  value="Diarios">Diarios</option>
												<option  name="otro_ingreso" value="Trimestrales">Trimestrales</option>
												<option  name="otro_ingreso"  value="Mensuales">Mensuales</option>				
											</select>
											
										</div>										
									</div>
									<div class="col-lg-6">
										<div class="question">											
										<input type="text" id="extra-monto" name="extra_monto" value="" placeholder="ESCRIBE EL MONTO" required>
										</div>
									</div>
									<div class="col-lg-12">
										<div class="question">
											<label for="">¿Tienes deudas?</label>
										<input type="text" id="monto-renta" name="monto_renta1" value="" placeholder="¿cuánto pagas Mensual por créditos (consumo/hipotecário)?" required>
											<input type="text" id="monto-renta" name="monto_renta2" value="" placeholder="¿cual es el crédito mensual de tu tarjeta de crédito??" required>
										</div>
									</div>
									<!--<div class="col-lg-12 text-center">
										<h3>DATO CRÉDITO HIPOTECARIO</h3>
										<ul class="justify-content-center">
											<li>
												<h4>Tasa de interés</h4>
											</li>
											<li>
												<div id="cap-tasa" class="noUi-target noUi-ltr noUi-horizontal"></div>
											</li>
											<li>
											<input type="number" name="cap_tasa" id="input-with-keypress-7" value="">
											</li>
										</ul>
										<ul class="justify-content-center">
											<li>
												<h4>Plazo</h4>
											</li>
											<li>
												<div id="cap-plazo" class="noUi-target noUi-ltr noUi-horizontal"></div>
											</li>
											<li>
											<input type="number" name="cap_plazo" id="input-with-keypress-8" value="">
											</li>
										</ul>
									</div>-->
									<div class="col-12 text-center">
										<button type="button" onclick="calcular_deuda();" id="btsubmit">Calcular</button>
										<!--<p class="small">*Los valores anteriores son sólo referenciales, y por lo tanto, no compromete a madesal.cl, para más información contáctate con nuestros ejecutivos.</p>
									</div>-->
									<div id="capa-deuda" class="col-12 text-center result" style="display: none;">
										<p>EL MONTO DEL DIVIDENDO DE TU CRÉDITO ES:</p>
									
										<div class="price" id="respuesta_deuda"></div>
									
										<p class="small">*Los valores y tasas de interés anteriores son sólo referenciales, y por lo tanto, no compromete a madesal.cl, para más información contáctate con nuestros ejecutivos.</p>
									</div>
								</div>		
							</form>
						</div>
                    </div>
               </div>
            </div>
        </div>
	</section>
