<?php
	include 'connect.php';

	$ids_comunas = $_POST['ids_comunas'];
	$ids_inmuebles = $_POST['ids_inmuebles'];
    $precio_minimo = $_POST['precio_minimo'];
    $precio_maximo = $_POST['precio_maximo'];

    $where =" WHERE 1 = 1 ";

	if($precio_minimo != 'NaN' and $precio_maximo!='NaN'){
		$where .= ' AND ';
		$where .= ' precio >= '. $precio_minimo .' and precio <= '. $precio_maximo;
	}
	else{
		$conector = '';
	}
    if(count($ids_comunas) > 0){
        $where .= ' AND ';
        $where .= ' comuna_id in ('.implode(',',$ids_comunas).')';
    }
    else{
        $conector = '';
    }
    if(count($ids_inmuebles) > 0){
        $clave = array_search(3, $ids_inmuebles);
        if($ids_inmuebles[$clave] == 3){
            unset($ids_inmuebles[$clave]);
            if(count($ids_inmuebles) > 0) {
                $aux2 = implode(",", $ids_inmuebles);
                $where .= ' AND tipoinmueble_id in ('. $aux2 .') OR p.id in (select proyecto_id from modelos where check_tw=1) ';
            }else{
                $having = ' HAVING estado_tw = 1';
            }
        }else{
            $where .= ' AND tipoinmueble_id IN (' . implode(",", $ids_inmuebles) . ')';           
        }
    }
	else{
		$conector = '';
	}
    $order_by = ' ORDER BY orden ASC';


echo'
	<section class="project project--int">
		<div class="container">
			<div class="row d-flex justify-content-between">
	';

    $consulta = '
        SELECT 
                                p.id as id, 
                                p.orden as orden,
                                p.tipoinmueble_id as tipoinmueble_id,
                                p.tipoetapa_id as tipoetapa_id,
                                p.img_home as img_home,
                                p.nombre_proyecto as nombre_proyecto,
                                p.dormitorios as dormitorios, 
                                p.banos as banos,
                                p.estacionamiento as estacionamiento,
                                p.valor_estacionamiento as valor_estacionamiento,
                                p.superficie as superficie, 
                                p.precio,
                                ep.nombre_estado,
                                te.nombre_etapa,
                                (select sum(m2.check_stock) from modelos m2 where m2.proyecto_id=p.id) as stock,
                                (CASE
                                    WHEN (select sum(m2.check_stock) from modelos m2 where m2.proyecto_id=p.id) > 0 THEN "stock"    
                                    ELSE "sin stock"
                                END) as estado_stock,
                                (CASE
                                    WHEN (select sum(m2.check_tw) from modelos m2 where m2.proyecto_id=p.id) > 0 THEN "1"    
                                    ELSE "0"
                                END) as estado_tw
                            from proyectos p 
                            left join estado_proyectos ep on ep.id=p.estado_id 
                            left join tipo_etapas te on te.id=p.tipoetapa_id 
    '. $where . '
        group by p.id 
    '. $having.$order_by;

	if ($resultado= mysqli_query($conexion,$consulta)) {
	    while ($r01 = mysqli_fetch_assoc($resultado)) {
            if($r01['tipoinmueble_id']!=''){
                $query = 'SELECT * FROM tipo_inmuebles WHERE id='.$r01['tipoinmueble_id'];
                $resul= mysqli_query($conexion,$query);
                $ri = mysqli_fetch_assoc($resul);
                $imagen = '/storage/'.$r01['img_home'];
                $ruta_proyecto = 'ficha-proyecto/'.$r01['id'];
                ?>

                    <article class="project__box">
                        <figure>
                            <div class="label-type">
                            <?php
                                if ($r01['estado_stock'] == 'sin stock') {
                                    echo $r01['estado_stock'];
                                }
                                else{
                                    echo $r01['nombre_estado']; 
                                }                           
                            ?>
                            </div>
                            <img src="<?php echo $imagen;?>" alt="">
                            <figcaption>
                                <h4><?php echo $ri['nombre_inmueble'];?></h4>
                                <h2><?php echo $r01['nombre_proyecto']?></h2>
                                <h5><?php echo $r01['nombre_etapa']?></h5>
                            </figcaption>
                        </figure>
                        <ul class="box-features">
                            <li>
                                <i class="icon-dormitorio"></i>
                                <span><?php echo $r01['dormitorios']?></span>
                            </li>
                            <li>
                                <i class="icon-bano"></i>
                                <span><?php echo $r01['banos']?></span>
                            </li>
                            <li>
                                <i class="icon-estacionamiento"></i>
                                <span><?php echo $r01['estacionamiento']?></span>
                            </li>
                        </ul>

                        <div class="box-desc">
                                <div class="col">
                                    <h5>Superficie:</h5>
                                    <p><?php echo $r01['superficie']?> M2</p>
                                </div>
                                <div class="col">
                                    <h5>Precio desde:</h5>
                                    <p><?php echo $r01['precio']?> UF</p>
                                </div>
                                <?php if($r01['valor_estacionamiento'] !=  NULL){ ?>
                                <div class="col">
                                    <h5> Estacionamiento:</h5>
                                    <p><?php echo $r01['valor_estacionamiento']; ?> UF</p>
                                </div>
                                <?php
                                }
                                ?>
                        </div>


                        <ul class="box-bt">
                            <li><a href="<?php echo $ruta_proyecto ?>">Ver más</a></li>
                        <li><a href="<?php echo $ruta_proyecto ?>,#cotizar-form">Cotizar</a></li>
                        </ul>
                    </article>
<?php

            }
	    }
	}else{
        echo '<h2>NO SE ENCONTRARON RESULTADOS QUE MOSTRAR</h2>';
    }
	echo ' </div>
		</div>
	</section>
	';
?>
