<?php
	include '../public/includes/funciones.php';

	$i=0;
	if(isset($_GET['Comuna'])){
    	foreach($_GET['Comuna'] as $valor)
    	{
    		$id_comuna = getId('comunas',$valor,'nombre_comuna');
    		$comunas[$i]= $id_comuna;
    		$i++;
    	}
	}
	$i=0;
	if(isset($_GET['Subsidio'])){
	foreach($_GET['Subsidio'] as $valor)
	{
		$id_subsidio = getId('subsidios', $valor ,'nombre_subsidio');
		$subsidios[$i]= $id_subsidio;
		$i++;
	}
	}

	$i=0;
	if(isset($_GET['Inmueble'])){
	foreach($_GET['Inmueble'] as $valor)
	{
		$id_inmueble = getId('tipo_inmuebles',$valor,'nombre_inmueble');
		$inmuebles[$i]= $id_inmueble;
		$i++;
	}
	}
	$i=0;
	if(isset($_GET['Estado'])){
	foreach($_GET['Estado'] as $valor)
	{
		$id_estado = getId('estado_proyectos',$valor,'nombre_estado');
		$estados[$i]= $id_estado;
		$i++;
	}
}
?>

<form name="form-filtros" id="form-filtros" method="GET">
	<ul>
		<li>
			<div class="dropdown">
				<button class="btn dropdown-toggle" type="button" id="dropdownComuna" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="icon-casa"></i>
					Comuna
				</button>
				<div class="dropdown-menu" aria-labelledby="dropdownComuna">
					<?php
					include '../public/includes/connect.php';
					$qc = 'SELECT * FROM comunas';
					$rc = mysqli_query($conexion,$qc);
					while($dc = mysqli_fetch_assoc($rc)) {
					?>
					<input type="checkbox" name="Comuna[]"  id="Comuna<?php echo $dc['id']?>" value="<?php echo $dc['nombre_comuna']?>" onclick="agregarFiltro('Comuna<?php echo $dc['id']?>')" <?php if (isset($comunas) and in_array($dc['id'], $comunas)){ ?> checked <?php } ?>>
					<label for="Comuna<?php echo $dc['id']?>"><?php echo $dc['nombre_comuna']?></label>
					<?php  } ?>
				</div>
			</div>
		</li>
		<li>
			<div class="dropdown">
				<button class="btn dropdown-toggle" type="button" id="dropdownSubsidio" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="icon-valor"></i>
					Proyecto con subsidio
				</button>
				<div class="dropdown-menu" aria-labelledby="dropdownSubsidio">
					<?php
					$qs = 'SELECT * FROM subsidios';
					$rs = mysqli_query($conexion,$qs);
					while($ds = mysqli_fetch_assoc($rs)) {
					?>
						<input type="checkbox" name="Subsidio[]"  id="Subsidio<?php echo $ds['id']?>" value="<?php echo $ds['nombre_subsidio']?>"  onclick="agregarFiltro('Subsidio<?php echo $ds['id']?>')" <?php if (isset($subsidios) and in_array($ds['id'], $subsidios)){ ?> checked <?php } ?>>
						<label for="Subsidio<?php echo $ds['id']?>"><?php echo $ds['nombre_subsidio']?></label>
					<?php  } ?>
				</div>
			</div>
		</li>
			<li>
				<div class="dropdown">
					<button class="btn dropdown-toggle" type="button" id="dropdownTipo" data-toggle="dropdown" aria-haspopup="true"
					aria-expanded="false">
					<i class="icon-pisos"></i>
					Tipo de Proyecto
				</button>
				<div class="dropdown-menu" aria-labelledby="dropdownTipo">
					<?php
					$qi = 'SELECT * FROM tipo_inmuebles';
					$ri = mysqli_query($conexion,$qi);
					while($di = mysqli_fetch_assoc($ri)) {
					?>
						<input type="checkbox" name="Inmueble[]"  id="Inmueble<?php echo $di['id']?>" value="<?php echo $di['nombre_inmueble']?>"  onclick="agregarFiltro('Inmueble<?php echo $di['id']?>')"<?php if (isset($inmuebles) and in_array($di['id'], $inmuebles)){ ?> checked <?php } ?>>
						<label for="Inmueble<?php echo $di['id']?>"><?php echo $di['nombre_inmueble']?></label>

					<?php  } ?>
				</div>
			</div>
		</li>
		<li>
			<div class="dropdown">
				<button class="btn dropdown-toggle" type="button" id="dropdownEstado" data-toggle="dropdown" aria-haspopup="true"
				aria-expanded="false">
				<i class="icon-plano"></i>
				Estado del proyecto
			</button>
			<div class="dropdown-menu" aria-labelledby="dropdownEstado">
					<?php
					$qp = 'SELECT * FROM estado_proyectos';
					$rp = mysqli_query($conexion,$qp);
					while($dp = mysqli_fetch_assoc($rp)) {
					?>
						<input type="checkbox" name="Estado[]"  id="Estado<?php echo $dp['id']?>" value="<?php echo $dp['nombre_estado']?>" onclick="agregarFiltro('Estado<?php echo $dp['id']?>')"<?php if (isset($estados) and in_array($dp['id'], $estados)){ ?> checked <?php } ?>>
						<label for="Estado<?php echo $dp['id']?>"><?php echo $dp['nombre_estado']?></label>

				<?php  } ?>
			</div>
		</div>
		</li>

		<li>
			<div id="slider" class="noUi-target noUi-ltr noUi-horizontal"></div>
			<span class="example-val" id="slider-margin-value-min"></span>
			<input type="hidden" name="MinSlider" id="MinSlider">
			<span class="example-val" id="slider-margin-value-max"></span>
			<input type="hidden" name="MaxSlider" id="MaxSlider">
			<input type="hidden" name="orden_precio" id="orden_precio" value="ASC">
		</li>
		<li>
			<button class="btn-green" onclick="updateResultado(0,1);" id="bhsubmit">Descubrir</button>
		</li>

	</ul>
</form>
