<?php
	include 'connect.php';

	$comuna = $_GET['comuna2'];
	$inmueble = $_GET['inmueble2'];
	$dormitorio = $_GET['dormitorio2'];
	$bano = $_GET['bano2'];
	$estacionamiento = $_GET['estacionamiento2'];
	$estado = $_GET['estado2'];
	$subsidio = $_GET['subsidio2'];
    $precio_minimo = $_GET['precio_minimo'];
    $precio_maximo = $_GET['precio_maximo'];


	$exc_comuna = $_GET['exc-comuna2'];
	$exc_inm = $_GET['exc-inmueble2'];
	$exc_dorm = $_GET['exc-dormitorio2'];
	$exc_bano = $_GET['exc-bano2'];
	$exc_estac = $_GET['exc-estacionamiento2'];
	$exc_estado = $_GET['exc-estado2'];
	$exc_subsidio = $_GET['exc-subsidio2'];


    $where =" WHERE 1 = 1 ";

	if($exc_comuna==0 || $exc_inm==0 || $exc_dorm==0 || $exc_bano==0 ||$exc_estac==0 || $exc_estado==0 || $exc_subsidio==0 || $precio_minimo != 'NaN' and $precio_maximo!='NaN'){
		// $consulta .= ' WHERE ';
		$conector = '';
	}


	if($comuna!='0' && $exc_comuna==0){
		$where.= ' AND comuna_id='.$comuna;
	}else{
		$conector = '';
	}

    if($inmueble!='0' && $exc_inm==0){
        $aux = explode(",",$inmueble);
        $clave = array_search(3, $aux);
        if($aux[$clave] == 3){
            unset($aux[$clave]);
            if(count($aux) > 0) {
                $aux2 = implode(",", $aux);
                $where .= ' AND tipoinmueble_id in ('. $aux2 .') OR p.id in (select proyecto_id from modelos where check_tw=1) ';
            }else{
                $having = ' HAVING estado_tw = 1';
            }
        }else{
            $where .= ' AND tipoinmueble_id IN ('.$inmueble.')';           
        }
    }

	if($dormitorio!='0' && $exc_dorm==0){
		$where.= ' AND  dormitorios="'.$dormitorio.'"';
	}

	if($bano!='0' &&  $exc_bano==0){
		$where.= ' AND  banos="'.$bano.'"';
	}

	if($estacionamiento!='0' && $exc_estac==0){
		$where.= ' AND  estacionamiento="'.$estacionamiento.'"';
	}

	if($estado!='0' && $exc_estado==0){
		$where.= ' AND  estado_id='.$estado;
	}

	if($subsidio!='0' && $exc_subsidio==0){

		$where.= ' AND  subsidio_id='.$subsidio;
	}
    if($precio_minimo != 'NaN' and $precio_maximo!='NaN'){
        $where.= ' AND  precio >= '. $precio_minimo .' and precio <= '. $precio_maximo;
    }

    $order_by = ' ORDER BY orden ASC';

    $consulta = '
        SELECT 
                                p.id as id, 
                                p.orden as orden,
                                p.tipoinmueble_id as tipoinmueble_id,
                                p.tipoetapa_id as tipoetapa_id,
                                p.img_home as img_home,
                                p.nombre_proyecto as nombre_proyecto,
                                p.dormitorios as dormitorios, 
                                p.banos as banos,
                                p.estacionamiento as estacionamiento,
                                p.valor_estacionamiento as valor_estacionamiento,
                                p.superficie as superficie, 
                                p.precio,
                                ep.nombre_estado,
                                te.nombre_etapa,
                                (select sum(m2.check_stock) from modelos m2 where m2.proyecto_id=p.id) as stock,
                                (CASE
                                    WHEN (select sum(m2.check_stock) from modelos m2 where m2.proyecto_id=p.id) > 0 THEN "stock"    
                                    ELSE "sin stock"
                                END) as estado_stock,
                                (CASE
                                    WHEN (select sum(m2.check_tw) from modelos m2 where m2.proyecto_id=p.id) > 0 THEN "1"    
                                    ELSE "0"
                                END) as estado_tw
                            from proyectos p 
                            left join estado_proyectos ep on ep.id=p.estado_id 
                            left join tipo_etapas te on te.id=p.tipoetapa_id 
    '. $where . '
        group by p.id 
    '. $having.$order_by;

	$resultado= mysqli_query($conexion,$consulta);

?>
	<section class="project project--int">
		<div class="container">
			<div class="row d-flex justify-content-between">
				<?php
				if($resultado->num_rows > 0){
				while ($r01 = mysqli_fetch_assoc($resultado)){
                    // print_r($r01);
    				$query = 'SELECT * FROM tipo_inmuebles WHERE id='.$r01['tipoinmueble_id'];
    				$resul= mysqli_query($conexion,$query);
    				$ri = mysqli_fetch_assoc($resul);
    				$imagen = '/storage/'.$r01['img_home'];
                    $ruta_proyecto = 'ficha-proyecto/'.$r01['id'];
    				?>

                    <article class="project__box">
                        <figure>
                            <div class="label-type">
                            <?php
                                if ($r01['estado_stock'] == 'sin stock') {
                                    echo $r01['estado_stock'];
                                }
                                else{
                                    echo $r01['nombre_estado']; 
                                }                           
                            ?>
                            </div>
                            <img src="<?php echo $imagen;?>" alt="">
                            <figcaption>
                                <h4><?php echo $ri['nombre_inmueble'];?></h4>
                                <h2><?php echo $r01['nombre_proyecto']?></h2>
                                <h5><?php echo $r01['nombre_etapa']?></h5>
                            </figcaption>
                        </figure>
                        <ul class="box-features">
                            <li>
                                <i class="icon-dormitorio"></i>
                                <span><?php echo $r01['dormitorios']?></span>
                            </li>
                            <li>
                                <i class="icon-bano"></i>
                                <span><?php echo $r01['banos']?></span>
                            </li>
                            <li>
                                <i class="icon-estacionamiento"></i>
                                <span><?php echo $r01['estacionamiento']?></span>
                            </li>
                        </ul>

                        <div class="box-desc">
                                <div class="col">
                                    <h5>Superficie:</h5>
                                    <p><?php echo $r01['superficie']?> M2</p>
                                </div>
                                <div class="col">
                                    <h5>Precio desde:</h5>
                                    <p><?php echo $r01['precio']?> UF</p>
                                </div>
                                <?php if($r01['valor_estacionamiento'] !=  NULL){ ?>
                                <div class="col">
                                    <h5> Estacionamiento:</h5>
                                    <p><?php echo $r01['valor_estacionamiento']; ?> UF</p>
                                </div>
                                <?php
                                }
                                ?>
                        </div>


                        <ul class="box-bt">
                            <li><a href="<?php echo $ruta_proyecto ?>">Ver más</a></li>
                        <li><a href="<?php echo $ruta_proyecto ?>,#cotizar-form">Cotizar</a></li>
                        </ul>
                    </article>


				<?php
                };

				}else{
					echo '<h2>NO SE ENCONTRARON RESULTADOS QUE MOSTRAR</h2>';
				}?>

			</div>
		</div>
	</section>
