
$(window).on("load", function (e) {
    $(".loading").fadeOut('slow');
});

$( document ).ready(function() {

    //  $('.label-result').click(function () {
    //  $(this).parent().hide();
    //  });
    $('#modelos>li>a').each(function(){
        $(this).removeClass('active');
        if(window.location.href == $(this).attr('href')){
            $(this).addClass('active');
        }
    });

    var myLazyLoad = new LazyLoad({
        elements_selector: ".lazy",
        load_delay: 500 //adjust according to use case
    })

    $('.main-head-xs__slide').slick({
        arrows: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    arrows: false,
                    dots: true
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });

    $('.slide-images').slick({
        arrows: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    arrows: true,
                    dots: false
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });

    $("input[type=radio]").click(function () {
        var previousValue = $(this).data('storedValue');

        if (previousValue) {
            $(this).prop('checked', !previousValue);
            $(this).data('storedValue', !previousValue);
        }

        else {
            $(this).data('storedValue', true);
            $("input[type=radio]:not(:checked)").data("storedValue", false);
        }
    });

    $('.dropdown').on({
        "click": function (event) {
            if ($(event.target).closest('.dropdown-toggle').length) {
                $(this).data('closable', true);
            } else {
                $(this).data('closable', false);
            }
        },
        "hide.bs.dropdown": function (event) {
            hide = $(this).data('closable');
            $(this).data('closable', true);
            return hide;
        }
    });

    if ($('.dropdown-menu').is(':visible')) {
        $('.slick-dots').hide();
    }

    $(".navbar-toggler").click(function(){
        $('header').toggleClass('open');
    })



    $('.slide-termin').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    centerMode: true,
    asNavFor: '.slide-termin--nav',
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    arrows: true    
                }
            }           
        ]
    });
    $('.slide-termin--nav').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    dots: false,
    arrows: false,
    centerMode: true,
    focusOnSelect: true,
    centerPadding: '10px',
    asNavFor: '.slide-termin'
    });

    $('.slide-equip').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.slide-equip--nav'
    });

    $('.slide-equip--nav').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        dots: false,
        arrows: false,
        centerMode: true,
        focusOnSelect: true,
        centerPadding: '10px',
        asNavFor: '.slide-equip'
    });

    $('.slide-spot').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.slide-spot--nav'
    });

    $('.slide-spot--nav').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        dots: false,
        arrows: false,
        centerMode: true,
        focusOnSelect: true,
        centerPadding: '10px',
        asNavFor: '.slide-spot'
    });

    $('.nav-tabs a[data-toggle=tab]').on('shown.bs.tab', function (e) {
        $('.slide-equip').slick('refresh')
        $('.slide-equip--nav').slick('refresh')
        $('.slide-spot').slick('refresh')
        $('.slide-spot--nav').slick('refresh')
    })


    $('#nav-baja').zoom();
    $('#nav-alta').zoom();
    $('#nav-3ra').zoom();
    $('#nav-av').zoom();
    $('#master-plan').zoom();

    $('.c-bttn > li > a[href^="#"]').on('click', function (e) {
		e.preventDefault()
        var target = this.hash
        var $target = $(target)
          $('html, body').animate({
            'scrollTop': $target.offset().top -100
          }, 600, 'swing')
    })

    $( "body" ).mouseup(function() {
        $('#MinSlider').val($('#slider > div > div:nth-child(2) > div > div.noUi-tooltip').text());
        $('#MaxSlider').val($('#slider > div > div:nth-child(3) > div > div.noUi-tooltip').text());

    });
    $(document).on('click', '[data-lightbox]', lity);

    $('.modelos__boxes .title').click(function(){
      $('.box-item').toggleClass('active');
    });

});


    function dropFiltroMobile(filtro){
        $("#filtro-"+filtro).remove();
        $("#exc-"+filtro).val(1);
        updateResultado(0,2);
    }



    var bhn = document.getElementById('bhsubmit');
    if(bhn){
        bhn.addEventListener('click',function(e){
            e.preventDefault();
        });
    }
    var btn = document.getElementById('btsubmit');
    if(btn){
        btn.addEventListener('click',function(e){
            e.preventDefault();
        })
    }


