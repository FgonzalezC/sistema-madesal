'use strict';
(function(window) {

    var _self;

    var Madesal = {
        vars: {
            ajaxUrlexample           : '/controller/action/',
            var1                     : $("#id").val(),

        },
        init: function() {
            _self = this;

            _self.btnDescubrirProyecto                                 = $("#btn_descubrir_proyecto");

            _self.setEvents();
        },
        setEvents: function() {

            $('#btn_descubrir').on('click', function(e){
                e.preventDefault();
                $("#proyectos_destacados").remove();
                $("#seccion_invierte_home").remove();
                $('#section_main_head_proyectos').show();
                _self.actions.updateResultadoDesktopHome(0,1);
                _self.actions.mostrar_filtros_escritorio();
                $("#main_head_desktop").remove();
            });

            _self.btnDescubrirProyecto.on('click', function(e){
                e.preventDefault();
                _self.actions.updateResultado(0,1)
            });

            $('#btn_descubrir_invertir').on('click', function(e){
                e.preventDefault();
                $("#inversion").remove();
                $("#destacados_inversion").remove();
                $('#banner_inversion').remove();
                $("#cotiza_inversion").remove();
                $("#section_main_head_proyectos").show();
                _self.actions.updateResultadoDesktopHome(0,1);
                _self.actions.mostrar_filtros_escritorio();
                $("#main_head_desktop").remove();


            });

            $('#btn_descubrir_movil').on('click', function(e){
                e.preventDefault();
                $('header').addClass('header-int');
                $('#proyectos_destacados').remove();
                $('#section_main_head_proyectos').show();
                $('#section_mobile_filter_proyecto').show();
                _self.actions.mostrar_filtros_mobil_home();
                _self.actions.updateResultadoMobil(0,100);
                $('#filtro_mobile_home').remove();
            });

            $('#btn_mejora_tu_busqueda').on('click', function(e){
                e.preventDefault();
                $('header').addClass('header-int');
                $('#proyectos_destacados').remove();
                $('#section_main_head_proyectos').show();
                $('#section_mobile_filter_proyecto').show();
                _self.actions.mostrar_filtros_mobile();
                _self.actions.updateResultadoMobil(0,200);
                $('#filtro_mobile_home').remove();
            });

        },
        actions:{

            updateResultadoDesktopHome: function (orden,form) {
                var parametros,pagina;
                var margin_slider = $("#margin_slider").val();
                var array_margin_slider = margin_slider.split(',');
                var precio_minimo, precio_maximo;
                precio_minimo = parseInt($('#MinSlider').val());
                precio_maximo = parseInt($('#MaxSlider').val());
                    // if(array_margin_slider != 'NaN'){
                    //     precio_minimo = parseInt(array_margin_slider[0]);
                    //     precio_maximo = parseInt(array_margin_slider[1]);
                    // }else{
                    // }

                if(orden!='' && orden!=0) $('#orden_precio').val(orden);

                if(form==1){
                   parametros = $('#form-filtros-home').serialize();
                   pagina = 'includes/resultados_proyecto.php';
                }else{
                    var element = document.getElementById("menu-mobile");
                   element.classList.remove("show");
                    parametros = $('#form-mobile').serialize();
                    pagina = 'includes/resul_proymobile.php';
                }
                parametros = parametros+'&precio_minimo='+precio_minimo+'&precio_maximo='+precio_maximo;
                var MaxSlider = parseInt($('#MaxSlider').val());
                var MinSlider = parseInt($('#MinSlider').val());
                $.ajax({
                    data:parametros,
                    url: pagina,
                    type: 'GET',
                    success: function(response) {
                        if (response){
                            $('#resultado_proyecto').html(response);
                            var sliderProyecto = document.getElementById('slider_proyecto');
                            var rango_maximo = parseInt( document.getElementById('rango_maximo').value);
                            var rango_minimo = parseInt( document.getElementById('rango_minimo').value);

                            if (sliderProyecto.noUiSlider) {
                                sliderProyecto.noUiSlider.destroy();
                            }

                             noUiSlider.create(sliderProyecto, {
                                start: [precio_minimo, precio_maximo],
                                step: 100,
                                connect: true,
                                tooltips: [ wNumb({ decimals: 0 }), wNumb({ decimals: 0 }) ],
                                range: {
                                    'min': rango_minimo,
                                    'max': rango_maximo
                                }
                            });
                            var marginMin = document.getElementById('slider-margin-value-min'),
                                marginMax = document.getElementById('slider-margin-value-max');

                            sliderProyecto.noUiSlider.on('update', function (values, handle) {
                                if (handle) {
                                    marginMax.innerHTML = values[handle];
                                } else {
                                    marginMin.innerHTML = values[handle];
                                }
                            });
                            var margin_slider = document.getElementById('margin_slider');

                            sliderProyecto.noUiSlider.on('change', function( values, handle ) {
                                margin_slider.value = values;
                            });

                        }
                    }

                });
            },
            // permite mostrar los filtro en vista escritorio
            mostrar_filtros_escritorio: function (){
                $("#lista").html('<li>Filtros:</li>');
                $("input:checkbox:checked").each(function() {
                    var valor_filtro = $(this).val();
                    var id_filtro = 'filtro-'+$.trim($(this).val().replace(/\s/g, ''));
                    $('#form-filtros').find(":checkbox").each(function(){
                        var valor_checkbox = $(this).val();
                        if(valor_filtro == valor_checkbox){
                            $(this).prop('checked', true);
                        }
                    });
                    $("#lista").append('<li id="'+ id_filtro +'"  onclick="Madesal.actions.dropFiltroEscritorio(\''+ valor_filtro +'\');"><div class="label-result" ><p>'+ valor_filtro +'</p><i class="icon-close" onclick="Madesal.actions.dropFiltroEscritorio(\''+ valor_filtro +'\');"></i></div></li>');
                });
            },
            // permite mostrar los filtro en vista escritorio
            mostrar_filtros_mobil_home: function (){
                $("#lista").html('<li>Filtros:</li>');
                $("#form-mobile-home").find("input:checkbox:checked").each(function() {
                    var nombre = $(this).data('nombre');
                    var id_filtro = 'filtro-'+$.trim(nombre.replace(/\s/g, ''));
                    $('select option').each(function () {
                        var valor_select = $(this).text();
                        if(nombre == valor_select){
                            $(this).prop('selected', true);
                        }
                    });
                    $("#lista").append('<li id="'+ id_filtro +'"  onclick="Madesal.actions.dropFiltroMobilHome(\''+ nombre +'\');"><div class="label-result" ><p>'+ nombre +'</p><i class="icon-close" onclick="Madesal.actions.dropFiltroMobilHome(\''+ nombre +'\');"></i></div></li>');
                });
            },
            //permite mostrar el resultado de busqueda en desde el form mobil
            updateResultadoMobil: function (orden,form) {
                var parametros,pagina;
                if(form=='100'){
                    var margin_slider = $("#margin_slider").val();
                    var array_margin_slider = margin_slider.split(',');
                    if(array_margin_slider != 'NaN'){
                        var precio_minimo = parseInt(array_margin_slider[0]);
                        var precio_maximo = parseInt(array_margin_slider[1]);
                    }
                    var element = document.getElementById("menu-mobile-home");
                    parametros = $('#form-mobile-home').serialize();
                    var array_param = parametros.split('&');
                    var array_comunas = [];
                    var array_inmuebles = [];
                    $("#form-mobile-home").find("input:checkbox:checked").each(function() {
                        var ck= $(this);
                        var id = ck.data('id');
                        if(ck.attr('name').indexOf('comuna') >= 0  && ck.prop('checked')){
                           array_comunas.push(id);
                        }
                        if(ck.attr('name').indexOf('type') >= 0  && ck.prop('checked')){
                           array_inmuebles.push(id);
                        }
                    });
                    pagina = 'includes/resul_proymobile_home.php';
                }
                if(form=='200'){
                    var margin_slider = $("#margin_slider_avanzado").val();
                    var array_margin_slider = margin_slider.split(',');
                    if(array_margin_slider != 'NaN'){
                        var precio_minimo = parseInt(array_margin_slider[0]);
                        var precio_maximo = parseInt(array_margin_slider[1]);
                    }
                    var element = document.getElementById("menu-mobile");
                    parametros = $('#form-mobile').serialize();
                    pagina = 'includes/resul_proymobile.php';
                }
                element.classList.remove("show");
                var MaxSlider = parseInt($('#MaxSlider').val());
                var MinSlider = parseInt($('#MinSlider').val());

                if(form=='100'){
                    $.ajax({
                        data: {
                            "ids_comunas":array_comunas,
                            "ids_inmuebles" : array_inmuebles,
                            "precio_minimo" : precio_minimo,
                            "precio_maximo" : precio_maximo
                        },
                        url: pagina,
                        type: 'POST',
                        dataType: "html",
                        success: function(response) {
                            if(response){
                                $('#resultado_proyecto').html(response);

                                var rango_maximo = parseInt( document.getElementById('rango_maximo').value);
                                var rango_minimo = parseInt( document.getElementById('rango_minimo').value);

                                var sliderXsAvanzado = document.getElementById('slider-xs-avanzado');
                                precio_minimo = (precio_minimo)?precio_minimo:rango_minimo;
                                precio_maximo = (precio_maximo)?precio_maximo:rango_maximo;
                                noUiSlider.create(sliderXsAvanzado, {
                                    start: [precio_minimo, precio_maximo],
                                    step: 100,
                                    connect: true,
                                    tooltips: [ wNumb({ decimals: 0 }), wNumb({ decimals: 0 }) ],
                                    range: {
                                        'min': rango_minimo,
                                        'max': rango_maximo
                                    }
                                });
                                var marginMin = document.getElementById('slider-margin-value-min'),
                                    marginMax = document.getElementById('slider-margin-value-max');

                                sliderXsAvanzado.noUiSlider.on('update', function (values, handle) {
                                    if (handle) {
                                        marginMax.innerHTML = values[handle];
                                    } else {
                                        marginMin.innerHTML = values[handle];
                                    }
                                });
                                sliderXsAvanzado.noUiSlider.on('change', function( values, handle ) {
                                    margin_slider_avanzado.value = values;
                                });

                                var margin_slider_avanzado = document.getElementById('margin_slider_avanzado');

                            }
                        }
                    });
                }
                if(form=='200'){
                   parametros = parametros+'&precio_minimo='+precio_minimo+'&precio_maximo='+precio_maximo;
                   $.ajax({
                data:parametros,
                url: pagina,
                type: 'GET',
                success: function(response) {
                    if (response){
                         $('#resultado_proyecto').html(response);
                    }
                }

                   });

                }
            },
            mostrar_filtros_mobile: function (){
                $("#lista").html('<li>Filtros:</li>');
                $("#form-mobile").find(":selected").each(function() {
                    var id = $(this).val();
                    var nombre = $(this).text();
                    if(!nombre.includes('Selecciona')){
                        if( $(this).parent().attr('id') == 'dormitorio2'){ nombre += ' DORM'}
                        if( $(this).parent().attr('id') == 'bano2'){ nombre += ' BAÑO'}
                        if( $(this).parent().attr('id') == 'estacionamiento2'){ nombre += ' ESTA'}

                        var id_filtro = 'filtro-'+$.trim(nombre.replace(/\s/g, ''));

                        $("#lista").append('<li id="'+ id_filtro +'"  onclick="Madesal.actions.dropFiltroMobilHome(\''+ nombre +'\');"><div class="label-result" ><p>'+ nombre +'</p><i class="icon-close" onclick="Madesal.actions.dropFiltroMobilHome(\''+ nombre +'\');"></i></div></li>');
                    }
                });
            },
            // quita un filtro del la vista de escritorio
            dropFiltroEscritorio: function (filtro){
                var id_filtro = 'filtro-'+$.trim(filtro.replace(/\s/g, ''));
                $('#'+id_filtro).remove();
                $("#exc-"+filtro).val(1);
                $("#form-filtros").find(":checked").each(function() {
                    if($.trim($(this).val().replace(/\s/g, '')) == $.trim(filtro.replace(/\s/g, '')) ){
                        $(this).prop('checked', false);
                    }
                });
                _self.actions.updateResultado(0,1)

            },
            // quita un filtro del la vista de escritorio
            dropFiltroMobilHome: function (filtro){
                var id_filtro = 'filtro-'+$.trim(filtro.replace(/\s/g, ''));
                $('#'+id_filtro).remove();
                $("#exc-"+filtro).val(1);
                $("#form-mobile").find(":selected").each(function() {
                    var id = $(this).val();
                    var nombre = $(this).text();
                    if( $(this).parent().attr('id') == 'dormitorio2'){ nombre += ' DORM'}
                    if( $(this).parent().attr('id') == 'bano2'){ nombre += ' BAÑO'}
                    if( $(this).parent().attr('id') == 'estacionamiento2'){ nombre += ' ESTA'}
                    if($.trim(nombre.replace(/\s/g, '')) == $.trim(filtro.replace(/\s/g, '')) ){
                        $(this).attr('selected', false);
                        $(this).parent().val(0);
                    }
                });
                _self.actions.updateResultadoMobil(0,200);

            },
            updateResultado: function (orden,form) {
                var parametros,pagina;

                var margin_slider = $("#margin_slider").val();
                var array_margin_slider = margin_slider.split(',');

                if(array_margin_slider != 'NaN'){
                    var precio_minimo = parseInt(array_margin_slider[0]);
                    var precio_maximo = parseInt(array_margin_slider[1]);
                }

                if(orden!='' && orden!=0) $('#orden_precio').val(orden);

                var margin_slider = $("#margin_slider").val();
                var array_margin_slider = margin_slider.split(',');

                if(array_margin_slider != 'NaN'){
                    var precio_minimo = parseInt(array_margin_slider[0]);
                    var precio_maximo = parseInt(array_margin_slider[1]);
                }

                if(orden!='' && orden!=0) $('#orden_precio').val(orden);

                if(form==1){
                   parametros = $('#form-filtros').serialize();
                   pagina = 'includes/resultados_proyecto.php';
                }else{
                    var element = document.getElementById("menu-mobile");
                   element.classList.remove("show");
                    parametros = $('#form-mobile').serialize();
                    pagina = 'includes/resul_proymobile.php';
                }
                parametros = parametros+'&precio_minimo='+precio_minimo+'&precio_maximo='+precio_maximo;
                $.ajax({
                    data:parametros,
                    url: pagina,
                    type: 'GET',
                    success: function(response) {
                        if (response){
                             $('#resultado_proyecto').html(response);

                        }
                    }

                });
                if(form==2) _self.actions.mostrar_filtros_mobile();
            },

            agregarFiltro: function (filtro){
                var etiqueta, filtro2;
                var title = $("#"+filtro).val();
                etiqueta = filtro+'-etiqueta';
                filtro2 = "'"+etiqueta+"'";
                if($('#'+filtro).prop('checked')){
                    $("#lista").append('<li id="'+etiqueta+'" onclick="Madesal.actions.dropFiltro('+filtro2+');"><div class="label-result" ><p>'+title+'</p><i class="icon-close" onclick="Madesal.actions.dropFiltro('+filtro2+');"></i></div></li>');
                }else{
                    _self.actions.dropFiltro(etiqueta);
                }
            },

            dropFiltro: function (etiqueta){
                var filtro = etiqueta.split("-");
                $("#"+etiqueta).remove();
                $("#"+filtro[0]).prop('checked', false);
                _self.actions.updateResultado(0,1);
            }

        },
        helper:{

        }
    };
    window.Madesal = Madesal;
})(window);
$(window).ready(function(){
    Madesal.init();
})
