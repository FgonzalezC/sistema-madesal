
$(window).on("load", function (e) {
    $(".loading").fadeOut('slow')
});

$( document ).ready(function() {
    var myLazyLoad = new LazyLoad({
        elements_selector: ".lazy",
        load_delay: 300 //adjust according to use case
    }) 
    
});
