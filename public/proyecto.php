<?php
    include '../public/includes/connect.php';

    $consulta = 'SELECT * FROM seccion_proyectos';
    $resultado= mysqli_query($conexion,$consulta);
    $r01 = mysqli_fetch_assoc($resultado);

?>

    <!-- MAIN HEAD -->
    <section class="main-head main-head--project">
        <div class="main-head__image">
            <img data-src="/storage/<?php echo $r01['img']; ?>" class="d-none d-md-block lazy" alt="Madesal">
        </div>
        <div class="main-head__title d-none d-lg-block">
            <?php echo "<h1>".$r01['titulo']."<span>".$r01['titulo2']."</span></h1>"; ?>
        </div>
        <div class="main-head__form d-none d-lg-block">
            <!-- formulario filtros home vista escrotorio por defecto -->
            @include('includes/filtro_default_home_escritorio')
            <!-- ./formulario filtros home vista escrotorio por defecto -->

        </div>

        <div class="mobile-filter d-block d-lg-none lazy">
            <div class="dropdown">
                <button class="btn dropdown-toggle" type="button" id="dropdownSearch" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Mejorar tu busqueda
                </button>
                <!-- formulario mejora tu busqueda -->
                @include('includes/filtro_mejora_tu_busqueda_movil')
                <!-- formulario mejora tu busqueda -->

            </div>
        </div>

        <div class="form-result">
            <div class="form-result__title d-block d-lg-none">
                <h4>Resultados de busqueda</h4>
            </div>
            <ul id="lista">
                <li>
                    Filtros:
                </li>
            </ul>
            <div class="dropdown d-none d-lg-block">
                <select name="orden" onchange="updateResultado(this.value,1)">
                    <option value="ASC">Ordenar por</option>
                    <option value="DESC">Precio de mayor a menor</option>
                    <option value="ASC">Precio de menor a mayor</option>
                </select>
            </div>
        </div>

    </section>
    <!-- /MAIN HEAD DESKTOP -->

    <div id="resultado_proyecto">
        <section class="project project--int">
            <div class="container">
                <div class="row d-flex justify-content-between">
                    <?php
                    $consulta = 'SELECT * FROM proyectos';
                    $resultado= mysqli_query($conexion,$consulta);
                    while ($r01 = mysqli_fetch_assoc($resultado)){
                        $query = 'SELECT * FROM tipo_inmuebles WHERE id='.$r01['tipoinmueble_id'];
                        $resul= mysqli_query($conexion,$query);
                        $ri = mysqli_fetch_assoc($resul);
                        $ruta_proyecto = 'ficha-proyecto/'.$r01['id'];
                    ?>

                    <article class="project__box">
                        <figure>
                            <div class="label-type">
                                <?php echo $ri['nombre_inmueble'];?>
                            </div>
                            <img data-src="/storage/<?php echo $r01['img_home'];?>" class="lazy" alt="">
                            <figcaption>
                                <h4><?php echo $ri['nombre_inmueble'];?></h4>
                                <h2><?php echo $r01['nombre_proyecto']?></h2>
                            </figcaption>
                        </figure>
                        <ul class="box-features">
                            <li>
                                <i class="icon-dormitorio"></i>
                                <span><?php echo $r01['dormitorios']?></span>
                            </li>
                            <li>
                                <i class="icon-bano"></i>
                                <span><?php echo $r01['banos']?></span>
                            </li>
                            <li>
                                <i class="icon-estacionamiento"></i>
                                <span><?php echo $r01['estacionamiento']?></span>
                            </li>
                        </ul>
                        <ul class="box-desc">
                            <li>Desde:</li>
                            <li>
                                <ul class="mts-price">
                                    <li>Superficie:  <?php echo $r01['superficie']?> M2</li>
                                    <li>Precio: <?php echo $r01['precio']?> UF</li>
                                </ul>
                            </li>
                        </ul>
                        <ul class="box-bt">
                            <li><a href="<?php echo $ruta_proyecto ?>">Ver más</a></li>
                            <li><a href="<?php echo $ruta_proyecto ?>,#cotizar-form">Cotizar</a></li>
                        </ul>
                    </article>
                    <?php  } ?>

                </div>
            </div>
        </section>
    </div>
