<?php

namespace App\Models\Admin;
use App\Models\Admin\Cotizacion;

use Illuminate\Database\Eloquent\Model;

class AsuntoCotizacion extends Model
{
    protected $table = "asuntocotizaciones";
    protected $fillable = ['nombre'];
    protected $guarded = ['id'];

      public function cotizaciones(){

    	return $this->hasMany(Cotizacion::class);
    }

}
