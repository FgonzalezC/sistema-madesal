<?php

namespace App\Models\Admin;

use App\Models\Admin\Empleado;
use App\Models\Admin\Proyecto;

use Illuminate\Database\Eloquent\Model;

class ProyectoEjecutivo extends Model
{
    protected $table = "proyecto_ejecutivos";
    protected $fillable = ['proyecto_id','empleado_id'];
    protected $guarded = ['id'];

  public function empleado(){

    	return $this->belongsTo(Empleado::class);
    }

  public function proyecto(){

    	return $this->belongsTo(Proyecto::class);
    }
}