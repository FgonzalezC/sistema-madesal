<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use App\Models\Admin\TipoIngreso;

class FormularioSimulador extends Model
{
     protected $table = "formulario_simulador";
    protected $fillable = ['monto_renta','co_renta','ingreso_id','extra_monto','monto_renta2','monto_renta3'];
    protected $guarded = ['id'];

      public function tipoingreso(){

    	return $this->belongsTo(TipoIngreso::class);
    }
}
