<?php

namespace App\Models\Admin;

use App\Models\Admin\Modelo;
use Illuminate\Database\Eloquent\Model;

class TipoCocina extends Model
{
    protected $table = "tipococinas";
    protected $fillable = ['nombre_cocina'];
    protected $guarded = ['id'];

      public function modelos(){

    	return $this->hasMany(Modelo::class);
    }
}
