<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Parametro extends Model
{
    protected $table = "parametroprecio";
    protected $fillable = ['precio_min','precio_max'];
    protected $guarded = ['id'];
}
