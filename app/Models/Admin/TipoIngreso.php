<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use App\Models\Admin\FormularioSimulador;

class TipoIngreso extends Model
{
    protected $table = "tipoingresos";
    protected $fillable = ['nombre'];
    protected $guarded = ['id'];

     public function formulariosimulador(){

    	return $this->hasMany(FormularioSimulador::class);
    }
}
