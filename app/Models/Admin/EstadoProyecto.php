<?php

namespace App\Models\Admin;

use App\Models\Admin\Proyecto;
use Illuminate\Database\Eloquent\Model;

class EstadoProyecto extends Model
{
     protected $table = "estado_proyectos";
    protected $fillable = ['nombre_estado'];
    protected $guarded = ['id'];

    public function proyectos(){

    	return $this->hasMany(Proyecto::class);
    }
}
