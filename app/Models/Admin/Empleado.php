<?php

namespace App\Models\Admin;

use App\Models\Admin\Cargo;
use App\Models\Admin\Proyecto;
use App\Models\Admin\ProyectoEjecutivos;
use Illuminate\Database\Eloquent\Model;

class Empleado extends Model
{
    protected $table = "empleados";
    protected $fillable = ['nombre_empleado','cargo_id','telefono','correo','img'];
    protected $guarded = ['id'];

    public function cargo(){

    	return $this->belongsTo(Cargo::class);
    }
    public function proyectos(){

    	return $this->hasMany(Proyecto::class);
    }
     public function proyectoejecutivos(){

    	return $this->hasMany(ProyectoEjecutivos::class);
    }

}
 