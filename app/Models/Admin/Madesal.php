<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Madesal extends Model
{
    protected $table = "madesals";
    protected $fillable = ['titulo','img','titulo1','bajada1','img1','titulo2','bajada2','img2'];
    protected $guarded = ['id'];
}
