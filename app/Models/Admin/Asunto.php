<?php

namespace App\Models\Admin;


use App\Models\Admin\Contacto;
use App\Models\Admin\Cotizacion;
use App\Models\Admin\Formulario_contacto;
use App\Models\Admin\SeccionFichaProyecto;
use App\Models\Admin\GuardarContacto;
use Illuminate\Database\Eloquent\Model;

class Asunto extends Model
{
    protected $table = "asuntos";
    protected $fillable = ['nombre_asunto'];
    protected $guarded = ['id'];

     public function formulario_contacto(){

    	return $this->belongsTo(Formulario_contacto::class);
    }

      public function guardarcontacto(){

      return $this->belongsTo(GuardarContacto::class);
    }
      public function cotizaciones(){

    	return $this->hasMany(Cotizacion::class);
    }
      public function seccionfichaproyecto(){

    	return $this->hasMany(SeccionFichaProyecto::class);
    }
        public function contactos(){

      return $this->hasMany(Contacto::class);
    }
    
}
