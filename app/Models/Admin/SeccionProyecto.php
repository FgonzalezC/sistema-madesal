<?php
namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class SeccionProyecto extends Model
{
     protected $table = "seccion_proyectos";
    protected $fillable = ['titulo','titulo2','img'];
    protected $guarded = ['id'];
}
