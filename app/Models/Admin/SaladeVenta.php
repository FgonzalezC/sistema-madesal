<?php

namespace App\Models\Admin;
use App\Models\Admin\Proyecto;


use Illuminate\Database\Eloquent\Model;

class SaladeVenta extends Model
{
    protected $table = "saladeventas";
    protected $fillable = ['nombre','direccion','telefono','telefono2','mail','mail2','horario','mapa'];
    protected $guarded = ['id'];

      public function proyectos(){

    	return $this->hasMany(Proyecto::class);
    }
}



