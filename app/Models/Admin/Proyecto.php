<?php

namespace App\Models\Admin;

use App\Models\Admin\Comuna;
use App\Models\Admin\Cotizacion;
use App\Models\Admin\Empleado;
use App\Models\Admin\EstadoProyecto;
use App\Models\Admin\Formulario_postventas;
use App\Models\Admin\Modelo;
use App\Models\Admin\Planta;
use App\Models\Admin\Subsidio;
use App\Models\Admin\TipoEtapa;
use App\Models\Admin\TipoInmueble;
use App\Models\Admin\Unidad;
use App\Models\Admin\SaladeVenta;
use App\Models\Admin\ProyectoEjecutivo;
use Illuminate\Database\Eloquent\Model;


class Proyecto extends Model
{
    protected $table = "proyectos";
    protected $fillable = ['nombre_proyecto','orden','tipoetapa_id','tipoinmueble_id','dormitorios','banos','estacionamiento','destacado','destacado2','superficie','precio','img_home','masterplan','desktopimg1','desktopimg2','desktopimg3','desktopimg4','desktopimg5','movilimg1','movilimg2','movilimg3','movilimg4','movilimg5','descripcion_slider','comuna_id','estado_id','subsidio_id','salaventas_id'];
    protected $guarded = ['id'];


    public function comuna(){

    	return $this->belongsTo(Comuna::class);
    }
    public function tipoinmueble(){

    	return $this->belongsTo(TipoInmueble::class);
    }
     public function estado_proyecto(){

    	return $this->belongsTo(EstadoProyecto::class);
    }
    public function subsidio(){

        return $this->belongsTo(Subsidio::class);
    }
     public function tipoetapa(){

        return $this->belongsTo(TipoEtapa::class);
    }

    public function formulario_postventas(){

    	return $this->hasMany(Formulario_postventas::class);
    }
    public function modelos(){

        return $this->hasMany(Modelo::class);
    }
   
       public function cotizacion(){

        return $this->belongsTo(Cotizacion::class);
    }
        public function empleados(){

        return $this->hasMany(Empleado::class);
    }
      
    public function salaventas_id(){

        return $this->belongsTo(SaladeVenta::class);
    }
         public function proyectoejecutivo(){

        return $this->hasMany(ProyectoEjecutivo::class);
    }
}
