<?php

namespace App\Models\Admin;

use App\Models\Admin\Proyecto;
use Illuminate\Database\Eloquent\Model;

class SeccionFichaProyecto extends Model
{
    protected $table = "seccion_fichaproyectos";
    protected $fillable = ['modelo_id','proyecto_id','equipamiento','ubicacion_entorno','timg1','timg2','timg3','timg4','timg5','eimg1','eimg2','eimg3','eimg4','eimg5','uimg1','uimg2','uimg3','uimg4','uimg5'];
    protected $guarded = ['id'];

       public function proyecto(){

    	return $this->belongsTo(Proyecto::class);
    }
}
