<?php

namespace App\Models\Admin;

use App\Models\Admin\Modelo;
use Illuminate\Database\Eloquent\Model;

class Planta extends Model
{
    protected $table = "plantas";
    protected $fillable = ['nombre','img_baja','img_alta','img_3era','img_areaverde','modelo_id'];
    protected $guarded = ['id'];

    public function modelo(){
    return $this->belongsTo(Modelo::class);
    }
}


