<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Asesoria extends Model
{
    protected $table = "asesoramos";
    protected $fillable = ['titulo','img','img1','img2'];
    protected $guarded = ['id'];
}
