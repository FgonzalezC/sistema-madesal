<?php

namespace App\Models\Admin;

use App\Models\Admin\Modelo;
use Illuminate\Database\Eloquent\Model;

class Unidad extends Model
{
    protected $table = "unidades";
    protected $fillable = ['nombre','img_unidad','precio','superficie_util','superf_areaverde','modelo_id'];
    protected $guarded = ['id'];

       public function modelo(){

    	return $this->belongsTo(Modelo::class);
    }
}


