<?php

namespace App\Models\Admin;

use App\Models\Admin\Modelo;
use App\Models\Admin\Proyecto;
use Illuminate\Database\Eloquent\Model;

class TipoInmueble extends Model
{
    protected $table = "tipo_inmuebles";
    protected $fillable = ['nombre_inmueble'];
    protected $guarded = ['id'];

     public function proyectos(){

    	return $this->hasMany(Proyecto::class);
    }
     public function modelos(){

    	return $this->hasMany(Modelo::class);
    }
}
