<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class ProcesoCompra extends Model
{
    protected $table = "seccion_procesocompas";
    protected $fillable = ['titulo','titulo1','bajada1','img1','titulodoc','bajadadoc','imgdoc','titulo2','bajada2','img2','titulo3','bajada3','img3','titulo4','bajada4','img4','titulo5','bajada5','img5'];
    protected $guarded = ['id'];
}


      