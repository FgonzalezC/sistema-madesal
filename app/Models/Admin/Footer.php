<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Footer extends Model
{
       protected $table = "footers";
    protected $fillable = ['img_footer','direccion','telefono','link_face','link_instagram','linktwitter'];
    protected $guarded = ['id'];
}
