<?php

namespace App\Models\Admin;

use App\Models\Admin\Proyecto;
use Illuminate\Database\Eloquent\Model;

class TipoEtapa extends Model
{
    protected $table = "tipo_etapas";
    protected $fillable = ['nombre_etapa'];
    protected $guarded = ['id'];

     public function proyectos(){

    	return $this->hasMany(Proyecto::class);
    }
}
