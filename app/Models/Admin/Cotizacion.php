<?php

namespace App\Models\Admin;

use App\Models\Admin\Asunto;
use Illuminate\Database\Eloquent\Model;

class Cotizacion extends Model
{
    protected $table = "cotizaciones";
    protected $fillable = ['nombre_cotizacion','apellido_cotizacion','telefono_cotizacion','email_cotizacion','asunto_id','mensaje_cotizacion'];
    protected $guarded = ['id'];

  
     public function asunto(){

    	return $this->belongsTo(Asunto::class);
    }
}

