<?php

namespace App\Models\Admin;


use App\Models\Admin\Empleado;
use Illuminate\Database\Eloquent\Model;

class Cargo extends Model
{
     protected $table = "cargos";
    protected $fillable = ['nombre'];
    protected $guarded = ['id'];
    public function empleados(){

    	return $this->hasMany(Empleado::class);
    }
}
