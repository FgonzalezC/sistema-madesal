<?php

namespace App\Models\Admin;

use App\Models\Admin\Proyecto;
use Illuminate\Database\Eloquent\Model;

class Comuna extends Model
{
    protected $table = "comunas";
    protected $fillable = ['nombre_comuna'];
    protected $guarded = ['id'];

    public function proyectos(){

    	return $this->hasMany(Proyecto::class);
    }
    
}
