<?php

namespace App\Models\Admin;

use App\Models\Admin\Asunto;
use Illuminate\Database\Eloquent\Model;

class GuardarContacto extends Model
{
     protected $table = "guardar_contactos";
    protected $fillable = ['nombre','apellido','telefono','email','asunto_id','mensaje'];
    protected $guarded = ['id'];

     public function asunto(){

    	return $this->hasOne(Asunto::class);
    }
}
