<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Simulador extends Model
{
    protected $table = "simuladores";
    protected $fillable = ['nombre','img'];
    protected $guarded = ['id'];
}
