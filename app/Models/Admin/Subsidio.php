<?php

namespace App\Models\Admin;

use App\Models\Admin\Proyecto;
use Illuminate\Database\Eloquent\Model;

class Subsidio extends Model
{
     protected $table = "subsidios";
    protected $fillable = ['nombre_subsidio'];
    protected $guarded = ['id'];

    public function proyectos(){

    	return $this->hasMany(Proyecto::class);
    }
}
