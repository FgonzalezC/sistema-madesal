<?php

namespace App\Models\Admin;

use App\Models\Admin\AsuntoCotizacion;
use Illuminate\Database\Eloquent\Model;

class Formulario_cotizacion extends Model
{
     protected $table = "formulario_cotizaciones";
    protected $fillable = ['nombre','apellido','telefono','email','asuntocot_id','mensaje'];
    protected $guarded = ['id'];

     public function asuntocot(){

    	return $this->hasOne(AsuntoCotizacion::class);
    }
}
