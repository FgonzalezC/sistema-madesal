<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Sucursal extends Model
{
    protected $table = "sucursales";
    protected $fillable = ['nombre','direccion','telefono','telefono2','correo','correo2','horario','mapa'];
    protected $guarded = ['id'];

}
