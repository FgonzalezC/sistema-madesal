<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class SeccionInversionista extends Model
{
    protected $table = "seccion_inversionistas";

    protected $fillable = ['titulo','img','subtitulo','texto1','texto2','titulo1','bajada1','titulo2','bajada2','titulo3','bajada3','titulo4','bajada4','seccion1','seccion2','punto1','punto2','punto3','punto4','titulodestacado','img1','img2','img3','imgmovil1','imgmovil2','imgmovil3'];

    protected $guarded = ['id'];
}
