<?php

namespace App\Models\Admin;

use App\Models\Admin\Proyecto;
use App\Models\Admin\TipoCocina;
use App\Models\Admin\TipoInmueble;
use App\Models\Admin\Unidad;
use App\Models\Admin\Planta;
use Illuminate\Database\Eloquent\Model;

class Modelo extends Model
{
    protected $table = "modelos";
    protected $fillable = [
      'nombre_modelo',
      'archivo',
      'precio_desde',
      'archivo',
      'slider1','slider2','slider3','slider4','slider5','slidermovil1','slidermovil2','slidermovil3','slidermovil4',
      'slidermovil5',
      'bajada',
      'mtrs',
      'tipoinmueble_id',
      'banos',
      'dormitorio',
      'tipococina_id',
      'estacionamiento',
      'galeria',
      'terminaciones',
      'equipamiento',
      'ubicacion',
      'gimg1',
      'gimg2',
      'gimg3',
      'gimg4',
      'gimg5',
      'gimg6',
      'gimg7',
      'gimg8',
      'gimg9',
      'gimg10',
      'timg1',
      'timg2',
      'timg3',
      'timg4',
      'timg5',
      'timg6',
      'timg7',
      'timg8',
      'timg9',
      'timg10',
      'eimg1',
      'eimg2',
      'eimg3',
      'eimg4',
      'eimg5',
      'eimg6',
      'eimg7',
      'eimg8',
      'eimg9',
      'eimg10',
      'uimg1',
      'uimg2',
      'uimg3',
      'uimg4',
      'uimg5',
      'uimg6',
      'uimg7',
      'uimg8',
      'uimg9',
      'uimg10',
      'vt1',
      've1',
      'vu1',
      'proyecto_id',
      'stock',
      'check_stock',
      'check_tw'
    ];

    protected $guarded = ['id'];

    public function tipoinmueble(){
    	return $this->belongsTo(TipoInmueble::class);
    }
    
    public function tipococina(){

    	return $this->belongsTo(TipoCocina::class);
    }
    
    public function proyecto(){

    	return $this->belongsTo(Proyecto::class);
    }
    
    public function unidades(){

        return $this->hasMany(Unidad::class);
    }
    
    public function planta(){

        return $this->belongsTo(Planta::class);
    }
}
