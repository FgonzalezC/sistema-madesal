<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Financiamiento extends Model
{
    protected $table = "seccion_financiamientos";
    protected $fillable = ['titulo','img','subtitulo','titulo1','bajada1','titulo2','bajada2','titulo3','bajada3'];
    protected $guarded = ['id'];
}

