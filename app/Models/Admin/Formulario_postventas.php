<?php

namespace App\Models\Admin;

use App\Models\Admin\Proyecto;

use Illuminate\Database\Eloquent\Model;

class Formulario_postventas extends Model
{
    
    protected $table = "formulario_postventas";
    protected $fillable = ['nombre','apellido','rut','telefono','mail','proyecto_id','direccion','imagen','comentario'];
    protected $guarded = ['id'];


    public function proyecto(){

    	return $this->belongsTo(Proyecto::class);
    }
}
