<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Admin\Formulario_contacto;
use App\Models\Admin\GuardarContacto;
use Mail;
use App\Models\Admin\Cotizacion;



class ProcesoComprasController extends Controller
{
    /* public function index(Request $request)
    {
        return \View::make('/financiamiento');
    }
     public function procesocompra(Request $request)
    {
          $procesocompra = \DB::table('seccion_procesocompas')
             ->get();
        $data = array('procesocompra' => $procesocompra); 
        return \View::make('/financiamiento')->with($data, $procesocompra);
    }
      public function financiamiento(Request $request)
    {
          $financiamiento = \DB::table('seccion_financiamientos')
             ->get();
        $data = array('financiamiento' => $financiamiento); 
        return \View::make('/financiamiento')->with($data, $financiamiento);
    }*/
    public function index(Request $request)
    {
        return \View::make('/financiamiento');
    }
   
    public function financiamiento(Request $request)
    {
        

          $financiamiento = \DB::table('seccion_financiamientos')
             ->get();

           $procesocompra = \DB::table('seccion_procesocompas')
             ->get();
       
        
        $resul = array('financiamiento'=>$financiamiento,'procesocompra'=>$procesocompra);


        return \View::make('/financiamiento')->with('resultado',$resul);
    }
    public function asesoramos(Request $request)
    {
        

          $asesoria = \DB::table('asesoramos')
             ->get();

           $procesocompra = \DB::table('seccion_procesocompas')
             ->get();
       
        
        $resul = array('asesoria'=>$asesoria,'procesocompra'=>$procesocompra);


        return \View::make('/asesoria')->with('resultado',$resul);
    }

    
    public function index2(Request $request)//financiamiento
    {
       $this->validate($request, [
            'nombre'    =>  'max:60',      
            'apellido'      =>  'max:60',
            'telefono'      =>  'max:12',
            'email'      =>  'email'      
                
           
        ]);

       
        $formulario2 = new  GuardarContacto();
        $formulario2->nombre = $request->input('nombre');
        $formulario2->apellido = $request->input('apellido');
        $formulario2->telefono = $request->input('telefono');
        $formulario2->email = $request->input('email');
        $formulario2->asunto_id = $request->input('asunto_id');
        $formulario2->mensaje = $request->input('mensaje');

     
     Mail::send('mail',$request->all(),function($msj) {

            $msj->from('contacto@madesal.cl');
            $msj->subject('Sirena - Email Estándar');
            $msj->to('madesal-test@leads.getsirena.com');
            $msj->cc('contacto@madesal.cl');

        });



        if(!$formulario2->save() and Mail::failures() ){

         return back()->with('msj2','Datos sin éxito'); 


        }
       else{          

       
            return back()->with('msj','Datos enviado con éxito'); 


        }
    }

 public function index3(Request $request)//asesoria
    {
       $this->validate($request, [
            'nombre_cotizacion'    =>  'max:60max:60',      
            'apellido_cotizacion'      =>  'max:60',
            'telefono_cotizacion'      =>  'max:12',
            'email_cotizacion'      =>  'email'  
        ]);
       
        $formulario2 = new  Cotizacion();
        $formulario2->nombre_cotizacion = $request->input('nombre_cotizacion');
        $formulario2->apellido_cotizacion = $request->input('apellido_cotizacion');
        $formulario2->telefono_cotizacion = $request->input('telefono_cotizacion');
        $formulario2->email_cotizacion = $request->input('email_cotizacion');
        $formulario2->asunto_id = $request->input('asunto_id');
        $formulario2->mensaje_cotizacion = $request->input('mensaje_cotizacion');

     
     Mail::send('mail_cotizacion',$request->all(),function($msj) {

           $msj->from('madesal.prueba@gmail.com');       
           $msj->subject('Sirena - Email Estándar');
           $msj->to('madesal-test@leads.getsirena.com');



        });



        if(!$formulario2->save() and Mail::failures() ){

         return back()->with('msj2','Datos sin éxito'); 


        }
       else{          

       
            return back()->with('msj','Datos enviado con éxito'); 


        }
    }
     
}
