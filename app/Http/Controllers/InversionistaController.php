<?php

namespace App\Http\Controllers;

use App\Models\Admin\Cotizacion;
use Illuminate\Http\Request;
use App\Models\Admin\Proyecto;
use App\Models\Admin\Unidad;
use App\Models\Admin\Comuna;
use App\Models\Admin\Subsidio;
use App\Models\Admin\TipoInmueble;
use App\Models\Admin\EstadoProyecto;
use App\Models\Admin\Parametro;


use Mail;


class InversionistaController extends Controller
{
    public function index(Request $request){
        $Comuna = $request->get('Comuna');//trae de la lista el dato a buscar en la consulta
        $Subsidio = $request->get('Subsidio');//trae de la lista el dato a buscar en la consulta
        $Inmueble = $request->get('Inmueble');//trae de la lista el dato a buscar en la consulta
        $Estado = $request->get('Estado');//trae de la lista el dato a buscar en la consulta
        $Min_Slider = $request->get('Min_Slider');
        $Max_Slider = $request->get('Max_Slider');
        //slider vars to integer
        $Min_Slider = (int)$Min_Slider[0];
        $Max_Slider = (int)$Max_Slider[0];
        $precio_maximo = Unidad::max('precio');

        $rango_maximo = Parametro::max('precio_max');
        $rango_minimo = Parametro::min('precio_min');
        $comunas = Comuna::all();
        $subsidios = Subsidio::all();
        $tipos_de_inmuebles = TipoInmueble::all();
        $estados_de_proyecto = EstadoProyecto::all();

        // $proyecto = \DB::table('proyectos')->where('proyectos.destacado2','=','1')->get();

        $proyecto = \DB::select('select distinct
                            p.id as id, 
                            p.orden as orden,
                            p.tipoinmueble_id as tipoinmueble_id,
                            p.tipoetapa_id as tipoetapa_id,
                            p.img_home as img_home,
                            p.nombre_proyecto as nombre_proyecto,
                            p.dormitorios as dormitorios, 
                            p.banos as banos,
                            p.estacionamiento as estacionamiento,
                            p.valor_estacionamiento as valor_estacionamiento,
                            p.superficie as superficie, 
                            p.precio,
                            ep.nombre_estado,
                            te.nombre_etapa,
                            (select sum(m2.check_stock) from modelos m2 where m2.proyecto_id=p.id) as stock,
                            (CASE
                                WHEN (select sum(m2.check_stock) from modelos m2 where m2.proyecto_id=p.id) > 0 THEN "stock"    
                                ELSE "sin stock"
                            END) as estado_stock
                        from proyectos p 
                        left join estado_proyectos ep on ep.id=p.estado_id 
                        left join tipo_etapas te on te.id=p.tipoetapa_id 
                        where p.destacado2=1
                        ORDER BY p.orden ASC,stock DESC
            ');
        $inversionista = \DB::table('seccion_inversionistas')->get();

        $resul = array(
            'proyecto'=>$proyecto,
            'inversionista' => $inversionista,
            'precio_maximo' => $precio_maximo,
            'comunas' => $comunas,
            'subsidios' => $subsidios,
            'tipos_de_inmuebles' => $tipos_de_inmuebles,
            'estados_de_proyecto' => $estados_de_proyecto

        );
            $data = array(
                'rango_maximo' => $rango_maximo,
                'rango_minimo' => $rango_minimo,
                'comunas' => $comunas,
                'lista_proyectos' => $proyecto,
                'subsidios' => $subsidios,
                'tipos_de_inmuebles' => $tipos_de_inmuebles,
                'estados_de_proyecto' => $estados_de_proyecto

            );

        return \View::make('/inversionista')->with($data,'resultado',$resul);
    }



    public function index2(Request $request)
    {
       $this->validate($request, [
            'nombre_cotizacion'     =>  'max:60max:60',
            'apellido_cotizacion'   =>  'max:60',
            'telefono_cotizacion'   =>  'max:12',
            'email_cotizacion'      =>  'email'
        ]);

        $formulario2 = new  Cotizacion();
        $formulario2->nombre_cotizacion = $request->input('nombre_cotizacion');
        $formulario2->apellido_cotizacion = $request->input('apellido_cotizacion');
        $formulario2->telefono_cotizacion = $request->input('telefono_cotizacion');
        $formulario2->email_cotizacion = $request->input('email_cotizacion');
        $formulario2->asunto_id = $request->input('asunto_id');
        $formulario2->mensaje_cotizacion = $request->input('mensaje_cotizacion');
        Mail::send('mail_cotizacion',$request->all(),function($msj) {
            $msj->from('contacto@madesal.cl');
            $msj->subject('Sirena - Email Estándar');
            $msj->to('madesal-test@leads.getsirena.com');
            $msj->cc('contacto@madesal.cl');

        });
        if(!$formulario2->save() and Mail::failures() ){
            return back()->with('msj2','Datos sin éxito');
        }
       else{
            return back()->with('msj','Datos enviado con éxito');
        }
    }
      public function saladeventa(Request $request)
    {
        $saladeventa = \DB::table('saladeventas')
             ->get();
        $resul = array('saladeventa'=>$saladeventa);
        return \View::make('/inversionista')->with('resultado',$resul);
    }



}
