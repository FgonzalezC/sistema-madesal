<?php
namespace App\Http\Controllers;

use App\Models\Admin\Modelo;
use App\Models\Admin\Proyecto;
use App\Models\Admin\Unidad;
use App\Models\Admin\Comuna;
use App\Models\Admin\Subsidio;
use App\Models\Admin\TipoInmueble;
use App\Models\Admin\EstadoProyecto;
use App\Models\Admin\Parametro;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProyectosController extends Controller
{
    public function index(Request $request) {
        $Comuna = $request->get('Comuna'); //trae de la lista el dato a buscar en la consulta
        $Subsidio = $request->get('Subsidio'); //trae de la lista el dato a buscar en la consulta
        $Inmueble = $request->get('Inmueble'); //trae de la lista el dato a buscar en la consulta
        $Estado = $request->get('Estado'); //trae de la lista el dato a buscar en la consulta
        $MinSlider = $request->get('MinSlider');
        $MaxSlider = $request->get('MaxSlider');
        //slider vars to integer
        $Min_Slider = (int)$MinSlider;
        $Max_Slider = (int)$MaxSlider;
        $rango_maximo = Parametro::max('precio_max');
        $rango_minimo = Parametro::min('precio_min');

        $comunas = Comuna::all();
        $subsidios = Subsidio::all();
        $tipos_de_inmuebles = TipoInmueble::all();
        $estados_de_proyecto = EstadoProyecto::all();

        $proyecto = DB::select('
        SELECT 
                                p.id as id, 
                                p.orden as orden,
                                p.tipoinmueble_id as tipoinmueble_id,
                                p.tipoetapa_id as tipoetapa_id,
                                p.img_home as img_home,
                                p.nombre_proyecto as nombre_proyecto,
                                p.dormitorios as dormitorios, 
                                p.banos as banos,
                                p.estacionamiento as estacionamiento,
                                p.valor_estacionamiento as valor_estacionamiento,
                                p.superficie as superficie, 
                                p.precio,
                                ep.nombre_estado,
                                te.nombre_etapa,
                                (select sum(m2.check_stock) from modelos m2 where m2.proyecto_id=p.id) as stock,
                                (CASE
                                    WHEN (select sum(m2.check_stock) from modelos m2 where m2.proyecto_id=p.id) > 0 THEN "stock"    
                                    ELSE "sin stock"
                                END) as estado_stock,
                                (CASE
                                    WHEN (select sum(m2.check_tw) from modelos m2 where m2.proyecto_id=p.id) > 0 THEN "1"    
                                    ELSE "0"
                                END) as estado_tw
                            from proyectos p 
                            left join estado_proyectos ep on ep.id=p.estado_id 
                            left join tipo_etapas te on te.id=p.tipoetapa_id 
                        ORDER BY p.orden ASC,stock DESC
                        ');


        $data = array(
            'proyecto'              => $proyecto,
            'rango_maximo'          => $rango_maximo,
            'rango_minimo'          => $rango_minimo,
            'comunas'               => $comunas,
            'subsidios'             => $subsidios,
            'tipos_de_inmuebles'    => $tipos_de_inmuebles,
            'estados_de_proyecto'   => $estados_de_proyecto
        );
        
        return \View::make('/proyecto')->with($data, $proyecto);

    }

    public function ficha($id) // este id es del proyecto
    {

        $proyectos = Proyecto::find($id);

        $estado = \DB::table('proyectos')
        ->select('nombre_estado')
        ->where('modelos.proyecto_id', $id)
        ->join('modelos', 'proyectos.id', 'modelos.proyecto_id')
        ->join('estado_proyectos','estado_proyectos.id','proyectos.estado_id')
         ->distinct()
         ->get();

         $valor = \DB::table('proyectos')
        ->select('valor_estacionamiento')
        ->where('modelos.proyecto_id', $id)
        ->join('modelos', 'proyectos.id', 'modelos.proyecto_id')
        ->join('estado_proyectos','estado_proyectos.id','proyectos.estado_id')
         ->distinct()
         ->get();

        $modelo = \DB::table('modelos')->where('proyecto_id', $id)->get();

        $inversionista = \DB::table('seccion_inversionistas')->get();

         $saladeventas = \DB::table('saladeventas')
            ->join('proyectos','proyectos.salaventas_id','saladeventas.id')
            ->where('proyectos.id', $id)
            ->get();

            $empleado = \DB::table('proyecto_ejecutivos')
             ->where('proyectos.id', $id)
             ->join('proyectos','proyectos.id','proyecto_ejecutivos.proyecto_id')
            ->join('empleados','empleados.id','proyecto_ejecutivos.empleado_id')
            ->join('cargos','empleados.cargo_id','cargos.id')
            ->get();

        $resul = array(
            'proyecto' => $proyectos,
            'estados' => $estado,
            'valores' => $valor,
            'modelo' => $modelo,
            'empleado' => $empleado,
            'inversionista'=>$inversionista,
            'saladeventas'=>$saladeventas
        );
        return \View::make('/ficha-proyecto')->with('resultado', $resul);
    }

    public function index2(Request $request)
    {
        $this->validate($request, ['nombre_cotizacion' => 'required', 'apellido_cotizacion' => 'required', 'telefono_cotizacion' => 'required', 'email_cotizacion' => 'required', 'asunto_id' => 'required', 'mensaje_cotizacion' => 'required']);

        $formulario2 = new Cotizacion();
        $formulario2->nombre_cotizacion = $request->input('nombre_cotizacion');
        $formulario2->apellido_cotizacion = $request->input('apellido_cotizacion');
        $formulario2->telefono_cotizacion = $request->input('telefono_cotizacion');
        $formulario2->email_cotizacion = $request->input('email_cotizacion');
        $formulario2->asunto_id = $request->input('asunto_id');
        $formulario2->mensaje_cotizacion = $request->input('mensaje_cotizacion');

        $formulario2->save();
        return view('/ficha-proyecto');
    }
    public function ficha_modelo($id, $proyecto_id, Request $request)
    {
         $valor = \DB::table('proyectos')
        ->select('valor_estacionamiento')
        ->where('modelos.id', $id)

        ->join('modelos', 'proyectos.id', 'modelos.proyecto_id')
        ->join('estado_proyectos','estado_proyectos.id','proyectos.estado_id')
         ->distinct()
         ->get();
        $inversionista = \DB::table('seccion_inversionistas')->get();
        //$proyectos = Proyecto::find($id);
       $empleado = \DB::table('proyecto_ejecutivos')
             ->where('proyectos.id', $proyecto_id)
             ->join('proyectos','proyectos.id','proyecto_ejecutivos.proyecto_id')
            ->join('empleados','empleados.id','proyecto_ejecutivos.empleado_id')
            ->get();

        $modelo = \DB::table('modelos')->where('proyecto_id', $proyecto_id)->get();
        $fichas = \DB::table('modelos')->where('id', $id)->get();


        $unidad = \DB::table('unidades')->where('modelo_id', $id)->get();

        $Unidad = $request->get('id');

        $unidad_ficha = \DB::table('unidades')->where('id', $Unidad)->get();

        $planta = \DB::table('plantas')->where('modelo_id', $id)->get();

        $proyectos = \DB::table('proyectos')->select('modelos.id as id', 'modelos.proyecto_id', 'nombre_proyecto', 'desktopimg1', 'desktopimg2', 'desktopimg3', 'desktopimg4', 'desktopimg5', 'movilimg1', 'movilimg2', 'movilimg3', 'movilimg4', 'movilimg5', 'tipoetapa_id', 'masterplan', 'descripcion_slider', 'precio','modelos.precio_desde', 'superficie', 'nombre_modelo', 'mtrs', 'dormitorio', 'modelos.banos', 'tipococina_id', 'modelos.estacionamiento','modelos.galeria','modelos.terminaciones', 'modelos.equipamiento', 'modelos.ubicacion','modelos.gimg1', 'modelos.gimg2', 'modelos.gimg3', 'modelos.gimg4', 'modelos.gimg5', 'modelos.gimg6', 'modelos.gimg7', 'modelos.gimg8', 'modelos.gimg9', 'modelos.gimg10' ,'modelos.timg1', 'modelos.timg2', 'modelos.timg3', 'modelos.timg4', 'modelos.timg5', 'modelos.timg6', 'modelos.timg7', 'modelos.timg8', 'modelos.timg9', 'modelos.timg10', 'modelos.eimg1', 'modelos.eimg2', 'modelos.eimg3', 'modelos.eimg4', 'modelos.eimg5', 'modelos.eimg6', 'modelos.eimg7', 'modelos.eimg8', 'modelos.eimg9', 'modelos.eimg10', 'modelos.uimg1', 'modelos.uimg2', 'modelos.uimg3', 'modelos.uimg4', 'modelos.uimg5', 'modelos.uimg6', 'modelos.uimg7', 'modelos.uimg8', 'modelos.uimg9', 'modelos.uimg10', 'modelos.vt1', 'modelos.ve1', 'modelos.vu1', 'modelos.tour360', 'proyectos.tipoinmueble_id','slider1','slider2','slider3','slider4','slider5','slidermovil1','slidermovil2','slidermovil3','slidermovil4','slidermovil5'
        )->where('modelos.id', $id)->join('modelos', 'proyectos.id', 'modelos.proyecto_id')
         ->distinct()
         ->get();


          $saladeventas = \DB::table('saladeventas')
            ->join('proyectos','proyectos.salaventas_id','saladeventas.id')
            ->where('proyectos.id', $proyecto_id)
            ->get();

        //dd($proyectos);
        $resul = array(
            'valores' => $valor,
            'proyecto' => $proyectos,
            'modelo' => $modelo,
            'ficha' => $fichas,
            'unidad' => $unidad,
            'unidad_ficha' => $unidad_ficha,
            'empleado' => $empleado,
            'planta' => $planta,
            'saladeventas'=>$saladeventas,
            'inversionista'=>$inversionista
        );
       return View('/ficha-proyecto_modelo')->with('resultado', $resul);
    }

    public function destacados()
    {

        $proyecto = DB::select('
            select distinct
                            p.id as id, 
                            p.orden as orden,
                            p.tipoinmueble_id as tipoinmueble_id,
                            p.tipoetapa_id as tipoetapa_id,
                            p.img_home as img_home,
                            p.nombre_proyecto as nombre_proyecto,
                            p.dormitorios as dormitorios, 
                            p.banos as banos,
                            p.estacionamiento as estacionamiento,
                            p.valor_estacionamiento as valor_estacionamiento,
                            p.superficie as superficie, 
                            p.precio,
                            ep.nombre_estado,
                            te.nombre_etapa,
                            (select sum(m2.check_stock) from modelos m2 where m2.proyecto_id=p.id) as stock,
                            (CASE
                                WHEN (select sum(m2.check_stock) from modelos m2 where m2.proyecto_id=p.id) > 0 THEN "stock"    
                                ELSE "sin stock"
                            END) as estado_stock
                        from proyectos p 
                        left join estado_proyectos ep on ep.id=p.estado_id 
                        left join tipo_etapas te on te.id=p.tipoetapa_id 
                        where p.destacado=1
                        order by p.orden ASC,stock DESC
        ');

        $rango_maximo = Parametro::max('precio_max');
        $rango_minimo = Parametro::min('precio_min');
        $comunas = Comuna::all();
        $subsidios = Subsidio::all();
        $tipos_de_inmuebles = TipoInmueble::all();
        $estados_de_proyecto = EstadoProyecto::all();

        $data = array(
            'proyecto' => $proyecto,
            'rango_maximo' => $rango_maximo,
            'rango_minimo' => $rango_minimo,
            'comunas' => $comunas,
            'subsidios' => $subsidios,
            'tipos_de_inmuebles' => $tipos_de_inmuebles,
            'estados_de_proyecto' => $estados_de_proyecto
        );
        return \View::make('/home')->with($data, $proyecto);
    }
    public function buscar(Request $request)
    {

        $comuna2 = $request->get('comuna2');
        $inmueble2 = $request->get('inmueble2');
        $dormitorio2 = $request->get('dormitorio2');
        $bano2 = $request->get('bano2');
        $estado2 = $request->get('estado2');
        $subsidio2 = $request->get('subsidio2');

        if ($comuna2 != NULL and $inmueble2 and $dormitorio2 and $bano2 and $estado2 and $subsidio2)
        {

            $proyecto = \DB::select('
        SELECT DISTINCT proyectos.id,
        proyectos.nombre_proyecto,
        proyectos.tipoinmueble_id,
        proyectos.dormitorios,
        proyectos.banos,proyectos.estacionamiento,
        proyectos.destacado,
        proyectos.superficie,
        proyectos.precio,
        proyectos.estado_id,
        proyectos.img_home
        FROM proyectos, comunas
        WHERE
        comunas.id=' . $comuna2 . ' and
        proyectos.dormitorios="' . $dormitorio2 . '" and
        proyectos.banos="' . $bano2 . '" and
        proyectos.estado_id="' . $estado2 . '" and
        proyectos.subsidio_id="' . $subsidio2 . '" and
        proyectos.tipoinmueble_id="' . $inmueble2 . '"

        ');
            // dd($request);

        }
        if ($comuna2 != NULL and $inmueble2)
        {

            $proyecto = \DB::select('
        SELECT DISTINCT proyectos.id,
        proyectos.nombre_proyecto,
        proyectos.tipoinmueble_id,
        proyectos.dormitorios,
        proyectos.banos,proyectos.estacionamiento,
        proyectos.destacado,
        proyectos.superficie,
        proyectos.precio,
        proyectos.estado_id,
        proyectos.img_home
        FROM proyectos, comunas
        WHERE
        comunas.id=' . $comuna2 . 'and
        proyectos.tipoinmueble_id="' . $inmueble2 . '"

        ');
            // dd($request);

        }

        $data = array(
            'proyecto' => $proyecto
        );
        return \View::make('/proyecto')->with($data, $proyecto);

    }
    public function filtromovil(Request $request)
    {
        $Comuna = $request->get('Comuna'); //trae de la lista el dato a buscar en la consult
        $Inmueble = $request->get('Inmueble'); //trae de la lista el dato a buscar en la consulta
        $MinSlider = $request->get('MinSlider');
        $MaxSlider = $request->get('MaxSlider');
        //slider vars to integer
        $Min_Slider = (int)$MinSlider;
        $Max_Slider = (int)$MaxSlider;
        if ($Comuna)
        {

            if ($Inmueble and $Comuna)
            {

                foreach ($Comuna as $comunas)
                {

                    $aux[] = \DB::table('comunas')->select('id')
                        ->where('comunas.nombre_comuna', '=', $comunas)->get();

                }

                foreach ($aux as $auxito)
                {

                    $papita[] = $auxito[0]->id;
                }

                foreach ($Inmueble as $inmueble)
                {

                    $aux3[] = \DB::table('tipo_inmuebles')->select('id')
                        ->where('tipo_inmuebles.nombre_inmueble', '=', $inmueble)->get();

                }

                foreach ($aux3 as $auxito3)
                {

                    $papita3[] = $auxito3[0]->id;
                }

                $papita = implode(',', $papita);
                $papita3 = implode(',', $papita3);

                /* consulta que trae todos los proyectos de las comunas seleccionadas en la vista home*/

                $proyecto = \DB::select('
                SELECT DISTINCT proyectos.id,
                proyectos.nombre_proyecto,
                proyectos.tipoinmueble_id,
                proyectos.dormitorios,
                proyectos.banos,proyectos.estacionamiento,

                proyectos.superficie,
                proyectos.precio,
                proyectos.comuna_id,
                proyectos.img_home

                FROM proyectos, comunas

                WHERE  proyectos.comuna_id in (' . $papita . ')  and
                proyectos.tipoinmueble_id  in (' . $papita3 . ')and
                proyectos.precio between (' . $Min_Slider . ') and
                (' . $Max_Slider . ')    ');

                $data = array(
                    'proyecto' => $proyecto
                );
                return \View::make('/proyecto')->with($data, $proyecto);

                unset($id_comunas);

            }

            else
            {

                foreach ($Comuna as $comunas)
                {

                    $aux[] = \DB::table('comunas')->select('id')
                        ->where('comunas.nombre_comuna', '=', $comunas)->get();

                }

                foreach ($aux as $auxito)
                {

                    $papita[] = $auxito[0]->id;
                }

                $papita = implode(',', $papita);

                /* consulta que trae todos los proyectos de las comunas seleccionadas en la vista home*/

                $proyecto = \DB::select('
                SELECT DISTINCT proyectos.id,
                proyectos.nombre_proyecto,
                proyectos.tipoinmueble_id,
                proyectos.dormitorios,
                proyectos.banos,proyectos.estacionamiento,

                proyectos.superficie,
                proyectos.precio,
                proyectos.comuna_id,
                proyectos.img_home

                FROM proyectos, comunas

                WHERE  proyectos.comuna_id in (' . $papita . ')and
                proyectos.precio between (' . $Min_Slider . ') and
                (' . $Max_Slider . ')
                    ');

                $data = array(
                    'proyecto' => $proyecto
                );
                return \View::make('/proyecto')->with($data, $proyecto);

                unset($id_comunas);
            }

        }
    }

}


