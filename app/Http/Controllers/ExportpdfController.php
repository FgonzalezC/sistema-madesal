<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Admin\Modelo;
use App\Models\Admin\Proyecto;



class ExportpdfController extends Controller
{
    public function ficha_exportarpdf($id){
       
        $modelo = \DB::table('modelos')
        ->where('proyecto_id',$id)    
        ->get();
    

        $ficha = \DB::table('seccion_fichaproyectos')
        ->select('terminaciones','equipamiento', 'ubicacion_entorno', 'imgfinanciamiento','img1','img2','img3','img4','img5')
        ->get();
        
        $proyectos = Proyecto::find($id);



        $pdf = \PDF::loadView('pdf', compact( 'ficha','modelo','proyectos'));
        return $pdf->download('ficha.pdf');
    }

    public function ficha_modelo_exportarpdf($id,$proyecto_id){

        
    

      $ficha = \DB::table('seccion_fichaproyectos')
      ->select('terminaciones','equipamiento', 'ubicacion_entorno', 'imgfinanciamiento','img1','img2','img3','img4','img5')
      ->get();
      
     
    
      $proyectos = \DB::table('proyectos')
      ->select(
      'modelos.id as id',
      'modelos.proyecto_id',
     'nombre_proyecto',
     'desktopimg1',
     'desktopimg2',
     'desktopimg3',
     'desktopimg4',
     'desktopimg5',
      'tipoetapa_id',
      'masterplan',
      'descripcion_slider',
      'precio',
      'superficie',
      'nombre_modelo',
      'mtrs',
      'dormitorio',
      'modelos.banos',
      'tipococina_id',
      'modelos.estacionamiento',
      'modelos.terminaciones',
      'proyectos.tipoinmueble_id'

      )
      ->where('modelos.id',$id)    
      ->join('modelos','proyectos.id','modelos.proyecto_id')     
      ->distinct()   
      ->get();
     
      $pdf = \PDF::loadView('pdf_ficha_modelo', compact( 'ficha','proyectos'));
      return $pdf->download('ficha_modelo.pdf');


    }
}
