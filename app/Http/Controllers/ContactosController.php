<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Models\Admin\GuardarContacto;
use Mail;

class ContactosController extends Controller
{
     public function index(Request $request)
    {
    	  $contacto = \DB::table('contactos')
             ->get();
        $data = array('contacto' => $contacto); 
        return \View::make('/contacto')->with($data, $contacto);



    }
     public function index2(Request $request)
    {
       $this->validate($request, [
            'nombre'    =>  'max:60',      
            'apellido'      =>  'max:60',
            'telefono'      =>  'max:12',
            'email'      =>  'email'      
                
           
        ]);
     



        $formulario2 = new  GuardarContacto();
        $formulario2->nombre = $request->input('nombre');
        $formulario2->apellido = $request->input('apellido');
        $formulario2->telefono = $request->input('telefono');
        $formulario2->email = $request->input('email');
        $formulario2->asunto_id = $request->input('asunto_id');
        $formulario2->mensaje = $request->input('mensaje');
       
          Mail::send('mail',$request->all(),function($msj) {

            $msj->from('contacto@madesal.cl');
            $msj->subject('Sirena - Email Estándar');
            $msj->to('madesal-test@leads.getsirena.com');
            $msj->cc('contacto@madesal.cl');
            
           


        });



        if(!$formulario2->save() and Mail::failures() ){

         return back()->with('msj2','Datos sin éxito'); 
        }
       else{          

       
            return back()->with('msj','Datos enviado con éxito'); 


        }
        return view('/contacto');
    }

   
}
