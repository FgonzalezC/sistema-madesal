<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Admin\Cotizacion;
use App\Models\Admin\AsuntoCotizacion;
use Mail;

class SimuladoresController extends Controller
{
      public function index(Request $request)
    { 
      $respuesta=0;          
      $respuesta2=0;
      $respuesta3=0;
      $respuesta4=0;
      $precio_propiedad=0;
      $pie=0;
      $plazo=0;
      $tasa=0;
      $monto=0;
      $div_plazo=0;
      $div_tasa=0;
      $cap_plazo=0;
      $cap_tasa=0;
      $monto_renta=NULL;
      $co_renta=NULL;
      $otro_ingreso=NULL;
      $extra_monto=NULL;
      $monto_renta1=NULL;
      $monto_renta2=NULL;
      $valor_pie=NULL;
      $valor=NULL;
      $sinrespuesta=0;      
    

      /* API UF */

      /*$apiUrl = 'https://mindicador.cl/api';
      //Es necesario tener habilitada la directiva allow_url_fopen para usar file_get_contents
      if ( ini_get('allow_url_fopen') ) {
            $json = file_get_contents($apiUrl);     
      }
       else {
    //De otra forma utilizamos cURL
        $curl = curl_init($apiUrl);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $json = curl_exec($curl);
       curl_close($curl); 
      }
  
      $dailyIndicators = json_decode($json);*/
      $valor_uf=  28000;

      
      if($request->dividendo=="dividendo"){


        $precio_propiedad = $request->get('precio_propiedad');
        $plazo = $request->get('plazo');
        $pie = $request->get('pie');
        $valor = $request->get('valor');
        $valor_pie=$request->get('valor_pie');
        $tasa=$request->get('tasa');

          /*calculos*/
     
        if($precio_propiedad>0 and $plazo>0 and $pie>0 and $tasa>0){

          $vp = ($pie/100)*$precio_propiedad;            
          $i=($tasa/100)/12;
          $n=$plazo*12;    
        
          $a=  $vp*(($i*(pow(1+$i,$n)))/(pow(1+$i,$n))-1);   
          $dividendo_peso=$a*$valor_uf; 
          $respuesta=number_format($dividendo_peso, 0, ',', '.');
                
        }
        else{

          $sinrespuesta=1;       
        }
          
      }
  


        if($request->click_hipotecario=="click_hipotecario"){
        
          $monto=$request->get('monto');
          $div_plazo=$request->get('div_plazo');
          $div_tasa=$request->get('div_tasa');
          $pie = $request->get('pie');
          $valor = $request->get('valor');

          if($monto>0 and $div_plazo>0){

            $hipotecario_uf=($div_plazo/100)*$monto;      //UF
            $hipotecario_peso=$hipotecario_uf*$valor_uf; // peso
            $respuesta2=number_format($hipotecario_peso, 0, ',', '.');
            $respuesta4=number_format($hipotecario_uf, 0, ',', '.');

          }
          else{
            $sinrespuesta=1;
          }

        }

       if($request->endeudamiento=="endeudamiento"){

          $monto_renta=$request->get('monto_renta');
          $co_renta=$request->get('co_renta');
          $otro_ingreso=$request->get('otro_ingreso');
          $extra_monto=$request->get('extra_monto');
          $monto_renta1=$request->get('monto_renta1');
          $monto_renta2=$request->get('monto_renta2');
          $cap_tasa=$request->get('cap_tasa');
          $cap_plazo=$request->get('cap_plazo');

         
          if($cap_tasa>0 and $cap_plazo>0){

            $endeudamiento=$cap_tasa*$cap_plazo;
            $respuesta3=number_format($endeudamiento, 0, ',', '.');

          }
          else{
            $sinrespuesta=1;
          }
      }

 
        return \View::make('/simuladores',compact('respuesta',
        'respuesta2',
        'respuesta3',
        'respuesta4',
        'precio_propiedad',
        'pie',
        'plazo',
        'tasa',
        'monto',
        'div_plazo',
        'div_tasa',
        'cap_tasa',
        'cap_plazo',
        'monto_renta',
        'co_renta',
        'otro_ingreso',
        'extra_monto',
        'monto_renta1',
        'monto_renta2',
        'valor_pie',
        'valor',
        'valor_uf',
       'sinrespuesta'));
      }
      

      public function index2(Request $request)
    {
       $this->validate($request, [
            'nombre_cotizacion'    =>  'max:60max:60',      
            'apellido_cotizacion'      =>  'max:60',
            'telefono_cotizacion'      =>  'max:12',
            'email_cotizacion'      =>  'email'
        ]);

        $formulario2 = new  Cotizacion();
        $formulario2->nombre_cotizacion = $request->input('nombre_cotizacion');
        $formulario2->apellido_cotizacion = $request->input('apellido_cotizacion');
        $formulario2->telefono_cotizacion = $request->input('telefono_cotizacion');
        $formulario2->email_cotizacion = $request->input('email_cotizacion');
        $formulario2->asunto_id = $request->input('asunto_id');
        $formulario2->mensaje_cotizacion = $request->input('mensaje_cotizacion');

     
      
     
        Mail::send('mail_cotizacion',$request->all(),function($msj) {

            $msj->from('madesal.prueba@gmail.com');       
            $msj->subject('Sirena - Email Estándar');
            $msj->to('madesal-test@leads.getsirena.com');



        });



        if(!$formulario2->save() and Mail::failures() ){

         return back()->with('msj2','Datos sin éxito'); 


        }
       else{          

       
            return back()->with('msj','Datos enviado con éxito'); 


        }
      
       
    }

}
