<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Admin\Cotizacion;
use Mail;

class MadesalsController extends Controller
{
      public function index(Request $request)
    {
    	$empleado = \DB::table('empleados')
    	->get();
    	  $madesal = \DB::table('madesals')
            
             ->get();
        $resul = array('madesal' => $madesal,'empleado'=>$empleado); 
        return \View::make('/madesal')->with('resultado', $resul);
    }
     public function index4(Request $request)
    {


        $this->validate($request, [
            'nombre_cotizacion'    =>  'max:60max:60',      
            'apellido_cotizacion'      =>  'max:60',
            'telefono_cotizacion'      =>  'max:12',
            'email_cotizacion'      =>  'email'
        ]);

        
          $formulario2 = new  Cotizacion();
        $formulario2->nombre_cotizacion = $request->input('nombre_cotizacion');
        $formulario2->apellido_cotizacion = $request->input('apellido_cotizacion');
        $formulario2->telefono_cotizacion = $request->input('telefono_cotizacion');
        $formulario2->email_cotizacion = $request->input('email_cotizacion');
         $formulario2->asunto_id = $request->input('asunto_id');
        $formulario2->mensaje_cotizacion = $request->input('mensaje_cotizacion');

     
      
     Mail::send('mail_cotizacion',$request->all(),function($msj) {

            $msj->from('contacto@madesal.cl');
            $msj->subject('Sirena - Email Estándar');
            $msj->to('madesal-test@leads.getsirena.com');
            $msj->cc('contacto@madesal.cl');




        });


        if(!$formulario2->save() and Mail::failures() ){

         return back()->with('msj2','Datos sin éxito'); 


        }
       else{          

       
            return back()->with('msj','Datos enviado con éxito'); 


        }
    }
}
