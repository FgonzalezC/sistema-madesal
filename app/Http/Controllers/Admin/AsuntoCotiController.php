<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ValidacionAsuntoCoti;
use App\Models\Admin\AsuntoCotizacion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AsuntoCotiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $datas = AsuntoCotizacion::orderBy('id')->get();
      return view('admin.asuntocoti.index', compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function crear()
    {
        return view('admin.asuntocoti.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function guardar(ValidacionAsuntoCoti $request)
    {
        $valor=AsuntoCotizacion::create($request->all());
        
       return redirect('admin/asuntocoti')->with('mensaje','Asunto creado con exito.');
    }

    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editar($id)
    {
        $data = AsuntoCotizacion::findOrFail($id);
        return view('admin.asunto.editar', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function actualizar(ValidacionAsuntoCoti $request, $id)
    {
        AsuntoCotizacion::findOrFail($id)->update($request->all());
        return redirect('admin/asuntocoti')->with('mensaje', 'Asunto actualizado con exito');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function eliminar(Request $request, $id)
    {
        if ($request->ajax()) {
            if (AsuntoCotizacion::destroy($id)) {
                return response()->json(['mensaje' => 'ok']);
            } else {
                return response()->json(['mensaje' => 'ng']);
            }
        } else {
            abort(404);
        }
    }
}
