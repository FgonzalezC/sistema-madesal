<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\Sucursal;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
class SucursalController extends Controller
{
   public function index(Request $request)
   {
     $datas = Sucursal::orderBy('id')->get();
     return view('admin.sucursal.index', compact('datas'));
 }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function crear()
    {

        return view('admin.sucursal.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function guardar(Request $request)
    {
     $this->validate($request, [

        'nombre'    =>  'required',
        'direccion'    =>  'required',      
        'telefono'      =>  'required',      
        'correo'      =>  'required|email',
        
        'horario'      =>  'required',         
        'mapa'      =>  'required',


    ]);




     $sucursal = new Sucursal();
     $sucursal->nombre = $request->input('nombre');
     $sucursal->direccion = $request->input('direccion');
     $sucursal->telefono = $request->input('telefono');
     $sucursal->telefono2 = $request->input('telefono2');
     $sucursal->correo = $request->input('correo');
     $sucursal->correo2 = $request->input('correo2');
     $sucursal->horario = $request->input('horario');
     $sucursal->mapa = $request->input('mapa');
     $sucursal->save();

     
     return redirect('admin/sucursal')->with('mensaje','Seccion Sucursal creada con exito.');
 }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editar($id)
    {
        $data = Sucursal::findOrFail($id);
        return view('admin.sucursal.editar', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function actualizar(Request $request, $id)
    {

       $this->validate($request, [

        'nombre'    =>  'required',
        'direccion'    =>  'required',      
        'telefono'      =>  'required',      
        'correo'      =>  'required|email',
        'horario'      =>  'required',         
        'mapa'      =>  'required',


    ]);
        $data = Sucursal::find($id);
        $data->nombre = $request->input('nombre');
        $data->direccion = $request->input('direccion');
        $data->telefono = $request->input('telefono');
        $data->telefono2 = $request->input('telefono2');
        $data->correo = $request->input('correo');
        $data->correo2 = $request->input('correo2');
        $data->horario = $request->input('horario');
        $data->mapa = $request->input('mapa');

        $data->save();

        return redirect('admin/sucursal')->with('mensaje', 'Seccion Sucursal actualizado con exito');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function eliminar(Request $request, $id)
    {
        if ($request->ajax()) {
            if (Sucursal::destroy($id)) {
                return response()->json(['mensaje' => 'ok']);
            } else {
                return response()->json(['mensaje' => 'ng']);
            }
        } else {
            abort(404);
        }
    }
}
