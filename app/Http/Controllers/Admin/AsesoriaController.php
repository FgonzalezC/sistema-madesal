<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ValidacionAsesoria;
use App\Models\Admin\Asesoria;
use Illuminate\Http\Request;

class AsesoriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $datas = Asesoria::orderBy('id')->get();
      return view('admin.asesoria.index', compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function crear()
    {

        return view('admin.asesoria.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function guardar(ValidacionAsesoria $request)
    {
        $this->validate($request, [
            'titulo'    =>  'required'          
        ]);

        /* Validación para que la imagen sólo sea en estos formatos y campo requerido. Quitar gif si es necesario*/
        $this->validate($request, [
            'img'   => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'img1'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'img2'  => 'image|mimes:jpg,png,gif,jpeg|max:2048'
        ]);

      

        $asesoria = new Asesoria();
        $asesoria->titulo = $request->input('titulo');
       // dd($request);

        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
         if (request()->hasFile('img')){ 
        $image = $request->file('img');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $asesoria->img = $new_name;
        }
        else
        {
            $asesoria->img = 'Sin imagen';
        } 
        /*----*/

        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
         if (request()->hasFile('img1')){ 
        $image = $request->file('img1');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $asesoria->img1 = $new_name;
        }
        else
        {
            $asesoria->img1 = 'Sin imagen';
        } 

        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
         if (request()->hasFile('img2')){ 
        $image = $request->file('img2');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $asesoria->img2 = $new_name;
        }
        else
        {
            $asesoria->img2 = 'Sin imagen';
        }

        $asesoria->save();
     //  Proyecto::create($request->all());
        return redirect('admin/asesoria')->with('mensaje','Te Asesoramos creado con exito.');
    }

    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editar($id)
    {
        $data = Asesoria::findOrFail($id);
        return view('admin.asesoria.editar', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function actualizar(ValidacionAsesoria $request, $id)
    {
       
        $this->validate($request, [
            'titulo'    =>  'required'
        ]);

        $this->validate($request, [
            'img'      => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'img1'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'img2'  => 'image|mimes:jpg,png,gif,jpeg|max:2048'
            
        ]);

        $data = Asesoria::find($id);
        $data->titulo = $request->get('titulo');
        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
         if (request()->hasFile('img')){ 
        $image = $request->file('img');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $data->img = $new_name;
        }
        /*------*/

        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
         if (request()->hasFile('img1')){ 
        $image = $request->file('img1');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $data->img1 = $new_name;
        }

        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
         if (request()->hasFile('img2')){ 
        $image = $request->file('img2');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $data->img2 = $new_name;
        }

       
        $data->save();

       // Proyecto::findOrFail($id)->update($request->all());
        return redirect('admin/asesoria')->with('mensaje', 'Asesoria actualizado con exito');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function eliminar(Request $request, $id)
    {
        if ($request->ajax()) {
            if (Asesoria::destroy($id)) {
                return response()->json(['mensaje' => 'ok']);
            } else {
                return response()->json(['mensaje' => 'ng']);
            }
        } else {
            abort(404);
        }
    }


}
