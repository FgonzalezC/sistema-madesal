<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ValidacionPlanta;
use App\Models\Admin\Planta;
use Illuminate\Http\Request;

class PlantaController extends Controller
{
       /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $datas = Planta::orderBy('id')->get();
      return view('admin.planta.index', compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function crear()
    {
        return view('admin.planta.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function guardar(ValidacionPlanta $request)
    {
      $this->validate($request, [
            
            'modelo_id'=>'required'
         
        ]);

        /* Validación para que la imagen sólo sea en estos formatos y campo requerido. Quitar gif si es necesario*/
        $this->validate($request, [
            'img_baja'      => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'img_alta'      => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'img_3era'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'img_areaverde'  => 'image|mimes:jpg,png,gif,jpeg|max:2048'
            
        ]);

        $planta = new Planta();
     

        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
         if (request()->hasFile('img_baja')){ 
        $image = $request->file('img_baja');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $planta->img_baja = $new_name;
        }
        
        /*----*/
        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
         if (request()->hasFile('img_alta')){ 
        $image = $request->file('img_alta');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $planta->img_alta = $new_name;
        }
       
        /*----*/

        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
         if (request()->hasFile('img_3era')){ 
        $image = $request->file('img_3era');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $planta->img_3era = $new_name;
        }
      

        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
         if (request()->hasFile('img_areaverde')){ 
        $image = $request->file('img_areaverde');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $planta->img_areaverde = $new_name;
        }
       


       
        $planta->modelo_id = $request->input('modelo_id');
        $planta->save(); /* save en db al objeto con los valores*/

     //  Proyecto::create($request->all());
        return redirect('admin/planta')->with('mensaje','Planta creado con exito.');
    }

    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editar($id)
    {
        $data = Planta::findOrFail($id);
        return view('admin.planta.editar', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function actualizar(ValidacionPlanta $request, $id)
    {
        $this->validate($request, [
            
            'modelo_id'=>'required'
         
        ]);

        /* Validación para que la imagen sólo sea en estos formatos y campo requerido. Quitar gif si es necesario*/
        $this->validate($request, [
            'img_baja'      => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'img_alta'      => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'img_3era'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'img_areaverde'  => 'image|mimes:jpg,png,gif,jpeg|max:2048'
            
        ]);
        $data = Planta::find($id);
     
        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
         if (request()->hasFile('img_baja')){ 
        $image = $request->file('img_baja');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $data->img_baja = $new_name; 
        }
        /*------*/
        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
         if (request()->hasFile('img_alta')){ 
        $image = $request->file('img_alta');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $data->img_alta = $new_name;
        }
        /*------*/

        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
         if (request()->hasFile('img_3era')){ 
        $image = $request->file('img_3era');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $data->img_3era = $new_name;
        }

        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
         if (request()->hasFile('img_areaverde')){ 
        $image = $request->file('img_areaverde');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $data->img_areaverde = $new_name;
        }

        $data->modelo_id = $request->get('modelo_id');
        $data->save();

            return redirect()->back()->with('mensaje', 'Planta actualizada con exito.');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function eliminar(Request $request, $id)
    {
        if ($request->ajax()) {
            if (Planta::destroy($id)) {
                return response()->json(['mensaje' => 'ok']);
            } else {
                return response()->json(['mensaje' => 'ng']);
            }
        } else {
            abort(404);
        }
    }


    public function eliminar_fotoplanta(Request $request,$id,$dato){


        $data = Planta::findOrFail($id);

        /********************slider1**************************************/

        if($data->img_baja==$dato){


         \DB::table('plantas')
         ->where('id', $id)  
         ->update(['img_baja'=>NULL]);

         $url=public_path('/storage/'.$dato);

         unlink($url);
         return redirect()->route('editar_planta', ['id' => $id]);

     }

      if($data->img_alta==$dato){


         \DB::table('plantas')
         ->where('id', $id)  
         ->update(['img_alta'=>NULL]);

         $url=public_path('/storage/'.$dato);

         unlink($url);
         return redirect()->route('editar_planta', ['id' => $id]);


    }

      if($data->img_3era==$dato){


         \DB::table('plantas')
         ->where('id', $id)  
         ->update(['img_3era'=>NULL]);

         $url=public_path('/storage/'.$dato);

         unlink($url);
         return redirect()->route('editar_planta', ['id' => $id]);




     }
        if($data->img_areaverde==$dato){


         \DB::table('plantas')
         ->where('id', $id)  
         ->update(['img_areaverde'=>NULL]);

         $url=public_path('/storage/'.$dato);

         unlink($url);
         return redirect()->route('editar_planta', ['id' => $id]);




     }
}
}
