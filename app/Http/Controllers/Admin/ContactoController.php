<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\Contacto;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContactoController extends Controller
{
    public function index(Request $request)
    {
       $datas = Contacto::orderBy('id')->get();
      return view('admin.contacto.index', compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function crear()
    {

        return view('admin.contacto.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function guardar(Request $request)
    {
       $this->validate($request, [
            'titulo'    =>  'required',      
            'subtitulo'      =>  'required'
            
           
        ]);
        $this->validate($request, [
            'imagen'      => 'image|mimes:jpg,png,gif,jpeg|max:2048'
            
        ]);
   

    

        $contacto = new Contacto();
        $contacto->titulo = $request->input('titulo');

        if (request()->hasFile('imagen')){ 
            $image = $request->file('imagen');
            $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
            $image->move(public_path('/storage'), $new_name);
            $contacto->imagen = $new_name;
            }
            else
            {
                $contacto->imagen = 'Sin imagen';
            } 

        $contacto->subtitulo = $request->input('subtitulo');
        $contacto->save();

     
        return redirect('admin/contacto')->with('mensaje','Seccion Contacto creada con exito.');
    }

    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editar($id)
    {
        $data = Contacto::findOrFail($id);
        return view('admin.contacto.editar', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function actualizar(Request $request, $id)
    {
       
        $this->validate($request, [
            'titulo'    =>  'required',         
            'subtitulo'      =>  'required'
            
        ]);

        $this->validate($request, [
            'imagen'      => 'image|mimes:jpg,png,gif,jpeg|max:2048'
            
        ]);

      

        $data = Contacto::find($id);
        $data->titulo = $request->input('titulo');

        if (request()->hasFile('imagen')){ 
            $image = $request->file('imagen');
            $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
            $image->move(public_path('/storage'), $new_name);
            $data->imagen = $new_name;
            }
            else
            {
                $data->imagen = 'Sin imagen';
            } 

        $data->subtitulo = $request->input('subtitulo');
      
        $data->save();

        return redirect('admin/contacto')->with('mensaje', 'Seccion Contacto actualizado con exito');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function eliminar(Request $request, $id)
    {
        if ($request->ajax()) {
            if (Contacto::destroy($id)) {
                return response()->json(['mensaje' => 'ok']);
            } else {
                return response()->json(['mensaje' => 'ng']);
            }
        } else {
            abort(404);
        }
    }
    
}
