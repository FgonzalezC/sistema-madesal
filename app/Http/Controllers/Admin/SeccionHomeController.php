<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\SeccionHome;
use App\Http\Requests\ValidacionSeccionHome;

class SeccionHomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $datas = SeccionHome::orderBy('id')->get();
      return view('admin.seccion-home.index', compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function crear(Request $request)
    {
        return view('admin.seccion-home.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function guardar(ValidacionSeccionHome $request)
    {
        //echo $request->file('img1')->store('/','public');
        //echo $request->file('img2')->store('/','public');

        /* Campos requeridos */
        $this->validate($request, [
            'titulo_principal'    =>  'required',
            'titulo_principal2'    =>  'required',
            'subtitulo'     =>  'required',
            'subtitulo1'     =>  'required',
            'subtitulo2'     =>  'required',
            'texto'     =>  'required',
            'titulo_movil'     =>  'required',
            'msubtitulo1'     =>  'required',
            'msubtitulo2'     =>  'required',
            'msubtitulo3'     =>  'required',
            'msubtitulo4'     =>  'required',
            
        ]);

        /* Validación para que la imagen sólo sea en estos formatos y campo requerido. Quitar gif si es necesario*/
        $this->validate($request, [
            'img'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'img1'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'img2'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'img3'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'movilimg1'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'movilimg2'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'movilimg3'  => 'image|mimes:jpg,png,gif,jpeg|max:2048'
        ]);

        /* Objeto SeccionHome vacío */
        $seccion = new SeccionHome();
        $seccion->titulo_principal = $request->input('titulo_principal');
        $seccion->titulo_principal2 = $request->input('titulo_principal2');
        $seccion->subtitulo = $request->input('subtitulo');
        $seccion->subtitulo1 = $request->input('subtitulo1');
        $seccion->subtitulo2 = $request->input('subtitulo2');
        $seccion->texto = $request->input('texto');

    
         if (request()->hasFile('img')){ 
        $image = $request->file('img');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $seccion->img = $new_name;
        }
        else
        {
            $seccion->img = 'Sin imagen';
        } 


        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
         if (request()->hasFile('img1')){ 
        $image = $request->file('img1');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $seccion->img1 = $new_name;
        }
        else
        {
            $seccion->img1 = 'Sin imagen';
        } 

        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
        if (request()->hasFile('img2')){
        $image2 = $request->file('img2');
        $new_name2 = date('dmY-Hisu') . '-' . $image2->getClientOriginalName();
        $image2->move(public_path('/storage'), $new_name2);
        $seccion->img2 = $new_name2;
        }
         else
        {
            $seccion->img2 = 'Sin imagen';
        } 

        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
        if (request()->hasFile('img3')){
        $image3 = $request->file('img3');
        $new_name3 = date('dmY-Hisu') . '-' . $image3->getClientOriginalName();
        $image3->move(public_path('/storage'), $new_name3);
        $seccion->img3 = $new_name3;
        }
         else
        {
            $seccion->img3 = 'Sin imagen';
        } 
        $seccion->titulo_movil = $request->input('titulo_movil');
        $seccion->msubtitulo1 = $request->input('msubtitulo1');
        $seccion->msubtitulo2 = $request->input('msubtitulo2');
        $seccion->msubtitulo3 = $request->input('msubtitulo3');
        $seccion->msubtitulo4 = $request->input('msubtitulo4');

        if (request()->hasFile('movilimg1')){ 
        $image = $request->file('movilimg1');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $seccion->movilimg1 = $new_name;
        }
        else
        {
            $seccion->movilimg1 = 'Sin imagen';
        } 

        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
        if (request()->hasFile('movilimg2')){
        $image2 = $request->file('movilimg2');
        $new_name2 = date('dmY-Hisu') . '-' . $image2->getClientOriginalName();
        $image2->move(public_path('/storage'), $new_name2);
        $seccion->movilimg2 = $new_name2;
        }
         else
        {
            $seccion->movilimg2 = 'Sin imagen';
        } 

        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
        if (request()->hasFile('movilimg3')){
        $image3 = $request->file('movilimg3');
        $new_name3 = date('dmY-Hisu') . '-' . $image3->getClientOriginalName();
        $image3->move(public_path('/storage'), $new_name3);
        $seccion->movilimg3 = $new_name3;
        }
         else
        {
            $seccion->movilimg3 = 'Sin imagen';
        } 
        $seccion->save(); /* save en db al objeto con los valores*/

        //SeccionHome::create($request->all());
        return redirect('admin/seccion-home')->with('mensaje','Sección home creada con éxito.'); 
    }

    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editar($id)
    {
        $data = SeccionHome::find($id);
        return view('admin.seccion-home.editar', compact('data', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function actualizar(ValidacionSeccionHome $request, $id)
    {

         $this->validate($request, [
            'titulo_principal'    =>  'required',
            'titulo_principal2'    =>  'required',
            'subtitulo'     =>  'required',
            'subtitulo1'     =>  'required',
            'subtitulo2'     =>  'required',
            'texto'     =>  'required',
            'titulo_movil'     =>  'required',
            'msubtitulo1'     =>  'required',
            'msubtitulo2'     =>  'required',
            'msubtitulo3'     =>  'required',
            'msubtitulo4'     =>  'required',
            
        ]);

        /* Validación para que la imagen sólo sea en estos formatos y campo requerido. Quitar gif si es necesario*/
        $this->validate($request, [
            'img'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'img1'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'img2'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'img3'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'movilimg1'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'movilimg2'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'movilimg3'  => 'image|mimes:jpg,png,gif,jpeg|max:2048'
        ]);

        $data = SeccionHome::find($id);
        $data->titulo_principal = $request->get('titulo_principal');
        $data->titulo_principal2 = $request->get('titulo_principal2');
        $data->subtitulo = $request->get('subtitulo');
        $data->subtitulo1 = $request->get('subtitulo1');
        $data->subtitulo2 = $request->get('subtitulo2');
        $data->texto = $request->get('texto');


        if (request()->hasFile('img')){
        $image = $request->file('img');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $data->img = $new_name;
        }

        if (request()->hasFile('img1')){
        $image = $request->file('img1');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $data->img1 = $new_name;
        }

        if (request()->hasFile('img2')){
        $image2 = $request->file('img2');
        $new_name2 = date('dmY-Hisu') . '-' . $image2->getClientOriginalName();
        $image2->move(public_path('/storage'), $new_name2);
        $data->img2 = $new_name2;
        }

        if (request()->hasFile('img3')){
        $image3 = $request->file('img3');
        $new_name3 = date('dmY-Hisu') . '-' . $image3->getClientOriginalName();
        $image3->move(public_path('/storage'), $new_name3);
        $data->img3 = $new_name3;
        }

        if (request()->hasFile('movilimg1')){
        $image = $request->file('movilimg1');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $data->movilimg1 = $new_name;
        }
         if (request()->hasFile('movilimg2')){
        $image = $request->file('movilimg2');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $data->movilimg2 = $new_name;
        }
         if (request()->hasFile('movilimg3')){
        $image = $request->file('movilimg3');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $data->movilimg3 = $new_name;
        }
        
        $data->save();

        //SeccionHome::findOrFail($id)->update($request->all());
        return redirect('admin/seccion-home')->with('mensaje', 'Sección home actualizada con éxito.');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function eliminar(Request $request, $id)
    {
        if ($request->ajax()) {
            if (SeccionHome::destroy($id)) {
                return response()->json(['mensaje' => 'ok']);
            } else {
                return response()->json(['mensaje' => 'ng']);
            }
        } else {
            abort(404);
        }
    }
}
