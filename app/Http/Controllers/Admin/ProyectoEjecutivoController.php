<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ValidacionProyecto;
use App\Models\Admin\ProyectoEjecutivo;
use Illuminate\Http\Request;

class ProyectoEjecutivoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $datas = ProyectoEjecutivo::orderBy('id')->paginate(5);
      return view('admin.proyectoejecutivo.index', compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function crear()
    {

        return view('admin.proyectoejecutivo.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function guardar(ValidacionProyecto $request)
    {
       $this->validate($request, [
            'proyecto_id'    =>  'required',
            'empleado_id'    =>  'required'
           
        ]);

        $proyectoeje = new ProyectoEjecutivo();
       
        $proyectoeje->proyecto_id = $request->input('proyecto_id');
        $proyectoeje->empleado_id = $request->input('empleado_id');
       


        $proyectoeje->save(); /* save en db al objeto con los valores*/

     //  Proyecto::create($request->all());
        return redirect('admin/proyectoejecutivo')->with('mensaje','Asignacion creada con exito.');
    }

    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editar($id)
    {
        $data = ProyectoEjecutivo::findOrFail($id);
        return view('admin.proyectoejecutivo.editar', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function actualizar(ValidacionProyecto $request, $id)
    {
       
        $this->validate($request, [
            'proyecto_id'    =>  'required',
          	'empleado_id'    =>  'required'
        ]);

       
       

        $data = ProyectoEjecutivo::find($id);
       
        $data->proyecto_id = $request->get('proyecto_id');
        $data->empleado_id = $request->get('empleado_id');
     
        $data->save();

       // Proyecto::findOrFail($id)->update($request->all());
        return redirect('admin/proyectoejecutivo')->with('mensaje', 'Asignacion actualizada con exito');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function eliminar(Request $request, $id)
    {
        if ($request->ajax()) {
            if (ProyectoEjecutivo::destroy($id)) {
                return response()->json(['mensaje' => 'ok']);
            } else {
                return response()->json(['mensaje' => 'ng']);
            }
        } else {
            abort(404);
        }
    }
}
