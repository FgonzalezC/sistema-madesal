<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ValidacionTipoCocina;
use App\Models\Admin\TipoCocina;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TipoCocinaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $datas = TipoCocina::orderBy('id')->get();
      return view('admin.tipococina.index', compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function crear()
    {
        return view('admin.tipococina.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function guardar(ValidacionTipoCocina $request)
    {
        $valor=TipoCocina::create($request->all());
        
       return redirect('admin/tipococina')->with('mensaje','Tipo Cocina creado con exito.');
    }

    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editar($id)
    {
        $data = TipoCocina::findOrFail($id);
        return view('admin.tipococina.editar', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function actualizar(ValidacionTipoCocina $request, $id)
    {
        TipoCocina::findOrFail($id)->update($request->all());
        return redirect('admin/tipococina')->with('mensaje', 'Tipo Cocina actualizado con exito');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function eliminar(Request $request, $id)
    {
        if ($request->ajax()) {
            if (TipoCocina::destroy($id)) {
                return response()->json(['mensaje' => 'ok']);
            } else {
                return response()->json(['mensaje' => 'ng']);
            }
        } else {
            abort(404);
        }
    }
}
