<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ValidacionTipoEtapa;
use App\Models\Admin\TipoEtapa;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TipoEtapaController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $datas = TipoEtapa::orderBy('id')->get();
      return view('admin.tipoetapa.index', compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function crear()
    {
        return view('admin.tipoetapa.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function guardar(ValidacionTipoEtapa $request)
    {
        $valor=TipoEtapa::create($request->all());
        
       return redirect('admin/tipoetapa')->with('mensaje','Tipo Etapa creado con exito.');
    }

    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editar($id)
    {
        $data = TipoEtapa::findOrFail($id);
        return view('admin.tipoetapa.editar', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function actualizar(ValidacionTipoEtapa $request, $id)
    {
        TipoEtapa::findOrFail($id)->update($request->all());
        return redirect('admin/tipoetapa')->with('mensaje', 'Tipo Etapa actualizado con exito');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function eliminar(Request $request, $id)
    {
        if ($request->ajax()) {
            if (TipoEtapa::destroy($id)) {
                return response()->json(['mensaje' => 'ok']);
            } else {
                return response()->json(['mensaje' => 'ng']);
            }
        } else {
            abort(404);
        }
    }
}
