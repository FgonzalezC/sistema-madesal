<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ValidacionFinanciamiento;
use App\Models\Admin\Financiamiento;
use Illuminate\Http\Request;

class FinanciamientoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $datas = Financiamiento::orderBy('id')->get();
      return view('admin.financiamiento.index', compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function crear()
    {

        return view('admin.financiamiento.crear');
    }

    /**
     * Store a newly created resource in storage
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function guardar(ValidacionFinanciamiento $request)
    {
       $this->validate($request, [
            'titulo'    =>  'required',
            'subtitulo'   =>  'required',
           	'titulo1'   =>  'required',
            'bajada1'   =>  'required',
            'titulo2'   =>  'required',
            'bajada2'   =>  'required',
            'titulo3'   =>  'required',
           	'bajada3'   =>  'required'
            
        ]);

        /* Validación para que la imagen sólo sea en estos formatos y campo requerido. Quitar gif si es necesario*/
        $this->validate($request, [
            'img'      => 'image|mimes:jpg,png,gif,jpeg|max:2048'
            
            
        ]);

        $financiamiento = new Financiamiento();
        $financiamiento->titulo = $request->input('titulo');
        $financiamiento->subtitulo = $request->input('subtitulo');
        $financiamiento->titulo1 = $request->input('titulo1');
        $financiamiento->bajada1 = $request->input('bajada1');
        $financiamiento->titulo2 = $request->input('titulo2');
        $financiamiento->bajada2 = $request->input('bajada2');
        $financiamiento->titulo3 = $request->input('titulo3');
        $financiamiento->bajada3 = $request->input('bajada3');
       

        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
         if (request()->hasFile('img')){ 
        $image = $request->file('img');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $financiamiento->img = $new_name;
        }
        else
        {
            $financiamiento->img = 'Sin imagen';
        } 
       
        $financiamiento->save(); /* save en db al objeto con los valores*/

     //  Proyecto::create($request->all());
        return redirect('admin/financiamiento')->with('mensaje','Seccion Financiamiento creado con exito.');
    }

    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editar($id)
    {
        $data = Financiamiento::findOrFail($id);
        return view('admin.financiamiento.editar', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function actualizar(ValidacionFinanciamiento $request, $id)
    {
       
       $this->validate($request, [
            'titulo'    =>  'required',
            'subtitulo'   =>  'required',
           	'titulo1'   =>  'required',
            'bajada1'   =>  'required',
            'titulo2'   =>  'required',
            'bajada2'   =>  'required',
            'titulo3'   =>  'required',
           	'bajada3'   =>  'required'
            
        ]);

        /* Validación para que la imagen sólo sea en estos formatos y campo requerido. Quitar gif si es necesario*/
        $this->validate($request, [
            'img'      => 'image|mimes:jpg,png,gif,jpeg|max:2048'
            
            
        ]);


        $data = Financiamiento::find($id);
        $data->titulo = $request->get('titulo');
        $data->subtitulo = $request->get('subtitulo');
        $data->titulo1 = $request->get('titulo1');
        $data->bajada1 = $request->get('bajada1');
        $data->titulo2 = $request->get('titulo2');
        $data->bajada2 = $request->get('bajada2');
        $data->titulo3 = $request->get('titulo3');
        $data->bajada3 = $request->get('bajada3');
        

        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
         if (request()->hasFile('img')){ 
        $image = $request->file('img');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $data->img = $new_name;
        }
       
        $data->save();

       // Proyecto::findOrFail($id)->update($request->all());
        return redirect('admin/financiamiento')->with('mensaje', 'Seccion Financiamiento actualizado con exito');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function eliminar(Request $request, $id)
    {
        if ($request->ajax()) {
            if (Financiamiento::destroy($id)) {
                return response()->json(['mensaje' => 'ok']);
            } else {
                return response()->json(['mensaje' => 'ng']);
            }
        } else {
            abort(404);
        }
    }
}
