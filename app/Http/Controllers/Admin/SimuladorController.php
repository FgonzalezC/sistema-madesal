<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\Simulador;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SimuladorController extends Controller
{
   public function index(Request $request)
    {
       $datas = Simulador::orderBy('id')->get();
      return view('admin.simulador.index', compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function crear()
    {

        return view('admin.simulador.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function guardar(Request $request)
    {
       $this->validate($request, [
            'nombre'    =>  'required'     
           
            
           
        ]);
        $this->validate($request, [
            'img'      => 'image|mimes:jpg,png,gif,jpeg|max:2048'
            
        ]);
   

    

        $simulador = new Simulador();
        $simulador->nombre = $request->input('nombre');

        if (request()->hasFile('img')){ 
            $image = $request->file('img');
            $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
            $image->move(public_path('/storage'), $new_name);
            $simulador->img = $new_name;
            }
            else
            {
                $simulador->img = 'Sin img';
            } 

        
        $simulador->save();

     
        return redirect('admin/simulador')->with('mensaje','Seccion Simulador creada con exito.');
    }

    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editar($id)
    {
        $data = Simulador::findOrFail($id);
        return view('admin.simulador.editar', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function actualizar(Request $request, $id)
    {
       
        $this->validate($request, [
            'nombre'    =>  'required'
            
        ]);

        $this->validate($request, [
            'img'      => 'image|mimes:jpg,png,gif,jpeg|max:2048'
            
        ]);

      

        $data = Simulador::find($id);
        $data->nombre = $request->input('nombre');

        if (request()->hasFile('img')){ 
            $image = $request->file('img');
            $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
            $image->move(public_path('/storage'), $new_name);
            $data->img = $new_name;
            }
            else
            {
                $data->img = 'Sin imagen';
            } 

       
      
        $data->save();

        return redirect('admin/simulador')->with('mensaje', 'Seccion Simulador actualizado con exito');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function eliminar(Request $request, $id)
    {
        if ($request->ajax()) {
            if (Simulador::destroy($id)) {
                return response()->json(['mensaje' => 'ok']);
            } else {
                return response()->json(['mensaje' => 'ng']);
            }
        } else {
            abort(404);
        }
    }
}
