<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\SaladeVenta;
use Illuminate\Http\Request;

class SaladeVentaController extends Controller
{
     public function index(Request $request)
    {
       $datas = SaladeVenta::orderBy('id')->get();
      return view('admin.saladeventa.index', compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function crear()
    {

        return view('admin.saladeventa.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function guardar(Request $request)
    {
       $this->validate($request, [
            'nombre'    =>  'required',      
            'direccion'      =>  'required',      
            'telefono'      =>  'required',              
            'mail'      =>  'required',   
            'horario'      =>  'required',      
            'mapa'      =>  'required',
            
           
        ]);


        $saladeventa = new SaladeVenta();
        $saladeventa->nombre = $request->input('nombre');
		$saladeventa->direccion = $request->input('direccion');
		$saladeventa->telefono = $request->input('telefono');
        $saladeventa->telefono2 = $request->input('telefono2');
		$saladeventa->mail = $request->input('mail');
        $saladeventa->mail2 = $request->input('mail2');
		$saladeventa->horario = $request->input('horario');
		$saladeventa->mapa = $request->input('mapa');
        $saladeventa->save();

     
        return redirect('admin/saladeventa')->with('mensaje','Seccion Sala de Ventas creada con exito.');
    }

    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editar($id)
    {
        $data = SaladeVenta::findOrFail($id);
        return view('admin.saladeventa.editar', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function actualizar(Request $request, $id)
    {
       
      $this->validate($request, [
            'nombre'    =>  'required',      
            'direccion'      =>  'required',      
            'telefono'      =>  'required',      
            'mail'      =>  'required',      
            'horario'      =>  'required',      
            'mapa'      =>  'required',
            
           
        ]);
      

        $data = SaladeVenta::find($id);
        $data->nombre = $request->input('nombre');
        $data->direccion = $request->input('direccion');
        $data->telefono = $request->input('telefono');
        $data->telefono2 = $request->input('telefono2');

        $data->mail = $request->input('mail');
        $data->mail2 = $request->input('mail2');
        $data->horario = $request->input('horario');
        $data->mapa = $request->input('mapa');

      
        $data->save();

        return redirect('admin/saladeventa')->with('mensaje', 'Seccion Sala de Venta actualizado con exito');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function eliminar(Request $request, $id)
    {
        if ($request->ajax()) {
            if (SaladeVenta::destroy($id)) {
                return response()->json(['mensaje' => 'ok']);
            } else {
                return response()->json(['mensaje' => 'ng']);
            }
        } else {
            abort(404);
        }
    }
}
