<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ValidacionProyecto;
use App\Models\Admin\Proyecto;
use App\Models\Admin\Modelo;

use Illuminate\Http\Request;
use Excel;


class ProyectoController extends Controller
{
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   public function index()
   {
    $datas = Proyecto::orderBy('orden')->paginate(25);




    return view('admin.proyecto.index', compact('datas'));
}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function crear()
    {

        return view('admin.proyecto.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function guardar(ValidacionProyecto $request)
    {
       $this->validate($request, [
        'nombre_proyecto'       =>  'required',
        'orden'                 =>  'numeric',
        'dormitorios'           =>  'required',
        'banos'                 =>  'required',
        'estacionamiento'       =>  'required',
        'superficie'            =>  'required',
        'precio'                =>  'required|numeric',
        'descripcion_slider'    =>  'required'
    ]);

       /* Validación para que la imagen sólo sea en estos formatos y campo requerido. Quitar gif si es necesario*/
       $this->validate($request, [
        'archivo'  => 'mimes:pdf|max:2048',
        'img_home'      => 'image|mimes:jpg,png,gif,jpeg|max:2048',
        'masterplan'      => 'image|mimes:jpg,png,gif,jpeg|max:2048',
        'desktopimg1'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
        'desktopimg2'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
        'desktopimg3'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
        'desktopimg4'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
        'desktopimg5'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
        'movilimg1'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
        'movilimg2'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
        'movilimg3'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
        'movilimg4'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
        'movilimg5'  => 'image|mimes:jpg,png,gif,jpeg|max:2048'
    ]);

       //dd($request->input('nombre_proyecto'));
       $var = false;
       switch ($request->input('destacado')) {
        case '0':
        $var = false;
        break;
        case '1':
        $var=true;
        break;
    }

    $var2 = false;
    switch ($request->input('destacado2')) {
        case '0':
        $var2 = false;
        break;
        case '1':
        $var2=true;
        break;
    }

        $proyecto = new Proyecto();
        $proyecto->nombre_proyecto = $request->input('nombre_proyecto');
        $proyecto->orden = $request->input('orden');
        if (request()->hasFile('archivo')){ 
            $image = $request->file('archivo');
            $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
            $image->move(public_path('/storage/FichasProyectoPDF'), $new_name);
            $proyecto->archivo = $new_name;
        }
        $proyecto->tipoetapa_id = $request->input('tipoetapa_id');
        $proyecto->tipoinmueble_id = $request->input('tipoinmueble_id');
        $proyecto->dormitorios = $request->input('dormitorios');
        $proyecto->banos = $request->input('banos');
        $proyecto->estacionamiento = $request->input('estacionamiento');
        $proyecto->valor_estacionamiento = $request->input('valor_estacionamiento');
        $proyecto->destacado = $var;
        $proyecto->destacado2 = $var2;
        $proyecto->superficie = $request->input('superficie');
        $proyecto->precio = $request->input('precio');

        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
         if (request()->hasFile('img_home')){ 
        $image = $request->file('img_home');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $proyecto->img_home = $new_name;
    }

    /*----*/
    /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
    if (request()->hasFile('masterplan')){ 
        $image = $request->file('masterplan');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $proyecto->masterplan = $new_name;
    }

    /*----*/

    /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
    if (request()->hasFile('desktopimg1')){ 
        $image = $request->file('desktopimg1');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $proyecto->desktopimg1 = $new_name;
    }


    /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
    if (request()->hasFile('desktopimg2')){ 
        $image = $request->file('desktopimg2');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $proyecto->desktopimg2 = $new_name;
    }


    /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
    if (request()->hasFile('desktopimg3')){ 
        $image = $request->file('desktopimg3');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $proyecto->desktopimg3 = $new_name;
    }


    /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
    if (request()->hasFile('desktopimg4')){ 
        $image = $request->file('desktopimg4');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $proyecto->desktopimg4 = $new_name;
    }

    /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
    if (request()->hasFile('desktopimg5')){ 
        $image = $request->file('desktopimg5');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $proyecto->desktopimg5 = $new_name;
    }


    if (request()->hasFile('movilimg1')){ 
        $image = $request->file('movilimg1');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $proyecto->movilimg1 = $new_name;
    }


    /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
    if (request()->hasFile('movilimg2')){ 
        $image = $request->file('movilimg2');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $proyecto->movilimg2 = $new_name;
    }


    /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
    if (request()->hasFile('movilimg3')){ 
        $image = $request->file('movilimg3');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $proyecto->movilimg3 = $new_name;
    }


    /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
    if (request()->hasFile('movilimg4')){ 
        $image = $request->file('movilimg4');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $proyecto->movilimg4 = $new_name;
    }


    /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
    if (request()->hasFile('movilimg5')){ 
        $image = $request->file('movilimg5');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $proyecto->movilimg5 = $new_name;
    }



    $proyecto->descripcion_slider = $request->input('descripcion_slider');
    $proyecto->comuna_id = $request->input('comuna_id');
    $proyecto->estado_id = $request->input('estado_id');
    $proyecto->subsidio_id = $request->input('subsidio_id');
    $proyecto->salaventas_id = $request->input('salaventas_id');


    $proyecto->save(); /* save en db al objeto con los valores*/

     //  Proyecto::create($request->all());
    return redirect('admin/proyecto')->with('mensaje','Proyecto creado con exito.');
}


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editar($id)
    {
        $data = Proyecto::findOrFail($id);

        $modelos = \DB::table('modelos')
        ->select('modelos.id',
            'tipo_etapas.nombre_etapa',
            'proyectos.nombre_proyecto',
            'modelos.nombre_modelo',
            'modelos.stock',
            'modelos.precio_desde',
            'modelos.img_fichaproyecto',
            'modelos.slider1',
            'modelos.slider2',
            'modelos.slider3',
            'modelos.slider4',
            'modelos.slider5',
            'modelos.slidermovil1',
            'modelos.slidermovil2',
            'modelos.slidermovil3',
            'modelos.slidermovil4',
            'modelos.slidermovil5',
            'modelos.bajada',
            'modelos.mtrs',
            'modelos.tipoinmueble_id',
            'modelos.banos',
            'modelos.dormitorio',
            'modelos.tipococina_id',
            'modelos.estacionamiento',
            'modelos.galeria',
            'modelos.terminaciones',
            'modelos.equipamiento',
            'modelos.ubicacion',
            'modelos.gimg1',
            'modelos.gimg2',
            'modelos.gimg3',
            'modelos.gimg4',
            'modelos.gimg5',
            'modelos.gimg6',
            'modelos.gimg7',
            'modelos.gimg8',
            'modelos.gimg9',
            'modelos.gimg10',
            'modelos.timg1',
            'modelos.timg2',
            'modelos.timg3',
            'modelos.timg4',
            'modelos.timg5',
            'modelos.timg6',
            'modelos.timg7',
            'modelos.timg8',
            'modelos.timg9',
            'modelos.timg10',
            'modelos.eimg1',
            'modelos.eimg2',
            'modelos.eimg3',
            'modelos.eimg4',
            'modelos.eimg5',
            'modelos.eimg6',
            'modelos.eimg7',
            'modelos.eimg8',
            'modelos.eimg9',
            'modelos.eimg10',
            'modelos.uimg1',
            'modelos.uimg2',
            'modelos.uimg3',
            'modelos.uimg4',
            'modelos.uimg5',
            'modelos.uimg6',
            'modelos.uimg7',
            'modelos.uimg8',
            'modelos.uimg9',
            'modelos.uimg10',
            'modelos.vt1',
            'modelos.vu1',
            'modelos.ve1',
            'modelos.ve1',
            'modelos.tour360',
            'modelos.proyecto_id'
            )
            ->join('proyectos','proyectos.id','modelos.proyecto_id')
            ->join('tipo_etapas','proyectos.tipoetapa_id','tipo_etapas.id')
            ->where('proyectos.id',$id)
            ->get();
           return view('admin.proyecto.editar', compact('data','modelos'));
        }

        /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
        public function actualizar(ValidacionProyecto $request, $id)
        {

            $this->validate($request, [
            'nombre_proyecto'    =>  'required',
           // 'tipoetapa_id'    =>  'required',
           // 'tipo_inmueble'     =>  'required',
            'dormitorios'      =>  'required',
            'banos'      =>  'required',
            'estacionamiento'      =>  'required',
            'superficie'      =>  'required',
            'precio'      =>  'required|numeric',
            'descripcion_slider'      =>  'required'
            //'comuna->nombre_comuna'      =>  'required'
            ]);

        $this->validate($request, [
            'archivo'  => 'mimes:pdf|max:2048',
            'img_home'      => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'masterplan'      => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'desktopimg1'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'desktopimg2'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'desktopimg3'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'desktopimg4'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'desktopimg5'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'movilimg1'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'movilimg2'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'movilimg3'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'movilimg4'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'movilimg5'  => 'image|mimes:jpg,png,gif,jpeg|max:2048'
            ]);

            $var = false;
            switch ($request->get('destacado')) {
                case '0':
                $var = false;
                break;
                case '1':
                $var=true;
                break;
            }
            $var2 = false;

            switch ($request->get('destacado2')) {
                case '0':
                $var2 = false;
                break;
                case '1':
                $var2=true;
                break;
            }

        $data = Proyecto::find($id);
        $data->nombre_proyecto = $request->get('nombre_proyecto');
        $data->orden = $request->get('orden');
        if (request()->hasFile('archivo')){ 
            $image = $request->file('archivo');
            $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
            $image->move(public_path('/storage/FichasProyectoPDF'), $new_name);
            $data->archivo = $new_name;
        }
        $data->tipoetapa_id = $request->get('tipoetapa_id');
        $data->tipoinmueble_id = $request->get('tipoinmueble_id');
        $data->dormitorios = $request->get('dormitorios');
        $data->banos = $request->get('banos');
        $data->estacionamiento = $request->get('estacionamiento');
        $data->valor_estacionamiento = $request->get('valor_estacionamiento');
        $data->destacado = $var;
        $data->destacado2 = $var2;
        $data->superficie = $request->get('superficie');
        $data->precio = $request->get('precio');

        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
         if (request()->hasFile('img_home')){ 
        $image = $request->file('img_home');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $data->img_home = $new_name;
        }
        /*------*/
        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
         if (request()->hasFile('masterplan')){ 
        $image = $request->file('masterplan');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $data->masterplan = $new_name;
        }
            /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
            if (request()->hasFile('desktopimg1')){ 
                $image = $request->file('desktopimg1');
                $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
                $image->move(public_path('/storage'), $new_name);
                $data->desktopimg1 = $new_name;
            }

            /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
            if (request()->hasFile('desktopimg2')){ 
                $image = $request->file('desktopimg2');
                $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
                $image->move(public_path('/storage'), $new_name);
                $data->desktopimg2 = $new_name;
            }

            /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
            if (request()->hasFile('desktopimg3')){ 
                $image = $request->file('desktopimg3');
                $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
                $image->move(public_path('/storage'), $new_name);
                $data->desktopimg3 = $new_name;
            }

            /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
            if (request()->hasFile('desktopimg4')){ 
                $image = $request->file('desktopimg4');
                $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
                $image->move(public_path('/storage'), $new_name);
                $data->desktopimg4 = $new_name;
            }

            /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
            if (request()->hasFile('desktopimg5')){ 
                $image = $request->file('desktopimg5');
                $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
                $image->move(public_path('/storage'), $new_name);
                $data->desktopimg5 = $new_name;
            }

            if (request()->hasFile('movilimg1')){ 
                $image = $request->file('movilimg1');
                $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
                $image->move(public_path('/storage'), $new_name);
                $data->movilimg1 = $new_name;
            }

            /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
            if (request()->hasFile('movilimg2')){ 
                $image = $request->file('movilimg2');
                $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
                $image->move(public_path('/storage'), $new_name);
                $data->movilimg2 = $new_name;
            }

            /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
            if (request()->hasFile('movilimg3')){ 
                $image = $request->file('movilimg3');
                $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
                $image->move(public_path('/storage'), $new_name);
                $data->movilimg3 = $new_name;
            }

            /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
            if (request()->hasFile('movilimg4')){ 
                $image = $request->file('movilimg4');
                $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
                $image->move(public_path('/storage'), $new_name);
                $data->movilimg4 = $new_name;
            }

            /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
            if (request()->hasFile('movilimg5')){ 
                $image = $request->file('movilimg5');
                $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
                $image->move(public_path('/storage'), $new_name);
                $data->movilimg5 = $new_name;
            }

            $data->descripcion_slider = $request->get('descripcion_slider');
            $data->comuna_id = $request->get('comuna_id');
            $data->estado_id = $request->get('estado_id');
            $data->subsidio_id = $request->get('subsidio_id');
            $data->salaventas_id = $request->input('salaventas_id');
            $data->save();

       // Proyecto::findOrFail($id)->update($request->all());
           // return redirect('admin/proyecto')->with('mensaje', 'Proyecto actualizado con exito');
                return redirect()->back()->with('mensaje', 'Proyecto actualizado con exito');


        }
        /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
        public function eliminar(Request $request, $id)
        {
            if ($request->ajax()) {
                if (Proyecto::destroy($id)) {
                    return response()->json(['mensaje' => 'ok']);
                    } else {
                        return response()->json(['mensaje' => 'ng']);
                    }
                    } else {
                        abort(404);
                    }
                }
        public function eliminar_fotoproyecto(Request $request,$id,$dato){


        $data = Proyecto::findOrFail($id);

        /********************slider1**************************************/

        if($data->img_home==$dato){


         \DB::table('proyectos')
         ->where('id', $id)  
         ->update(['img_home'=>NULL]);

         $url=public_path('/storage/'.$dato);

         unlink($url);
         return redirect()->route('editar_proyecto', ['id' => $id]);

     }

      if($data->masterplan==$dato){


         \DB::table('proyectos')
         ->where('id', $id)  
         ->update(['masterplan'=>NULL]);

         $url=public_path('/storage/'.$dato);

         unlink($url);
         return redirect()->route('editar_proyecto', ['id' => $id]);


    }

      if($data->desktopimg1==$dato){


         \DB::table('proyectos')
         ->where('id', $id)  
         ->update(['desktopimg1'=>NULL]);

         $url=public_path('/storage/'.$dato);

         unlink($url);
        return redirect()->route('editar_proyecto', ['id' => $id]);



     }
        if($data->desktopimg2==$dato){


         \DB::table('proyectos')
         ->where('id', $id)  
         ->update(['desktopimg2'=>NULL]);

         $url=public_path('/storage/'.$dato);

         unlink($url);
        return redirect()->route('editar_proyecto', ['id' => $id]);



     }
     if($data->desktopimg3==$dato){


         \DB::table('proyectos')
         ->where('id', $id)  
         ->update(['desktopimg3'=>NULL]);

         $url=public_path('/storage/'.$dato);

         unlink($url);
        return redirect()->route('editar_proyecto', ['id' => $id]);



     }
     if($data->desktopimg4==$dato){


         \DB::table('proyectos')
         ->where('id', $id)  
         ->update(['desktopimg4'=>NULL]);

         $url=public_path('/storage/'.$dato);

         unlink($url);
        return redirect()->route('editar_proyecto', ['id' => $id]);



     }
     if($data->desktopimg5==$dato){


         \DB::table('proyectos')
         ->where('id', $id)  
         ->update(['desktopimg5'=>NULL]);

         $url=public_path('/storage/'.$dato);

         unlink($url);
        return redirect()->route('editar_proyecto', ['id' => $id]);



     }if($data->movilimg1==$dato){


         \DB::table('proyectos')
         ->where('id', $id)  
         ->update(['movilimg1'=>NULL]);

         $url=public_path('/storage/'.$dato);

         unlink($url);
        return redirect()->route('editar_proyecto', ['id' => $id]);



     }
     if($data->movilimg2==$dato){


         \DB::table('proyectos')
         ->where('id', $id)  
         ->update(['movilimg2'=>NULL]);

         $url=public_path('/storage/'.$dato);

         unlink($url);
        return redirect()->route('editar_proyecto', ['id' => $id]);



     }
     if($data->movilimg3==$dato){


         \DB::table('proyectos')
         ->where('id', $id)  
         ->update(['movilimg3'=>NULL]);

         $url=public_path('/storage/'.$dato);

         unlink($url);
        return redirect()->route('editar_proyecto', ['id' => $id]);



     }
     if($data->movilimg4==$dato){


         \DB::table('proyectos')
         ->where('id', $id)  
         ->update(['movilimg4'=>NULL]);

         $url=public_path('/storage/'.$dato);

         unlink($url);
        return redirect()->route('editar_proyecto', ['id' => $id]);



     }
     if($data->movilimg5==$dato){


         \DB::table('proyectos')
         ->where('id', $id)  
         ->update(['movilimg5'=>NULL]);

         $url=public_path('/storage/'.$dato);

         unlink($url);
        return redirect()->route('editar_proyecto', ['id' => $id]);



     }
     

}






                public function import(Request $request){

                    \Excel::load($request->excel_proyecto, function($reader) {

                        $excel_proyecto = $reader->get();

            // iteracción
                        $reader->each(function($row) {

                            $proyecto = new Proyecto();
                            $proyecto->nombre_proyecto=$row->nombre_proyecto;
                            $proyecto->tipoetapa_id=$row->tipoetapa_id;
                            $proyecto->tipoinmueble_id=$row->tipoinmueble_id;
                            $proyecto->dormitorios=$row->dormitorios;
                            $proyecto->banos=$row->banos;
                            $proyecto->estacionamiento=$row->estacionamiento;
                            $proyecto->destacado=$row->destacado;
                            $proyecto->destacado2=$row->destacado2;
                            $proyecto->superficie=$row->superficie;
                            $proyecto->precio=$row->precio;
                            $proyecto->img_home=$row->img_home;
                            $proyecto->masterplan=$row->masterplan;
                            $proyecto->desktopimg1=$row->desktopimg1;
                            $proyecto->desktopimg2=$row->desktopimg2;
                            $proyecto->desktopimg3=$row->desktopimg3;
                            $proyecto->desktopimg4=$row->desktopimg4;
                            $proyecto->desktopimg5=$row->desktopimg5;
                            $proyecto->movilimg1=$row->movilimg1;
                            $proyecto->movilimg2=$row->movilimg2;
                            $proyecto->movilimg3=$row->movilimg3;
                            $proyecto->movilimg4=$row->movilimg4;
                            $proyecto->movilimg5=$row->movilimg5;
                            $proyecto->descripcion_slider=$row->descripcion_slider;
                            $proyecto->comuna_id=$row->comuna_id;
                            $proyecto->estado_id=$row->estado_id;
                            $proyecto->subsidio_id=$row->subsidio_id;
                            $proyecto->salaventas_id = $row->salaventas_id;
            //dd($proyecto);
                            $proyecto->save();


                            });

                            });

                            return redirect()->intended('admin/proyecto');
                        }
                    }
