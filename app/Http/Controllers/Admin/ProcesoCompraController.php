<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ValidacionProcesoCompra;
use App\Models\Admin\ProcesoCompra;
use Illuminate\Http\Request;

class ProcesoCompraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $datas = ProcesoCompra::orderBy('id')->get();
      return view('admin.procesocompra.index', compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function crear()
    {

        return view('admin.procesocompra.crear');
    }

    /**
     * Store a newly created resource in storage
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function guardar(ValidacionProcesoCompra $request)
    {
       $this->validate($request, [
            'titulo'    =>  'required',
            'titulo1'   =>  'required',
           	'bajada1'   =>  'required',
            'titulo2'   =>  'required',
            'bajada2'   =>  'required',
            'titulo3'   =>  'required',
            'bajada3'   =>  'required',
           	'titulo4'   =>  'required',
            'bajada4'   =>  'required',
            'titulo5'   =>  'required',
            'bajada5'   =>  'required',
            'titulodoc'   =>  'required',
            'bajadadoc'   =>  'required',
        ]);

        /* Validación para que la imagen sólo sea en estos formatos y campo requerido. Quitar gif si es necesario*/
        $this->validate($request, [
            'img1'      => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'imgdoc'      => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'img2'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'img3'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'img4'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'img5'  => 'image|mimes:jpg,png,gif,jpeg|max:2048'
            
        ]);

        $procesocompra = new ProcesoCompra();
        $procesocompra->titulo = $request->input('titulo');
        $procesocompra->titulo1 = $request->input('titulo1');
        $procesocompra->bajada1 = $request->input('bajada1');
        $procesocompra->titulodoc = $request->input('titulodoc');
        $procesocompra->bajadadoc = $request->input('bajadadoc');
        $procesocompra->titulo2 = $request->input('titulo2');
        $procesocompra->bajada2 = $request->input('bajada2');
        $procesocompra->titulo3 = $request->input('titulo3');
        $procesocompra->bajada3 = $request->input('bajada3');
        $procesocompra->titulo4 = $request->input('titulo4');
        $procesocompra->bajada4 = $request->input('bajada4');
        $procesocompra->titulo5 = $request->input('titulo5');
        $procesocompra->bajada5 = $request->input('bajada5');

        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
         if (request()->hasFile('img1')){ 
        $image = $request->file('img1');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $procesocompra->img1 = $new_name;
        }
        else
        {
            $procesocompra->img1 = 'Sin imagen';
        } 
        /*----*/
        if (request()->hasFile('imgdoc')){ 
        $image = $request->file('imgdoc');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $procesocompra->imgdoc = $new_name;
        }
        else
        {
            $procesocompra->imgdoc = 'Sin imagen';
        } 
        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
         if (request()->hasFile('img2')){ 
        $image = $request->file('img2');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $procesocompra->img2 = $new_name;
        }
        else
        {
            $procesocompra->img2 = 'Sin imagen';
        } 

        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
         if (request()->hasFile('img3')){ 
        $image = $request->file('img3');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $procesocompra->img3 = $new_name;
        }
        else
        {
            $procesocompra->img3 = 'Sin imagen';
        }

        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
         if (request()->hasFile('img4')){ 
        $image = $request->file('img4');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $procesocompra->img4 = $new_name;
        }
        else
        {
            $procesocompra->img4 = 'Sin imagen';
        }

        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
         if (request()->hasFile('img5')){ 
        $image = $request->file('img5');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $procesocompra->img5 = $new_name;
        }
        else
        {
            $procesocompra->img5 = 'Sin imagen';
        }
        $procesocompra->save(); /* save en db al objeto con los valores*/

     //  Proyecto::create($request->all());
        return redirect('admin/procesocompra')->with('mensaje','Proceso Compra creado con exito.');
    }

    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editar($id)
    {
        $data = ProcesoCompra::findOrFail($id);
        return view('admin.procesocompra.editar', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function actualizar(ValidacionProcesoCompra $request, $id)
    {
       
        $this->validate($request, [
            'titulo'    =>  'required',
            'titulo1'   =>  'required',
           	'bajada1'   =>  'required',
            'titulodoc'   =>  'required',
            'bajadadoc'   =>  'required',
            'titulo2'   =>  'required',
            'bajada2'   =>  'required',
            'titulo3'   =>  'required',
            'bajada3'   =>  'required',
           	'titulo4'   =>  'required',
            'bajada4'   =>  'required',
            'titulo5'   =>  'required',
            'bajada5'   =>  'required'
        ]);

        /* Validación para que la imagen sólo sea en estos formatos y campo requerido. Quitar gif si es necesario*/
        $this->validate($request, [
            'img1'      => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'imgdoc'      => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'img2'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'img3'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'img4'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'img5'  => 'image|mimes:jpg,png,gif,jpeg|max:2048'
            
        ]);


        $data = ProcesoCompra::find($id);
        $data->titulo = $request->get('titulo');
        $data->titulo1 = $request->get('titulo1');
        $data->bajada1 = $request->get('bajada1');
        $data->titulodoc = $request->get('titulodoc');
        $data->bajadadoc = $request->get('bajadadoc');
        $data->titulo2 = $request->get('titulo2');
        $data->bajada2 = $request->get('bajada2');
        $data->titulo3 = $request->get('titulo3');
        $data->bajada3 = $request->get('bajada3');
        $data->titulo4 = $request->get('titulo4');
        $data->bajada4 = $request->get('bajada4');
        $data->titulo5 = $request->get('titulo5');
        $data->bajada5 = $request->get('bajada5');

        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
         if (request()->hasFile('img1')){ 
        $image = $request->file('img1');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $data->img1 = $new_name;
        }
        /*------*/
        if (request()->hasFile('imgdoc')){ 
        $image = $request->file('imgdoc');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $data->imgdoc = $new_name;
        }
        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
         if (request()->hasFile('img2')){ 
        $image = $request->file('img2');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $data->img2 = $new_name;
        }

        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
         if (request()->hasFile('img3')){ 
        $image = $request->file('img3');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $data->img3 = $new_name;
        }

        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
         if (request()->hasFile('img4')){ 
        $image = $request->file('img4');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $data->img4 = $new_name;
        }

        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
         if (request()->hasFile('img5')){ 
        $image = $request->file('img5');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $data->img5 = $new_name;
        }
        $data->save();

       // Proyecto::findOrFail($id)->update($request->all());
        return redirect('admin/procesocompra')->with('mensaje', 'Proceso Compra actualizado con exito');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function eliminar(Request $request, $id)
    {
        if ($request->ajax()) {
            if (ProcesoCompra::destroy($id)) {
                return response()->json(['mensaje' => 'ok']);
            } else {
                return response()->json(['mensaje' => 'ng']);
            }
        } else {
            abort(404);
        }
    }
}
