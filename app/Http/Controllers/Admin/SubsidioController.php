<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ValidacionSubsidio;
use App\Http\Controllers\Controller;
use App\Models\Admin\Subsidio;
use Illuminate\Http\Request;

class SubsidioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $datas = Subsidio::orderBy('id')->get();
      return view('admin.subsidio.index', compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function crear()
    {
        return view('admin.subsidio.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function guardar(ValidacionSubsidio $request)
    {
         $this->validate($request, [
            'nombre_subsidio'    =>  'required',
        ]);
         
        $subsidio = new Subsidio();
        $subsidio->nombre_subsidio = $request->input('nombre_subsidio');
        $subsidio->save();
      //  Subsidio::create($request->all()); 
        return redirect('admin/subsidio')->with('mensaje','Subsidio creado con exito.');
    }

    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editar($id)
    {
        $data = Subsidio::findOrFail($id);
        return view('admin.subsidio.editar', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function actualizar(ValidacionSubsidio $request, $id)
    {
        Subsidio::findOrFail($id)->update($request->all());
        return redirect('admin/subsidio')->with('mensaje', 'Subsidio actualizado con exito');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function eliminar(Request $request, $id)
    {
        if ($request->ajax()) {
            if (Subsidio::destroy($id)) {
                return response()->json(['mensaje' => 'ok']);
            } else {
                return response()->json(['mensaje' => 'ng']);
            }
        } else {
            abort(404);
        }
    }
}
