<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ValidacionUnidad;
use App\Models\Admin\Unidad;
use Illuminate\Http\Request;

class UnidadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $datas = Unidad::orderBy('id')->get();
      return view('admin.unidad.index', compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function crear()
    {
        return view('admin.unidad.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function guardar(ValidacionUnidad $request)
    {
      $this->validate($request, [
            'nombre'    =>  'required',
            'precio'    =>  'required',
            'superficie_util'    =>  'required',
            'superf_areaverde'    =>  'required'

         
        ]);

        /* Validación para que la imagen sólo sea en estos formatos y campo requerido. Quitar gif si es necesario*/
        $this->validate($request, [
            'img_unidad'      => 'image|mimes:jpg,png,gif,jpeg|max:2048'
            
            
        ]);

        $unidad = new Unidad();
        $unidad->nombre = $request->input('nombre');

        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
         if (request()->hasFile('img_unidad')){ 
        $image = $request->file('img_unidad');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $unidad->img_unidad = $new_name;
        }
        else
        {
            $unidad->img_unidad = 'Sin imagen';
        } 
       
        $unidad->precio = $request->input('precio');
        $unidad->superficie_util = $request->input('superficie_util');
        $unidad->superf_areaverde = $request->input('superf_areaverde');
        $unidad->proyecto_id = $request->input('proyecto_id');
        $unidad->save(); /* save en db al objeto con los valores*/

     //  Proyecto::create($request->all());
        return redirect('admin/unidad')->with('mensaje','Unidad creado con exito.');
    }

    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editar($id)
    {
        $data = Unidad::findOrFail($id);
        return view('admin.unidad.editar', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function actualizar(ValidacionUnidad $request, $id)
    {

        $this->validate($request, [
            'nombre'    =>  'required',
            'precio'    =>  'required',
            'superficie_util'    =>  'required',
            'superf_areaverde'    =>  'required'

         
        ]);
        $this->validate($request, [
            'img_unidad'      => 'image|mimes:jpg,png,gif,jpeg|max:2048'
            
            
        ]);

        /* Validación para que la imagen sólo sea en estos formatos y campo requerido. Quitar gif si es necesario*/
   

        $unidad = Unidad::find($id);
        $unidad->nombre = $request->get('nombre');

       if (request()->hasFile('img_unidad')){ 
        $image = $request->file('img_unidad');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $unidad->img_unidad = $new_name;
        }
        else
        {
            $unidad->img_unidad = 'Sin imagen';
        } 
       
        $unidad->precio = $request->get('precio');
        $unidad->superficie_util = $request->get('superficie_util');
        $unidad->superf_areaverde = $request->get('superf_areaverde');
        $unidad->modelo_id = $request->get('modelo_id');
        $unidad->save(); /* save en db al objeto con los valores*/

     //  Proyecto::create($request->all());
      
        return redirect('admin/unidad')->with('mensaje', 'Unidad actualizada con exito');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function eliminar(Request $request, $id)
    {
        if ($request->ajax()) {
            if (Unidad::destroy($id)) {
                return response()->json(['mensaje' => 'ok']);
            } else {
                return response()->json(['mensaje' => 'ng']);
            }
        } else {
            abort(404);
        }
    }
}
