<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ValidacionModelo;
use App\Models\Admin\Modelo;
use App\Models\Admin\Planta;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Excel;

class ModeloController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
     {
      $datas = Modelo::orderBy('id')->paginate(10);
      return view('admin.modelo.index', compact('datas'));
      
  }
  
       public function index2($id)
     {
              $data = Modelo::findOrFail($id);


      return view('admin.modelo.listado', compact('data'));
  }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function crear()
    {
        return view('admin.modelo.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function guardar(ValidacionModelo $request)
    {
         $this->validate($request, [
            'nombre_modelo' =>'required', 
              
            'bajada' =>'required',      
            'mtrs'    =>  'required',
            'tipoinmueble_id'     =>  'required',
            'banos'      =>  'required',
            'dormitorio'      =>  'required',
            'tipococina_id'      =>  'required',
            'galeria'      =>  'required',
            'terminaciones'      =>  'required',
            'equipamiento'      =>  'required',
            'ubicacion'      =>  'required',
            'proyecto_id'      =>  'required'
           
        ]);

        /* Validación para que la imagen sólo sea en estos formatos y campo requerido. Quitar gif si es necesario*/
        $this->validate($request, [
            'img_fichaproyecto'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'gimg1'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'gimg2'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'gimg3'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'gimg4'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'gimg5'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'gimg6'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'gimg7'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'gimg8'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'gimg9'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'gimg10'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'archivo'  => 'mimes:pdf|max:6000',
            'timg1'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'timg2'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'timg3'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'timg4'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'timg5'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'timg6'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'timg7'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'timg8'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'timg9'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'timg10'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'eimg1'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'eimg2'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'eimg3'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'eimg4'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'eimg5'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'eimg6'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'eimg7'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'eimg8'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'eimg9'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'eimg10'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'uimg1'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'uimg2'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'uimg3'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'uimg4'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'uimg5'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'uimg6'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'uimg7'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'uimg8'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'uimg9'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'uimg10'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'slider1'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'slider2'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'slider3'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'slider4'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'slider5'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'slidermovil1'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'slidermovil2'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'slidermovil3'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'slidermovil4'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'slidermovil5'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',

        ]);
        $var = false;
         switch ($request->input('check_stock')) {
            case '0':
                $var = false;
                break;
            case '1':
                $var = true;
                break;
        }
        $var2 = false;
         switch ($request->input('check_tw')) {
            case '0':
                $var2 = false;
                break;
            case '1':
                $var2 = true;
                break;
        }
       
        /* Objeto SeccionHome vacío */
        $modelo = new Modelo();
        $modelo->nombre_modelo = $request->input('nombre_modelo');
        $modelo->precio_desde = $request->input('precio_desde');
        if (request()->hasFile('img_fichaproyecto')){ 
            $image = $request->file('img_fichaproyecto');
            $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
            $image->move(public_path('/storage'), $new_name);
            $modelo->img_fichaproyecto = $new_name;
        }
        if (request()->hasFile('archivo')){ 
            $image = $request->file('archivo');
            $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
            $image->move(public_path('/storage/FichasProyectoPDF'), $new_name);
            $modelo->archivo = $new_name;
        }
         /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
        if (request()->hasFile('slider1')){ 
            $image = $request->file('slider1');
            $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
            $image->move(public_path('/storage'), $new_name);
            $modelo->slider1 = $new_name;
        }
       

    /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
    if (request()->hasFile('slider2')){
        $image2 = $request->file('slider2');
        $new_name2 = date('dmY-Hisu') . '-' . $image2->getClientOriginalName();
        $image2->move(public_path('/storage'), $new_name2);
        $modelo->slider2 = $new_name2;
    }

    /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
    if (request()->hasFile('slider3')){
        $image3 = $request->file('slider3');
        $new_name3 = date('dmY-Hisu') . '-' . $image3->getClientOriginalName();
        $image3->move(public_path('/storage'), $new_name3);
        $modelo->slider3 = $new_name3;
    }

    /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
    if (request()->hasFile('slider4')){
        $image4 = $request->file('slider4');
        $new_name4 = date('dmY-Hisu') . '-' . $image4->getClientOriginalName();
        $image4->move(public_path('/storage'), $new_name4);
        $modelo->slider4 = $new_name4;
    }

    /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
    if (request()->hasFile('slider5')){
        $image5 = $request->file('slider5');
        $new_name5 = date('dmY-Hisu') . '-' . $image5->getClientOriginalName();
        $image5->move(public_path('/storage'), $new_name5);
        $modelo->slider5 = $new_name5;
    }


    /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
    if (request()->hasFile('slidermovil1')){ 
        $image = $request->file('slidermovil1');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $modelo->slidermovil1 = $new_name;
    }


    /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
    if (request()->hasFile('slidermovil2')){
        $image2 = $request->file('slidermovil2');
        $new_name2 = date('dmY-Hisu') . '-' . $image2->getClientOriginalName();
        $image2->move(public_path('/storage'), $new_name2);
        $modelo->slidermovil2 = $new_name2;
    }

    /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
    if (request()->hasFile('slidermovil3')){
        $image3 = $request->file('slidermovil3');
        $new_name3 = date('dmY-Hisu') . '-' . $image3->getClientOriginalName();
        $image3->move(public_path('/storage'), $new_name3);
        $modelo->slidermovil3 = $new_name3;
    }

    /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
    if (request()->hasFile('slidermovil4')){
        $image4 = $request->file('slidermovil4');
        $new_name4 = date('dmY-Hisu') . '-' . $image4->getClientOriginalName();
        $image4->move(public_path('/storage'), $new_name4);
        $modelo->slidermovil4 = $new_name4;
    }

    /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
    if (request()->hasFile('slidermovil5')){
        $image5 = $request->file('slidermovil5');
        $new_name5 = date('dmY-Hisu') . '-' . $image5->getClientOriginalName();
        $image5->move(public_path('/storage'), $new_name5);
        $modelo->slidermovil5 = $new_name5;
    }

   
        $modelo->stock = $request->input('stock');
        $modelo->check_stock = $var;
        $modelo->check_tw = $var2;
        $modelo->bajada = $request->input('bajada');
        $modelo->mtrs = $request->input('mtrs');
        $modelo->tipoinmueble_id = $request->input('tipoinmueble_id');
        $modelo->banos = $request->input('banos');
        $modelo->dormitorio = $request->input('dormitorio');
        $modelo->tipococina_id = $request->input('tipococina_id');
        $modelo->estacionamiento = $request->input('estacionamiento');
        $modelo->galeria = $request->input('galeria');
        $modelo->terminaciones = $request->input('terminaciones');
        $modelo->equipamiento = $request->input('equipamiento');
        $modelo->ubicacion = $request->input('ubicacion');
        


      if (request()->hasFile('gimg1')){ 
        $image = $request->file('gimg1');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $modelo->gimg1 = $new_name;
    }


    /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
    if (request()->hasFile('gimg2')){
        $image2 = $request->file('gimg2');
        $new_name2 = date('dmY-Hisu') . '-' . $image2->getClientOriginalName();
        $image2->move(public_path('/storage'), $new_name2);
        $modelo->gimg2 = $new_name2;
    }

    /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
    if (request()->hasFile('gimg3')){
        $image3 = $request->file('gimg3');
        $new_name3 = date('dmY-Hisu') . '-' . $image3->getClientOriginalName();
        $image3->move(public_path('/storage'), $new_name3);
        $modelo->gimg3 = $new_name3;
    }

    /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
    if (request()->hasFile('gimg4')){
        $image4 = $request->file('gimg4');
        $new_name4 = date('dmY-Hisu') . '-' . $image4->getClientOriginalName();
        $image4->move(public_path('/storage'), $new_name4);
        $modelo->gimg4 = $new_name4;
    }

    /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
    if (request()->hasFile('gimg5')){
        $image5 = $request->file('gimg5');
        $new_name5 = date('dmY-Hisu') . '-' . $image5->getClientOriginalName();
        $image5->move(public_path('/storage'), $new_name5);
        $modelo->gimg5 = $new_name5;
    }
    if (request()->hasFile('gimg6')){ 
        $image = $request->file('gimg6');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $modelo->gimg6 = $new_name;
    }


    /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
    if (request()->hasFile('gimg7')){
        $image2 = $request->file('gimg7');
        $new_name2 = date('dmY-Hisu') . '-' . $image2->getClientOriginalName();
        $image2->move(public_path('/storage'), $new_name2);
        $modelo->gimg7 = $new_name2;
    }

    /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
    if (request()->hasFile('gimg8')){
        $image3 = $request->file('gimg8');
        $new_name3 = date('dmY-Hisu') . '-' . $image3->getClientOriginalName();
        $image3->move(public_path('/storage'), $new_name3);
        $modelo->gimg8 = $new_name3;
    }

    /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
    if (request()->hasFile('gimg9')){
        $image4 = $request->file('gimg9');
        $new_name4 = date('dmY-Hisu') . '-' . $image4->getClientOriginalName();
        $image4->move(public_path('/storage'), $new_name4);
        $modelo->gimg9 = $new_name4;
    }

    /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
    if (request()->hasFile('gimg10')){
        $image5 = $request->file('gimg10');
        $new_name5 = date('dmY-Hisu') . '-' . $image5->getClientOriginalName();
        $image5->move(public_path('/storage'), $new_name5);
        $modelo->gimg10 = $new_name5;
    }

    /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
    if (request()->hasFile('timg1')){ 
        $image = $request->file('timg1');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $modelo->timg1 = $new_name;
    }


    /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
    if (request()->hasFile('timg2')){
        $image2 = $request->file('timg2');
        $new_name2 = date('dmY-Hisu') . '-' . $image2->getClientOriginalName();
        $image2->move(public_path('/storage'), $new_name2);
        $modelo->timg2 = $new_name2;
    }

    /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
    if (request()->hasFile('timg3')){
        $image3 = $request->file('timg3');
        $new_name3 = date('dmY-Hisu') . '-' . $image3->getClientOriginalName();
        $image3->move(public_path('/storage'), $new_name3);
        $modelo->timg3 = $new_name3;
    }

    /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
    if (request()->hasFile('timg4')){
        $image4 = $request->file('timg4');
        $new_name4 = date('dmY-Hisu') . '-' . $image4->getClientOriginalName();
        $image4->move(public_path('/storage'), $new_name4);
        $modelo->timg4 = $new_name4;
    }

    /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
    if (request()->hasFile('timg5')){
        $image5 = $request->file('timg5');
        $new_name5 = date('dmY-Hisu') . '-' . $image5->getClientOriginalName();
        $image5->move(public_path('/storage'), $new_name5);
        $modelo->timg5 = $new_name5;
    }
    if (request()->hasFile('timg6')){ 
        $image = $request->file('timg6');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $modelo->timg6 = $new_name;
    }


    /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
    if (request()->hasFile('timg7')){
        $image2 = $request->file('timg7');
        $new_name2 = date('dmY-Hisu') . '-' . $image2->getClientOriginalName();
        $image2->move(public_path('/storage'), $new_name2);
        $modelo->timg7 = $new_name2;
    }

    /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
    if (request()->hasFile('timg8')){
        $image3 = $request->file('timg8');
        $new_name3 = date('dmY-Hisu') . '-' . $image3->getClientOriginalName();
        $image3->move(public_path('/storage'), $new_name3);
        $modelo->timg8 = $new_name3;
    }

    /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
    if (request()->hasFile('timg9')){
        $image4 = $request->file('timg9');
        $new_name4 = date('dmY-Hisu') . '-' . $image4->getClientOriginalName();
        $image4->move(public_path('/storage'), $new_name4);
        $modelo->timg9 = $new_name4;
    }

    /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
    if (request()->hasFile('timg10')){
        $image5 = $request->file('timg10');
        $new_name5 = date('dmY-Hisu') . '-' . $image5->getClientOriginalName();
        $image5->move(public_path('/storage'), $new_name5);
        $modelo->timg10 = $new_name5;
    }


    if (request()->hasFile('eimg1')){
        $image1 = $request->file('eimg1');
        $new_name1 = date('dmY-Hisu') . '-' . $image1->getClientOriginalName();
        $image1->move(public_path('/storage'), $new_name1);
        $modelo->eimg1 = $new_name1;
    }

    /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
    if (request()->hasFile('eimg2')){
        $image2 = $request->file('eimg2');
        $new_name2 = date('dmY-Hisu') . '-' . $image2->getClientOriginalName();
        $image2->move(public_path('/storage'), $new_name2);
        $modelo->eimg2 = $new_name2;
    }

    /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
    if (request()->hasFile('eimg3')){
        $image3 = $request->file('eimg3');
        $new_name3 = date('dmY-Hisu') . '-' . $image3->getClientOriginalName();
        $image3->move(public_path('/storage'), $new_name3);
        $modelo->eimg3 = $new_name3;
    }

    /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
    if (request()->hasFile('eimg4')){
        $image4 = $request->file('eimg4');
        $new_name4 = date('dmY-Hisu') . '-' . $image4->getClientOriginalName();
        $image4->move(public_path('/storage'), $new_name4);
        $modelo->eimg4 = $new_name4;
    }

    /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
    if (request()->hasFile('eimg5')){
        $image5 = $request->file('eimg5');
        $new_name5 = date('dmY-Hisu') . '-' . $image5->getClientOriginalName();
        $image5->move(public_path('/storage'), $new_name5);
        $modelo->eimg5 = $new_name5;
    }
    if (request()->hasFile('eimg6')){
        $image1 = $request->file('eimg6');
        $new_name1 = date('dmY-Hisu') . '-' . $image1->getClientOriginalName();
        $image1->move(public_path('/storage'), $new_name1);
        $modelo->eimg6 = $new_name1;
    }

    /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
    if (request()->hasFile('eimg7')){
        $image2 = $request->file('eimg7');
        $new_name2 = date('dmY-Hisu') . '-' . $image2->getClientOriginalName();
        $image2->move(public_path('/storage'), $new_name2);
        $modelo->eimg7 = $new_name2;
    }

    /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
    if (request()->hasFile('img8')){
        $image3 = $request->file('img8');
        $new_name3 = date('dmY-Hisu') . '-' . $image3->getClientOriginalName();
        $image3->move(public_path('/storage'), $new_name3);
        $modelo->img8 = $new_name3;
    }

    /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
    if (request()->hasFile('eimg9')){
        $image4 = $request->file('eimg9');
        $new_name4 = date('dmY-Hisu') . '-' . $image4->getClientOriginalName();
        $image4->move(public_path('/storage'), $new_name4);
        $modelo->eimg9 = $new_name4;
    }

    /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
    if (request()->hasFile('eimg10')){
        $image5 = $request->file('eimg10');
        $new_name5 = date('dmY-Hisu') . '-' . $image5->getClientOriginalName();
        $image5->move(public_path('/storage'), $new_name5);
        $modelo->eimg10 = $new_name5;
    }


    if (request()->hasFile('uimg1')){
        $image1 = $request->file('uimg1');
        $new_name1 = date('dmY-Hisu') . '-' . $image1->getClientOriginalName();
        $image1->move(public_path('/storage'), $new_name1);
        $modelo->uimg1 = $new_name1;
    }


    /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
    if (request()->hasFile('uimg2')){
        $image2 = $request->file('uimg2');
        $new_name2 = date('dmY-Hisu') . '-' . $image2->getClientOriginalName();
        $image2->move(public_path('/storage'), $new_name2);
        $modelo->uimg2 = $new_name2;
    }

    /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
    if (request()->hasFile('uimg3')){
        $image3 = $request->file('uimg3');
        $new_name3 = date('dmY-Hisu') . '-' . $image3->getClientOriginalName();
        $image3->move(public_path('/storage'), $new_name3);
        $modelo->uimg3 = $new_name3;
    }

    /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
    if (request()->hasFile('uimg4')){
        $image4 = $request->file('uimg4');
        $new_name4 = date('dmY-Hisu') . '-' . $image4->getClientOriginalName();
        $image4->move(public_path('/storage'), $new_name4);
        $modelo->uimg4 = $new_name4;
    }

    /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
    if (request()->hasFile('uimg5')){
        $image5 = $request->file('uimg5');
        $new_name5 = date('dmY-Hisu') . '-' . $image5->getClientOriginalName();
        $image5->move(public_path('/storage'), $new_name5);
        $modelo->uimg5 = $new_name5;
    }
    if (request()->hasFile('uimg6')){
        $image1 = $request->file('uimg6');
        $new_name1 = date('dmY-Hisu') . '-' . $image1->getClientOriginalName();
        $image1->move(public_path('/storage'), $new_name1);
        $modelo->uimg6 = $new_name1;
    }


    /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
    if (request()->hasFile('uimg7')){
        $image2 = $request->file('uimg7');
        $new_name2 = date('dmY-Hisu') . '-' . $image2->getClientOriginalName();
        $image2->move(public_path('/storage'), $new_name2);
        $modelo->uimg7 = $new_name2;
    }

    /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
    if (request()->hasFile('uimg8')){
        $image3 = $request->file('uimg8');
        $new_name3 = date('dmY-Hisu') . '-' . $image3->getClientOriginalName();
        $image3->move(public_path('/storage'), $new_name3);
        $modelo->uimg8 = $new_name3;
    }

    /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
    if (request()->hasFile('uimg9')){
        $image4 = $request->file('uimg9');
        $new_name4 = date('dmY-Hisu') . '-' . $image4->getClientOriginalName();
        $image4->move(public_path('/storage'), $new_name4);
        $modelo->uimg9 = $new_name4;
    }

    /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
    if (request()->hasFile('uimg10')){
        $image5 = $request->file('uimg10');
        $new_name5 = date('dmY-Hisu') . '-' . $image5->getClientOriginalName();
        $image5->move(public_path('/storage'), $new_name5);
        $modelo->uimg10 = $new_name5;
    }
    $modelo->vt1 = $request->input('vt1');
    $modelo->ve1 = $request->input('ve1');
    $modelo->vu1 = $request->input('vu1');
    $modelo->tour360 = $request->input('tour360');
    $modelo->proyecto_id = $request->input('proyecto_id');
    $modelo->save(); /* save en db al objeto con los valores*/
    return redirect('admin/modelo')->with('mensaje','Modelo creado con exito.');
}


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editar($id)
    {
        $data = Modelo::findOrFail($id);

        $plantas=\DB::table('plantas')
        ->select('plantas.id',
            'plantas.img_baja',
            'plantas.img_alta',
            'plantas.img_3era',
            'plantas.img_areaverde',
            'plantas.modelo_id'
        )
        ->join('modelos','modelos.id','plantas.modelo_id')
        ->where('modelos.id',$id)
        ->get();
        return view('admin.modelo.editar', compact('data','plantas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function actualizar(ValidacionModelo $request, $id)
    {

          $this->validate($request, [
            'nombre_modelo' =>'required', 
             
            'bajada' =>'required',      
            'mtrs'    =>  'required',
            'tipoinmueble_id'     =>  'required',
            'banos'      =>  'required',
            'dormitorio'      =>  'required',
            'tipococina_id'      =>  'required',
            
            'galeria' => 'required',
            'terminaciones'      =>  'required',
            'equipamiento'      =>  'required',
            'ubicacion'      =>  'required',
            'proyecto_id'      =>  'required'

           
        ]);

        /* Validación para que la imagen sólo sea en estos formatos y campo requerido. Quitar gif si es necesario*/
        $this->validate($request, [
            'img_fichaproyecto'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'gimg1'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'gimg2'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'gimg3'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'gimg4'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'gimg5'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'gimg6'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'gimg7'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'gimg8'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'gimg9'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'gimg10'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'archivo'  => 'mimes:pdf|max:6000',
            'timg1'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'timg2'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'timg3'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'timg4'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'timg5'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'timg6'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'timg7'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'timg8'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'timg9'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'timg10'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'eimg1'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'eimg2'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'eimg3'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'eimg4'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'eimg5'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'eimg6'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'eimg7'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'eimg8'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'eimg9'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'eimg10'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'uimg1'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'uimg2'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'uimg3'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'uimg4'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'uimg5'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'uimg6'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'uimg7'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'uimg8'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'uimg9'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'uimg10'  => 'image|mimes:jpg,png,gif,jpeg|max:2048'
            

        ]);
        $var = false;
        switch ($request->get('check_stock')) {
            case '0':
                $var  = false;
                break;
            case '1':
                $var = true;
                break;
        }
        $var2 = false;
        switch ($request->get('check_tw')) {
            case '0':
                $var2  = false;
                break;
            case '1':
                $var2 = true;
                break;
        }

        $data = Modelo::find($id);
        $data->nombre_modelo = $request->get('nombre_modelo');
        $data->precio_desde = $request->get('precio_desde');
        if (request()->hasFile('img_fichaproyecto')){
            $image1 = $request->file('img_fichaproyecto');
            $new_name1 = date('dmY-Hisu') . '-' . $image1->getClientOriginalName();
            $image1->move(public_path('/storage'), $new_name1);
            $data->img_fichaproyecto = $new_name1;
        }
        if (request()->hasFile('archivo')){
            $image1 = $request->file('archivo');
            $new_name1 = date('dmY-Hisu') . '-' . $image1->getClientOriginalName();
            $image1->move(public_path('/storage/FichasProyectoPDF'), $new_name1);
            $data->archivo = $new_name1;
        }
        if (request()->hasFile('slider1')){

        $image1 = $request->file('slider1');
        $new_name1 = date('dmY-Hisu') . '-' . $image1->getClientOriginalName();
        $image1->move(public_path('/storage'), $new_name1);
        $data->slider1 = $new_name1;
    }


    if (request()->hasFile('slider2')){
        $image2 = $request->file('slider2');
        $new_name2 = date('dmY-Hisu') . '-' . $image2->getClientOriginalName();
        $image2->move(public_path('/storage'), $new_name2);
        $data->slider2 = $new_name2;
    }
    if (request()->hasFile('slider3')){
        $image3 = $request->file('slider3');
        $new_name3 = date('dmY-Hisu') . '-' . $image3->getClientOriginalName();
        $image3->move(public_path('/storage'), $new_name3);
        $data->slider3 = $new_name3;
    }

    if (request()->hasFile('slider4')){
        $image4 = $request->file('slider4');
        $new_name4 = date('dmY-Hisu') . '-' . $image4->getClientOriginalName();
        $image4->move(public_path('/storage'), $new_name4);
        $data->slider4 = $new_name4;
    }

    if (request()->hasFile('slider5')){
        $image5 = $request->file('slider5');
        $new_name5 = date('dmY-Hisu') . '-' . $image5->getClientOriginalName();
        $image5->move(public_path('/storage'), $new_name5);
        $data->slider5 = $new_name5;
    }
    if (request()->hasFile('slidermovil1')){
        $image1 = $request->file('slidermovil1');
        $new_name1 = date('dmY-Hisu') . '-' . $image1->getClientOriginalName();
        $image1->move(public_path('/storage'), $new_name1);
        $data->slidermovil1 = $new_name1;
    }

    if (request()->hasFile('slidermovil2')){
        $image2 = $request->file('slidermovil2');
        $new_name2 = date('dmY-Hisu') . '-' . $image2->getClientOriginalName();
        $image2->move(public_path('/storage'), $new_name2);
        $data->slidermovil2 = $new_name2;
    }
    if (request()->hasFile('slidermovil3')){
        $image3 = $request->file('slidermovil3');
        $new_name3 = date('dmY-Hisu') . '-' . $image3->getClientOriginalName();
        $image3->move(public_path('/storage'), $new_name3);
        $data->slidermovil3 = $new_name3;
    }

    if (request()->hasFile('slidermovil4')){
        $image4 = $request->file('slidermovil4');
        $new_name4 = date('dmY-Hisu') . '-' . $image4->getClientOriginalName();
        $image4->move(public_path('/storage'), $new_name4);
        $data->slidermovil4 = $new_name4;
    }

    if (request()->hasFile('slidermovil5')){
        $image5 = $request->file('slidermovil5');
        $new_name5 = date('dmY-Hisu') . '-' . $image5->getClientOriginalName();
        $image5->move(public_path('/storage'), $new_name5);
        $data->slidermovil5 = $new_name5;
    }
        $data->stock = $request->get('stock');
        $data->check_stock = $var;
        $data->check_tw = $var2;
        $data->bajada = $request->get('bajada');
        $data->mtrs = $request->get('mtrs');
        $data->tipoinmueble_id = $request->get('tipoinmueble_id');
        $data->banos = $request->get('banos');
        $data->dormitorio = $request->get('dormitorio');
        $data->tipococina_id = $request->get('tipococina_id');
        $data->estacionamiento = $request->get('estacionamiento');
        $data->galeria = $request->get('galeria');
        $data->terminaciones = $request->get('terminaciones');
        $data->equipamiento = $request->get('equipamiento');
        $data->ubicacion = $request->get('ubicacion');

    if (request()->hasFile('gimg1')){
        $image1 = $request->file('gimg1');
        $new_name1 = date('dmY-Hisu') . '-' . $image1->getClientOriginalName();
        $image1->move(public_path('/storage'), $new_name1);
        $data->gimg1 = $new_name1;
    }

    if (request()->hasFile('gimg2')){
        $image2 = $request->file('gimg2');
        $new_name2 = date('dmY-Hisu') . '-' . $image2->getClientOriginalName();
        $image2->move(public_path('/storage'), $new_name2);
        $data->gimg2 = $new_name2;
    }
    if (request()->hasFile('gimg3')){
        $image3 = $request->file('gimg3');
        $new_name3 = date('dmY-Hisu') . '-' . $image3->getClientOriginalName();
        $image3->move(public_path('/storage'), $new_name3);
        $data->gimg3 = $new_name3;
    }

    if (request()->hasFile('gimg4')){
        $image4 = $request->file('gimg4');
        $new_name4 = date('dmY-Hisu') . '-' . $image4->getClientOriginalName();
        $image4->move(public_path('/storage'), $new_name4);
        $data->gimg4 = $new_name4;
    }

    if (request()->hasFile('gimg5')){
        $image5 = $request->file('gimg5');
        $new_name5 = date('dmY-Hisu') . '-' . $image5->getClientOriginalName();
        $image5->move(public_path('/storage'), $new_name5);
        $data->gimg5 = $new_name5;
    }
    if (request()->hasFile('gimg6')){
        $image1 = $request->file('gimg6');
        $new_name1 = date('dmY-Hisu') . '-' . $image1->getClientOriginalName();
        $image1->move(public_path('/storage'), $new_name1);
        $data->gimg6 = $new_name1;
    }

    if (request()->hasFile('gimg7')){
        $image2 = $request->file('gimg7');
        $new_name2 = date('dmY-Hisu') . '-' . $image2->getClientOriginalName();
        $image2->move(public_path('/storage'), $new_name2);
        $data->gimg7 = $new_name2;
    }
    if (request()->hasFile('gimg8')){
        $image3 = $request->file('gimg8');
        $new_name3 = date('dmY-Hisu') . '-' . $image3->getClientOriginalName();
        $image3->move(public_path('/storage'), $new_name3);
        $data->gimg8 = $new_name3;
    }

    if (request()->hasFile('gimg9')){
        $image4 = $request->file('gimg9');
        $new_name4 = date('dmY-Hisu') . '-' . $image4->getClientOriginalName();
        $image4->move(public_path('/storage'), $new_name4);
        $data->gimg9 = $new_name4;
    }

    if (request()->hasFile('gimg10')){
        $image5 = $request->file('gimg10');
        $new_name5 = date('dmY-Hisu') . '-' . $image5->getClientOriginalName();
        $image5->move(public_path('/storage'), $new_name5);
        $data->gimg10 = $new_name5;
    }
    if (request()->hasFile('timg1')){
        $image1 = $request->file('timg1');
        $new_name1 = date('dmY-Hisu') . '-' . $image1->getClientOriginalName();
        $image1->move(public_path('/storage'), $new_name1);
        $data->timg1 = $new_name1;
    }

    if (request()->hasFile('timg2')){
        $image2 = $request->file('timg2');
        $new_name2 = date('dmY-Hisu') . '-' . $image2->getClientOriginalName();
        $image2->move(public_path('/storage'), $new_name2);
        $data->timg2 = $new_name2;
    }
    if (request()->hasFile('timg3')){
        $image3 = $request->file('timg3');
        $new_name3 = date('dmY-Hisu') . '-' . $image3->getClientOriginalName();
        $image3->move(public_path('/storage'), $new_name3);
        $data->timg3 = $new_name3;
    }

    if (request()->hasFile('timg4')){
        $image4 = $request->file('timg4');
        $new_name4 = date('dmY-Hisu') . '-' . $image4->getClientOriginalName();
        $image4->move(public_path('/storage'), $new_name4);
        $data->timg4 = $new_name4;
    }

    if (request()->hasFile('timg5')){
        $image5 = $request->file('timg5');
        $new_name5 = date('dmY-Hisu') . '-' . $image5->getClientOriginalName();
        $image5->move(public_path('/storage'), $new_name5);
        $data->timg5 = $new_name5;
    }
    if (request()->hasFile('timg6')){
        $image1 = $request->file('timg6');
        $new_name1 = date('dmY-Hisu') . '-' . $image1->getClientOriginalName();
        $image1->move(public_path('/storage'), $new_name1);
        $data->timg6 = $new_name1;
    }

    if (request()->hasFile('timg7')){
        $image2 = $request->file('timg7');
        $new_name2 = date('dmY-Hisu') . '-' . $image2->getClientOriginalName();
        $image2->move(public_path('/storage'), $new_name2);
        $data->timg7 = $new_name2;
    }
    if (request()->hasFile('timg8')){
        $image3 = $request->file('timg8');
        $new_name3 = date('dmY-Hisu') . '-' . $image3->getClientOriginalName();
        $image3->move(public_path('/storage'), $new_name3);
        $data->timg8 = $new_name3;
    }

    if (request()->hasFile('timg9')){
        $image4 = $request->file('timg9');
        $new_name4 = date('dmY-Hisu') . '-' . $image4->getClientOriginalName();
        $image4->move(public_path('/storage'), $new_name4);
        $data->timg9 = $new_name4;
    }

    if (request()->hasFile('timg10')){
        $image5 = $request->file('timg10');
        $new_name5 = date('dmY-Hisu') . '-' . $image5->getClientOriginalName();
        $image5->move(public_path('/storage'), $new_name5);
        $data->timg10 = $new_name5;
    }

    if (request()->hasFile('eimg1')){
        $image1 = $request->file('eimg1');
        $new_name1 = date('dmY-Hisu') . '-' . $image1->getClientOriginalName();
        $image1->move(public_path('/storage'), $new_name1);
        $data->eimg1 = $new_name1;
    }


    if (request()->hasFile('eimg2')){
        $image2 = $request->file('eimg2');
        $new_name2 = date('dmY-Hisu') . '-' . $image2->getClientOriginalName();
        $image2->move(public_path('/storage'), $new_name2);
        $data->eimg2 = $new_name2;
    }

    if (request()->hasFile('eimg3')){
        $image3 = $request->file('eimg3');
        $new_name3 = date('dmY-Hisu') . '-' . $image3->getClientOriginalName();
        $image3->move(public_path('/storage'), $new_name3);
        $data->eimg3 = $new_name3;
    }

    if (request()->hasFile('eimg4')){
        $image4 = $request->file('eimg4');
        $new_name4 = date('dmY-Hisu') . '-' . $image4->getClientOriginalName();
        $image4->move(public_path('/storage'), $new_name4);
        $data->eimg4 = $new_name4;
    }

    if (request()->hasFile('eimg5')){
        $image5 = $request->file('eimg5');
        $new_name5 = date('dmY-Hisu') . '-' . $image5->getClientOriginalName();
        $image5->move(public_path('/storage'), $new_name5);
        $data->eimg5 = $new_name5;
    }
    if (request()->hasFile('eimg6')){
        $image1 = $request->file('eimg6');
        $new_name1 = date('dmY-Hisu') . '-' . $image1->getClientOriginalName();
        $image1->move(public_path('/storage'), $new_name1);
        $data->eimg6 = $new_name1;
    }


    if (request()->hasFile('eimg7')){
        $image2 = $request->file('eimg7');
        $new_name2 = date('dmY-Hisu') . '-' . $image2->getClientOriginalName();
        $image2->move(public_path('/storage'), $new_name2);
        $data->eimg7 = $new_name2;
    }

    if (request()->hasFile('eimg8')){
        $image3 = $request->file('eimg8');
        $new_name3 = date('dmY-Hisu') . '-' . $image3->getClientOriginalName();
        $image3->move(public_path('/storage'), $new_name3);
        $data->eimg8 = $new_name3;
    }

    if (request()->hasFile('eimg9')){
        $image4 = $request->file('eimg9');
        $new_name4 = date('dmY-Hisu') . '-' . $image4->getClientOriginalName();
        $image4->move(public_path('/storage'), $new_name4);
        $data->eimg9 = $new_name4;
    }

    if (request()->hasFile('eimg10')){
        $image5 = $request->file('eimg10');
        $new_name5 = date('dmY-Hisu') . '-' . $image5->getClientOriginalName();
        $image5->move(public_path('/storage'), $new_name5);
        $data->eimg10 = $new_name5;
    }

    if (request()->hasFile('uimg1')){
        $image1 = $request->file('uimg1');
        $new_name1 = date('dmY-Hisu') . '-' . $image1->getClientOriginalName();
        $image1->move(public_path('/storage'), $new_name1);
        $data->uimg1 = $new_name1;
    }


    if (request()->hasFile('uimg2')){
        $image2 = $request->file('uimg2');
        $new_name2 = date('dmY-Hisu') . '-' . $image2->getClientOriginalName();
        $image2->move(public_path('/storage'), $new_name2);
        $data->uimg2 = $new_name2;
    }

    if (request()->hasFile('uimg3')){
        $image3 = $request->file('uimg3');
        $new_name3 = date('dmY-Hisu') . '-' . $image3->getClientOriginalName();
        $image3->move(public_path('/storage'), $new_name3);
        $data->uimg3 = $new_name3;
    }

    if (request()->hasFile('uimg4')){
        $image4 = $request->file('uimg4');
        $new_name4 = date('dmY-Hisu') . '-' . $image4->getClientOriginalName();
        $image4->move(public_path('/storage'), $new_name4);
        $data->uimg4 = $new_name4;
    }

    if (request()->hasFile('uimg5')){
        $image5 = $request->file('uimg5');
        $new_name5 = date('dmY-Hisu') . '-' . $image5->getClientOriginalName();
        $image5->move(public_path('/storage'), $new_name5);
        $data->uimg5 = $new_name5;
    }
    if (request()->hasFile('uimg6')){
        $image1 = $request->file('uimg6');
        $new_name1 = date('dmY-Hisu') . '-' . $image1->getClientOriginalName();
        $image1->move(public_path('/storage'), $new_name1);
        $data->uimg6 = $new_name1;
    }


    if (request()->hasFile('uimg7')){
        $image2 = $request->file('uimg7');
        $new_name2 = date('dmY-Hisu') . '-' . $image2->getClientOriginalName();
        $image2->move(public_path('/storage'), $new_name2);
        $data->uimg7 = $new_name2;
    }

    if (request()->hasFile('uimg8')){
        $image3 = $request->file('uimg8');
        $new_name3 = date('dmY-Hisu') . '-' . $image3->getClientOriginalName();
        $image3->move(public_path('/storage'), $new_name3);
        $data->uimg8 = $new_name3;
    }

    if (request()->hasFile('uimg9')){
        $image4 = $request->file('uimg9');
        $new_name4 = date('dmY-Hisu') . '-' . $image4->getClientOriginalName();
        $image4->move(public_path('/storage'), $new_name4);
        $data->uimg9 = $new_name4;
    }

    if (request()->hasFile('uimg10')){
        $image5 = $request->file('uimg10');
        $new_name5 = date('dmY-Hisu') . '-' . $image5->getClientOriginalName();
        $image5->move(public_path('/storage'), $new_name5);
        $data->uimg10 = $new_name5;
    }

    $data->vt1 = $request->get('vt1');
    $data->ve1 = ''.$request->get('ve1').'';
    $data->vu1 = $request->get('vu1');
    $data->tour360 = $request->get('tour360');
    $data->proyecto_id = $request->get('proyecto_id');




    $data->save();
    return redirect()->back()->with('mensaje', 'Modelo actualizado con exito');

    
    //return redirect('admin/proyecto')->with('mensaje', 'Modelo actualizado con exito');
}
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function eliminar(Request $request, $id)
    {
        if ($request->ajax()) {
            if (Modelo::destroy($id)) {
                return response()->json(['mensaje' => 'ok']);
            } else {
                return response()->json(['mensaje' => 'ng']);
            }
        } else {
            abort(404);
        }
    }
    public function import(Request $request){

        \Excel::load($request->excel, function($reader) {

            $excel_modelo = $reader->get();

            // iteracción
            $reader->each(function($row) {

                $modelo = new Modelo();
                $modelo->nombre_modelo=$row->nombre_modelo;
                $modelo->bajada=$row->bajada;
                $modelo->mtrs=$row->mtrs;
                $modelo->tipoinmueble_id=$row->tipoinmueble_id;
                $modelo->banos=$row->banos;
                $modelo->dormitorio=$row->dormitorio;
                $modelo->tipococina_id=$row->tipococina_id;
                $modelo->estacionamiento=$row->estacionamiento;
                $modelo->terminaciones=$row->terminaciones;
                $modelo->proyecto_id=$row->proyecto_id;

            //dd($proyecto);
                $modelo->save();


            });

        });

        return redirect()->intended('admin/modelo');
    }

    public function eliminar_fotomodelo(Request $request,$id,$dato){


        $data = Modelo::findOrFail($id);

        /********************slider1**************************************/
        if($data->slider1==$dato){
         \DB::table('modelos')
         ->where('id', $id)  
         ->update(['slider1'=>NULL]);
         $url=public_path('/storage/'.$dato);
         unlink($url);
         return redirect()->route('editar_modelo', ['id' => $id]);
     }

     if($data->slider2==$dato){

         \DB::table('modelos')
         ->where('id', $id)  
         ->update(['slider2'=>NULL]);
         $url=public_path('/storage/'.$dato);
         unlink($url);
         return redirect()->route('editar_modelo', ['id' => $id]);

     }


     if($data->slider3==$dato){

        \DB::table('modelos')
        ->where('id', $id)  
        ->update(['slider3'=>NULL]);
        $url=public_path('/storage/'.$dato);
        unlink($url);
        return redirect()->route('editar_modelo', ['id' => $id]);


    }

    if($data->slider4==$dato){
     \DB::table('modelos')
     ->where('id', $id)  
     ->update(['slider4'=>NULL]);
     $url=public_path('/storage/'.$dato);
     unlink($url);
     return redirect()->route('editar_modelo', ['id' => $id]);
 }
 if($data->slider5==$dato){
   \DB::table('modelos')
   ->where('id', $id)  
   ->update(['slider5'=>NULL]);
   $url=public_path('/storage/'.$dato);
   unlink($url);
   return redirect()->route('editar_modelo', ['id' => $id]);

}

/**************************slidermovil1 *****************************/

if($data->slidermovil1==$dato){
    \DB::table('modelos')
    ->where('id', $id)  
    ->update(['slidermovil1'=>NULL]);
    $url=public_path('/storage/'.$dato);
    unlink($url);
    return redirect()->route('editar_modelo', ['id' => $id]);
}

if($data->slidermovil2==$dato){
    \DB::table('modelos')
    ->where('id', $id)  
    ->update(['slidermovil2'=>NULL]);
    $url=public_path('/storage/'.$dato);
    unlink($url);
    return redirect()->route('editar_modelo', ['id' => $id]);
}   


if($data->slidermovil3==$dato){

    \DB::table('modelos')
    ->where('id', $id)  
    ->update(['slidermovil3'=>NULL]);
    $url=public_path('/storage/'.$dato);
    unlink($url);
    return redirect()->route('editar_modelo', ['id' => $id]);
}

if($data->slidermovil4==$dato){
    \DB::table('modelos')
    ->where('id', $id)  
    ->update(['slidermovil4'=>NULL]);
    $url=public_path('/storage/'.$dato);
    unlink($url);
    return redirect()->route('editar_modelo', ['id' => $id]);
}

if($data->slidermovil5==$dato){
    \DB::table('modelos')
    ->where('id', $id)  
    ->update(['slidermovil5'=>NULL]);
    $url=public_path('/storage/'.$dato);
    unlink($url);
    return redirect()->route('editar_modelo', ['id' => $id]);
}


/*********************timg1 **************************/




if($data->timg1==$dato){
    \DB::table('modelos')
    ->where('id', $id)  
    ->update(['timg1'=>NULL]);
    $url=public_path('/storage/'.$dato);
    unlink($url);
    return redirect()->route('editar_modelo', ['id' => $id]);
}

if($data->timg2==$dato){

    \DB::table('modelos')
    ->where('id', $id)  
    ->update(['timg2'=>NULL]);        
    $url=public_path('/storage/'.$dato);
    unlink($url);
    return redirect()->route('editar_modelo', ['id' => $id]);

}


if($data->timg3==$dato){

    \DB::table('modelos')
    ->where('id', $id)  
    ->update(['timg3'=>NULL]);
    $url=public_path('/storage/'.$dato);
    unlink($url);
    return redirect()->route('editar_modelo', ['id' => $id]);
}

if($data->timg4==$dato){
    \DB::table('modelos')
    ->where('id', $id)  
    ->update(['timg4'=>NULL]);
    $url=public_path('/storage/'.$dato);
    unlink($url);
    return redirect()->route('editar_modelo', ['id' => $id]);
}

if($data->timg5==$dato){
    \DB::table('modelos')
    ->where('id', $id)  
    ->update(['timg5'=>NULL]);        
    $url=public_path('/storage/'.$dato);
    unlink($url);
    return redirect()->route('editar_modelo', ['id' => $id]);
}
if($data->timg6==$dato){

    \DB::table('modelos')
    ->where('id', $id)  
    ->update(['timg6'=>NULL]);
    $url=public_path('/storage/'.$dato);        
    unlink($url);
    return redirect()->route('editar_modelo', ['id' => $id]);
}

if($data->timg7==$dato){

    \DB::table('modelos')
    ->where('id', $id)  
    ->update(['timg7'=>NULL]);
    $url=public_path('/storage/'.$dato);
    unlink($url);
    return redirect()->route('editar_modelo', ['id' => $id]);
}

if($data->timg8==$dato){

    \DB::table('modelos')
    ->where('id', $id)  
    ->update(['timg8'=>NULL]);
    $url=public_path('/storage/'.$dato);
    unlink($url);
    return redirect()->route('editar_modelo', ['id' => $id]);
}
if($data->timg9==$dato){

    \DB::table('modelos')
    ->where('id', $id)  
    ->update(['timg9'=>NULL]);

    $url=public_path('/storage/'.$dato);

    unlink($url);
    return redirect()->route('editar_modelo', ['id' => $id]);
}
if($data->timg10==$dato){

    \DB::table('modelos')
    ->where('id', $id)  
    ->update(['timg10'=>NULL]);

    $url=public_path('/storage/'.$dato);

    unlink($url);
    return redirect()->route('editar_modelo', ['id' => $id]);   
}   

/*******************eimg****************************************************/





if($data->eimg1==$dato){


    \DB::table('modelos')
    ->where('id', $id)  
    ->update(['eimg1'=>NULL]);

    $url=public_path('/storage/'.$dato);

    unlink($url);
    return redirect()->route('editar_modelo', ['id' => $id]);





}

if($data->eimg2==$dato){

    \DB::table('modelos')
    ->where('id', $id)  
    ->update(['eimg2'=>NULL]);

    $url=public_path('/storage/'.$dato);

    unlink($url);

    return redirect()->route('editar_modelo', ['id' => $id]);

}


if($data->eimg3==$dato){

    \DB::table('modelos')
    ->where('id', $id)  
    ->update(['eimg3'=>NULL]);
    $url=public_path('/storage/'.$dato);
    unlink($url);
    return redirect()->route('editar_modelo', ['id' => $id]);


}

if($data->eimg4==$dato){
 \DB::table('modelos')
 ->where('id', $id)  
 ->update(['eimg4'=>NULL]);

 $url=public_path('/storage/'.$dato);

 unlink($url);
 return redirect()->route('editar_modelo', ['id' => $id]);
}

if($data->eimg5==$dato){
   \DB::table('modelos')
   ->where('id', $id)  
   ->update(['eimg5'=>NULL]);

   $url=public_path('/storage/'.$dato);

   unlink($url);
   return redirect()->route('editar_modelo', ['id' => $id]);

}
if($data->eimg6==$dato){

  \DB::table('modelos')
  ->where('id', $id)  
  ->update(['eimg6'=>NULL]);

  $url=public_path('/storage/'.$dato);

  unlink($url);
  return redirect()->route('editar_modelo', ['id' => $id]);


}
if($data->eimg7==$dato){

  \DB::table('modelos')
  ->where('id', $id)  
  ->update(['eimg7'=>NULL]);

  $url=public_path('/storage/'.$dato);

  unlink($url);
  return redirect()->route('editar_modelo', ['id' => $id]);


}
if($data->eimg8==$dato){

  \DB::table('modelos')
  ->where('id', $id)  
  ->update(['eimg8'=>NULL]);

  $url=public_path('/storage/'.$dato);

  unlink($url);
  return redirect()->route('editar_modelo', ['id' => $id]);


}
if($data->eimg9==$dato){

  \DB::table('modelos')
  ->where('id', $id)  
  ->update(['eimg9'=>NULL]);

  $url=public_path('/storage/'.$dato);

  unlink($url);
  return redirect()->route('editar_modelo', ['id' => $id]);


}
if($data->eimg10==$dato){

  \DB::table('modelos')
  ->where('id', $id)  
  ->update(['eimg10'=>NULL]);

  $url=public_path('/storage/'.$dato);

  unlink($url);
  return redirect()->route('editar_modelo', ['id' => $id]);

}


/*****************uimg*******************/





if($data->uimg1==$dato){


 \DB::table('modelos')
 ->where('id', $id)  
 ->update(['uimg1'=>NULL]);

 $url=public_path('/storage/'.$dato);

 unlink($url);
 return redirect()->route('editar_modelo', ['id' => $id]);





}

if($data->uimg2==$dato){

 \DB::table('modelos')
 ->where('id', $id)  
 ->update(['uimg2'=>NULL]);

 $url=public_path('/storage/'.$dato);

 unlink($url);

 return redirect()->route('editar_modelo', ['id' => $id]);

}


if($data->uimg3==$dato){

    \DB::table('modelos')
    ->where('id', $id)  
    ->update(['uimg3'=>NULL]);

    $url=public_path('/storage/'.$dato);

    unlink($url);
    return redirect()->route('editar_modelo', ['id' => $id]);


}

if($data->uimg4==$dato){
 \DB::table('modelos')
 ->where('id', $id)  
 ->update(['uimg4'=>NULL]);

 $url=public_path('/storage/'.$dato);

 unlink($url);
 return redirect()->route('editar_modelo', ['id' => $id]);
}

if($data->uimg5==$dato){
   \DB::table('modelos')
   ->where('id', $id)  
   ->update(['uimg5'=>NULL]);

   $url=public_path('/storage/'.$dato);

   unlink($url);
   return redirect()->route('editar_modelo', ['id' => $id]);

}
if($data->uimg6==$dato){

  \DB::table('modelos')
  ->where('id', $id)  
  ->update(['uimg6'=>NULL]);

  $url=public_path('/storage/'.$dato);

  unlink($url);
  return redirect()->route('editar_modelo', ['id' => $id]);


}
if($data->uimg7==$dato){

  \DB::table('modelos')
  ->where('id', $id)  
  ->update(['uimg7'=>NULL]);

  $url=public_path('/storage/'.$dato);

  unlink($url);
  return redirect()->route('editar_modelo', ['id' => $id]);


}
if($data->uimg8==$dato){

  \DB::table('modelos')
  ->where('id', $id)  
  ->update(['uimg8'=>NULL]);

  $url=public_path('/storage/'.$dato);

  unlink($url);
  return redirect()->route('editar_modelo', ['id' => $id]);


}
if($data->uimg9==$dato){

  \DB::table('modelos')
  ->where('id', $id)  
  ->update(['uimg9'=>NULL]);

  $url=public_path('/storage/'.$dato);

  unlink($url);
  return redirect()->route('editar_modelo', ['id' => $id]);


}
if($data->uimg10==$dato){

  \DB::table('modelos')
  ->where('id', $id)  
  ->update(['uimg10'=>NULL]);

  $url=public_path('/storage/'.$dato);

  unlink($url);
  return redirect()->route('editar_modelo', ['id' => $id]);




}


if($data->img_fichaproyecto==$dato){

  \DB::table('modelos')
  ->where('id', $id)  
  ->update(['img_fichaproyecto'=>NULL]);

  $url=public_path('/storage/'.$dato);

  unlink($url);
  return redirect()->route('editar_modelo', ['id' => $id]);




}


}

}
