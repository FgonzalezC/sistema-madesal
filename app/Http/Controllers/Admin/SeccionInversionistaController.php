<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ValidacionSeccionInversionista;
use App\Models\Admin\SeccionInversionista;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SeccionInversionistaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $datas = SeccionInversionista::orderBy('id')->get();
      return view('admin.inversionista.index', compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function crear()
    {

        return view('admin.inversionista.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function guardar(ValidacionSeccionInversionista $request)
    {
       $this->validate($request, [
            'titulo'    =>  'required',
            'subtitulo'    =>  'required',
            'texto1'     =>  'required',
            'texto2'      =>  'required',
            'titulo1'      =>  'required',
            'bajada1'      =>  'required',
            'titulo2'      =>  'required',
            'bajada2'      =>  'required',
            'titulo3'      =>  'required',
            'bajada3'      =>  'required',
            'titulo4'      =>  'required',
            'bajada4'      =>  'required',
            'seccion1'      =>  'required',
            'seccion2'      =>  'required',
            'punto1'      =>  'required',
            'punto2'      =>  'required',
            'punto3'      =>  'required',
            'punto4'      =>  'required',
            'titulodestacado'      =>  'required'
            
        ]);

        /* Validación para que la imagen sólo sea en estos formatos y campo requerido. Quitar gif si es necesario*/
        $this->validate($request, [
            'img'      => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'masterplan'      => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'img1'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'img2'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'img3'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'imgmovil1'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'imgmovil2'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'imgmovil3'  => 'image|mimes:jpg,png,gif,jpeg|max:2048'

            
        ]);

        $inversionista = new SeccionInversionista();
        $inversionista->titulo = $request->input('titulo');
        $inversionista->subtitulo = $request->input('subtitulo');
        $inversionista->texto1 = $request->input('texto1');
        $inversionista->texto2 = $request->input('texto2');
        $inversionista->titulo1 = $request->input('titulo1');
        $inversionista->bajada1 = $request->input('bajada1');
        $inversionista->titulo2 = $request->input('titulo2');
        $inversionista->bajada2 = $request->input('bajada2');
        $inversionista->titulo3 = $request->input('titulo3');
        $inversionista->bajada3 = $request->input('bajada3');
        $inversionista->titulo4 = $request->input('titulo4');
        $inversionista->bajada4 = $request->input('bajada4');
        $inversionista->seccion1 = $request->input('seccion1');
        $inversionista->seccion2 = $request->input('seccion2');
        $inversionista->punto1 = $request->input('punto1');
        $inversionista->punto2 = $request->input('punto2');
        $inversionista->punto3 = $request->input('punto3');
        $inversionista->punto4 = $request->input('punto4');
        $inversionista->titulodestacado = $request->input('titulodestacado');

        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
         if (request()->hasFile('img')){ 
        $image = $request->file('img');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $inversionista->img = $new_name;
        }
        else
        {
            $inversionista->img = 'Sin imagen';
        } 
        /*----*/
        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
         if (request()->hasFile('img1')){ 
        $image = $request->file('img1');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $inversionista->img1 = $new_name;
        }
        else
        {
            $inversionista->img1 = 'Sin imagen';
        } 
        /*----*/

        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
         if (request()->hasFile('img2')){ 
        $image = $request->file('img2');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $inversionista->img2 = $new_name;
        }
        else
        {
            $inversionista->img2 = 'Sin imagen';
        }
        /*----*/

        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
         if (request()->hasFile('img3')){ 
        $image = $request->file('img3');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $inversionista->img3 = $new_name;
        }
        else
        {
            $inversionista->img3 = 'Sin imagen';
        } 
         /*----*/

        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
         if (request()->hasFile('imgmovil1')){ 
        $image = $request->file('imgmovil1');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $inversionista->imgmovil1 = $new_name;
        }
        else
        {
            $inversionista->imgmovil1 = 'Sin imagen';
        }
         /*----*/

        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
         if (request()->hasFile('imgmovil2')){ 
        $image = $request->file('imgmovil2');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $inversionista->imgmovil2 = $new_name;
        }
        else
        {
            $inversionista->imgmovil2 = 'Sin imagen';
        }
         /*----*/

        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
         if (request()->hasFile('imgmovil3')){ 
        $image = $request->file('imgmovil3');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $inversionista->imgmovil3 = $new_name;
        }
        else
        {
            $inversionista->imgmovil3 = 'Sin imagen';
        }

        $inversionista->save(); /* save en db al objeto con los valores*/

     //  Proyecto::create($request->all());
        return redirect('admin/inversionista')->with('mensaje','Seccion Inversionista creado con exito.');
    }

    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editar($id)
    {
        $data = SeccionInversionista::findOrFail($id);
        return view('admin.inversionista.editar', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function actualizar(ValidacionSeccionInversionista $request, $id)
    {
       
        $this->validate($request, [
            'titulo'    =>  'required',
            'subtitulo'    =>  'required',
            'texto1'     =>  'required',
            'texto2'      =>  'required',
            'titulo1'      =>  'required',
            'bajada1'      =>  'required',
            'titulo2'      =>  'required',
            'bajada2'      =>  'required',
            'titulo3'      =>  'required',
            'bajada3'      =>  'required',
            'titulo4'      =>  'required',
            'bajada4'      =>  'required',
            'seccion1'      =>  'required',
            'seccion2'      =>  'required',
            'punto1'      =>  'required',
            'punto2'      =>  'required',
            'punto3'      =>  'required',
            'punto4'      =>  'required',
            'titulodestacado'      =>  'required'
            
        ]);

        /* Validación para que la imagen sólo sea en estos formatos y campo requerido. Quitar gif si es necesario*/
        $this->validate($request, [
            'img'      => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'masterplan'      => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'img1'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'img2'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'img3'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'imgmovil1'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'imgmovil2'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'imgmovil3'  => 'image|mimes:jpg,png,gif,jpeg|max:2048'

            
        ]);
       

        $data = SeccionInversionista::find($id);
        $data->titulo = $request->get('titulo');
        $data->subtitulo = $request->get('subtitulo');
        $data->texto1 = $request->get('texto1');
        $data->texto2 = $request->get('texto2');
        $data->titulo1 = $request->get('titulo1');
        $data->bajada1 = $request->get('bajada1');
        $data->titulo2 = $request->get('titulo2');
        $data->bajada2 = $request->get('bajada2');
        $data->titulo3 = $request->get('titulo3');
        $data->bajada3 = $request->get('bajada3');
        $data->titulo4 = $request->get('titulo4');
        $data->bajada4 = $request->get('bajada4');
        $data->seccion1 = $request->get('seccion1');
        $data->seccion2 = $request->get('seccion2');
        $data->punto1 = $request->get('punto1');
        $data->punto2 = $request->get('punto2');
        $data->punto3 = $request->get('punto3');
        $data->punto4 = $request->get('punto4');
        $data->titulodestacado = $request->get('titulodestacado');

        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
         if (request()->hasFile('img')){ 
        $image = $request->file('img');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $data->img = $new_name;
        }
        /*------*/
        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
         if (request()->hasFile('img1')){ 
        $image = $request->file('img1');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $data->img1 = $new_name;
        }
        /*------*/

        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
         if (request()->hasFile('img2')){ 
        $image = $request->file('img2');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $data->img2 = $new_name;
        }

        /*------*/

        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
         if (request()->hasFile('img3')){ 
        $image = $request->file('img3');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $data->img3 = $new_name;
        }

        /*------*/

        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
         if (request()->hasFile('imgmovil1')){ 
        $image = $request->file('imgmovil1');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $data->imgmovil1 = $new_name;
        }
        /*------*/

        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
         if (request()->hasFile('imgmovil2')){ 
        $image = $request->file('imgmovil2');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $data->imgmovil2 = $new_name;
        }
        /*------*/

        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
         if (request()->hasFile('imgmovil3')){ 
        $image = $request->file('imgmovil3');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $data->imgmovil3 = $new_name;
        }

        
        $data->save();

       // Proyecto::findOrFail($id)->update($request->all());
        return redirect('admin/inversionista')->with('mensaje', 'Seccion Inversionista actualizado con exito');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function eliminar(Request $request, $id)
    {
        if ($request->ajax()) {
            if (SeccionInversionista::destroy($id)) {
                return response()->json(['mensaje' => 'ok']);
            } else {
                return response()->json(['mensaje' => 'ng']);
            }
        } else {
            abort(404);
        }
    }
}
