<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ValidacionSeccionFichaProyecto;
use App\Models\Admin\SeccionFichaProyecto;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SeccionFichaproyectoController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){

      $datas = SeccionFichaProyecto::orderBy('id')->get();
      return view('admin.seccion-fichaproyecto.index', compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function crear(Request $request)
    {
        return view('admin.seccion-fichaproyecto.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function guardar(ValidacionSeccionFichaProyecto $request)
    {
        //echo $request->file('img1')->store('/','public');
        //echo $request->file('img2')->store('/','public');

        /* Campos requeridos */
        $this->validate($request, [
            'modelo_id' =>'required',    
            'proyecto_id' =>'required',      
            'terminaciones'    =>  'required',
            'equipamiento'     =>  'required',
            'ubicacion_entorno'      =>  'required',

           
        ]);

        /* Validación para que la imagen sólo sea en estos formatos y campo requerido. Quitar gif si es necesario*/
        $this->validate($request, [
            'img'=>'image|mimes:jpg,png,gif,jpeg|max:2048',
            'timg1'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'timg2'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'timg3'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'timg4'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'timg5'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'eimg1'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'eimg2'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'eimg3'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'eimg4'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'eimg5'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'uimg1'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'uimg2'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'uimg3'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'uimg4'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'uimg5'  => 'image|mimes:jpg,png,gif,jpeg|max:2048'

        ]);
       
        /* Objeto SeccionHome vacío */
        $seccion = new SeccionFichaProyecto();
        $seccion->modelo_id = $request->input('modelo_id');
        $seccion->proyecto_id = $request->input('proyecto_id');
       
        $seccion->terminaciones = $request->input('terminaciones');
        $seccion->equipamiento = $request->input('equipamiento');
        $seccion->ubicacion_entorno = $request->input('ubicacion_entorno');


      

        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
        if (request()->hasFile('timg1')){
        $image1 = $request->file('timg1');
        $new_name1 = date('dmY-Hisu') . '-' . $image1->getClientOriginalName();
        $image1->move(public_path('/storage'), $new_name1);
        $seccion->timg1 = $new_name1;
        }
         else
        {
            $seccion->timg1 = 'Sin imagen';
        } 

        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
        if (request()->hasFile('timg2')){
        $image2 = $request->file('timg2');
        $new_name2 = date('dmY-Hisu') . '-' . $image2->getClientOriginalName();
        $image2->move(public_path('/storage'), $new_name2);
        $seccion->timg2 = $new_name2;
        }
         else
        {
            $seccion->timg2 = 'Sin imagen';
        } 
        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
        if (request()->hasFile('timg3')){
        $image3 = $request->file('timg3');
        $new_name3 = date('dmY-Hisu') . '-' . $image3->getClientOriginalName();
        $image3->move(public_path('/storage'), $new_name3);
        $seccion->timg3 = $new_name3;
        }
         else
        {
            $seccion->timg3 = 'Sin imagen';
        }
        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
        if (request()->hasFile('timg4')){
        $image4 = $request->file('timg4');
        $new_name4 = date('dmY-Hisu') . '-' . $image4->getClientOriginalName();
        $image4->move(public_path('/storage'), $new_name4);
        $seccion->timg4 = $new_name4;
        }
         else
        {
            $seccion->timg4 = 'Sin imagen';
        }
        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
        if (request()->hasFile('timg5')){
        $image5 = $request->file('timg5');
        $new_name5 = date('dmY-Hisu') . '-' . $image5->getClientOriginalName();
        $image5->move(public_path('/storage'), $new_name5);
        $seccion->timg5 = $new_name5;
        }
         else
        {
            $seccion->timg5 = 'Sin imagen';
        }
      
         if (request()->hasFile('eimg1')){
        $image1 = $request->file('eimg1');
        $new_name1 = date('dmY-Hisu') . '-' . $image1->getClientOriginalName();
        $image1->move(public_path('/storage'), $new_name1);
        $seccion->eimg1 = $new_name1;
        }
         else
        {
            $seccion->eimg1 = 'Sin imagen';
        } 

        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
        if (request()->hasFile('eimg2')){
        $image2 = $request->file('eimg2');
        $new_name2 = date('dmY-Hisu') . '-' . $image2->getClientOriginalName();
        $image2->move(public_path('/storage'), $new_name2);
        $seccion->eimg2 = $new_name2;
        }
         else
        {
            $seccion->eimg2 = 'Sin imagen';
        } 
        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
        if (request()->hasFile('eimg3')){
        $image3 = $request->file('eimg3');
        $new_name3 = date('dmY-Hisu') . '-' . $image3->getClientOriginalName();
        $image3->move(public_path('/storage'), $new_name3);
        $seccion->eimg3 = $new_name3;
        }
         else
        {
            $seccion->eimg3 = 'Sin imagen';
        }
        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
        if (request()->hasFile('eimg4')){
        $image4 = $request->file('eimg4');
        $new_name4 = date('dmY-Hisu') . '-' . $image4->getClientOriginalName();
        $image4->move(public_path('/storage'), $new_name4);
        $seccion->eimg4 = $new_name4;
        }
         else
        {
            $seccion->eimg4 = 'Sin imagen';
        }
        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
        if (request()->hasFile('eimg5')){
        $image5 = $request->file('eimg5');
        $new_name5 = date('dmY-Hisu') . '-' . $image5->getClientOriginalName();
        $image5->move(public_path('/storage'), $new_name5);
        $seccion->eimg5 = $new_name5;
        }
         else
        {
            $seccion->eimg5 = 'Sin imagen';
        }

   if (request()->hasFile('uimg1')){
        $image1 = $request->file('uimg1');
        $new_name1 = date('dmY-Hisu') . '-' . $image1->getClientOriginalName();
        $image1->move(public_path('/storage'), $new_name1);
        $seccion->uimg1 = $new_name1;
        }
         else
        {
            $seccion->uimg1 = 'Sin imagen';
        } 

        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
        if (request()->hasFile('uimg2')){
        $image2 = $request->file('uimg2');
        $new_name2 = date('dmY-Hisu') . '-' . $image2->getClientOriginalName();
        $image2->move(public_path('/storage'), $new_name2);
        $seccion->uimg2 = $new_name2;
        }
         else
        {
            $seccion->uimg2 = 'Sin imagen';
        } 
        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
        if (request()->hasFile('uimg3')){
        $image3 = $request->file('uimg3');
        $new_name3 = date('dmY-Hisu') . '-' . $image3->getClientOriginalName();
        $image3->move(public_path('/storage'), $new_name3);
        $seccion->uimg3 = $new_name3;
        }
         else
        {
            $seccion->uimg3 = 'Sin imagen';
        }
        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
        if (request()->hasFile('uimg4')){
        $image4 = $request->file('uimg4');
        $new_name4 = date('dmY-Hisu') . '-' . $image4->getClientOriginalName();
        $image4->move(public_path('/storage'), $new_name4);
        $seccion->uimg4 = $new_name4;
        }
         else
        {
            $seccion->uimg4 = 'Sin imagen';
        }
        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
        if (request()->hasFile('uimg5')){
        $image5 = $request->file('uimg5');
        $new_name5 = date('dmY-Hisu') . '-' . $image5->getClientOriginalName();
        $image5->move(public_path('/storage'), $new_name5);
        $seccion->uimg5 = $new_name5;
        }
         else
        {
            $seccion->uimg5 = 'Sin imagen';
        }


        $seccion->save(); /* save en db al objeto con los valores*/

        //SeccionHome::create($request->all());
        return redirect('admin/seccion-fichaproyecto')->with('mensaje','Sección ficha proyecto creada con éxito.'); 
    }

    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editar($id)
    {
        $data = SeccionFichaProyecto::find($id);
        return view('admin.seccion-fichaproyecto.editar', compact('data', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function actualizar(ValidacionSeccionFichaProyecto $request, $id)
    {

             $this->validate($request, [
            'modelo_id' =>'required',
            'proyecto_id' =>'required',
            'terminaciones'    =>  'required',
            'equipamiento'     =>  'required',
            'ubicacion_entorno'      =>  'required',
           
        ]);

        /* Validación para que la imagen sólo sea en estos formatos y campo requerido. Quitar gif si es necesario*/
        $this->validate($request, [
            
            'timg1'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'timg2'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'timg3'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'timg4'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'timg5'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'eimg1'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'eimg2'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'eimg3'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'eimg4'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'eimg5'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'uimg1'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'uimg2'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'uimg3'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'uimg4'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'uimg5'  => 'image|mimes:jpg,png,gif,jpeg|max:2048'

        ]);

        $data = SeccionFichaProyecto::find($id);
         $data->modelo_id = $request->get('modelo_id');
         $data->proyecto_id = $request->get('proyecto_id');
        $data->terminaciones = $request->get('terminaciones');
        $data->equipamiento = $request->get('equipamiento');
        $data->ubicacion_entorno = $request->get('ubicacion_entorno');

     
        if (request()->hasFile('timg1')){
        $image1 = $request->file('timg1');
        $new_name1 = date('dmY-Hisu') . '-' . $image1->getClientOriginalName();
        $image2->move(public_path('/storage'), $new_name1);
        $data->timg1 = $new_name1;
        }

        if (request()->hasFile('timg2')){
        $image2 = $request->file('timg2');
        $new_name2 = date('dmY-Hisu') . '-' . $image2->getClientOriginalName();
        $image2->move(public_path('/storage'), $new_name2);
        $data->timg2 = $new_name2;
        }
        if (request()->hasFile('timg3')){
        $image3 = $request->file('timg3');
        $new_name3 = date('dmY-Hisu') . '-' . $image3->getClientOriginalName();
        $image3->move(public_path('/storage'), $new_name3);
        $data->timg3 = $new_name3;
        }
        if (request()->hasFile('timg4')){
        $image4 = $request->file('timg4');
        $new_name4 = date('dmY-Hisu') . '-' . $image4->getClientOriginalName();
        $image4->move(public_path('/storage'), $new_name4);
        $data->timg4 = $new_name4;
        }
        if (request()->hasFile('timg5')){
        $image5 = $request->file('timg5');
        $new_name5 = date('dmY-Hisu') . '-' . $image5->getClientOriginalName();
        $image5->move(public_path('/storage'), $new_name5);
        $data->timg5 = $new_name5;
        }
                if (request()->hasFile('eimg1')){
        $image1 = $request->file('eimg1');
        $new_name1 = date('dmY-Hisu') . '-' . $image1->getClientOriginalName();
        $image2->move(public_path('/storage'), $new_name1);
        $data->eimg1 = $new_name1;
        }

        if (request()->hasFile('eimg2')){
        $image2 = $request->file('eimg2');
        $new_name2 = date('dmY-Hisu') . '-' . $image2->getClientOriginalName();
        $image2->move(public_path('/storage'), $new_name2);
        $data->eimg2 = $new_name2;
        }
        if (request()->hasFile('eimg3')){
        $image3 = $request->file('eimg3');
        $new_name3 = date('dmY-Hisu') . '-' . $image3->getClientOriginalName();
        $image3->move(public_path('/storage'), $new_name3);
        $data->eimg3 = $new_name3;
        }
        if (request()->hasFile('eimg4')){
        $image4 = $request->file('eimg4');
        $new_name4 = date('dmY-Hisu') . '-' . $image4->getClientOriginalName();
        $image4->move(public_path('/storage'), $new_name4);
        $data->eimg4 = $new_name4;
        }
        if (request()->hasFile('eimg5')){
        $image5 = $request->file('eimg5');
        $new_name5 = date('dmY-Hisu') . '-' . $image5->getClientOriginalName();
        $image5->move(public_path('/storage'), $new_name5);
        $data->eimg5 = $new_name5;
        }
                   if (request()->hasFile('uimg1')){
        $image1 = $request->file('timg1');
        $new_name1 = date('dmY-Hisu') . '-' . $image1->getClientOriginalName();
        $image2->move(public_path('/storage'), $new_name1);
        $data->uimg1 = $new_name1;
        }

        if (request()->hasFile('uimg2')){
        $image2 = $request->file('uimg2');
        $new_name2 = date('dmY-Hisu') . '-' . $image2->getClientOriginalName();
        $image2->move(public_path('/storage'), $new_name2);
        $data->uimg2 = $new_name2;
        }
        if (request()->hasFile('uimg3')){
        $image3 = $request->file('uimg3');
        $new_name3 = date('dmY-Hisu') . '-' . $image3->getClientOriginalName();
        $image3->move(public_path('/storage'), $new_name3);
        $data->uimg3 = $new_name3;
        }
        if (request()->hasFile('uimg4')){
        $image4 = $request->file('uimg4');
        $new_name4 = date('dmY-Hisu') . '-' . $image4->getClientOriginalName();
        $image4->move(public_path('/storage'), $new_name4);
        $data->uimg4 = $new_name4;
        }
        if (request()->hasFile('uimg5')){
        $image5 = $request->file('uimg5');
        $new_name5 = date('dmY-Hisu') . '-' . $image5->getClientOriginalName();
        $image5->move(public_path('/storage'), $new_name5);
        $data->uimg5 = $new_name5;
        }
        
        
        $data->save();

        //SeccionHome::findOrFail($id)->update($request->all());
        return redirect('admin/seccion-fichaproyecto')->with('mensaje', 'Sección Ficha Proyecto actualizada con éxito.');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function eliminar(Request $request, $id)
    {
        if ($request->ajax()) {
            if (SeccionFichaProyecto::destroy($id)) {
                return response()->json(['mensaje' => 'ok']);
            } else {
                return response()->json(['mensaje' => 'ng']);
            }
        } else {
            abort(404);
        }
    }
}
