<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\SeccionProyecto;
use App\Http\Requests\ValidacionSeccionProyecto;


class SeccionProyectossController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $datas = SeccionProyecto::orderBy('id')->get();
      return view('admin.seccion-proyectos.index', compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function crear()
    {
        return view('admin.seccion-proyectos.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function guardar(ValidacionSeccionProyecto $request)
    {
         $this->validate($request, [
            'titulo'    =>  'required',
            'titulo2'    =>  'required'
           
            //'comuna->nombre_comuna'      =>  'required'
        ]);

        /* Validación para que la imagen sólo sea en estos formatos y campo requerido. Quitar gif si es necesario*/
        $this->validate($request, [
            'img'      => 'image|mimes:jpg,png,gif,jpeg|max:2048'
           
        ]);

      

        $proyecto = new SeccionProyecto();
        $proyecto->titulo = $request->input('titulo');
        $proyecto->titulo2 = $request->input('titulo2');
       

        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
         if (request()->hasFile('img')){ 
        $image = $request->file('img');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $proyecto->img = $new_name;
        }
        else
        {
            $proyecto->img = 'Sin imagen';
        } 
        /*----*/
       
        $proyecto->save(); /* save en db al objeto con los valores*/ 
        return redirect('admin/seccion-proyectos')->with('mensaje','Seccion Proyecto creado con exito.');
    }

    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editar($id)
    {
        $data = SeccionProyecto::findOrFail($id);
        return view('admin.seccion-proyectos.editar', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function actualizar(ValidacionSeccionProyecto $request, $id)
    {
        $this->validate($request, [
            'titulo'    =>  'required',
            'titulo2'    =>  'required'
           
            
            //'comuna->nombre_comuna'      =>  'required'
        ]);

        $this->validate($request, [
            'img_home'      => 'image|mimes:jpg,png,gif,jpeg|max:2048'
            
        ]);

       

        $data = SeccionProyecto::find($id);
        $data->titulo = $request->get('titulo');
        $data->titulo2 = $request->get('titulo2');
       

        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
         if (request()->hasFile('img')){ 
        $image = $request->file('img');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $data->img = $new_name;
        }
       
        $data->save();
        return redirect('admin/seccion-proyectos')->with('mensaje', 'Seccion Proyecto actualizado con exito');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function eliminar(Request $request, $id)
    {
        if ($request->ajax()) {
            if (SeccionProyecto::destroy($id)) {
                return response()->json(['mensaje' => 'ok']);
            } else {
                return response()->json(['mensaje' => 'ng']);
            }
        } else {
            abort(404);
        }
    }
}
