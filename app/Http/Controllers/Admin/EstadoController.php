<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ValidacionEstado;
use App\Models\Admin\EstadoProyecto;
use Illuminate\Http\Request;

class EstadoController extends Controller
{
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   public function index()
   {
     $datas = EstadoProyecto::orderBy('id')->get();
     return view('admin.estado.index', compact('datas'));
 }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function crear()
    {
        return view('admin.estado.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function guardar(ValidacionEstado $request)
    {
        EstadoProyecto::create($request->all()); 
        return redirect('admin/estado')->with('mensaje','Estado creado con exito.');
    }

    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editar($id)
    {
        $data = EstadoProyecto::findOrFail($id);
        return view('admin.estado.editar', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function actualizar(ValidacionEstado $request, $id)
    {
        EstadoProyecto::findOrFail($id)->update($request->all());
        return redirect('admin/estado')->with('mensaje', 'Estado actualizado con exito');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function eliminar($id)
    {



       try {
            //User::where('id', $id)->delete();
         $estado_proyecto = EstadoProyecto::find($id); 
         $estado_proyecto -> delete();
        return redirect('admin/estado')->with('mensaje','Eliminado');



         return redirect()->intended('admin');
     } catch (\Illuminate\Database\QueryException $e) {
        return redirect('admin/estado')->with('error','No es posible borrar estado, ya que se utiliza en algún proyecto');
    }
}

}
