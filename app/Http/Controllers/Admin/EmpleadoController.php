<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ValidacionEmpleado;
use App\Models\Admin\Empleado;
use Illuminate\Http\Request;

class EmpleadoController extends Controller
{
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $datas = Empleado::orderBy('id')->get();
      return view('admin.empleado.index', compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function crear()
    {
        return view('admin.empleado.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function guardar(ValidacionEmpleado $request)
    {
        $this->validate($request, [
            'nombre_empleado'    =>  'required',
            'cargo_id'      =>  'required',
            'telefono'      =>  'required',
            'correo'      =>  'required',
           
           
        ]);

        /* Validación para que la imagen sólo sea en estos formatos y campo requerido. Quitar gif si es necesario*/
        $this->validate($request, [
            'img'      => 'image|mimes:jpg,png,gif,jpeg|max:2048'
        ]);

      

        $empleado = new Empleado();
        $empleado->nombre_empleado = $request->input('nombre_empleado');
        $empleado->cargo_id = $request->input('cargo_id');
        $empleado->telefono = $request->input('telefono');
        $empleado->correo = $request->input('correo');
        

        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
         if (request()->hasFile('img')){ 
        $image = $request->file('img');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $empleado->img = $new_name;
        }
        else
        {
            $empleado->img = 'Sin imagen';
        } 
      
        $empleado->save(); /* save en db al objeto con los valores*/
        return redirect('admin/empleado')->with('mensaje','Empleado creado con exito.');
    }

    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editar($id)
    {
        $data = Empleado::findOrFail($id);
        return view('admin.empleado.editar', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function actualizar(ValidacionEmpleado $request, $id)
    {
          $this->validate($request, [
            'nombre_empleado'    =>  'required',
            'cargo_id'      =>  'required',
            'telefono'      =>  'required',
            'correo'      =>  'required',
            
           
        ]);

        /* Validación para que la imagen sólo sea en estos formatos y campo requerido. Quitar gif si es necesario*/
        $this->validate($request, [
            'img'      => 'image|mimes:jpg,png,gif,jpeg|max:2048'
        ]);


        $data = Empleado::find($id);
        $data->nombre_empleado = $request->get('nombre_empleado');
        $data->cargo_id = $request->get('cargo_id');
        $data->telefono = $request->get('telefono');
        $data->correo = $request->get('correo');
        

        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
         if (request()->hasFile('img')){ 
        $image = $request->file('img');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $data->img = $new_name;
        }
      
       
        $data->save();
        return redirect('admin/empleado')->with('mensaje', 'Empleado actualizado con exito');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function eliminar(Request $request, $id)
    {
        if ($request->ajax()) {
            if (Empleado::destroy($id)) {
                return response()->json(['mensaje' => 'ok']);
            } else {
                return response()->json(['mensaje' => 'ng']);
            }
        } else {
            abort(404);
        }
    }
}
