<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ValidacionAsunto;
use App\Models\Admin\Asunto;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AsuntoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $datas = Asunto::orderBy('id')->get();
      return view('admin.asunto.index', compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function crear()
    {
        return view('admin.asunto.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function guardar(ValidacionAsunto $request)
    {
        $valor=Asunto::create($request->all());
        
       return redirect('admin/asunto')->with('mensaje','Asunto creado con exito.');
    }

    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editar($id)
    {
        $data = Asunto::findOrFail($id);
        return view('admin.asunto.editar', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function actualizar(ValidacionAsunto $request, $id)
    {
        Asunto::findOrFail($id)->update($request->all());
        return redirect('admin/asunto')->with('mensaje', 'Asunto actualizado con exito');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function eliminar(Request $request, $id)
    {
        if ($request->ajax()) {
            if (Asunto::destroy($id)) {
                return response()->json(['mensaje' => 'ok']);
            } else {
                return response()->json(['mensaje' => 'ng']);
            }
        } else {
            abort(404);
        }
    }
}
