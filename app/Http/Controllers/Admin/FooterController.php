<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\Footer;
use Illuminate\Http\Request;

class FooterController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $datas = Footer::orderBy('id')->get();
      return view('admin.footer.index', compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function crear()
    {
        return view('admin.footer.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function guardar(Request $request)
    {
        $this->validate($request, [
            'direccion'      =>  'required',
            'telefono'      =>  'required',
            'correo'      =>  'required',
            'link_face'      =>  'required',
            'link_instagram'      =>  'required',
            'link_twitter'      =>  'required'
           
        ]);

        /* Validación para que la imagen sólo sea en estos formatos y campo requerido. Quitar gif si es necesario*/
        $this->validate($request, [
            'img_footer'      => 'image|mimes:jpg,png,gif,jpeg|max:2048'
        ]);

      

        $footer = new Footer();
        if (request()->hasFile('img_footer')){ 
        $image = $request->file('img_footer');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $footer->img_footer = $new_name;
        }
        else
        {
            $footer->img_footer = 'Sin imagen';
        } 
        $footer->direccion = $request->input('direccion');
        $footer->telefono = $request->input('telefono');
        $footer->correo = $request->input('correo');
        $footer->link_face = $request->input('link_face');
        $footer->link_instagram = $request->input('link_instagram');
        $footer->link_twitter = $request->input('link_twitter');
        $footer->save(); /* save en db al objeto con los valores*/
        return redirect('admin/footer')->with('mensaje','Footer creado con exito.');
    }

    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editar($id)
    {
        $data = Footer::findOrFail($id);
        return view('admin.footer.editar', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function actualizar(Request $request,$id)
    {
        $this->validate($request, [
            'direccion'      =>  'required',
            'telefono'      =>  'required',
            'correo'      =>  'required',
            'link_face'      =>  'required',
            'link_instagram'      =>  'required',
            'link_twitter'      =>  'required'
           
        ]);

        /* Validación para que la imagen sólo sea en estos formatos y campo requerido. Quitar gif si es necesario*/
        $this->validate($request, [
            'img_footer'      => 'image|mimes:jpg,png,gif,jpeg|max:2048'
        ]);


        $data = Footer::find($id);
        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
         if (request()->hasFile('img')){ 
        $image = $request->file('img');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $data->img = $new_name;
        }
        $data->direccion = $request->get('direccion');
        $data->telefono = $request->get('telefono');
         $data->correo = $request->get('correo');
        $data->link_face = $request->get('link_face');
        $data->link_instagram = $request->get('link_instagram');
        $data->link_twitter = $request->get('link_twitter');
        $data->save();
        return redirect('admin/footer')->with('mensaje', 'Footer actualizado con exito');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function eliminar(Request $request, $id)
    {
        if ($request->ajax()) {
            if (Footer::destroy($id)) {
                return response()->json(['mensaje' => 'ok']);
            } else {
                return response()->json(['mensaje' => 'ng']);
            }
        } else {
            abort(404);
        }
    }
}
