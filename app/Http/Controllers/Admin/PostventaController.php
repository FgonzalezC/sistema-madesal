<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Admin\SeccionPostventa;
use App\Http\Requests\ValidacioPostventa;
use App\Http\Controllers\Controller;



class PostventaController extends Controller
{
    public function index()
    {
       $datas = SeccionPostventa::orderBy('id')->get();
      return view('admin.postventa.index', compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function crear()
    {

        return view('admin.postventa.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function guardar(Request $request)
    {
       $this->validate($request, [
            'titulo'    =>  'required',      
            'titulo1'      =>  'required',
            'btn1'      =>  'required',
            'btn2'      =>  'required',
            'btn3'      =>  'required',
            'btn4'      =>  'required',
            'btn5'      =>  'required',
            'btn6'      =>  'required',
            'titulo2'      =>  'required',
            'btn7'      =>  'required',
            'btn8'      =>  'required',
            'btn9'      =>  'required',
            'titulo3'      =>  'required'
           
        ]);
        $this->validate($request, [
            'img1'      => 'image|mimes:jpg,png,gif,jpeg|max:2048'
            
        ]);
   

    

        $proyecto = new SeccionPostventa();
        $proyecto->titulo = $request->input('titulo');

        if (request()->hasFile('img1')){ 
            $image = $request->file('img1');
            $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
            $image->move(public_path('/storage'), $new_name);
            $proyecto->img1 = $new_name;
            }
            else
            {
                $proyecto->img1 = 'Sin imagen';
            } 

        $proyecto->titulo1 = $request->input('titulo1');
        $proyecto->btn1 = $request->input('btn1');
        $proyecto->btn2 = $request->input('btn2');
        $proyecto->btn3 = $request->input('btn3');      
        $proyecto->btn4 = $request->input('btn4');
        $proyecto->btn5 = $request->input('btn5');
        $proyecto->btn6 = $request->input('btn6');
        $proyecto->titulo2 = $request->input('titulo2');
        $proyecto->btn7 = $request->input('btn7');
        $proyecto->btn8 = $request->input('btn8');
        $proyecto->btn9 = $request->input('btn9');
        $proyecto->titulo3 = $request->input('titulo3');
        $proyecto->save();

     
        return redirect('admin/postventa')->with('mensaje','Postventa creada con exito.');
    }

    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editar($id)
    {
        $data = SeccionPostventa::findOrFail($id);
        return view('admin.postventa.editar', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function actualizar(Request $request, $id)
    {
       
        $this->validate($request, [
            'titulo'    =>  'required',         
           
            'titulo1'      =>  'required',
            'btn1'      =>  'required',
            'btn2'      =>  'required',
            'btn3'      =>  'required',
            'btn4'      =>  'required',
            'btn5'      =>  'required',
            'btn6'      =>  'required',
            'titulo2'      =>  'required',
          
            'btn7'      =>  'required',
            'btn8'      =>  'required',
            'btn9'      =>  'required',
            'titulo3'      =>  'required'
        ]);

        $this->validate($request, [
            'img1'      => 'image|mimes:jpg,png,gif,jpeg|max:2048'
            
        ]);

      

        $data = SeccionPostventa::find($id);
        $data->titulo = $request->input('titulo');

        if (request()->hasFile('img1')){ 
            $image = $request->file('img1');
            $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
            $image->move(public_path('/storage'), $new_name);
            $data->img1 = $new_name;
            }
            else
            {
                $data->img1 = 'Sin imagen';
            } 

        $data->titulo1 = $request->input('titulo1');
        $data->btn1 = $request->input('btn1');
        $data->btn2 = $request->input('btn2');
        $data->btn3 = $request->input('btn3');      
        $data->btn4 = $request->input('btn4');
        $data->btn5 = $request->input('btn5');
        $data->btn6 = $request->input('btn6');
        $data->titulo2 = $request->input('titulo2');
      
        $data->btn7 = $request->input('btn7');
        $data->btn8 = $request->input('btn8');
        $data->btn9 = $request->input('btn9');
        $data->titulo3 = $request->input('titulo3');
        $data->save();

        return redirect('admin/postventa')->with('mensaje', 'Proyecto actualizado con exito');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function eliminar(Request $request, $id)
    {
        if ($request->ajax()) {
            if (SeccionPostventa::destroy($id)) {
                return response()->json(['mensaje' => 'ok']);
            } else {
                return response()->json(['mensaje' => 'ng']);
            }
        } else {
            abort(404);
        }
    }

  
   
}
