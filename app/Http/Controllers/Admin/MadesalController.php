<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Http\Requests\ValidacionMadesal;
use App\Models\Admin\Madesal;
use Illuminate\Http\Request;

class MadesalController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $datas = Madesal::orderBy('id')->get();
      return view('admin.madesal.index', compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function crear()
    {

        return view('admin.madesal.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function guardar(ValidacionMadesal $request)
    {
        $this->validate($request, [
            'titulo'    =>  'required',
            'titulo1'    =>  'required',
            'bajada1'    =>  'required',
            'bajada2'    =>  'required',
           // 'bajada3'    =>  'required',
            'titulo2'    =>  'required',
            'bajada4'    =>  'required'          
        ]);

        /* Validación para que la imagen sólo sea en estos formatos y campo requerido. Quitar gif si es necesario*/
        $this->validate($request, [
            'img'   => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'img1'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'img2'  => 'image|mimes:jpg,png,gif,jpeg|max:2048'
        ]);

      

        $madesal = new Madesal();
        $madesal->titulo = $request->input('titulo');
        $madesal->titulo1 = $request->input('titulo1');
        $madesal->bajada2 = $request->input('bajada2');
        $madesal->bajada3 = $request->input('bajada3');
        $madesal->bajada1 = $request->input('bajada1');
        $madesal->titulo2 = $request->input('titulo2');
        $madesal->bajada4 = $request->input('bajada4');

       // dd($request);

        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
         if (request()->hasFile('img')){ 
        $image = $request->file('img');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $madesal->img = $new_name;
        }
        else
        {
            $madesal->img = 'Sin imagen';
        } 
        /*----*/

        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
         if (request()->hasFile('img1')){ 
        $image = $request->file('img1');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $madesal->img1 = $new_name;
        }
        else
        {
            $madesal->img1 = 'Sin imagen';
        } 

        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
         if (request()->hasFile('img2')){ 
        $image = $request->file('img2');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $madesal->img2 = $new_name;
        }
        else
        {
            $madesal->img2 = 'Sin imagen';
        }

        $madesal->save();
     //  Proyecto::create($request->all());
        return redirect('admin/madesal')->with('mensaje','Sesscion Madesal creado con exito.');
    }

    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editar($id)
    {
        $data = Madesal::findOrFail($id);
        return view('admin.madesal.editar', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function actualizar(ValidacionMadesal $request, $id)
    {
       
        $this->validate($request, [
            'titulo'    =>  'required',
            'titulo1'    =>  'required',
            'bajada1'    =>  'required',
            'bajada2'    =>  'required',
       //     'bajada3'    =>  'required',
            'titulo2'    =>  'required',
            'bajada4'    =>  'required'
        ]);

        $this->validate($request, [
            'img'      => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'img1'  => 'image|mimes:jpg,png,gif,jpeg|max:2048',
            'img2'  => 'image|mimes:jpg,png,gif,jpeg|max:2048'
            
        ]);

        $data = Madesal::find($id);
        $data->titulo = $request->get('titulo');
        $data->titulo1 = $request->get('titulo1');
        $data->bajada1 = $request->get('bajada1');
        $data->bajada2 = $request->get('bajada2');
        $data->bajada3 = $request->get('bajada3');
        $data->titulo2 = $request->get('titulo2');
        $data->bajada4 = $request->get('bajada4');

        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
         if (request()->hasFile('img')){ 
        $image = $request->file('img');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $data->img = $new_name;
        }
        /*------*/

        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
         if (request()->hasFile('img1')){ 
        $image = $request->file('img1');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $data->img1 = $new_name;
        }

        /* Guarda imagen en 'public_path' y cambia el nombre para evitar sobreescritura  */
         if (request()->hasFile('img2')){ 
        $image = $request->file('img2');
        $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
        $image->move(public_path('/storage'), $new_name);
        $data->img2 = $new_name;
        }

       
        $data->save();

       // Proyecto::findOrFail($id)->update($request->all());
        return redirect('admin/madesal')->with('mensaje', 'Seccion Madesal actualizado con exito');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function eliminar(Request $request, $id)
    {
        if ($request->ajax()) {
            if (Madesal::destroy($id)) {
                return response()->json(['mensaje' => 'ok']);
            } else {
                return response()->json(['mensaje' => 'ng']);
            }
        } else {
            abort(404);
        }
    }

}
