<?php

namespace App\Http\Controllers;

use App\Models\Admin\Formulario_postventas;
use Illuminate\Http\Request;
use App\Models\Admin\Formulario_contacto;
use Mail;




class PostventasController extends Controller
{
    public function index(Request $request)
    {
          $postventas = \DB::table('postventas')
            
             ->get();
        $data = array('postventas' => $postventas); 
        return \View::make('/postventas')->with($data, $postventas);
    }

    public function guardar2(Request $request)
    {
       $this->validate($request, [
            'nombre'    =>  'max:60',      
            'apellido'      => 'max:60',
            'rut'      =>  'max:12',
            'telefono'      =>  'max:12',
            'mail'      =>  'email',                  
            'direccion'      =>  'max:60'
           
        ]);
        $this->validate($request, [
            'imagen'      => 'image|mimes:jpg,png,gif,jpeg|max:2048'
            
        ]);
   

    

        $formulario = new  Formulario_postventas();
        $formulario->nombre = $request->input('nombre');
        $formulario->apellido = $request->input('apellido');
        $formulario->rut = $request->input('rut');
        $formulario->telefono = $request->input('telefono');
        $formulario->mail = $request->input('mail');
        $formulario->proyecto_id = $request->input('proyecto_id');
        $formulario->direccion = $request->input('direccion');



       


        if (request()->hasFile('imagen')){ 
            $image = $request->file('imagen');
            $new_name = date('dmY-Hisu') . '-' . $image->getClientOriginalName();
            $image->move(public_path('/storage'), $new_name);
            $formulario->imagen = $new_name;
            }
            else
            {
                $formulario->imagen = 'Sin imagen';
            } 


        $formulario->mensaje = $request->input('mensaje');
    
        $formulario->check1 = $request->input('techo');
        $formulario->check2 = $request->input('electricidad');
        $formulario->check3 = $request->input('materiales');

        $formulario->check4 = $request->input('paredes');
        $formulario->check5 = $request->input('piso');
        $formulario->check6 = $request->input('cloacas');
        $formulario->save();

        $public_path = public_path();
        $url = $public_path.'/storage/'.$formulario->imagen;
  



        $data = array('nombre' => $formulario->nombre,
            'apellido' => $formulario->apellido,
            'rut' => $formulario->rut,
            'telefono' => $formulario->telefono,
            'mail' => $formulario->mail,
            'proyecto_id' => $formulario->proyecto_id,
            'direccion' => $formulario->direccion,  
            'imagen' =>  $formulario->imagen,        
            'imagen2' => $url,          
            'check1'=> $formulario->check1,
            'check2'=> $formulario->check2,
            'check3'=> $formulario->check3,
            'check4'=> $formulario->check4,
            'check5'=> $formulario->check5,
            'check6'=> $formulario->check6,
            'mensaje'=> $formulario->mensaje);



      
      Mail::send('mail_postventa',$data,function($msj) {

           $msj->from('contacto@madesal.cl');
            $msj->subject('Sirena - Email Estándar');
            $msj->to('madesal-test@leads.getsirena.com');
            $msj->cc('contacto@madesal.cl');     
              
 
        });

         Mail::send('mail_postventa',$data,function($msj) use ($url) {

            $msj->from('contacto@madesal.cl');
            $msj->subject('Sirena - Email Estándar');
            $msj->to('madesal-test@leads.getsirena.com');
            $msj->cc('contacto@madesal.cl');
           $msj->attach($url);

           
       

        });



        if(!$formulario->save() and Mail::failures() ){

         return back()->with('msj2','Datos sin éxito'); 


        }
       else{          

       
            return back()->with('msj','Datos enviado con éxito'); 


        }
   
    }

    
    public function guardar3(Request $request)
    {
    
      
            $this->validate($request, [
            'nombre'    =>  'max:60max:60',      
            'apellido'      =>  'max:60',
            'telefono'      =>  'max:12',
            'email'      =>  'email'
        ]);
      
        $formulario2 = new  Formulario_contacto();
        $formulario2->nombre = $request->input('nombre');
        $formulario2->apellido = $request->input('apellido');
        $formulario2->telefono = $request->input('telefono');
        $formulario2->email = $request->input('email');
        $formulario2->asunto_id = $request->input('asunto_id');
        $formulario2->mensaje = $request->input('mensaje');   

        $formulario2->check1 = $request->input('gas-fuga');
        $formulario2->check2 = $request->input('caneria');
        $formulario2->check3 = $request->input('calefont');    
         $formulario2->save();




           $data = array(
            'nombre' => $formulario2->nombre,
            'apellido' => $formulario2->apellido,           
            'telefono' => $formulario2->telefono,
            'email' => $formulario2->email,
            'asunto_id' => $formulario2->asunto_id,
            'mensaje'=> $formulario2->mensaje,         
            'check1'=> $formulario2->check1,
            'check2'=> $formulario2->check2,
            'check3'=> $formulario2->check3
           
            );


          Mail::send('mail_formulario_contacto',$data,function($msj)  {

           $msj->from('madesal.prueba@gmail.com');       
           $msj->subject('Sirena - Email Estándar');
           $msj->to('madesal-test@leads.getsirena.com');
           $msj->to('postventa@madesal.cl');
      
            

        });

         if(!$formulario2->save() and Mail::failures() ){

         return back()->with('msj2','Datos sin éxito'); 


        }
       else{          

       
            return back()->with('msj','Datos enviado con éxito'); 


        }
      
    }

}
