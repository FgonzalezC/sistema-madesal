<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidacionContacto extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'rut_persona'=>'required|max:50|unique:contactos,rut_persona,' . $this->route('id'),
            'nombre_persona'=>'required|max:50|unique:contactos,nombre_persona,' . $this->route('id'),
            'apellido_persona'=>'required|max:50|unique:contactos,apellido_persona,' . $this->route('id'),
            'mensaje'=>'required|max:50|unique:contactos,mensaje,' . $this->route('id'),
        ];
    }
}
