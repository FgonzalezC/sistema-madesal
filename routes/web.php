<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/welcome','HomeController@index')->name('welcome');

Route::get('/','ProyectosController@destacados')->name('home');
Route::get('/inicio','InicioController@destacados')->name('inicio');
/*Vistas Principales*/
Route::get('/home','ProyectosController@destacados')->name('home');
Route::get('/proyecto','ProyectosController@index')->name('proyecto');
Route::get('/ficha-proyecto/{id}', 'ProyectosController@ficha')->name('ficha-proyecto');
Route::get('/ficha-proyecto_modelo/{id}/{proyecto_id}', 'ProyectosController@ficha_modelo')->name('ficha-proyecto_modelo');
Route::get('/contacto','ContactosController@index')->name('contacto');
Route::post('/contacto', 'ContactosController@index2')->name('contacto_guardar');
Route::get('/inversionista','InversionistaController@index')->name('inversionista');
Route::post('/inversionista', 'InversionistaController@index2')->name('inversionista_guardar');
Route::get('/inversionista2', 'ProyectosController@buscar')->name('buscar');
Route::get('/proyecto2', 'ProyectosController@filtromovil')->name('proyecto2');

Route::get('/financiamiento','ProcesoComprasController@financiamiento')->name('financiamiento');
Route::get('/asesoria','ProcesoComprasController@asesoramos')->name('asesoria');
Route::get('/madesal','MadesalsController@index')->name('madesal');
Route::get('/postventas','PostventasController@index')->name('postventas');
Route::post('/postventas', 'PostventasController@guardar2')->name('postventas_guardar');
Route::get('/simuladores', 'SimuladoresController@index')->name('simuladores');

Route::get('/ficha_exportarpdf,{id}', 'ExportpdfController@ficha_exportarpdf')->name('ficha_exportarpdf');
Route::get('/ficha_modelo_exportarpdf,{id},{proyecto_id}', 'ExportpdfController@ficha_modelo_exportarpdf')->name('ficha_modelo_exportarpdf');
Route::post('/postventas/guardar3', 'PostventasController@guardar3'); 

Route::post('/financiamiento', 'ProcesoComprasController@index2')->name('financiamiento_guardar');
Route::post('/contacto', 'ContactosController@index2')->name('contacto_guardar');
Route::post('/simuladores', 'SimuladoresController@index2')->name('simulador_guardar');
Route::post('/simuladores/simulador_guardar2', 'SimuladoresController@simulador_guardar2'); 
Route::post('/asesoria', 'ProcesoComprasController@index3')->name('asesoria_guardar');
Route::post('/madesal', 'MadesalsController@index4')->name('madesal_guardar');






Route::get('seguridad/login','Seguridad\LoginController@index')->name('login');
Route::post('seguridad/login','Seguridad\LoginController@login')->name('login_post');
Route::get('seguridad/logout','Seguridad\LoginController@logout')->name('logout');

    Route::group(['prefix'=>'admin','namespace'=>'Admin','middleware' =>['auth','master']], function(){
    Route::get('','AdminController@index');
	
	 /*RUTAS DEL MENU*/
    Route::get('menu', 'MenuController@index')->name('menu');
    Route::get('menu/crear', 'MenuController@crear')->name('crear_menu');
    Route::post('menu', 'MenuController@guardar')->name('guardar_menu');
    Route::get('menu/{id}/editar', 'MenuController@editar')->name('editar_menu');
    Route::put('menu/{id}', 'MenuController@actualizar')->name('actualizar_menu');
    Route::get('menu/{id}/eliminar', 'MenuController@eliminar')->name('eliminar_menu');
    Route::post('menu/guardar-orden', 'MenuController@guardarOrden')->name('guardar_orden');
     /*RUTAS DE PERMISO*/
    Route::get('permiso', 'PermisoController@index')->name('permiso');
    Route::get('permiso/crear', 'PermisoController@crear')->name('crear_permiso');
    Route::post('permiso', 'PermisoController@guardar')->name('guardar_permiso');
    Route::get('permiso/{id}/editar', 'PermisoController@editar')->name('editar_permiso');
    Route::put('permiso/{id}', 'PermisoController@actualizar')->name('actualizar_permiso');
    Route::delete('permiso/{id}', 'PermisoController@eliminar')->name('eliminar_permiso');
	/*Rutas Rol*/
    Route::get('rol', 'RolController@index')->name('rol');
    Route::get('rol/crear', 'RolController@crear')->name('crear_rol');
    Route::post('rol', 'RolController@guardar')->name('guardar_rol');
    Route::get('rol/{id}/editar', 'RolController@editar')->name('editar_rol');
    Route::put('rol/{id}', 'RolController@actualizar')->name('actualizar_rol');
    Route::delete('rol/{id}', 'RolController@eliminar')->name('eliminar_rol');
        /*Rutas Sucursal*/
    Route::get('sucursal', 'SucursalController@index')->name('sucursal');
    Route::get('sucursal/crear', 'SucursalController@crear')->name('crear_sucursal');
    Route::post('sucursal', 'SucursalController@guardar')->name('guardar_sucursal');
    Route::get('sucursal/{id}/editar', 'SucursalController@editar')->name('editar_sucursal');
    Route::put('sucursal/{id}', 'SucursalController@actualizar')->name('actualizar_sucursal');
    Route::delete('sucursal/{id}', 'SucursalController@eliminar')->name('eliminar_sucursal');
    /*Rutas TipoIngreso*/
    Route::get('tipoingreso', 'TipoIngresoController@index')->name('tipoingreso');
    Route::get('tipoingreso/crear', 'TipoIngresoController@crear')->name('crear_tipoingreso');
    Route::post('tipoingreso', 'TipoIngresoController@guardar')->name('guardar_tipoingreso');
    Route::get('tipoingreso/{id}/editar', 'TipoIngresoController@editar')->name('editar_tipoingreso');
    Route::put('tipoingreso/{id}', 'TipoIngresoController@actualizar')->name('actualizar_tipoingreso');
    Route::delete('tipoingreso/{id}', 'TipoIngresoController@eliminar')->name('eliminar_tipoingreso');
     /*Rutas ProyectoEmpleado*/
    Route::get('proyectoejecutivo', 'ProyectoEjecutivoController@index')->name('proyectoejecutivo');
    Route::get('proyectoejecutivo/crear', 'ProyectoEjecutivoController@crear')->name('crear_proyectoejecutivo');
    Route::post('proyectoejecutivo', 'ProyectoEjecutivoController@guardar')->name('guardar_proyectoejecutivo');
    Route::get('proyectoejecutivo/{id}/editar', 'ProyectoEjecutivoController@editar')->name('editar_proyectoejecutivo');
    Route::put('proyectoejecutivo/{id}', 'ProyectoEjecutivoController@actualizar')->name('actualizar_proyectoejecutivo');
    Route::delete('proyectoejecutivo/{id}', 'ProyectoEjecutivoController@eliminar')->name('eliminar_proyectoejecutivo');
    /*Rutas Simuladores*/
    
    Route::get('simulador', 'SimuladorController@index')->name('simulador');
    Route::get('simulador/crear', 'SimuladorController@crear')->name('crear_simulador');
    Route::post('simulador', 'SimuladorController@guardar')->name('guardar_simulador');
    Route::get('simulador/{id}/editar', 'SimuladorController@editar')->name('editar_simulador');
    Route::put('simulador/{id}', 'SimuladorController@actualizar')->name('actualizar_simulador');
    Route::delete('simulador/{id}', 'SimuladorController@eliminar')->name('eliminar_simulador');
    
    /*Rutas Footer*/
    Route::get('footer', 'FooterController@index')->name('footer');
    Route::get('footer/crear', 'FooterController@crear')->name('crear_footer');
    Route::post('footer', 'FooterController@guardar')->name('guardar_footer');
    Route::get('footer/{id}/editar', 'FooterController@editar')->name('editar_footer');
    Route::put('footer/{id}', 'FooterController@actualizar')->name('actualizar_footer');
    Route::delete('footer/{id}', 'FooterController@eliminar')->name('eliminar_footer');
    /*Rutas Cargo*/
    Route::get('cargo', 'CargoController@index')->name('cargo');
    Route::get('cargo/crear', 'CargoController@crear')->name('crear_cargo');
    Route::post('cargo', 'CargoController@guardar')->name('guardar_cargo');
    Route::get('cargo/{id}/editar', 'CargoController@editar')->name('editar_cargo');
    Route::put('cargo/{id}', 'CargoController@actualizar')->name('actualizar_cargo');
    Route::delete('cargo/{id}', 'CargoController@eliminar')->name('eliminar_cargo');
    /*Rutas Menu_Rol*/
    Route::get('menu-rol','MenuRolController@index')->name('menu_rol');
    Route::post('menu-rol','MenuRolController@guardar')->name('guardar_menu_rol');
    /*RUTAS PERMISO_ROL*/
    Route::get('permiso-rol', 'PermisoRolController@index')->name('permiso_rol');
    Route::post('permiso-rol', 'PermisoRolController@guardar')->name('guardar_permiso_rol');
    /*Rutas Planta*/
    Route::get('planta', 'PlantaController@index')->name('planta');
    Route::get('planta/crear', 'PlantaController@crear')->name('crear_planta');
    Route::post('planta', 'PlantaController@guardar')->name('guardar_planta');
    Route::get('planta/{id}/editar', 'PlantaController@editar')->name('editar_planta');
    Route::put('planta/{id}', 'PlantaController@actualizar')->name('actualizar_planta');
    Route::delete('planta/{id}', 'PlantaController@eliminar')->name('eliminar_planta');



    //eliminar imagenes

    Route::get('eliminar_fotoplanta/{id}/{dato}', 'PlantaController@eliminar_fotoplanta')->name('eliminar_fotoplanta');
     Route::get('eliminar_fotoproyecto/{id}/{dato}', 'ProyectoController@eliminar_fotoproyecto')->name('eliminar_fotoproyecto');




    /*Rutas Rol*/
    Route::get('asunto', 'AsuntoController@index')->name('asunto');
    Route::get('asunto/crear', 'AsuntoController@crear')->name('crear_asunto');
    Route::post('asunto', 'AsuntoController@guardar')->name('guardar_asunto');
    Route::get('asunto/{id}/editar', 'AsuntoController@editar')->name('editar_asunto');
    Route::put('asunto/{id}', 'AsuntoController@actualizar')->name('actualizar_asunto');
    Route::delete('asunto/{id}', 'AsuntoController@eliminar')->name('eliminar_asunto');
      /*Rutas AsuntoCoti*/
    Route::get('asuntocoti', 'AsuntoCotiController@index')->name('asuntocoti');
    Route::get('asuntocoti/crear', 'AsuntoCotiController@crear')->name('crear_asuntocoti');
    Route::post('asuntocoti', 'AsuntoCotiController@guardar')->name('guardar_asuntocoti');
    Route::get('asuntocoti/{id}/editar', 'AsuntoCotiController@editar')->name('editar_asuntocoti');
    Route::put('asuntocoti/{id}', 'AsuntoCotiController@actualizar')->name('actualizar_asuntocoti');
    Route::delete('asuntocoti/{id}', 'AsuntoCotiController@eliminar')->name('eliminar_asuntocoti');
    
    /*Rutas Contacto*/
    Route::get('contacto', 'ContactoController@index')->name('contacto');
    Route::get('contacto/crear', 'ContactoController@crear')->name('crear_contacto');
    Route::post('contacto', 'ContactoController@guardar')->name('guardar_contacto');
    Route::get('contacto/{id}/editar', 'ContactoController@editar')->name('editar_contacto');
    Route::put('contacto/{id}', 'ContactoController@actualizar')->name('actualizar_contacto');
    Route::delete('contacto/{id}', 'ContactoController@eliminar')->name('eliminar_contacto');
    /*Rutas Empleado*/
    Route::get('empleado', 'EmpleadoController@index')->name('empleado');
    Route::get('empleado/crear', 'EmpleadoController@crear')->name('crear_empleado');
    Route::post('empleado', 'EmpleadoController@guardar')->name('guardar_empleado');
    Route::get('empleado/{id}/editar', 'EmpleadoController@editar')->name('editar_empleado');
    Route::put('empleado/{id}', 'EmpleadoController@actualizar')->name('actualizar_empleado');
    Route::delete('empleado/{id}', 'EmpleadoController@eliminar')->name('eliminar_empleado');
     /*Rutas Unidad*/
    Route::get('unidad', 'UnidadController@index')->name('unidad');
    Route::get('unidad/crear', 'UnidadController@crear')->name('crear_unidad');
    Route::post('unidad', 'UnidadController@guardar')->name('guardar_unidad');
    Route::get('unidad/{id}/editar', 'UnidadController@editar')->name('editar_unidad');
    Route::put('unidad/{id}', 'UnidadController@actualizar')->name('actualizar_unidad');
    Route::delete('unidad/{id}', 'UnidadController@eliminar')->name('eliminar_unidad');
     /*Rutas Proyecto*/
     
     Route::get('proyecto', 'ProyectoController@index')->name('proyecto');
     Route::get('proyecto/crear', 'ProyectoController@crear')->name('crear_proyecto');
     Route::post('proyecto', 'ProyectoController@guardar')->name('guardar_proyecto');
     Route::get('proyecto/{id}/editar', 'ProyectoController@editar')->name('editar_proyecto');
     Route::put('proyecto/{id}', 'ProyectoController@actualizar')->name('actualizar_proyecto');
     Route::delete('proyecto/{id}', 'ProyectoController@eliminar')->name('eliminar_proyecto');
    Route::post('/import', 'ProyectoController@import')->name('proyecto_import');
    
      /*RUTAS DE Estado*/
    Route::get('estado', 'EstadoController@index')->name('estado');
    Route::get('estado/crear', 'EstadoController@crear')->name('crear_estado');
    Route::post('estado', 'EstadoController@guardar')->name('guardar_estado');
    Route::get('estado/{id}/editar', 'EstadoController@editar')->name('editar_estado');
    Route::put('estado/{id}', 'EstadoController@actualizar')->name('actualizar_estado');
    Route::delete('estado/{id}', 'EstadoController@eliminar')->name('eliminar_estado');
    /*RUTAS DE Subsidio*/
    Route::get('subsidio', 'SubsidioController@index')->name('subsidio');
    Route::get('subsidio/crear', 'SubsidioController@crear')->name('crear_subsidio');
    Route::post('subsidio', 'SubsidioController@guardar')->name('guardar_subsidio');
    Route::get('subsidio/{id}/editar', 'SubsidioController@editar')->name('editar_subsidio');
    Route::put('subsidio/{id}', 'SubsidioController@actualizar')->name('actualizar_subsidio');
    Route::delete('subsidio/{id}', 'SubsidioController@eliminar')->name('eliminar_subsidio');
    /*Rutas Comunas*/
    Route::get('comuna', 'ComunaController@index')->name('comuna');
    Route::get('comuna/crear', 'ComunaController@crear')->name('crear_comuna');
    Route::post('comuna', 'ComunaController@guardar')->name('guardar_comuna');
    Route::get('comuna/{id}/editar', 'ComunaController@editar')->name('editar_comuna');
    Route::put('comuna/{id}', 'ComunaController@actualizar')->name('actualizar_comuna');
    Route::delete('comuna/{id}', 'ComunaController@eliminar')->name('eliminar_comuna');
     /*RUTAS DE Seccion Madesal*/
    Route::get('madesal', 'MadesalController@index')->name('madesal');
    Route::get('madesal/crear', 'MadesalController@crear')->name('crear_madesal');
    Route::post('madesal', 'MadesalController@guardar')->name('guardar_madesal');
    Route::get('madesal/{id}/editar', 'MadesalController@editar')->name('editar_madesal');
    Route::put('madesal/{id}', 'MadesalController@actualizar')->name('actualizar_madesal');
    Route::delete('madesal/{id}', 'MadesalController@eliminar')->name('eliminar_madesal');
    /*Rutas Te Asesoramos*/
    Route::get('asesoria', 'AsesoriaController@index')->name('asesoria');
    Route::get('asesoria/crear', 'AsesoriaController@crear')->name('crear_asesoria');
    Route::post('asesoria', 'AsesoriaController@guardar')->name('guardar_asesoria');
    Route::get('asesoria/{id}/editar', 'AsesoriaController@editar')->name('editar_asesoria');
    Route::put('asesoria/{id}', 'AsesoriaController@actualizar')->name('actualizar_asesoria');
    Route::delete('asesoria/{id}', 'AsesoriaController@eliminar')->name('eliminar_asesoria');
    /*Rutas Seccion Home*/
    Route::get('seccion-home', 'SeccionHomeController@index')->name('seccion-home');
    Route::get('seccion-home/crear', 'SeccionHomeController@crear')->name('crear_seccion-home');
    Route::post('seccion-home', 'SeccionHomeController@guardar')->name('guardar_seccion-home');
    Route::get('seccion-home/{id}/editar', 'SeccionHomeController@editar')->name('editar_seccion-home');
    Route::put('seccion-home/{id}', 'SeccionHomeController@actualizar')->name('actualizar_seccion-home');
    Route::delete('seccion-home/{id}', 'SeccionHomeController@eliminar')->name('eliminar_seccion-home');
    /*--------Rutas Seccion Proyectoss*/
     Route::get('seccion-proyectos', 'SeccionProyectossController@index')->name('seccion-proyectos');
    Route::get('seccion-proyectos/crear', 'SeccionProyectossController@crear')->name('crear_seccion-proyectos');
    Route::post('seccion-proyectos', 'SeccionProyectossController@guardar')->name('guardar_seccion-proyectos');
    Route::get('seccion-proyectos/{id}/editar', 'SeccionProyectossController@editar')->name('editar_seccion-proyectos');
    Route::put('seccion-proyectos/{id}', 'SeccionProyectossController@actualizar')->name('actualizar_seccion-proyectos');
    Route::delete('seccion-proyectos/{id}', 'SeccionProyectossController@eliminar')->name('eliminar_seccion-proyectos');
     /*Rutas Seccion Ficha Proyecto*/
    Route::get('seccion-fichaproyecto', 'SeccionFichaproyectoController@index')->name('seccion-fichaproyecto');
    Route::get('seccion-fichaproyecto/crear', 'SeccionFichaproyectoController@crear')->name('crear_seccion-fichaproyecto');
    Route::post('seccion-fichaproyecto', 'SeccionFichaproyectoController@guardar')->name('guardar_seccion-fichaproyecto');
    Route::get('seccion-fichaproyecto/{id}/editar', 'SeccionFichaproyectoController@editar')->name('editar_seccion-fichaproyecto');
    Route::put('seccion-fichaproyecto/{id}', 'SeccionFichaproyectoController@actualizar')->name('actualizar_seccion-fichaproyecto');
    Route::delete('seccion-fichaproyecto/{id}', 'SeccionFichaproyectoController@eliminar')->name('eliminar_seccion-fichaproyecto');
    /*Rutas Seccion Proceso Compra*/
    Route::get('procesocompra', 'ProcesoCompraController@index')->name('procesocompra');
    Route::get('procesocompra/crear', 'ProcesoCompraController@crear')->name('crear_procesocompra');
    Route::post('procesocompra', 'ProcesoCompraController@guardar')->name('guardar_procesocompra');
    Route::get('procesocompra/{id}/editar', 'ProcesoCompraController@editar')->name('editar_procesocompra');
    Route::put('procesocompra/{id}', 'ProcesoCompraController@actualizar')->name('actualizar_procesocompra');
    Route::delete('procesocompra/{id}', 'ProcesoCompraController@eliminar')->name('eliminar_procesocompra');
    /*Rutas Seccion Proceso Compra*/
    Route::get('financiamiento', 'FinanciamientoController@index')->name('financiamiento');
    Route::get('financiamiento/crear', 'FinanciamientoController@crear')->name('crear_financiamiento');
    Route::post('financiamiento', 'FinanciamientoController@guardar')->name('guardar_financiamiento');
    Route::get('financiamiento/{id}/editar', 'FinanciamientoController@editar')->name('editar_financiamiento');
    Route::put('financiamiento/{id}', 'FinanciamientoController@actualizar')->name('actualizar_financiamiento');
    Route::delete('financiamiento/{id}', 'FinanciamientoController@eliminar')->name('eliminar_financiamiento');
   /*Ruta Tipo Inmueble*/
    Route::get('tipoinmueble', 'TipoInmuebleController@index')->name('tipoinmueble');
    Route::get('tipoinmueble/crear', 'TipoInmuebleController@crear')->name('crear_tipoinmueble');
    Route::post('tipoinmueble', 'TipoInmuebleController@guardar')->name('guardar_tipoinmueble');
    Route::get('tipoinmueble/{id}/editar', 'TipoInmuebleController@editar')->name('editar_tipoinmueble');
    Route::put('tipoinmueble/{id}', 'TipoInmuebleController@actualizar')->name('actualizar_tipoinmueble');
    Route::delete('tipoinmueble/{id}', 'TipoInmuebleController@eliminar')->name('eliminar_tipoinmueble');
    /*Rutas Tipo Etapa*/
    Route::get('tipoetapa', 'TipoEtapaController@index')->name('tipoetapa');
    Route::get('tipoetapa/crear', 'TipoEtapaController@crear')->name('crear_tipoetapa');
    Route::post('tipoetapa', 'TipoEtapaController@guardar')->name('guardar_tipoetapa');
    Route::get('tipoetapa/{id}/editar', 'TipoEtapaController@editar')->name('editar_tipoetapa');
    Route::put('tipoetapa/{id}', 'TipoEtapaController@actualizar')->name('actualizar_tipoetapa');
    Route::delete('tipoetapa/{id}', 'TipoEtapaController@eliminar')->name('eliminar_tipoetapa');
    /*Rutas saladeventa*/
    Route::get('saladeventa', 'SaladeVentaController@index')->name('saladeventa');
    Route::get('saladeventa/crear', 'SaladeVentaController@crear')->name('crear_saladeventa');
    Route::post('saladeventa', 'SaladeVentaController@guardar')->name('guardar_saladeventa');
    Route::get('saladeventa/{id}/editar', 'SaladeVentaController@editar')->name('editar_saladeventa');
    Route::put('saladeventa/{id}', 'SaladeVentaController@actualizar')->name('actualizar_saladeventa');
    Route::delete('saladeventa/{id}', 'SaladeVentaController@eliminar')->name('eliminar_saladeventa');
    /*Rutas Tipo Cocina*/
    Route::get('tipococina', 'TipoCocinaController@index')->name('tipococina');
    Route::get('tipococina/crear', 'TipoCocinaController@crear')->name('crear_tipococina');
    Route::post('tipococina', 'TipoCocinaController@guardar')->name('guardar_tipococina');
    Route::get('tipococina/{id}/editar', 'TipoCocinaController@editar')->name('editar_tipococina');
    Route::put('tipococina/{id}', 'TipoCocinaController@actualizar')->name('actualizar_tipococina');
    Route::delete('tipococina/{id}', 'TipoCocinaController@eliminar')->name('eliminar_tipococina');

    /*Rutas Modelos*/
    Route::get('modelo', 'ModeloController@index')->name('modelo');
    Route::get('modelo/crear', 'ModeloController@crear')->name('crear_modelo');
    Route::post('modelo', 'ModeloController@guardar')->name('guardar_modelo');
    Route::get('modelo/{id}/editar', 'ModeloController@editar')->name('editar_modelo');
    Route::put('modelo/{id}', 'ModeloController@actualizar')->name('actualizar_modelo');
    Route::delete('modelo/{id}', 'ModeloController@eliminar')->name('eliminar_modelo');
    Route::post('/import_modelo', 'ModeloController@import')->name('modelo_import');


     //eliminar imagenes

    Route::get('eliminar_fotomodelo/{id}/{dato}', 'ModeloController@eliminar_fotomodelo')->name('eliminar_fotomodelo');
    Route::get('modelo/{id}', 'ModeloController@index2')->name('buscar_modelo');








    /*Rutas inversionista*/
    Route::get('inversionista', 'SeccionInversionistaController@index')->name('inversionista');
    Route::get('inversionista/crear', 'SeccionInversionistaController@crear')->name('crear_inversionista');
    Route::post('inversionista', 'SeccionInversionistaController@guardar')->name('guardar_inversionista');
    Route::get('inversionista/{id}/editar', 'SeccionInversionistaController@editar')->name('editar_inversionista');
    Route::put('inversionista/{id}', 'SeccionInversionistaController@actualizar')->name('actualizar_inversionista');
    Route::delete('inversionista/{id}', 'SeccionInversionistaController@eliminar')->name('eliminar_inversionista');



    /*Rutas postventa*/

    Route::get('postventa', 'PostventaController@index')->name('postventa');
    Route::get('postventa/crear', 'PostventaController@crear')->name('crear_postventa');
    Route::post('postventa', 'PostventaController@guardar')->name('guardar_postventa');
    Route::get('postventa/{id}/editar', 'PostventaController@editar')->name('editar_postventa');
    Route::put('postventa/{id}', 'PostventaController@actualizar')->name('actualizar_postventa');
    Route::delete('postventa/{id}', 'PostventaController@eliminar')->name('eliminar_postventa');
 
    

});
