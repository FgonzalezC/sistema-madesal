# ~~Elimine esta línea, los campos y secciones que no utilice y las descripciones por defecto~~

## Requerimientos

### Perfil: ~~Nombre Rol Intranet~~(rol_id)

#### Módulo: ~~nombre_del_modulo~~ (mod_id)

- [ ] Descripcion de la tarea que se desarrollará  ~~commit_id_sha1-hash~~
- [ ] Descripcion de la nueva implementación que debe ser desarrollada  ~~commit_id_sha1-hash~~
- [ ] Descripcion de funcionalidad que debe ser modificada o mantenida  ~~commit_id_sha1-hash~~, ~~commit_id_sha1-hash~~

#### Módulo: ~~nombre_del_modulo~~ (mod_id)

- [ ] Tarea Principal
    - [ ] Sub Tarea uno ~~commit_id_sha1-hash~~
    - [ ] Sub Tarea dos ~~commit_id_sha1-hash~~

### Bugs

- [ ] Descripcion de un bug detectado producto del desarrollo actual de esta issue ~~commit_id_sha1-hash~~


## Stored Procedures

* BD..nombre_procedimiento_almacenado

## Tablas Nuevas - Modificadas

**Nuevas**

* BD..nombre_nueva_tabla_uno
* ~~BD..nombre_nueva_tabla_dos~~


**Modificadas**

* BD..nombre_tabla_modificada_uno
* ~~BD..nombre_tabla_modificada_dos~~

## Querys SQL

* ~~Menú - Modulo : Permite agregar el nuevo módulo a todos los alumnos regulares~~

```sql
SELECT DISTINCT m.mod_modulo, m.mod_url, r.rol_perfil, m.mod_id_rol, m.mod_orden, m.mod_id
FROM UBB..MODULOS_MIGRADOS m with(nolock)
JOIN UBB..ROLES_MIGRADOS r ON m.mod_id_rol=r.rol_id_rol
WHERE m.mod_fecha_inicio <= getdate()
AND (m.mod_fecha_termino >= getdate() OR m.mod_fecha_termino is null)
AND r.rol_id_rol = xxxx AND mod_id in (xxxx)
```
