<!DOCTYPE html>
<html lang="es-Es">
@include('includes.head')
<body>

	<!-- Loading -->
	<div class="loading">
		<div class="loader"></div>
	</div>
	<!-- /Loading -->
	<header class="header-int">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-2 brand-image">
					<a href="{{ url('/home')}}">
						<img data-src="../images/logo-madesal.png" class="d-none d-lg-block lazy" alt="Madesal"s>
						<img data-src="../images/logo-madesal-xs.png" class="d-block d-lg-none lazy" alt="Madesal">
						</a>
				</div>
				<div class="col-lg-9 col-10">
					<nav class="navbar navbar-expand-lg">
						<div class="top">
							<ul class="d-flex">
								@include('includes.menu-superior')
							</ul>
						</div>
						<button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
							aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
							<span></span>
							<span></span>
							<span></span>
						</button>
						<div class="collapse navbar-collapse" id="navbarSupportedContent">
							<ul class="navbar-nav">
								@include('includes.menu')
							</ul>
							<div class="box-mobile d-block d-lg-none">
								<ul class="box-mobile__btns">
									<li>
										<a  href="{{ url('/contacto')}}">Ayuda</a>
									</li>
									<li>
										<a href="{{ url('/postventas')}}">Contacto</a>
									</li>
								</ul>
								<?php
								$footer = \DB::table('footers')
												->select('img_footer','direccion','telefono','link_face','link_instagram','link_twitter')
												->get();
								?>

								@foreach($footer as $footers)
								<ul class="box-mobile__social">
									<li><a target="_blanck" href="{{$footers->link_face}}"><i class="icon-facebook"></i></a></li>
									<li><a target="_blanck" href="{{$footers->link_instagram}}"><i class="icon-instagram"></i></a></li>
									<li><a target="_blanck" href="{{$footers->link_twitter}}"><i class="icon-twitter"></i></a></li>
								</ul>
								@endforeach
							</div>
						</div>
					</nav>
				</div>
			</div>
		</div>
	</header>
    <!-- HEAD PROJECT -->

    <section class="head-project">
        <div class="slide-images">
        	@if($resultado['proyecto']->desktopimg1 != NULL or $resultado['proyecto']->movilimg1 != NULL)
            <div class="slide-images__item">
            	<img data-src="{{URL::asset('storage/'.$resultado['proyecto']->desktopimg1)}}"  class="d-none d-lg-block lazy" alt=""/>
            	<img data-src="{{URL::asset('storage/'.$resultado['proyecto']->movilimg1)}}"  class="d-block d-lg-none lazy" alt=""/>
                <!-- <img data-src="images/thumb-project-item.jpg" class="d-none d-lg-block lazy" alt="">
                <img data-src="images/thumb-project-item-xs.jpg" class="d-block d-lg-none lazy" alt=""> -->
			</div>
			  @endif
			  @if($resultado['proyecto']->desktopimg2 != NULL or $resultado['proyecto']->movilimg2 != NULL)
			<div class="slide-images__item">
				<img data-src="{{URL::asset('storage/'.$resultado['proyecto']->desktopimg2)}}"  class="d-none d-lg-block lazy" alt=""/>
            	<img data-src="{{URL::asset('storage/'.$resultado['proyecto']->movilimg2)}}"  class="d-block d-lg-none lazy" alt=""/>
				<!-- <img data-src="images/thumb-project-item.jpg" class="d-none d-lg-block lazy" alt="">
				<img data-src="images/thumb-project-item-xs.jpg" class="d-block d-lg-none lazy" alt=""> -->
			</div>
			@endif
			@if($resultado['proyecto']->desktopimg3 != NULL or $resultado['proyecto']->movilimg3 != NULL)
			<div class="slide-images__item">
				<img data-src="{{URL::asset('storage/'.$resultado['proyecto']->desktopimg3)}}"  class="d-none d-lg-block lazy" alt=""/>
				<img data-src="{{URL::asset('storage/'.$resultado['proyecto']->movilimg3)}}"  class="d-block d-lg-none lazy" alt=""/>
				<!-- <img data-src="images/thumb-project-item.jpg" class="d-none d-lg-block lazy" alt="">
				<img data-src="images/thumb-project-item-xs.jpg" class="d-block d-lg-none lazy" alt=""> -->
			</div>
			@endif
			@if($resultado['proyecto']->desktopimg4 != NULL or $resultado['proyecto']->movilimg4 != NULL)
			<div class="slide-images__item">
				<img data-src="{{URL::asset('storage/'.$resultado['proyecto']->desktopimg4)}}"  class="d-none d-lg-block lazy" alt=""/>
				<img data-src="{{URL::asset('storage/'.$resultado['proyecto']->movilimg4)}}"  class="d-block d-lg-none lazy" alt=""/>
				<!-- <img data-src="images/thumb-project-item.jpg" class="d-none d-lg-block lazy" alt="">
				<img data-src="images/thumb-project-item-xs.jpg" class="d-block d-lg-none lazy" alt=""> -->
			</div>
			@endif
			@if($resultado['proyecto']->desktopimg5 != NULL or $resultado['proyecto']->movilimg5 != NULL)
			<div class="slide-images__item">
				<img data-src="{{URL::asset('storage/'.$resultado['proyecto']->desktopimg5)}}"  class="d-none d-lg-block lazy" alt=""/>
				<img data-src="{{URL::asset('storage/'.$resultado['proyecto']->movilimg5)}}"  class="d-block d-lg-none lazy" alt=""/>
				<!-- <img data-src="images/thumb-project-item.jpg" class="d-none d-lg-block lazy" alt="">
				<img data-src="images/thumb-project-item-xs.jpg" clas="d-block d-lg-none lazy" alt=""> -->
			</div>
			@endif
		</div>
		<div class="head-project__desc">
			<!--<h1>PORTAL ANIBAL PINTO</h1> -->
			<h1>{{$resultado['proyecto']->nombre_proyecto}}</h1>
			<!--<h3>Segunda etapa - casas</h3> -->
			<h3>{{\App\Models\Admin\TipoEtapa::find($resultado['proyecto']->tipoetapa_id)->nombre_etapa}}</h3>
			<p>{!! nl2br(e($resultado['proyecto']->descripcion_slider)) !!}</p>
			<!--<p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quo culpa explicabo corporis? Eius velit, eligendi temporibus necessitatibus voluptate, ullam dolorem possimus, ad fuga debitis voluptas vero ex vel numquam voluptatum.</p> -->
			<ul class="c-bttn">
				<!--<li>
					<a href="#">RESERVAR</a>
				</li>-->
				<li>
					<a href="#cotizar-form">COTIZAR</a>
				</li>
			</ul>
		</div>

    </section>

	<!-- /HEAD PROJECT -->
	@include('includes.msj')
    @include('includes.form-error')
	<!-- MODELOS -->
	<section class="modelos">


		<div class="container">
			<div class="row modelos__boxes">
				
				<div class="col-12 title">
					<h2>Modelos</h2>
					
				</div>
				
				@foreach($resultado['modelo'] as $modelos)
				
				<div class="col-lg-3 box-item">
					@foreach($resultado['estados'] as $proyecto)
					@if ($modelos->img_fichaproyecto != NULL and strlen($modelos->img_fichaproyecto) > 0 )
					<div class="label-type">{{$proyecto->nombre_estado}}</div>
					@endif
					@endforeach
					@if($modelos->img_fichaproyecto)
					<img src="{{URL::asset('storage/'.$modelos->img_fichaproyecto)}}" class="img-modelo" alt="">
					<div class="line"></div>
					@endif
					<a href="{{route('ficha-proyecto_modelo', ['id' => $modelos->id,$resultado['proyecto']->id ])}}">{{$modelos->nombre_modelo}} @if (!$modelos->check_stock) (sin stock) @endif </a>
					<ul>
						<li>
							<i class="icon-casa"></i>
							<span>{{$modelos->nombre_modelo}} </span>
						</li>
						<li>
							<i class="icon-plano"></i>
							<span>{{$modelos->mtrs}} Mts2</span>
						</li>
						<li>
							<i class="icon-dormitorio"></i>
							<span>{{$modelos->dormitorio}} Dormitorios</span>
						</li>
						
						<li>
							<i class="icon-bano"></i>
							<span>{{$modelos->banos}} Baños</span>
						</li>
						<li>
							<i class="icon-cocina"></i>
							<span>{{\App\Models\Admin\TipoCocina::find($modelos->tipococina_id)->nombre_cocina}}</span>
						</li>
						@foreach($resultado['valores'] as $valor)
						@if($modelos->estacionamiento != NULL )
						<li>
							<i class="icon-estacionamiento"></i>
							<span>{{$modelos->estacionamiento}} Estacionamiento @if($valor->valor_estacionamiento != NULL), {{$valor->valor_estacionamiento}} UF @endif</span>
						</li>
						@endif
						@endforeach
						<li>
						@if($modelos->precio_desde != NULL)
						<div class="price">
								<h3>Desde: <span>{{$modelos->precio_desde}} UF</span></h3>
							</div>
							@endif
						</li>
						@if($modelos->archivo != NULL)
						<li class="download">							
							<a href="../storage/FichasProyectoPDF/{{$modelos->archivo}} " download >Descargar ficha</a>							
						</li>
						@endif		
						<li>
							<a href="{{route('ficha-proyecto_modelo', ['id' => $modelos->id,$resultado['proyecto']->id ])}}">Ver más</a>
						</li>
					</ul>
				</div>
				@endforeach
			</div>
		</div>

	</section>
	<!-- /MODELOS -->

	<!-- TABS DESKTOP -->




	<!-- /TABS DESKTOP -->

	<!-- ACCORDEON MOBILE -->

	<!-- /ACCORDEON MOBILE -->

	<!-- BANNER INVERSIONISTAS -->
	<div class="container">
		<div class="row">


			@foreach($resultado['inversionista'] as $inversionistas)
			<div class="col-lg-12 d-none d-md-block banner-full text-center">
				<a href="{{ url('/financiamiento') }}">
				<img src="{{URL::asset('storage/'.$inversionistas->img3)}}" alt="">
				</a>
			</div>
			<div class = "col-lg-12 d-block d-md-none banner-full text-center ">
				<a href="{{ url('/financiamiento') }}">
				<img src ="{{URL::asset('storage/'.$inversionistas->imgmovil3)}}"alt =" ">	
				</a>			
 			</div>
			@endforeach

		</div>
	</div>
	<!-- /BANNER INVERSIONISTAS -->
<!--seccion Cotizar-->
	<!-- CONTACT PROJECT -->
	<section class="contact-project">
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<div class="contact-project__form" id="cotizar-form">
							    <form action="{{route('inversionista_guardar')}}" id= "form-general" class="gnrl-form" method="POST" autocomplete="off">
										{{ csrf_field() }}

								<h2>Cotiza este proyecto</h2>
								<input type="text" name="nombre_cotizacion" id="nombre_cotizacion" placeholder="Nombre" required>
								<input type="text" name="apellido_cotizacion" id="apellido_cotizacion" placeholder="Apellido"required>
								<input type="text" name="telefono_cotizacion" id="telefono_cotizacion" placeholder="Teléfono"required>
								<input type="text" name="email_cotizacion" id="email_cotizacion" placeholder="Email" required>
								 <select name="asunto_id" id="asunto_id" required>
                                        <option value="">Agregar Asunto</option>
                                        @foreach(\App\Models\Admin\Asunto::all() as $asunto)
                                        <option value="{{$asunto->id}}" required>{{$asunto->nombre_asunto}}</option>
                                    @endforeach
                                            
                                </select>
								<textarea name="mensaje_cotizacion" id="mensaje_cotizacion" placeholder="Mensaje" required></textarea>
								<button type="submit" class="btn-send"  >ENVIAR</button>
							</form>
						</div>
				</div> 

				@foreach($resultado['saladeventas']  as $saladeventa)
				<div class="col-lg-6">
						<div class="contact-project__dates">
						<!--	<div class="label">
								Cómo llegar
							</div> -->
							<div class="dates-text">
								<h2>Sala de ventas: {{$saladeventa->nombre}}</h2>
								<br>
								<p>Dirección: {{$saladeventa->direccion}} </p>
								<p>Teléfono: {{$saladeventa->telefono}}</p>
								
								@if($saladeventa->telefono2 !=NULL)
								<p>Celular: {{$saladeventa->telefono2}}</p>
								@endif

								<p>Mail: {{$saladeventa->mail}}</p>
								@if($saladeventa->mail2 !=NULL)
									<p>Mail 2: {{$saladeventa->mail2}}</p>
								@endif
			
							
							<p>Horario de Atención: {{$saladeventa->horario}}</p>
						</div>
						@if($saladeventa->mapa!=NULL)
						<div  style="width: 100%"><iframe width="100%" height="250" src="{{$saladeventa->mapa}}" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"><a href="https://www.maps.ie/coordinates.html">gps coordinates finder</a></iframe></div>
						@else
						<div  style="width: 100%"><iframe width="100%" height="250" src="https://maps.google.com/maps?width=100%&amp;height=600&amp;hl=en&amp;coord=-36.8294688,-73.0489664&amp;q=Cochcrane%20635%20concepci%C3%B3n+(Madesal%20Concepci%C3%B3n)&amp;ie=UTF8&amp;t=&amp;z=19&amp;iwloc=B&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"><a href="https://www.maps.ie/coordinates.html">gps coordinates finder</a></iframe></div>
						@endif					
						</div>
				</div>
				@endforeach
				<div class="col-12 ejecutive-sale">
					@foreach($resultado['empleado'] as $empleados)
					<article class="ejecutive-sale__box">
						<figure>
							<div class="img-container">
								<img src="{{URL::asset('storage/'.$empleados->img)}}">
							</div>							
							<figcaption>
								<div class="text">
									<h2>{{strtoupper($empleados->nombre_empleado)}}</h2>
									<h3>{{$empleados->nombre}}</h3>
								</div>
								<div class="dates">
									<p>{{$empleados->telefono}}</p>
									<p>{{$empleados->correo}}</p>
								</div>
							</figcaption>
						</figure>						
					</article>
					@endforeach
				</div>
			</div>
		</div>
	</section>
	<!-- /CONTACT PROJECT -->

	@include('includes.footer')

	<script src="../js/jquery.min.js"></script>
	<script src="../js/bootstrap.bundle.min.js"></script>
	<script src="../js/lazyload.min.js"></script>
	<script src="../js/nouislider.js"></script>
	<script src="../js/lity.js"></script>
	<script src="../js/slick.min.js"></script>
	<script src="../js/jquery.zoom.min.js"></script>
	<script src="../js/main.js"></script>
	@include('includes.chat')

</body>
</html>
