<!DOCTYPE html>
<html lang="es-Es">
@include('includes.head')
<body>
	<!-- Loading -->
	<div class="loading">
		<div class="loader"></div>
	</div>
	<!-- /Loading -->	
	<header class="header-int">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-2 brand-image">
					<a href="{{ url('/home')}}">
						<img data-src="images/logo-madesal.png" class="d-none d-lg-block lazy" alt="Madesal"s>
						<img data-src="images/logo-madesal-xs.png" class="d-block d-lg-none lazy" alt="Madesal">
						</a>
				</div>
				<div class="col-lg-9 col-10">
					<nav class="navbar navbar-expand-lg">
						<div class="top">
							<ul class="d-flex">
								@include('includes.menu-superior')
							</ul>
						</div>
						<button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
							aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
							<span></span>
							<span></span>
							<span></span>
						</button>
						<div class="collapse navbar-collapse" id="navbarSupportedContent">
							<ul class="navbar-nav">
								@include('includes.menu')
							</ul>
							<div class="box-mobile d-block d-lg-none">
								<ul class="box-mobile__btns">
									<li>
										<a  href="{{ url('/asesoria')}}">Ayuda</a>
									</li>
									<li>
										<a href="{{ url('/contacto')}}">Contacto</a>
									</li>
								</ul>
								<?php 
								$footer = \DB::table('footers')
												->select('img_footer','direccion','telefono','link_face','link_instagram','link_twitter')
												->get();
								?>

								@foreach($footer as $footers)
								<ul class="box-mobile__social">
									<li><a target="_blanck" href="{{$footers->link_face}}"><i class="icon-facebook"></i></a></li>
									<li><a target="_blanck" href="{{$footers->link_instagram}}"><i class="icon-instagram"></i></a></li>
									<li><a target="_blanck" href="{{$footers->link_twitter}}"><i class="icon-twitter"></i></a></li>
								</ul>
								@endforeach
							</div>
						</div>
					</nav>
				</div>
			</div>	
		</div>
	</header>

	
	<!-- MAIN HEAD -->
	<section class="main-head main-head--page">

		@foreach($resultado['madesal'] as $madesals)
		<div class="main-head__image">
            <img data-src="{{URL::asset('storage/'.$madesals->img)}}" class="d-none d-md-block lazy" alt="Madesal">      
		</div>
		<div class="main-head__title">
			<h1>{{$madesals->titulo}}</h1>
		</div>
		@endforeach	
	</section>
	@include('includes.msj')
	@include('includes.form-error')
	
    <!-- /MAIN HEAD DESKTOP -->
   @foreach($resultado['madesal'] as $madesals)
    <section class="main-madesal">
        <div class="container">
            <div class="row row--first">
                <div class="col-lg-7">
                    <h2>{{$madesals->titulo1}}</h2>
                    <p>{!! nl2br(e($madesals->bajada1)) !!}</p>
                    <p>{!! nl2br(e($madesals->bajada2)) !!}</p>
                    <p>{!! nl2br(e($madesals->bajada3)) !!}</p>
                </div>
                <div class="img-box">

                    <img src="{{URL::asset('storage/'.$madesals->img1)}}" alt="">
                </div>
            </div>
            <div class="row row--second justify-content-end">
                <div class="img-box">
                    <img src="{{URL::asset('storage/'.$madesals->img2)}}" alt="">
                </div>
                <div class="col-lg-7">
                    <h2>{{$madesals->titulo2}}</h2>
                    <p>{!! nl2br(e($madesals->bajada4)) !!}</p>
                </div>
            </div>
        </div>

    </section>
            @endforeach
 
     <section class="team-madesal">
     	
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Equipo</h2>
                </div>
                @foreach($resultado['empleado'] as $empleados)
                <div class="col-lg-3 col-6">
                    <div class="thumb-img">
                        <img src="{{URL::asset('storage/'.$empleados->img)}}">
                    </div>
                    <h3>{{\App\Models\Admin\Cargo::find($empleados->cargo_id)->nombre}}</h3>
                    <p>{{$empleados->nombre_empleado}}</p>
                    <p>{{$empleados->telefono}}</p>
                    <p>{{$empleados->correo}}</p>
                    
                </div>
                @endforeach

			</div>
			  <?php

              
				$sucursales = DB::table('sucursales')
				->get();

		

                ?>
                
			
			<div class="row">
						@foreach($sucursales as $sucursal)
				<div class="col-lg-4 map">
					<iframe
						src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d816286.6917456645!2d-73.44205959145212!3d-36.947249485622905!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1sconcepcion!5e0!3m2!1ses!2scl!4v1571339280048!5m2!1ses!2scl"
						height="175" frameborder="0" style="border:0;" allowfullscreen="">
					</iframe>
					<div class="box-info">
						<p>{{$sucursal->direccion}}</p>
						<p>{{$sucursal->telefono}}</p>
						<p>{{$sucursal->correo}}</p>                    
					</div>
				</div>
				@endforeach
				
			</div>            
        </div>
    </section>

	 <section class="contactanos">
         
        <div class="container">
           
            <div class="row">
                <div class="col-12 text-center primary-title" id="cotizar-form">
                    <h2>Cotiza tu proyecto inmobiliario</h2>
                </div>
                <div class="col-12">
                       
                        <form action="{{route('madesal_guardar')}}" id= "form-general" class="gnrl-form" method="POST" autocomplete="off">
                                {{ csrf_field() }}
  
                            <div class="row">
                            <div class="col-lg-6">
                              
                                <input type="text" name="nombre_cotizacion" id="nombre_cotizacion" placeholder="Nombre" required>
                                <input type="text" name="apellido_cotizacion" id="apellido_cotizacion" placeholder="Apellido" required>
                                <input type="text" name="telefono_cotizacion" id="telefono_cotizacion" placeholder="Teléfono" required>
                                <input type="text" name="email_cotizacion" id="email_cotizacion" placeholder="Email" required>
                            </div>
                            <div class="col-lg-6">
                                   <select name="asunto_id" id="asunto_id" required>
                                        <option value="">Agregar Asunto</option>
                                        @foreach(\App\Models\Admin\Asunto::all() as $asunto)
                                        <option value="{{$asunto->id}}" required>{{$asunto->nombre_asunto}}</option>
                                    @endforeach
                                            
                                </select>
                                    <textarea name="mensaje_cotizacion" id="mensaje_cotizacion" placeholder="Mensaje" required></textarea>
                            </div>
                            <div class="col-lg-12 text-center">
                                <button class="btn-gnrl btn-gnrl--green">ENVIAR</button>
                            </div>
                        </div>                        
                    </form>
                </div>                
            </div>
        </div>
    </section>
	<!-- /CONTACTANOS -->

		

	@include('includes.footer')
	<script src="js/jquery.min.js"></script>
	<script src="js/bootstrap.bundle.min.js"></script>
	<script src="js/lazyload.min.js"></script>
	<script src="js/nouislider.js"></script>
	<script src="js/slick.min.js"></script>
	<script src="js/main.js"></script>
	@include('includes.chat')
		
</body>
</html>