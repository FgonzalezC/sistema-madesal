<!DOCTYPE html>
<html lang="es-Es">
@include('includes.head')
<body>
    <!-- Loading -->
    <div class="loading">
        <div class="loader"></div>
    </div>
    <!-- /Loading -->   
    <header class="header-int">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-2 brand-image">
                    <a href="{{ url('/home')}}">
                    <img data-src="images/logo-madesal.png" class="d-none d-lg-block lazy" alt="Madesal">
                    <img data-src="images/logo-madesal-xs.png" class="d-block d-lg-none lazy" alt="Madesal">
                    </a>
                </div>
                <div class="col-lg-9 col-10">
                    <nav class="navbar navbar-expand-lg">
                        <div class="top">
                            <ul class="d-flex">
                                @include('includes.menu-superior')
                            </ul>
                        </div>
                        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span></span>
                            <span></span>
                            <span></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav">
                                @include('includes.menu')
                            </ul>
                            <div class="box-mobile d-block d-lg-none">
                                <ul class="box-mobile__btns">
                                    <li>
                                        <a  href="{{ url('/contacto')}}">Ayuda</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/postventas')}}">Contacto</a>
                                    </li>
                                </ul>
                                <?php 
                                $footer = \DB::table('footers')
                                                ->select('img_footer','direccion','telefono','link_face','link_instagram','link_twitter')
                                                ->get();
                                ?>

                                @foreach($footer as $footers)
                                <ul class="box-mobile__social">
                                    <li><a target="_blanck" href="{{$footers->link_face}}"><i class="icon-facebook"></i></a></li>
                                    <li><a target="_blanck" href="{{$footers->link_instagram}}"><i class="icon-instagram"></i></a></li>
                                    <li><a target="_blanck" href="{{$footers->link_twitter}}"><i class="icon-twitter"></i></a></li>
                                </ul>
                                @endforeach
                            </div>
                        </div>
                    </nav>
                </div>
            </div>  
        </div>
    </header>
 <?php  
            $postventa = \DB::table('postventas')
                        
                        ->get();

            /*$seccionC = \DB::table('seccion_homes')->count();*/
    ?> 
    
    
    <!-- MAIN HEAD -->
    <section class="main-head main-head--page">

        
         @foreach($postventa as $postventas)
        <div class="main-head__image">
            <img data-src="{{URL::asset('storage/'.$postventas->img1)}}" class="d-none d-md-block lazy" alt="Madesal">      
        </div>
       
        <div class="main-head__title">
            <h1>{{$postventas->titulo}}</h1>
        </div>      
        @endforeach
    </section>
    <!-- /MAIN HEAD DESKTOP -->
    
    <!-- POST VENTA -->
     @include('includes.msj') 
     @include('includes.form-error')   
    <section class="post-venta">
     <form action="{{route('postventas_guardar')}}"  class="gnrl-form" method="POST" autocomplete="off" action="upload" enctype="multipart/form-data">
                        {{ csrf_field() }}  
        @foreach($postventa as $postventas)

        <div class="container">
             
            <div class="row">
                

                <div class="col-12">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="post-venta-tab" data-toggle="tab" href="#post-venta" role="tab" aria-controls="post-venta"
                                aria-selected="true">Postventa</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="emergencia-tab" data-toggle="tab" href="#emergencia" role="tab" aria-controls="emergencia"
                                aria-selected="false">¿Tienes una emergencia?</a>
                        </li>                        
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="post-venta" role="tabpanel" aria-labelledby="post-venta-tab">
                           

                       
                                <div class="step">
                                     
                                    <div class="step__title">
                                    <h2>1. {{$postventas->titulo1}}</h2>
                                    </div>
                                    
                                    <ul class="check-list">
                                        <li>
                                            <input type="checkbox" name="techo" id="techo" value="{{$postventas->btn1}}">
                                            <label for="techo">
                                                <i class="icon-techo"></i>
                                            <p>{{$postventas->btn1}}</p>
                                            </label>
                                        </li>
                                        <li>
                                            <input type="checkbox" name="electricidad" id="electricidad" value="{{$postventas->btn2}}">
                                            <label for="electricidad">
                                                <i class="icon-tubos"></i>
                                                <p>{{$postventas->btn2}}</p>
                                            </label>
                                        </li>
                                        <li>
                                            <input type="checkbox" name="materiales" id="materiales" value="{{$postventas->btn3}}">
                                            <label for="materiales">
                                                <i class="icon-tubos"></i>
                                                <p>{{$postventas->btn3}}</p>
                                            </label>
                                        </li>
                                        <li>
                                            <input type="checkbox" name="paredes" id="paredes" value="{{$postventas->btn4}}">
                                            <label for="paredes">
                                                <i class="icon-muto"></i>
                                                <p>{{$postventas->btn4}}</p>
                                            </label>
                                        </li>
                                        <li>
                                            <input type="checkbox" name="piso" id="piso"  value="{{$postventas->btn5}}">
                                            <label for="piso">
                                                <i class="icon-box"></i>
                                                <p>{{$postventas->btn5}}</p>
                                            </label>
                                        </li>                                          
                                        <li>
                                            <input type="checkbox" name="cloacas" id="cloacas" value="{{$postventas->btn6}}">
                                            <label for="cloacas">
                                                <i class="icon-pintura"></i>
                                                <p>{{$postventas->btn6}}</p>
                                            </label>
                                        </li>
                                    </ul>

                                </div>
                                <div class="step">
                                    <div class="step__title">
                                        <h2>2. {{$postventas->titulo2}}</h2>
                                    </div>
                                    
                                    @endforeach
                                 
                                    <div class="col-form">
                                        <input type="text"  name="nombre" id="nombre" placeholder="Nombre" required>
                                        <input type="text"  name="apellido" id="apellido" placeholder="Apellido" required>
                                        <input type="text"  name="rut" id="rut" placeholder="Rut" required>
                                        <input type="text"  name="telefono" id="telefono" placeholder="Teléfono" required>
                                        <input type="text"  name="mail" id="mail" placeholder="Email" required>
                                    </div>
                                    <div class="col-form">
                                        <select name="proyecto_id" id="proyecto_id" required>
                                                <option value="">Selecciona un proyecto</option>
                                                @foreach(\App\Models\Admin\Proyecto::all() as $proy)
                                                <option value="{{$proy->id}}">{{$proy->nombre_proyecto}} / {{\App\Models\Admin\TipoEtapa::find($proy->tipoetapa_id)->nombre_etapa}}</option>
                                            @endforeach
                                            
                                        </select>
                                        <input type="text" id="direccion" name="direccion" placeholder="Dirección" required>
                                        <label for="file">Adjunta una imagen</label>
                                        <input type="file" id="imagen" name="imagen" required>
                                        <textarea name="mensaje" id="mensaje" placeholder="Mensaje" required></textarea>
                                    </div>
                                     <div class="col-12 text-center">
                                        <button type="submit" class="btn-send">Enviar</button>
                                    </div>
                                </div>
                            </form>                            
                        </div>

                        <div class="tab-pane fade" id="emergencia" role="tabpanel" aria-labelledby="emregencia-tab">
                     
                            
                                               
                                    <div class="step">   
                                    <form action="{{ url('postventas/guardar3') }}" id="form-general" class="gnrl-form" method="POST" autocomplete="off" >
                                   {{ csrf_field() }}                                
                                    <ul class="check-list">

                                        @foreach($postventa as $postventas)
                                        <li>
                                            <input type="checkbox" name="gas-fuga" id="gas-fuga" value="{{$postventas->btn7}}">
                                            <label for="gas-fuga">
                                                <i class="icon-gas"></i>
                                            <p>{{$postventas->btn7}}</p>
                                            </label>
                                        </li>
                                        <li>
                                            <input type="checkbox" name="caneria" id="caneria" value="{{$postventas->btn8}}">
                                            <label for="caneria">
                                                <i class="icon-union"></i>
                                                <p>{{$postventas->btn8}}</p>
                                            </label>
                                        </li>
                                        <li>
                                            <input type="checkbox" name="calefont" id="calefont" value="{{$postventas->btn9}}">
                                            <label for="calefont">
                                                <i class="icon-calefaccion"></i>
                                                <p>{{$postventas->btn9}}</p>
                                            </label>
                                        </li>     
                                            @endforeach                                   
                                    </ul>
                                </div>
                          
                                <div class="step">
                                    <div class="step__title">
                                        <h2>Contáctanos</h2>
                                    </div>
                                    <div class="col-form">
                                      
                                <input type="text" name="nombre" id="nombre" placeholder="Nombre" required>
                                <input type="text" name="apellido" id="apellido" placeholder="Apellido"required>
                                <input type="text" name="telefono" id="telefono" placeholder="Teléfono"required>
                                <input type="text" name="email" id="email" placeholder="Email"required>
                                    </div>

                                    <div class="col-form">
                                            <select name="asunto_id" id="asunto_id" required>
                                                    <option value="">Agregar Asunto</option>
                                                    @foreach(\App\Models\Admin\Asunto::all() as $asunto)
                                                    <option value="{{$asunto->id}}" required>{{$asunto->nombre_asunto}}</option>
                                                @endforeach
                                                
                                            </select>
                                    <textarea name="mensaje" id="mensaje" placeholder="Mensaje" required></textarea>
                                    </div>
                                    <div class="col-12 text-center">
                                        <button type="submit" class="btn-send">Enviar</button>
                                      </div>
                                </div>
                            </form>
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
    </section>


   @include('includes.footer')
   
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/lazyload.min.js"></script>
    <script src="js/nouislider.js"></script>
    <script src="js/slick.min.js"></script>
    <script src="js/main.js"></script>   
    @include('includes.chat')
   
</body>
</html>