<!DOCTYPE html>
<html lang="es-Es">
@include('includes.head')
<body>
    <!-- Loading -->
    <div class="loading">
        <div class="loader"></div>
    </div>
    <!-- /Loading -->   
    <header class="header-int">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-2 brand-image">
                   <a href="{{ url('/home')}}">
                        <img data-src="images/logo-madesal.png" class="d-none d-lg-block lazy" alt="Madesal"s>
                        <img data-src="images/logo-madesal-xs.png" class="d-block d-lg-none lazy" alt="Madesal">
                    </a>
                </div>
                <div class="col-lg-9 col-10">
                    <nav class="navbar navbar-expand-lg">
                        <div class="top">
                            <ul class="d-flex">
                                @include('includes.menu-superior')
                            </ul>
                        </div>
                        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span></span>
                            <span></span>
                            <span></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav">
                                @include('includes.menu')
                            </ul>
                            <div class="box-mobile d-block d-lg-none">
                                <ul class="box-mobile__btns">
                                    <li>
                                        <a  href="{{ url('/asesoria')}}">Ayuda</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/contacto')}}">Contacto</a>
                                    </li>
                                </ul>
                                <?php 
                                $footer = \DB::table('footers')
                                                ->select('img_footer','direccion','telefono','link_face','link_instagram','link_twitter')
                                                ->get();
                                ?>

                                @foreach($footer as $footers)
                                <ul class="box-mobile__social">
                                    <li><a target="_blanck" href="{{$footers->link_face}}"><i class="icon-facebook"></i></a></li>
                                    <li><a target="_blanck" href="{{$footers->link_instagram}}"><i class="icon-instagram"></i></a></li>
                                    <li><a target="_blanck" href="{{$footers->link_twitter}}"><i class="icon-twitter"></i></a></li>
                                </ul>
                                @endforeach
                            </div>
                        </div>
                    </nav>
                </div>
            </div>  
        </div>
    </header>
    
    <!-- MAIN HEAD -->
    @foreach($resultado['asesoria'] as $asesorias)
    <section class="main-head main-head--int">
       
        <div class="main-head__image">
            <img data-src="{{URL::asset('storage/'.$asesorias->img)}}" class="d-none d-md-block lazy" alt="Madesal">
        </div>
        <div class="main-head__title d-none d-lg-block">
            <h1>{{$asesorias->titulo}}</h1>
        </div>
              
    </section>
    @endforeach   
    <!-- /MAIN HEAD --> 
 <section class="proceso-compra">
     @include('includes.msj')
     @include('includes.form-error')
     @foreach($resultado['procesocompra'] as $procesocompras)
        <div class="container">
            <div class="row">
                <div class="col-12 text-center primary-title">
                    <h2>{{$procesocompras->titulo}}</h2>
                </div>
                  <div class="col-12">   
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="cotizacion-tab" data-toggle="tab" href="#cotizacion" role="tab" aria-controls="cotizacion" aria-selected="true">Cotización</a>
                            </li>
                             <li class="nav-item">
                                <a class="nav-link " id="entregadoc-tab" data-toggle="tab" href="#entregadoc" role="tab" aria-controls="entregadoc" aria-selected="true">Entrega de Documentos</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="reserva-tab" data-toggle="tab" href="#reserva" role="tab" aria-controls="reserva" aria-selected="false">Reserva</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="plan-tab" data-toggle="tab" href="#plan" role="tab" aria-controls="plan" aria-selected="false">Plan de pago</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="escritura-tab" data-toggle="tab" href="#escritura" role="tab" aria-controls="escritura" aria-selected="false">Escritura</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="entrega-tab" data-toggle="tab" href="#entrega" role="tab" aria-controls="entrega" aria-selected="false">Entrega</a>
                            </li>
                        </ul>
                     <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="cotizacion" role="tabpanel" aria-labelledby="cotizacion-tab">
                                <div class="row">
                                    <div class="col-lg-5">
                                        <img src="{{URL::asset('storage/'.$procesocompras->img1)}}">
                                    </div>
                                    <div class="col-lg-7">
                                        <h3>{{$procesocompras->titulo1}}</h3>
                                        <p>{!! nl2br(e($procesocompras->bajada1)) !!}</p>
                                        <a href="#cotizar-form">Cotizar ahora</a>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="entregadoc" role="tabpanel" aria-labelledby="entregadoc-tab">
                                <div class="row">
                                    <div class="col-lg-5 image">
                                        <img src="{{URL::asset('storage/'.$procesocompras->imgdoc)}}">
                                    </div>
                                    <div class="col-lg-7 thumb"> 
                                        <h3>{{$procesocompras->titulodoc}}</h3>
                                        <p>{!! nl2br(e($procesocompras->bajadadoc)) !!}</p>
                                        <a href="{{url('asesoria')}}#cotizar-form">Cotizar ahora</a>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="reserva" role="tabpanel" aria-labelledby="reserva-tab">
                                <div class="row">
                                    <div class="col-lg-5 image">
                                        <img src="{{URL::asset('storage/'.$procesocompras->img2)}}">
                                    </div>
                                    <div class="col-lg-7 thumb">
                                        <h3>{{$procesocompras->titulo2}}</h3>
                                        <p>{!! nl2br(e($procesocompras->bajada2)) !!}</p>
                                        <a href="#cotizar-form">Cotizar ahora</a>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="plan" role="tabpanel" aria-labelledby="plan-tab">
                                <div class="row">
                                    <div class="col-lg-5 image">
                                        <img src="{{URL::asset('storage/'.$procesocompras->img3)}}">
                                    </div>
                                    <div class="col-lg-7 desc">
                                        <h3>{{$procesocompras->titulo3}}</h3>
                                        <p>{!! nl2br(e($procesocompras->bajada3)) !!}</p>
                                        <a href="#cotizar-form" class="btn-gnrl">Cotizar ahora</a>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="escritura" role="tabpanel" aria-labelledby="escritura-tab">
                                <div class="row">
                                    <div class="col-lg-5 image">
                                        <img src="{{URL::asset('storage/'.$procesocompras->img4)}}">
                                    </div>
                                    <div class="col-lg-7 desc">
                                        <h3>{{$procesocompras->titulo4}}</h3>
                                        <p>{!! nl2br(e($procesocompras->bajada4)) !!}</p>
                                        <a href="#cotizar-form" class="btn-gnrl">Cotizar ahora</a>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="entrega" role="tabpanel" aria-labelledby="entrega-tab">
                                <div class="row">
                                    <div class="col-lg-5 image">
                                        <img src="{{URL::asset('storage/'.$procesocompras->img5)}}">
                                    </div>
                                    <div class="col-lg-7 desc">
                                        <h3>{{$procesocompras->titulo5}}</h3>
                                        <p>{!! nl2br(e($procesocompras->bajada5)) !!}</p>
                                        <a href="#cotizar-form" class="btn-gnrl btn-gnrl--green">Cotizar ahora</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
        @endforeach
    </section>
    <!-- /PROCESO DE COMPRA -->

    <!-- BANNER TWO COLUMN -->
    <div class="container">
    @foreach($resultado['asesoria'] as $asesorias)
        <div class="row banner-50">
            <div class="col-lg-6">
                <a href="{{ url('/asesoria')}}">
                <img src="{{URL::asset('storage/'.$asesorias->img1)}}" alt="">
                </a>
            </div>
            <div class="col-lg-6">
                 <a href="{{ url('/simuladores')}}">
                <img src="{{URL::asset('storage/'.$asesorias->img2)}}" alt="">
                </a>
            </div>
        </div>
    @endforeach
    </div>
    <!-- /BANNER TWO COLUMN -->
    
    <!-- CONTACTANOS -->
    <section class="contactanos">
         
        <div class="container">
           
            <div class="row">
                <div class="col-12 text-center primary-title" id="cotizar-form">
                    <h2>Cotiza tu proyecto inmobiliario</h2>
                </div>
                <div class="col-12">
                       
                        <form action="{{route('asesoria_guardar')}}" id= "form-general" class="gnrl-form" method="POST" autocomplete="off">
                                {{ csrf_field() }}
  
                            <div class="row">
                            <div class="col-lg-6">
                              
                                <input type="text" name="nombre_cotizacion" id="nombre_cotizacion" placeholder="Nombre" required>
                                <input type="text" name="apellido_cotizacion" id="apellido_cotizacion" placeholder="Apellido" required>
                                <input type="text" name="telefono_cotizacion" id="telefono_cotizacion" placeholder="Teléfono" required>
                                <input type="text" name="email_cotizacion" id="email_cotizacion" placeholder="Email" required>
                            </div>
                            <div class="col-lg-6">
                                     <select name="asunto_id" id="asunto_id" required>
                                        <option value="">Agregar Asunto</option>
                                        @foreach(\App\Models\Admin\Asunto::all() as $asunto)
                                        <option value="{{$asunto->id}}" required>{{$asunto->nombre_asunto}}</option>
                                    @endforeach
                                            
                                </select>
                                    <textarea name="mensaje_cotizacion" id="mensaje_cotizacion" placeholder="Mensaje" required></textarea>
                            </div>
                            <div class="col-lg-12 text-center">
                                <button class="btn-gnrl btn-gnrl--green">ENVIAR</button>
                            </div>
                        </div>                        
                    </form>
                </div>                
            </div>
        </div>
    </section>
  

    <!-- /CONTACTANOS -->
@include('includes.footer')

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/lazyload.min.js"></script>
    <script src="js/nouislider.js"></script>
    <script src="js/slick.min.js"></script>
    <script src="js/lity.js"></script>
    <script src="js/jquery.zoom.min.js"></script>
    <script src="js/main.js"></script> 
    @include('includes.chat')

</body>
</html>