@extends("theme.$theme.layout")
@section('titulo')
    PostVenta
@endsection

@section("scripts")
<script src="{{asset("assets/pages/scripts/admin/index.js")}}" type="text/javascript"></script>
@endsection

@section('contenido')
<div class="row">
    <div class="col-lg-12">
        @include('includes.mensaje')
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">PostVenta</h3>
                <a href="{{route('crear_postventa')}}" class="btn btn-success btn-sm pull-right">Crear PostVenta</a>
            </div>
            <div class="box-body">
                <table class="table table-striped table-bordered table-hover" id="tabla-data">
                    <thead>
                        <tr>
                            <th>Acción</th>
                            <th>Título</th> <!-- borrar cuando esté lista la tabla ajustada a la pantalla -->
                            <th>Título 1</th>
                            <th>Botón 1</th>
                            <th>Botón 2</th>
                            <th>Botón 3</th>
                            <th>Botón 4</th>
                            <th>Botón 5</th>
                            <th>Botón 6</th>
                            <th>Título 2</th>
                            <th>Botón 7</th>
                            <th>Botón 8</th>
                            <th>Botón 9</th>
                            <th>Título 3</th>

                           
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($datas as $data)
                        <tr>
                            <td> <!-- Inicio botones -->
                                <a href="{{route('editar_postventa', ['id' => $data->id])}}" class="btn-accion-tabla tooltipsC" title="Editar este registro">
                                    <i class="fa fa-fw fa-pencil"></i>
                                </a>
                                <form action="{{route('eliminar_postventa', ['id' => $data->id])}}" class="d-inline form-eliminar" method="POST">
                                    @csrf @method("delete")
                                    <button type="submit" class="btn-accion-tabla eliminar tooltipsC" title="Eliminar este registro">
                                        <i class="fa fa-fw fa-trash text-danger"></i>
                                    </button>
                                </form>
                            </td> <!-- fin botones -->

                            <td>{{$data->titulo}}</td>                          
                            <td>{{$data->titulo1}}</td>
                            <td>{{$data->btn1}}</td>
                            <td>{{$data->btn2}}</td>
                            <td>{{$data->btn3}}</td>
                            <td>{{$data->btn4}}</td>
                            <td>{{$data->btn5}}</td>
                            <td>{{$data->btn6}}</td>
                            <td>{{$data->titulo2 }}</td>
                            <td>{{$data->btn7}}</td>
                            <td>{{$data->btn8}}</td>
                            <td>{{$data->btn9}}</td>
                            <td>{{$data->titulo3}}</td>

                            
                         <!--  <td> 
                                <a href="{{route('editar_postventa', ['id' => $data->id])}}" class="btn-accion-tabla tooltipsC" title="Editar este registro">
                                    <i class="fa fa-fw fa-pencil"></i>
                                </a>
                                <form action="{{route('eliminar_postventa', ['id' => $data->id])}}" class="d-inline form-eliminar" method="POST">
                                    @csrf @method("delete")
                                    <button type="submit" class="btn-accion-tabla eliminar tooltipsC" title="Eliminar este registro">
                                        <i class="fa fa-fw fa-trash text-danger"></i>
                                    </button>
                                </form>
                            </td> -->
                            
                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection