<div class="form-group">
    <label for="nombre_empleado" class="col-lg-3 control-label requerido">Nombre Trabajador</label>
               <div class="col-lg-8">
                    <input type="text" name="nombre_empleado" id="nombre_empleado" class="form-control" value="{{old('nombre_empleado', $data->nombre_empleado ?? '')}}" required/>
                  </div>
</div>
<!----------->
<div class="form-group">
    <label for="cargo_id" class="col-lg-3 control-label requerido">Cargo</label>
               <div class="col-lg-8">
                   <select class="form-control" id="cargo_id" name="cargo_id" required>
                    <option value="" selected disabled hidden>Seleccionar</option>
                                        @foreach(\App\Models\Admin\Cargo::all() as $cargo)
                                            <option value="{{$cargo->id}}"  @if($cargo->id==$data->cargo_id) selected='selected' @endif>{{$cargo->nombre}}</option>
                                        @endforeach
                    </select>
               </div>
</div>
<!----------->
<div class="form-group">
    <label for="telefono" class="col-lg-3 control-label requerido">Telefono</label>
               <div class="col-lg-8">
                    <input type="number" name="telefono" id="telefono" class="form-control" value="{{old('telefono', $data->telefono ?? '')}}" required />
                  </div>
</div>
<!----------->
<div class="form-group">
    <label for="correo" class="col-lg-3 control-label requerido">Correo Electronico</label>
               <div class="col-lg-8">
                    <input type="text" name="correo" id="correo" class="form-control" value="{{old('correo', $data->correo ?? '')}}" required/>
                  </div>
</div>
<div class="form-group">
    <label for="img" class="col-lg-3 control-label requerido">Img Slider</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="img" id="img" class="form-control" value="{{old('img', $data->img ?? '')}}"  /><br>
                    <label ><img src="/storage/{{old('img', $data->img ?? '')}}" style="width: 120%;"></label>
                </div>
</div> 

