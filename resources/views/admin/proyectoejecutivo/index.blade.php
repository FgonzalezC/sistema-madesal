@extends("theme.$theme.layout")
@section('titulo')
Empleado
@endsection

@section("scripts")
<script src="{{asset("assets/pages/scripts/admin/index.js")}}" type="text/javascript"></script>
@endsection

@section('contenido')
<div class="row">
    <div class="col-lg-12">
        @include('includes.mensaje')
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Empleados</h3>
                <div class="box-tools pull-right">
                    <a href="{{route('crear_proyectoejecutivo')}}" class="btn btn-block btn-success btn-sm">
                        <i class="fa fa-fw fa-plus-circle"></i> Nuevo registro
                    </a>
                </div>
            </div>
            <div class="box-body">
                <table class="table table-striped table-bordered table-hover" id="tabla-data">
                    <thead>
                        <tr>
                            <th>Proyecto</th>
                            <th>Ejecutivo</th>                          
                            <th class="width70"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($datas as $data)
                        <tr>
                            <td>{{\App\Models\Admin\Proyecto::find($data->proyecto_id)->nombre_proyecto}}</td>
                            <td>{{\App\Models\Admin\Empleado::find($data->empleado_id)->nombre_empleado}}</td>                           
                            <td>
                                <a href="{{route('editar_proyectoejecutivo', ['id' => $data->id])}}" class="btn-accion-tabla tooltipsC" title="Editar este registro">
                                    <i class="fa fa-fw fa-pencil"></i>
                                </a>
                                <form action="{{route('eliminar_proyectoejecutivo', ['id' => $data->id])}}" class="d-inline form-eliminar" method="POST">
                                    @csrf @method("delete")
                                    <button type="submit" class="btn-accion-tabla eliminar tooltipsC" title="Eliminar este registro">
                                        <i class="fa fa-fw fa-trash text-danger"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
             <div class="col-sm-7">
                                <div class="dataTables_paginate paging_simple_numbers"
                                     id="example2_paginate">
                                    {{ $datas->links() }}
                                </div>
                            </div>
        </div>
        </div>
    </div>
</div>
@endsection