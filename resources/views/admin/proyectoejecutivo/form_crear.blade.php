<!----------->
<div class="form-group">
    <label for="proyecto_id" class="col-lg-3 control-label requerido">Proyecto</label>
               <div class="col-lg-8">
                   <select class="form-control" id="proyecto_id" name="proyecto_id" required>
                    <option value="" selected disabled hidden>Seleccionar</option>
                                        @foreach(\App\Models\Admin\Proyecto::all() as $proyecto)
                                            <option value="{{$proyecto->id}}">{{$proyecto->nombre_proyecto}} / {{\App\Models\Admin\TipoEtapa::find($proyecto->tipoetapa_id)->nombre_etapa}} </option>
                                        @endforeach
                    </select>
               </div>
</div>

<!----------->
<div class="form-group">
    <label for="empleado_id" class="col-lg-3 control-label requerido">Ejecutivo</label>
               <div class="col-lg-8">
                   <select class="form-control" id="empleado_id" name="empleado_id" required>
                    <option value="" selected disabled hidden>Seleccionar</option>
                                        @foreach(\App\Models\Admin\Empleado::all() as $empleado)
                                          @if($empleado->cargo_id == 1) <option value="{{$empleado->id}}">{{$empleado->nombre_empleado}}</option> @endif
                                        @endforeach
                    </select>
               </div>
</div>