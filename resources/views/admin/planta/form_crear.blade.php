<!-- ------------------Imagen Baja------------------ -->
<div class="form-group">
  <label for="img_baja" class="col-lg-3 control-label requerido">Img Baja</label>
  <div class="col-lg-8">
    <input type="file" class="custom-file-input" name="img_baja" id="img_baja" class="form-control" value="{{old('img_baja', $data->img_baja ?? '')}}"  />
  </div>
</div> 

<!-- ------------------Imagen alta------------------ -->
<div class="form-group">
  <label for="img_alta" class="col-lg-3 control-label requerido">Img Alta</label>
  <div class="col-lg-8">
    <input type="file" class="custom-file-input" name="img_alta" id="img_alta" class="form-control" value="{{old('img_alta', $data->img_alta ?? '')}}"  />
  </div>
</div> 
<!-- ------------------Imagen MAster Plan------------------ -->
<div class="form-group">
  <label for="img_3era" class="col-lg-3 control-label requerido">Img 3era</label>
  <div class="col-lg-8">
    <input type="file" class="custom-file-input" name="img_3era" id="img_3era" class="form-control" value="{{old('img_3era', $data->img_3era ?? '')}}"  />
  </div>
</div> 
<!-- ------------------Imagen MAster Plan------------------ -->
<div class="form-group">
  <label for="img_areaverde" class="col-lg-3 control-label requerido">Img Area Verde</label>
  <div class="col-lg-8">
    <input type="file" class="custom-file-input" name="img_areaverde" id="img_areaverde" class="form-control" value="{{old('img_areaverde', $data->img_areaverde ?? '')}}"  />
  </div>
</div> 
<!-- ------------------Modelo------------------ -->
<div class="form-group">
  <label for="modelo_id" class="col-lg-3 control-label requerido">Modelo</label>
  <div class="col-lg-8">
   <select class="form-control" id="modelo_id" name="modelo_id" required>


    <option value="" selected disabled hidden>Seleccionar</option>
    @foreach(\App\Models\Admin\Modelo::all() as $modelo)
    <?php
    
    $proyecto = \DB::table('modelos')
    ->select('nombre_etapa')
    ->where('modelos.id', $modelo->id)
    ->join('proyectos','proyectos.id','modelos.proyecto_id')
    ->join('tipo_etapas','proyectos.tipoetapa_id','tipo_etapas.id')
    ->get();
    ?>
    <option value="{{$modelo->id}}">{{$modelo->nombre_modelo}}  / {{\App\Models\Admin\Proyecto::find($modelo->proyecto_id)->nombre_proyecto}}
      /
      @foreach($proyecto as $etapa)
        {{$etapa->nombre_etapa}}
      @endforeach 
    </option>
    @endforeach
  </select>
</div>
</div>
<!---->