@extends("theme.$theme.layout")
@section('titulo')
Plantas
@endsection

@section("scripts")
<script src="{{asset("assets/pages/scripts/admin/index.js")}}" type="text/javascript"></script>
@endsection

@section('contenido')
<div class="row">
    <div class="col-lg-12">
        @include('includes.mensaje')
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Plantas</h3>
                <div class="box-tools pull-right">
                    <a href="{{route('crear_planta')}}" class="btn btn-block btn-success btn-sm">
                        <i class="fa fa-fw fa-plus-circle"></i> Nuevo registro
                    </a>
                </div>
            </div>
            <div class="box-body">
                <table class="table table-striped table-bordered table-hover" id="tabla-data">
                    <thead>
                        <tr>

                            <th>Imagen Planta Baja</th>
                            <th>Imagen Planta Alta</th>
                            <th>Imagen 3era Planta</th>
                            <th>Imagen Planta Verde</th>
                            <th>Nombre Modelo</th>
                            <th>Proyecto</th>

                            <th class="width70"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($datas as $data)
                        <tr>

                            <td>{{$data->img_baja}}</td>
                            <td>{{$data->img_alta}}</td>
                            <td>{{$data->img_3era}}</td>
                            <td>{{$data->img_areaverde}}</td>
                            <td>{{\App\Models\Admin\Modelo::find($data->modelo_id)->nombre_modelo}}</td>

                            <?php
                            $proyecto = \DB::table('modelos')
                            ->select('nombre_proyecto','nombre_etapa')
                            ->where('modelos.id', $data->modelo_id)
                            ->join('proyectos','proyectos.id','modelos.proyecto_id')
                            ->join('tipo_etapas','proyectos.tipoetapa_id','tipo_etapas.id')
                            ->get();
                            ?>
                            @foreach($proyecto as $mod)
                            <td>{{$mod->nombre_proyecto}} / {{$mod->nombre_etapa}}</td>
                            @endforeach
                            <td>
                                <a href="{{route('editar_planta', ['id' => $data->id])}}" class="btn-accion-tabla tooltipsC" title="Editar este registro">
                                    <i class="fa fa-fw fa-pencil"></i>
                                </a>
                                <form action="{{route('eliminar_planta', ['id' => $data->id])}}" class="d-inline form-eliminar" method="POST">
                                    @csrf @method("delete")
                                    <button type="submit" class="btn-accion-tabla eliminar tooltipsC" title="Eliminar este registro">
                                        <i class="fa fa-fw fa-trash text-danger"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection