@extends("theme.$theme.layout")
@section('titulo')
	Crear Planta
@endsection

@section('scripts')
<script src="{{asset("assets/pages/scripts/admin/menu/crear.js")}}" type="text/javascript"></script>
@endsection

@section('contenido')
	<div class="row">
        <div class="col-lg-12">
          @include('includes.form-error')
          @include('includes.mensaje')
        	<div class="box box-danger">
            	<div class="box-header with-border">
             		 <h3 class="box-title">Crear Planta</h3>
                  <a href="{{route('planta')}}" class="btn btn-info btn-sm pull-right"> Listado Planta</a>
           		 </div>
                <form action="{{route('guardar_planta')}}" id= "form-general" class="form-horizontal" method="POST" autocomplete="off" action="upload" enctype="multipart/form-data">
                {{ csrf_field() }}
                @csrf
           		   <div class="box-body ">
           		   	 @include('admin.planta.form_crear')
           		   </div>
                 <div class="box-footer">
                  <div class="col-lg-3"></div>
                  <div class="col-lg-6">
                    @include('includes.boton-form-crear')
                  </div>
                 </div>
               </form>
        	</div>
        </div>
    </div>
@endsection

