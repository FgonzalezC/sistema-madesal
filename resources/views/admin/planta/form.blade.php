
<div class="form-group">
  <label for="img_baja" class="col-lg-3 control-label requerido">Img Planta Baja</label>
  <div class="col-lg-8">
    <input type="file" class="custom-file-input" name="img_baja" id="img_baja" class="form-control" value="{{old('img_baja', $data->img_baja ?? '')}}"  /><br>
    @if($data->img_baja!=NULL)
    <label ><img src="/storage/{{old('img2', $data->img_baja ?? '')}}" style="width: 50%;"></label>
       <a onclick="return confirm('Esta seguro que quiere eliminar la imagen')" href="{{route('eliminar_fotoplanta',['id' => $data->id,'dato'=>$data->img_baja])}}" class="btn-accion-tabla eliminar tooltipsC" >
        <i class="fa fa-fw fa-trash text-danger"></i>
      </a>
    @endif
  </div>
</div> 

<div class="form-group">
  <label for="img_alta" class="col-lg-3 control-label requerido">Img Planta Alta</label>
  <div class="col-lg-8">
    <input type="file" class="custom-file-input" name="img_alta" id="img_alta" class="form-control" value="{{old('img_alta', $data->img_alta ?? '')}}"  /><br>
    @if($data->img_alta!=NULL)

    <label ><img src="/storage/{{old('img2', $data->img_alta ?? '')}}" style="width: 50%;"></label>
    <a onclick="return confirm('Esta seguro que quiere eliminar la imagen')" href="{{route('eliminar_fotoplanta',['id' => $data->id,'dato'=>$data->img_alta])}}" class="btn-accion-tabla eliminar tooltipsC" >
        <i class="fa fa-fw fa-trash text-danger"></i>
      </a>
    @endif
  </div>
</div> 
<div class="form-group">
  <label for="img_3era" class="col-lg-3 control-label requerido">Img 3era Planta</label>
  <div class="col-lg-8">
    <input type="file" class="custom-file-input" name="img_3era" id="img_3era" class="form-control" value="{{old('img_3era', $data->img_3era ?? '')}}"  /><br>
    @if($data->img_3era)

    <label ><img src="/storage/{{old('img2', $data->img_3era ?? '')}}" style="width: 50%;"></label>
    <a onclick="return confirm('Esta seguro que quiere eliminar la imagen')" href="{{route('eliminar_fotoplanta',['id' => $data->id,'dato'=>$data->img_3era])}}" class="btn-accion-tabla eliminar tooltipsC" >
        <i class="fa fa-fw fa-trash text-danger"></i>
      </a>
    @endif
  </div>
</div> 
<div class="form-group">
  <label for="img_areaverde" class="col-lg-3 control-label requerido">Img Area Verde</label>
  <div class="col-lg-8">
    <input type="file" class="custom-file-input" name="img_areaverde" id="img_areaverde" class="form-control" value="{{old('img_areaverde', $data->img_areaverde ?? '')}}"  /><br>
    @if($data->img_areaverde!=NULL)


    <label ><img src="/storage/{{old('img2', $data->img_areaverde ?? '')}}" style="width: 50%;"></label>
    <a onclick="return confirm('Esta seguro que quiere eliminar la imagen')" href="{{route('eliminar_fotoplanta',['id' => $data->id,'dato'=>$data->img_areaverde])}}" class="btn-accion-tabla eliminar tooltipsC" >
        <i class="fa fa-fw fa-trash text-danger"></i>
      </a>
    @endif
  </div>
</div> 
<div class="form-group">
  <label for="modelo_id" class="col-lg-3 control-label requerido">Modelo</label>
  <div class="col-lg-8">
   <select class="form-control" id="modelo_id" name="modelo_id" required>
    <?php
    $proyecto = \DB::table('modelos')
    ->select('nombre_etapa')
    ->where('modelos.id', $data->modelo_id)
    ->join('proyectos','proyectos.id','modelos.proyecto_id')
    ->join('tipo_etapas','proyectos.tipoetapa_id','tipo_etapas.id')
    ->get();
    ?>
    <option value="" selected disabled hidden>Seleccionar</option>

    
    @foreach(\App\Models\Admin\Modelo::all() as $modelo)
    <option value="{{ $modelo->id }}"  @if($modelo->id==$data->modelo_id) selected='selected' @endif >{{ $modelo->nombre_modelo }} / {{\App\Models\Admin\Proyecto::find($modelo->proyecto_id)->nombre_proyecto}}/
      @foreach($proyecto as $etapa)
      {{$etapa->nombre_etapa}}
      @endforeach

    </option>

    @endforeach
  </select>
</div>
</div>



