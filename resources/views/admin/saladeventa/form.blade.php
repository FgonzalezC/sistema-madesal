<div class="form-group">
    <label for="nombre" class="col-lg-3 control-label requerido">Nombre</label>
               <div class="col-lg-8">
                    <input type="text" name="nombre" id="nombre" class="form-control" value="{{old('nombre', $data->nombre ?? '')}}" required />
                </div>
</div> 
<div class="form-group">
    <label for="direccion" class="col-lg-3 control-label requerido">Direccion</label>
               <div class="col-lg-8">
                    <input type="text"  name="direccion" id="direccion" class="form-control" value="{{old('direccion', $data->direccion ?? '')}}"  />
                </div>
</div> 

<div class="form-group">
    <label for="telefono" class="col-lg-3 control-label requerido">Telefono</label>
               <div class="col-lg-8">
                    <input type="text"  name="telefono" id="telefono" class="form-control" value="{{old('telefono', $data->telefono ?? '')}}"  />
                </div>
</div> 
<div class="form-group">
    <label for="telefono2" class="col-lg-3 control-label requerido">Celular</label>
               <div class="col-lg-8">
                    <input type="text"  name="telefono2" id="telefono2" class="form-control" value="{{old('telefono2', $data->telefono2 ?? '')}}"  />
                </div>
</div> 
<div class="form-group">
    <label for="mail" class="col-lg-3 control-label requerido">Mail</label>
               <div class="col-lg-8">
                    <input type="text"  name="mail" id="mail" class="form-control" value="{{old('mail', $data->mail ?? '')}}"  />
                </div>
</div> 

<div class="form-group">
    <label for="mail2" class="col-lg-3 control-label requerido">Mail 2</label>
               <div class="col-lg-8">
                    <input type="text"  name="mail2" id="mail2" class="form-control" value="{{old('mail2', $data->mail2 ?? '')}}"  />
                </div>
</div>
<div class="form-group">
    <label for="horario" class="col-lg-3 control-label requerido">Horario</label>
               <div class="col-lg-8">
                    <input type="text"  name="horario" id="horario" class="form-control" value="{{old('horario', $data->horario ?? '')}}"  />
                </div>
</div> 
<div class="form-group">
    <label for="mapa" class="col-lg-3 control-label requerido">Mapa</label>
               <div class="col-lg-8">
                    <input type="text" name="mapa" id="mapa" class="form-control" value="{{old('mapa', $data->mapa ?? '')}}"  />
                </div>
</div> 




