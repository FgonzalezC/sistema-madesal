@extends("theme.$theme.layout")
@section('titulo')
     Sala de Ventas
@endsection

@section("scripts")
<script src="{{asset("assets/pages/scripts/admin/index.js")}}" type="text/javascript"></script>
@endsection

@section('contenido')
<div class="row">
    <div class="col-lg-12">
        @include('includes.mensaje')
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Sala de ventas</h3>
                <a href="{{route('crear_saladeventa')}}" class="btn btn-success btn-sm pull-right">Crear Sala de Venta</a>
            </div>
            <div class="box-body table-responsive no-padding">
                <table class="table table-striped table-bordered table-hover" id="tabla-data">
                    <thead>
                        <tr>
                            <th>Nombre Sucursal</th>
                            <th>Direccion</th>
                            <th>Telefono</th>
                            <th>Celular</th>
                            <th>Mail</th>
                            <th>Mail2</th>
                            <th>Horario</th>
                            <th>Mapa</th>
                            <th>Accion</th>
                            <th class="width70"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($datas as $data)
                            <tr>
                                <td>{{$data->nombre}}</td>
                                <td>{{$data->direccion}}</td>
                                <td>{{$data->telefono}}</td>
                                 <td>{{$data->telefono2}}</td>
                                <td>{{$data->mail}}</td>
                                <td>{{$data->mail2}}</td>
                                <td>{{$data->horario}}</td>
                                <td>{{$data->mapa}}</td>
                                <td>
                                    <a href="{{route("editar_saladeventa", ['id' => $data->id])}}" class="btn-accion-tabla tooltipsC" title="Editar este registro">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    <form action="{{route("eliminar_saladeventa",  ['id' => $data->id])}}" class="d-inline form-eliminar" method="POST">
                                        @csrf @method("delete")
                                        <button type="submit" class="btn-accion-tabla eliminar tooltipsC" title="Eliminar este registro"><i class="fa fa-times-circle text-danger"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection