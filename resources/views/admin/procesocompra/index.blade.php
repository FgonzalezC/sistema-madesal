@extends("theme.$theme.layout")
@section('titulo')
Proceso Compra
@endsection

@section("scripts")
<script src="{{asset("assets/pages/scripts/admin/index.js")}}" type="text/javascript"></script>
@endsection

@section('contenido')
<div class="row">
    <div class="col-lg-12">
        @include('includes.mensaje')
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Proyectos</h3>
                <div class="box-tools pull-right">

                    <a href="{{route('crear_procesocompra')}}" class="btn btn-block btn-success btn-sm">
                        <i class="fa fa-fw fa-plus-circle"></i> Nuevo registro
                    </a>

                </div>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover" id="tabla-data">
                    <thead>
                        <tr>
                            <th>Accion</th> <!-- borrar cuando esté lista la tabla ajustada a la pantalla -->
                            <th>Titulo</th>
                            <th>Titulo Cotizacion</th>
                            <th>Bajada Cotizacion</th>
                            <th>Imagen Cotizacion</th>
                            <th>Titulo Documentos</th>
                            <th>Bajada Documentos</th>
                            <th>Imagen Documentos</th>
                            <th>Titulo Reserva</th>
                            <th>Bajada Reserva</th>
                            <th>Imagen Reserva</th>
                            <th>Titulo Plan de Pago</th>
                            <th>Bajada Plan de Pago</th>
                            <th>Imagen Plan de Pago</th>
                            <th>Titulo Escritura</th>
                            <th>Bajada Escritura</th>
                            <th>Imagen Escritura</th>
                            <th>Titulo Entrega</th>
                            <th>Bajada Entrega</th>
                            <th>Imagen Entrega</th>
                            <th class="width70"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($datas as $data)
                        <tr>
                            <td> <!-- Inicio botones -->
                                <a href="{{route('editar_procesocompra', ['id' => $data->id])}}" class="btn-accion-tabla tooltipsC" title="Editar este registro">
                                    <i class="fa fa-fw fa-pencil"></i>
                                </a>
                                <form action="{{route('eliminar_procesocompra', ['id' => $data->id])}}" class="d-inline form-eliminar" method="POST">
                                    @csrf @method("delete")
                                    <button type="submit" class="btn-accion-tabla eliminar tooltipsC" title="Eliminar este registro">
                                        <i class="fa fa-fw fa-trash text-danger"></i>
                                    </button>
                                </form>
                            </td> <!-- fin botones -->

                            <td>{{$data->titulo}}</td>
                            <td>{{$data->titulo1}}</td>
                            <td>{{$data->bajada1}}</td>
                            <td>{{$data->img1}}</td>
                            <td>{{$data->titulodoc}}</td>
                            <td>{{$data->bajadadoc}}</td>
                            <td>{{$data->imgdoc}}</td>
                            <td>{{$data->titulo2}}</td>
                            <td>{{$data->bajada2}}</td>
                            <td>{{$data->img2}}</td>
                            <td>{{$data->titulo3}}</td>
                            <td>{{$data->bajada3}}</td>
                            <td>{{$data->img3 }}</td>
                            <td>{{$data->titulo4}}</td>
                            <td>{{$data->bajada4}}</td>
                            <td>{{$data->img4}}</td>
                            <td>{{$data->titulo5}}</td>
                            <td>{{$data->bajada5}}</td>
                            <td>{{$data->img5}}</td>                            
                         <!--  <td> 
                                <a href="{{route('editar_proyecto', ['id' => $data->id])}}" class="btn-accion-tabla tooltipsC" title="Editar este registro">
                                    <i class="fa fa-fw fa-pencil"></i>
                                </a>
                                <form action="{{route('eliminar_proyecto', ['id' => $data->id])}}" class="d-inline form-eliminar" method="POST">
                                    @csrf @method("delete")
                                    <button type="submit" class="btn-accion-tabla eliminar tooltipsC" title="Eliminar este registro">
                                        <i class="fa fa-fw fa-trash text-danger"></i>
                                    </button>
                                </form>
                            </td> -->
                            
                        </tr>
                        @endforeach

                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection