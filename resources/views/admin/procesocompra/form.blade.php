<div class="form-group">
    <label for="titulo" class="col-lg-3 control-label requerido">Titulo</label>
               <div class="col-lg-8">
                    <input type="text" name="titulo" id="titulo" class="form-control" value="{{old('titulo', $data->titulo ?? '')}}" required />
                </div>
</div> 
<!---->
<div class="form-group">
    <label for="titulo1" class="col-lg-3 control-label requerido">Titulo Cotizacion</label>
               <div class="col-lg-8">
                    <input type="text" name="titulo1" id="titulo1" class="form-control" value="{{old('titulo1', $data->titulo1 ?? '')}}" required />
                </div>
</div> 
<!---->

<div class="form-group">
    <label for="bajada1" class="col-lg-3 control-label requerido">Bajada Cotizacion</label>
               <div class="col-lg-8">
                    <textarea type="text" class="form-control" style="resize:none" name="bajada1" id="bajada1" rows="5" cols="33" wrap="hard" required>{{old('bajada1', $data->bajada1 ?? '')}}</textarea>
                </div>
</div> 
<!---->
<div class="form-group">
    <label for="imgdoc" class="col-lg-3 control-label requerido">Imagen Cotizacion</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="img1" id="img1" class="form-control" value="{{old('img1', $data->img1 ?? '')}}"  /><br>
                    <label ><img src="/storage/{{old('img2', $data->img1 ?? '')}}" style="width: 50%;"></label>
                </div>
</div> 
<div class="form-group">
    <label for="titulodoc" class="col-lg-3 control-label requerido">Titulo Documentos</label>
               <div class="col-lg-8">
                    <input type="text" name="titulodoc" id="titulodoc" class="form-control" value="{{old('titulodoc', $data->titulodoc ?? '')}}" required />
                </div>
</div> 
<!---->

<div class="form-group">
    <label for="bajadadoc" class="col-lg-3 control-label requerido">Bajada Documentos</label>
               <div class="col-lg-8">
                    <textarea type="text" class="form-control" style="resize:none" name="bajadadoc" id="bajadadoc" rows="5" cols="33" wrap="hard" required>{{old('bajadadoc', $data->bajadadoc ?? '')}}</textarea>
                </div>
</div> 
<!---->
<div class="form-group">
    <label for="imgdoc" class="col-lg-3 control-label requerido">Imagen Documentos</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="imgdoc" id="imgdoc" class="form-control" value="{{old('imgdoc', $data->imgdoc ?? '')}}"  /><br>
                    <label ><img src="/storage/{{old('img2', $data->imgdoc ?? '')}}" style="width: 50%;"></label>
                </div>
</div> 
<!---->
<div class="form-group">
    <label for="titulo2" class="col-lg-3 control-label requerido">Titulo Reserva</label>
               <div class="col-lg-8">
                    <input type="text" name="titulo2" id="titulo2" class="form-control" value="{{old('titulo2', $data->titulo2 ?? '')}}" required />
                </div>
</div> 
<!---->

<div class="form-group">
    <label for="bajada2" class="col-lg-3 control-label requerido">Bajada Reserva</label>
               <div class="col-lg-8">
                    <textarea type="text" class="form-control" style="resize:none" name="bajada2" id="bajada2" rows="5" cols="33" wrap="hard" required>{{old('bajada2', $data->bajada2 ?? '')}}</textarea>
                </div>
</div> 
<!---->
<div class="form-group">
    <label for="img2" class="col-lg-3 control-label requerido">Imagen Reserva</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="img2" id="img2" class="form-control" value="{{old('img2', $data->img2 ?? '')}}"  /><br>
                    <label ><img src="/storage/{{old('img2', $data->img2 ?? '')}}" style="width: 50%;"></label>
                </div>
</div> 
<!---->
<div class="form-group">
    <label for="titulo3" class="col-lg-3 control-label requerido">Titulo Plan de Pago</label>
               <div class="col-lg-8">
                    <input type="text" name="titulo3" id="titulo3" class="form-control" value="{{old('titulo3', $data->titulo3 ?? '')}}" required />
                </div>
</div> 
<!---->

<div class="form-group">
    <label for="bajada3" class="col-lg-3 control-label requerido">Bajada Plan de Pago</label>
               <div class="col-lg-8">
                    <textarea type="text" class="form-control" style="resize:none" name="bajada3" id="bajada3" rows="5" cols="33" wrap="hard" required>{{old('bajada3', $data->bajada3 ?? '')}}</textarea>
                </div>
</div> 
<!---->
<div class="form-group">
    <label for="img3" class="col-lg-3 control-label requerido">Imagen Plan de Pago</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="img3" id="img3" class="form-control" value="{{old('img3', $data->img3 ?? '')}}"  /><br>
                    <label ><img src="/storage/{{old('img2', $data->img3 ?? '')}}" style="width: 50%;"></label>
                </div>
</div> 
<!---->
<div class="form-group">
    <label for="titulo4" class="col-lg-3 control-label requerido">Titulo Escritura</label>
               <div class="col-lg-8">
                    <input type="text" name="titulo4" id="titulo4" class="form-control" value="{{old('titulo4', $data->titulo4 ?? '')}}" required />
                </div>
</div> 
<!---->

<div class="form-group">
    <label for="bajada4" class="col-lg-3 control-label requerido">Bajada Escritura</label>
               <div class="col-lg-8">
                    <textarea type="text" class="form-control" style="resize:none" name="bajada4" id="bajada4" rows="5" cols="33" wrap="hard" required>{{old('bajada4', $data->bajada4 ?? '')}}</textarea>
                </div>
</div> 
<!---->
<div class="form-group">
    <label for="img4" class="col-lg-3 control-label requerido">Imagen Escritura</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="img4" id="img4" class="form-control" value="{{old('img4', $data->img4 ?? '')}}"  /><br>
                    <label ><img src="/storage/{{old('img2', $data->img4 ?? '')}}" style="width: 50%;"></label>
                </div>
</div> 
<!---->
<div class="form-group">
    <label for="titulo5" class="col-lg-3 control-label requerido">Titulo Entrega</label>
               <div class="col-lg-8">
                    <input type="text" name="titulo5" id="titulo5" class="form-control" value="{{old('titulo5', $data->titulo5 ?? '')}}" required />
                </div>
</div> 
<!---->

<div class="form-group">
    <label for="bajada5" class="col-lg-3 control-label requerido">Bajada Entrega</label>
               <div class="col-lg-8">
                    <textarea type="text" class="form-control" style="resize:none" name="bajada5" id="bajada5" rows="5" cols="33" wrap="hard" required>{{old('bajada5', $data->bajada5 ?? '')}}</textarea>
                </div>
</div> 
<!---->
<div class="form-group">
    <label for="img5" class="col-lg-3 control-label requerido">Imagen Entrega</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="img5" id="img5" class="form-control" value="{{old('img5', $data->img5 ?? '')}}"  />
                    <br>
                    <label ><img src="/storage/{{old('img2', $data->img5 ?? '')}}" style="width: 50%;"></label>
                </div>
</div> 