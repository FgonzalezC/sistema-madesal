
<div class="form-group">
    <label for="img_footer" class="col-lg-3 control-label requerido">Logo</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="img_footer" id="img_footer" class="form-control" value="{{old('img_footer', $data->img_footer ?? '')}}"  /><br>
                    <label ><img src="/storage/{{old('img2', $data->img_footer ?? '')}}" style="width: 80%;"></label>
                </div>
</div> 
<!----------->
<div class="form-group">
    <label for="direccion" class="col-lg-3 control-label requerido">Direccion</label>
               <div class="col-lg-8">
                    <input type="text" name="direccion" id="direccion" class="form-control" value="{{old('direccion', $data->direccion ?? '')}}" required/>
                  </div>
</div>
<!----------->
<div class="form-group">
    <label for="telefono" class="col-lg-3 control-label requerido">Telefono</label>
               <div class="col-lg-8">
                    <input type="text" name="telefono" id="telefono" class="form-control" value="{{old('telefono', $data->telefono ?? '')}}" required />
                  </div>
</div>
<!----------->
<div class="form-group">
    <label for="correo" class="col-lg-3 control-label requerido">Correo</label>
               <div class="col-lg-8">
                    <input type="text" name="correo" id="correo" class="form-control" value="{{old('correo', $data->correo ?? '')}}" required />
                  </div>
</div>
<!----------->
<div class="form-group">
    <label for="link_face" class="col-lg-3 control-label requerido">Link de Facebook</label>
               <div class="col-lg-8">
                    <input type="text" name="link_face" id="link_face" class="form-control" value="{{old('link_face', $data->link_face ?? '')}}" required/>
                  </div>
</div>

<!----------->
<div class="form-group">
    <label for="link_instagram" class="col-lg-3 control-label requerido">Link de Instagram</label>
               <div class="col-lg-8">
                    <input type="text" name="link_instagram" id="link_instagram" class="form-control" value="{{old('link_instagram', $data->link_instagram ?? '')}}" required/>
                  </div>
</div>
<!----------->
<div class="form-group">
    <label for="link_twitter" class="col-lg-3 control-label requerido">Link de Twitter</label>
               <div class="col-lg-8">
                    <input type="text" name="link_twitter" id="link_twitter" class="form-control" value="{{old('link_twitter', $data->link_twitter ?? '')}}" required/>
                  </div>
</div>