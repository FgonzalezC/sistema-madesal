@extends("theme.$theme.layout")
@section('titulo')
Info Ficha Proyecto
@endsection

@section("scripts")
<script src="{{asset("assets/pages/scripts/admin/index.js")}}" type="text/javascript"></script>
@endsection

@section('contenido')
<div class="row">
    <div class="col-lg-12">
        @include('includes.mensaje')
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Sección Ficha Proyecto</h3>
                <div class="box-tools pull-right">

                    <a href="{{route('crear_seccion-fichaproyecto')}}" class="btn btn-block btn-success btn-sm">
                        <i class="fa fa-fw fa-plus-circle"></i> Nuevo registro
                    </a>

                </div>
            </div>
            <div class="box-body">
                <table class="table table-striped table-bordered table-hover" id="tabla-data" body style="overflow-x:hidden;">
                    <thead>
                        <tr>
                            <th>Modelo</th>
                           
                            <th>Terminaciones</th>
                            <th>Equipamiento</th>
                            <th>Ubicacion y entorno</th>
                            <th>Imagen Terminaciones</th>
                            <th>Imagen Terminaciones</th>
                            <th>Imagen Terminaciones</th>
                            <th>Imagen Terminaciones</th>
                            <th>Imagen Terminaciones</th>
                            <th>Imagen Equipamiento</th>
                            <th>Imagen Equipamiento</th>
                            <th>Imagen Equipamiento</th>
                            <th>Imagen Equipamiento</th>
                            <th>Imagen Equipamiento</th>
                            <th>Imagen Ubicacion</th>
                            <th>Imagen Ubicacion</th>
                            <th>Imagen Ubicacion</th>
                            <th>Imagen Ubicacion</th>
                            <th>Imagen Ubicacion</th>

                            <th class="width70"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($datas as $data)
                        <tr>
                             <td>
                                <a href="{{route('editar_seccion-fichaproyecto', ['id' => $data->id])}}" class="btn-accion-tabla tooltipsC" title="Editar este registro">
                                    <i class="fa fa-fw fa-pencil"></i>
                                </a>
                                <form action="{{route('eliminar_seccion-fichaproyecto', ['id' => $data->id])}}" class="d-inline form-eliminar" method="POST">
                                    @csrf @method("delete")
                                    <button type="submit" class="btn-accion-tabla eliminar tooltipsC" title="Eliminar este registro">
                                        <i class="fa fa-fw fa-trash text-danger"></i>
                                    </button>
                                </form>
                            </td>
                             <td>{{\App\Models\Admin\Modelo::find($data->modelo_id)->nombre_modelo}}</td>
                            <td>{{$data->terminaciones}}</td>
                            <td>{{$data->equipamiento}}</td>
                            <td>{{$data->ubicacion_entorno}}</td>
                            <td>{{$data->timg1}}</td>
                            <td>{{$data->timg2}}</td>
                            <td>{{$data->timg3}}</td>
                            <td>{{$data->timg4}}</td>
                            <td>{{$data->timg5}}</td>
                            <td>{{$data->eimg1}}</td>
                            <td>{{$data->eimg2}}</td>
                            <td>{{$data->eimg3}}</td>
                            <td>{{$data->eimg4}}</td>
                            <td>{{$data->eimg5}}</td>
                            <td>{{$data->uimg1}}</td>
                            <td>{{$data->uimg2}}</td>
                            <td>{{$data->uimg3}}</td>
                            <td>{{$data->uimg4}}</td>
                            <td>{{$data->uimg5}}</td>
                           
                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection