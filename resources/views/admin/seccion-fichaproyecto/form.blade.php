<div class="form-group">
    <label for="modelo_id" class="col-lg-3 control-label requerido">Modelo</label>
               <div class="col-lg-8">
                   <select class="form-control" id="modelo_id" name="modelo_id" required>
                    <option value="" selected disabled hidden>Seleccionar</option>
                                        @foreach(\App\Models\Admin\Modelo::all() as $modelo)
                                            <option value="{{$modelo->id}}">{{$modelo->nombre_modelo}}</option>
                                        @endforeach

                    </select>

               </div>
</div>
<div class="form-group">
    <label for="proyecto_id" class="col-lg-3 control-label requerido">Proyecto</label>
               <div class="col-lg-8">
                   <select class="form-control" id="proyecto_id" name="proyecto_id" required>
                    <option value="" selected disabled hidden>Seleccionar un Proyecto</option>
                                        @foreach(\App\Models\Admin\Proyecto::all() as $proyecto)
                                            <option value="{{$proyecto->id}}">{{$proyecto->nombre_proyecto}}</option>
                                        @endforeach

                    </select>

               </div>
</div>


<!-- ------------------Terminaciones------------------ -->
<div class="form-group">
  <label for="terminaciones" class="col-lg-3 control-label requerido">Terminaciones</label>
  <div class="col-lg-8">
    <textarea type="text" class="form-control" style="resize:none" name="terminaciones" id="terminaciones" rows="5" cols="33" wrap="hard" required>{{old('terminaciones', $data->terminaciones ?? '')}}</textarea> 
  </div>
</div> 

<!-- ------------------equipamiento------------------ -->
<div class="form-group">
  <label for="equipamiento" class="col-lg-3 control-label requerido">Equipamiento</label>
  <div class="col-lg-8">
    <textarea type="text" class="form-control" style="resize:none" name="equipamiento" id="equipamiento" rows="5" cols="33" wrap="hard" required>{{old('equipamiento', $data->equipamiento ?? '')}}</textarea> 
  </div>
</div> 

<!-- ------------------ubicacion_entorno------------------ -->
<!-- <form action="upload" method="post" enctype="multipart/form-data"> -->
<div class="form-group">
  <label for="ubicacion" class="col-lg-3 control-label requerido">Ubicacion y entorno</label>
  <div class="col-lg-8">
    <textarea type="text" class="form-control" style="resize:none" name="ubicacion" id="ubicacion" rows="5" cols="33" wrap="hard" required>{{old('ubicacion', $data->ubicacion ?? '')}}</textarea> 
  </div>
</div> 
<!-- </form> -->
<!-- ------------------Imagen Financiamiento  ------------------ -->

<!-- ------------------Imagen 1------------------ -->
<div class="form-group">
    <label for="tmig1" class="col-lg-3 control-label requerido">Foto Terminaciones</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="tmig1" id="tmig1" class="form-control" value="{{old('tmig1', $data->tmig1 ?? '')}}"  />
                </div>
</div> 
<!---->
<!-- ------------------Imagen terminacion 3 ------------------ -->
<div class="form-group">
    <label for="timg2" class="col-lg-3 control-label requerido">Foto Terminaciones</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="timg2" id="timg2" class="form-control" value="{{old('timg2', $data->timg2 ?? '')}}"  />
                </div>
</div> 
<!---->
<!-- ------------------Imagen terminacion 4 ------------------ -->
<div class="form-group">
    <label for="timg3" class="col-lg-3 control-label requerido">Foto Terminaciones</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="timg3" id="timg3" class="form-control" value="{{old('timg3', $data->timg3 ?? '')}}"  />
                </div>
</div> 
<!---->
<!-- ------------------Imagen terminacion 5 ------------------ -->
<div class="form-group">
    <label for="timg4" class="col-lg-3 control-label requerido">Foto Terminaciones</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="timg4" id="timg4" class="form-control" value="{{old('timg4', $data->timg4 ?? '')}}"  />
                </div>
</div> 
<!---->
<!-- ------------------Opciones de financiamiento------------------ -->
<div class="form-group">
    <label for="tmig5" class="col-lg-3 control-label requerido">Foto Terminaciones</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="tmig5" id="tmig5" class="form-control" value="{{old('tmig5', $data->tmig5 ?? '')}}"  />
                </div>
</div> 
<!---->
<div class="form-group">
    <label for="eimg1" class="col-lg-3 control-label requerido">Foto Equipamiento</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="eimg1" id="eimg1" class="form-control" value="{{old('eimg1', $data->eimg1 ?? '')}}"  />
                </div>
</div> 
<!---->
<!-- ------------------Imagen terminacion 3 ------------------ -->
<div class="form-group">
    <label for="eimg2" class="col-lg-3 control-label requerido">Foto Equipamiento</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="eimg2" id="eimg2" class="form-control" value="{{old('eimg2', $data->eimg2 ?? '')}}"  />
                </div>
</div> 
<!---->
<!-- ------------------Imagen terminacion 4 ------------------ -->
<div class="form-group">
    <label for="eimg3" class="col-lg-3 control-label requerido">Foto Equipamiento</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="eimg3" id="eimg3" class="form-control" value="{{old('eimg3', $data->eimg3 ?? '')}}"  />
                </div>
</div> 
<!---->
<!-- ------------------Imagen terminacion 5 ------------------ -->
<div class="form-group">
    <label for="eimg4" class="col-lg-3 control-label requerido">Foto Equipamiento</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="eimg4" id="eimg4" class="form-control" value="{{old('eimg4', $data->eimg4 ?? '')}}"  />
                </div>
</div> 
<!---->
<!-- ------------------Opciones de financiamiento------------------ -->
<div class="form-group">
    <label for="eimg5" class="col-lg-3 control-label requerido">Foto Equipamiento</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="eimg5" id="eimg5" class="form-control" value="{{old('eimg5', $data->eimg5 ?? '')}}"  />
                </div>
</div> 
<!---->
<div class="form-group">
    <label for="uimg1" class="col-lg-3 control-label requerido">Foto Ubicacion</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="uimg1" id="uimg1" class="form-control" value="{{old('uimg1', $data->uimg1 ?? '')}}"  />
                </div>
</div> 
<!---->
<!-- ------------------Imagen terminacion 3 ------------------ -->
<div class="form-group">
    <label for="uimg2" class="col-lg-3 control-label requerido">Foto Ubicacion</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="uimg2" id="uimg2" class="form-control" value="{{old('uimg2', $data->uimg2 ?? '')}}"  />
                </div>
</div> 
<!---->
<!-- ------------------Imagen terminacion 4 ------------------ -->
<div class="form-group">
    <label for="uimg3" class="col-lg-3 control-label requerido">Foto Ubicacion</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="uimg3" id="uimg3" class="form-control" value="{{old('uimg3', $data->uimg3 ?? '')}}"  />
                </div>
</div> 
<!---->
<!-- ------------------Imagen terminacion 5 ------------------ -->
<div class="form-group">
    <label for="uimg4" class="col-lg-3 control-label requerido">Foto Ubicacion</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="uimg4" id="uimg4" class="form-control" value="{{old('uimg4', $data->uimg4 ?? '')}}"  />
                </div>
</div> 
<!---->
<!-- ------------------Opciones de financiamiento------------------ -->
<div class="form-group">
    <label for="uimg5" class="col-lg-3 control-label requerido">Foto Ubicacion</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="uimg5" id="uimg5" class="form-control" value="{{old('uimg5', $data->uimg5 ?? '')}}"  />
                </div>
</div> 
<!---->

