@extends("theme.$theme.layout")
@section('titulo')
	Crear
@endsection

@section('scripts')
<script src="{{asset("assets/pages/scripts/admin/menu/crear.js")}}" type="text/javascript"></script>
<script src="{{asset("assets/pages/scripts/admin/customs/file-input.js")}}" type="text/javascript"></script> 
<script src="{{asset("assets/pages/scripts/admin/customs/bootstrap-4.3.1/js/bootstrap.min.js")}}" type="text/javascript"></script>
<!-- <link href="{{ asset('assets/pages/scripts/admin/customs/bootstrap-4.3.1/css/bootstrap.min.css') }}" rel="stylesheet"> -->
@endsection



@section('contenido')
	<div class="row">
        <div class="col-lg-12">
          @include('includes.form-error')
          @include('includes.mensaje')
        	<div class="box box-danger">
            	<div class="box-header with-border">
             		 <h3 class="box-title">Nuevo registro</h3>
                 <div class="box-tools pull-right">
                    <a href="{{route('seccion-fichaproyecto')}}" class="btn btn-block btn-info btn-sm">
                        <i class="fa fa-fw fa-reply-all"></i> Volver
                    </a>
                </div>
           		 </div>
               <form action="{{route('guardar_seccion-fichaproyecto')}}" id= "form-general" class="form-horizontal" method="POST" autocomplete="off" action="upload" enctype="multipart/form-data">
                {{ csrf_field() }}
                @csrf

           		   <div class="box-body ">
           		   	 @include('admin.seccion-fichaproyecto.form')
           		   </div>
                 <div class="box-footer" align="center">
                  <div class="col-lg-3"></div>
                  <div class="col-lg-6">
                    @include('includes.boton-form-crear')
                  </div>
                    
                 </div>
               </form>
        	</div>
        </div>
    </div>
@endsection