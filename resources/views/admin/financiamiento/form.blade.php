<div class="form-group">
    <label for="titulo" class="col-lg-3 control-label requerido">Titulo</label>
               <div class="col-lg-8">
                    <input type="text" name="titulo" id="titulo" class="form-control" value="{{old('titulo', $data->titulo ?? '')}}" required />
                </div>
</div> 
<!---->
<div class="form-group">
    <label for="img" class="col-lg-3 control-label requerido">Imagen Financiamiento</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="img" id="img" class="form-control" value="{{old('img', $data->img ?? '')}}"  /><br>
                    <label ><img src="/storage/{{old('img2', $data->img ?? '')}}" style="width: 50%;"></label>
                </div>
</div> 

<div class="form-group">
    <label for="subtitulo" class="col-lg-3 control-label requerido">Subtitulo</label>
               <div class="col-lg-8">
                    <input type="text" name="subtitulo" id="subtitulo" class="form-control" value="{{old('subtitulo', $data->subtitulo ?? '')}}" required />
                </div>
</div> 
<!---->
<div class="form-group">
    <label for="titulo1" class="col-lg-3 control-label requerido">Titulo Pie en Coutas</label>
               <div class="col-lg-8">
                    <input type="text" name="titulo1" id="titulo1" class="form-control" value="{{old('titulo1', $data->titulo1 ?? '')}}" required />
                </div>
</div> 

<!---->
<div class="form-group">
    <label for="bajada1" class="col-lg-3 control-label requerido">Bajada Pie en Couta</label>
               <div class="col-lg-8">
                    <textarea type="text" class="form-control" style="resize:none" name="bajada1" id="bajada1" rows="5" cols="33" wrap="hard" required>{{old('bajada1', $data->bajada1 ?? '')}}</textarea>
                </div>
</div> 

<div class="form-group">
    <label for="titulo2" class="col-lg-3 control-label requerido">Titulo Pago en Cheques</label>
               <div class="col-lg-8">
                    <input type="text" name="titulo2" id="titulo2" class="form-control" value="{{old('titulo2', $data->titulo2 ?? '')}}" required />
                </div>
</div> 
<!---->

<div class="form-group">
    <label for="bajada2" class="col-lg-3 control-label requerido">Bajada Pago en Cheques</label>
               <div class="col-lg-8">
                    <textarea type="text" class="form-control" style="resize:none" name="bajada2" id="bajada2" rows="5" cols="33" wrap="hard" required>{{old('bajada2', $data->bajada2 ?? '')}}</textarea>
                </div>
</div> 
<!---->
<div class="form-group">
    <label for="titulo3" class="col-lg-3 control-label requerido">Titulo Convenios</label>
               <div class="col-lg-8">
                    <input type="text" name="titulo3" id="titulo3" class="form-control" value="{{old('titulo3', $data->titulo3 ?? '')}}" required />
                </div>
</div> 
<!---->

<div class="form-group">
    <label for="bajada3" class="col-lg-3 control-label requerido">Bajada Convenios</label>
               <div class="col-lg-8">
                    <textarea type="text" class="form-control" style="resize:none" name="bajada3" id="bajada3" rows="5" cols="33" wrap="hard" required>{{old('bajada3', $data->bajada3 ?? '')}}</textarea>
                </div>
</div> 
