<div class="form-group">
    <label for="titulo" class="col-lg-3 control-label requerido">Titulo</label>
               <div class="col-lg-8">
                    <input type="text" name="titulo" id="titulo" class="form-control" value="{{old('titulo', $data->titulo ?? '')}}" required />
                </div>
</div> 
<!---->
<div class="form-group">
    <label for="img" class="col-lg-3 control-label requerido">Imagen </label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="img" id="img" class="form-control" value="{{old('img', $data->img ?? '')}}"  /><br>
                    <label ><img src="/storage/{{old('img', $data->img ?? '')}}" style="width: 50%;"></label>
                </div>
</div> 
<!---->
<div class="form-group">
    <label for="subtitulo" class="col-lg-3 control-label requerido">Subtitulo</label>
               <div class="col-lg-8">
                    <input type="text" name="subtitulo" id="subtitulo" class="form-control" value="{{old('subtitulo', $data->subtitulo ?? '')}}" required />
                </div>
</div> 
<!---->
<div class="form-group">
    <label for="texto1" class="col-lg-3 control-label requerido">Text 1</label>
               <div class="col-lg-8">
                    <textarea type="text" class="form-control" style="resize:none" name="texto1" id="texto1" rows="5" cols="33" wrap="hard" required>{{old('texto1', $data->texto1 ?? '')}}</textarea>
                </div>
</div> 
<!---->
<div class="form-group">
    <label for="texto2" class="col-lg-3 control-label requerido">Text 2</label>
               <div class="col-lg-8">
                    <textarea type="text" class="form-control" style="resize:none" name="texto2" id="texto2" rows="5" cols="33" wrap="hard" required>{{old('texto2', $data->texto2 ?? '')}}</textarea>
                </div>
</div> 
<!---->
<div class="form-group">
    <label for="titulo1" class="col-lg-3 control-label requerido">Titulo 3</label>
               <div class="col-lg-8">
                    <input type="text" name="titulo1" id="titulo1" class="form-control" value="{{old('titulo1', $data->titulo1 ?? '')}}" required />
                </div>
</div> 
<!---->
<div class="form-group">
    <label for="bajada1" class="col-lg-3 control-label requerido">Bajada 1</label>
               <div class="col-lg-8">
                    <input type="text" name="bajada1" id="bajada1" class="form-control" value="{{old('bajada1', $data->bajada1 ?? '')}}" required />
                </div>
</div> 
<!---->
<div class="form-group">
    <label for="titulo2" class="col-lg-3 control-label requerido">Titulo 2</label>
               <div class="col-lg-8">
                    <input type="text" name="titulo2" id="titulo2" class="form-control" value="{{old('titulo2', $data->titulo2 ?? '')}}" required />
                </div>
</div> 
<!---->
<div class="form-group">
    <label for="bajada2" class="col-lg-3 control-label requerido">Bajada 2</label>
               <div class="col-lg-8">
                    <input type="text" name="bajada2" id="bajada2" class="form-control" value="{{old('bajada2', $data->bajada2 ?? '')}}" required />
                </div>
</div> 
<!---->
<div class="form-group">
    <label for="titulo3" class="col-lg-3 control-label requerido">Titulo 3</label>
               <div class="col-lg-8">
                    <input type="text" name="titulo3" id="titulo3" class="form-control" value="{{old('titulo3', $data->titulo3 ?? '')}}" required />
                </div>
</div> 
<!---->
<div class="form-group">
    <label for="bajada3" class="col-lg-3 control-label requerido">Bajada 3</label>
               <div class="col-lg-8">
                    <input type="text" name="bajada3" id="bajada3" class="form-control" value="{{old('bajada3', $data->bajada3 ?? '')}}" required />
                </div>
</div> 
<!---->
<div class="form-group">
    <label for="titulo4" class="col-lg-3 control-label requerido">Titulo 4</label>
               <div class="col-lg-8">
                    <input type="text" name="titulo4" id="titulo4" class="form-control" value="{{old('titulo4', $data->titulo4 ?? '')}}" required />
                </div>
</div> 
<!---->
<div class="form-group">
    <label for="bajada4" class="col-lg-3 control-label requerido">Bajada 4</label>
               <div class="col-lg-8">
                    <input type="text" name="bajada4" id="bajada4" class="form-control" value="{{old('bajada4', $data->bajada4 ?? '')}}" required />
                </div>
</div> 
<!---->
<div class="form-group">
    <label for="seccion1" class="col-lg-3 control-label requerido">Primera Seccion</label>
               <div class="col-lg-8">
                    <input type="text" name="seccion1" id="seccion1" class="form-control" value="{{old('seccion1', $data->seccion1 ?? '')}}" required />
                </div>
</div> 
<!---->
<div class="form-group">
    <label for="seccion2" class="col-lg-3 control-label requerido">Segunda Seccion</label>
               <div class="col-lg-8">
                    <input type="text" name="seccion2" id="seccion2" class="form-control" value="{{old('seccion2', $data->seccion2 ?? '')}}" required />
                </div>
</div> 
<!---->
<div class="form-group">
    <label for="punto1" class="col-lg-3 control-label requerido">Punto 1</label>
               <div class="col-lg-8">
                    <input type="text" name="punto1" id="punto1" class="form-control" value="{{old('punto1', $data->punto1 ?? '')}}" required />
                </div>
</div> 
<!---->
<div class="form-group">
    <label for="punto2" class="col-lg-3 control-label requerido">Punto 2</label>
               <div class="col-lg-8">
                    <input type="text" name="punto2" id="punto2" class="form-control" value="{{old('punto2', $data->punto2 ?? '')}}" required />
                </div>
</div> 
<!---->
<div class="form-group">
    <label for="punto3" class="col-lg-3 control-label requerido">Punto 3</label>
               <div class="col-lg-8">
                    <input type="text" name="punto3" id="punto3" class="form-control" value="{{old('punto3', $data->punto3 ?? '')}}" required />
                </div>
</div> 
<!---->
<div class="form-group">
    <label for="punto4" class="col-lg-3 control-label requerido">Punto 4</label>
               <div class="col-lg-8">
                    <input type="text" name="punto4" id="punto4" class="form-control" value="{{old('punto4', $data->punto4 ?? '')}}" required />
                </div>
</div> 
<!---->
<div class="form-group">
    <label for="titulodestacado" class="col-lg-3 control-label requerido">Titulo Destacado</label>
               <div class="col-lg-8">
                    <input type="text" name="titulodestacado" id="titulodestacado" class="form-control" value="{{old('titulodestacado', $data->titulodestacado ?? '')}}" required />
                </div>
</div> 
<!---->
<div class="form-group">
    <label for="img1" class="col-lg-3 control-label requerido">Img Banner Simulador</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="img1" id="img1" class="form-control" value="{{old('img1', $data->img1 ?? '')}}"  /><br>
                    <label ><img src="/storage/{{old('img2', $data->img1 ?? '')}}" style="width: 40%;"></label>
                </div>
</div> 
<!---->
<div class="form-group">
    <label for="img2" class="col-lg-3 control-label requerido">Img Banner Proceso Compra</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="img2" id="img2" class="form-control" value="{{old('img2', $data->img2 ?? '')}}"  /><br>
                    <label ><img src="/storage/{{old('img2', $data->img2 ?? '')}}" style="width: 40%;"></label>
                </div>
</div> 


<!---->
<div class="form-group">
    <label for="img3" class="col-lg-3 control-label requerido">Img Banner Financiamiento</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="img3" id="img3" class="form-control" value="{{old('img3', $data->img3 ?? '')}}"  /> <br>
                    <label ><img src="/storage/{{old('img3', $data->img3 ?? '')}}" style="width: 40%;"></label>
                </div>
</div> 

<!---->
<!---->
<div class="form-group">
    <label for="imgmovil1" class="col-lg-3 control-label requerido">Img movil Simulador</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="imgmovil1" id="imgmovil1" class="form-control" value="{{old('imgmovil1', $data->imgmovil1 ?? '')}}"  /><br>
                    <label ><img src="/storage/{{old('img2', $data->imgmovil1 ?? '')}}" style="width: 40%;"></label>
                </div>
</div> 
<!---->
<div class="form-group">
    <label for="imgmovil2" class="col-lg-3 control-label requerido">Img movil Proceso Compra</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="imgmovil2" id="imgmovil2" class="form-control" value="{{old('imgmovil2', $data->imgmovil2 ?? '')}}"  /><br>
                    <label ><img src="/storage/{{old('img2', $data->imgmovil2 ?? '')}}" style="width: 40%;"></label>
                </div>
</div> 
<!---->
<div class="form-group">
    <label for="imgmovil3" class="col-lg-3 control-label requerido">Img movil Financiamiento</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="imgmovil3" id="imgmovil3" class="form-control" value="{{old('imgmovil3', $data->imgmovil3 ?? '')}}"  /><br>
                    <label ><img src="/storage/{{old('img2', $data->imgmovil3 ?? '')}}" style="width: 40%;"></label>
                </div>
</div> 
<!---->

