@extends("theme.$theme.layout")
@section('titulo')
Seccion Inversionista
@endsection

@section("scripts")
<script src="{{asset("assets/pages/scripts/admin/index.js")}}" type="text/javascript"></script>
@endsection

@section('contenido')
<div class="row">
    <div class="col-lg-12">
        @include('includes.mensaje')
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Seccion Inversionista</h3>
                <div class="box-tools pull-right">

                    <a href="{{route('crear_inversionista')}}" class="btn btn-block btn-success btn-sm">
                        <i class="fa fa-fw fa-plus-circle"></i> Nuevo registro
                    </a>

                </div>
            </div>
            <div class="box-body">
                 <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover" id="tabla-data">
                    <thead>
                        <tr>
                            <th>Accion</th> <!-- borrar cuando esté lista la tabla ajustada a la pantalla -->
                            <th>Titulo</th>
                            <th>Imagen</th>
                            <th>Subtitulo</th>
                            <th>Texto</th>
                            <th>texto</th>
                            <th>Titulo 1</th>
                            <th>Bajada</th>
                            <th>Titulo 2</th>
                            <th>Bajada 2</th>
                            <th>Titulo 3</th>
                            <th>Bajada 3</th>
                            <th>Titulo 4</th>
                            <th>Bajada 4</th>
                            <th>Seccion 1</th>
                            <th>Seccion 2</th>
                            <th>Item 1</th>
                            <th>Item 2</th>
                            <th>Item 3</th>
                            <th>Item 4</th>
                            <th>Titulo Destacado</th>
                            <th>Imagen 1</th>
                            <th>Imagen 2</th>
                            <th>Imagen 3</th>
                            <th>Imagen movil 1</th>
                            <th>Imagen movil 2</th>
                            <th>Imagen movil 3</th>
                            <th class="width70"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($datas as $data)
                        <tr>
                            <td> <!-- Inicio botones -->
                                <a href="{{route('editar_inversionista', ['id' => $data->id])}}" class="btn-accion-tabla tooltipsC" title="Editar este registro">
                                    <i class="fa fa-fw fa-pencil"></i>
                                </a>
                                <form action="{{route('eliminar_inversionista', ['id' => $data->id])}}" class="d-inline form-eliminar" method="POST">
                                    @csrf @method("delete")
                                    <button type="submit" class="btn-accion-tabla eliminar tooltipsC" title="Eliminar este registro">
                                        <i class="fa fa-fw fa-trash text-danger"></i>
                                    </button>
                                </form>
                            </td> <!-- fin botones -->

                            <td>{{$data->titulo}}</td>
                            <td>{{$data->img}}</td>
                            <td>{{$data->subtitulo}}</td>
                            <td>{{$data->img}}</td>
                            <td>{{$data->texto1}}</td>
                            <td>{{$data->texto2}}</td>
                            <td>{{$data->titulo1}}</td>
                            <td>{{$data->bajada1}}</td>
                            <td>{{$data->titulo2}}</td>
                            <td>{{$data->bajada2}}</td>
                            <td>{{$data->titulo3}}</td>
                            <td>{{$data->bajada3}}</td>
                            <td>{{$data->titulo4}}</td>
                            <td>{{$data->bajada4}}</td>
                            <td>{{$data->seccion1}}</td>
                            <td>{{$data->seccion2}}</td>
                            <td>{{$data->punto1}}</td>
                            <td>{{$data->punto2}}</td>
                            <td>{{$data->punto3}}</td>
                            <td>{{$data->punto4}}</td>
                            <td>{{$data->titulodestacado}}</td>
                            <td>{{$data->img1}}</td>
                            <td>{{$data->img2}}</td>
                            <td>{{$data->img3}}</td>
                            <td>{{$data->imgmovil1}}</td>
                            <td>{{$data->imgmovil2}}</td>
                            <td>{{$data->imgmovil3}}</td>                              

                         <!--  <td> 
                                <a href="{{route('editar_proyecto', ['id' => $data->id])}}" class="btn-accion-tabla tooltipsC" title="Editar este registro">
                                    <i class="fa fa-fw fa-pencil"></i>
                                </a>
                                <form action="{{route('eliminar_proyecto', ['id' => $data->id])}}" class="d-inline form-eliminar" method="POST">
                                    @csrf @method("delete")
                                    <button type="submit" class="btn-accion-tabla eliminar tooltipsC" title="Eliminar este registro">
                                        <i class="fa fa-fw fa-trash text-danger"></i>
                                    </button>
                                </form>
                            </td> -->
                            
                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
            </div>
        </div>
    </div>
</div>
@endsection