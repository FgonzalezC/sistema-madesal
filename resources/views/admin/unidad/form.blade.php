<div class="form-group">
    <label for="nombre" class="col-lg-3 control-label requerido">Nombre</label>
               <div class="col-lg-8">
                    <input type="text" name="nombre" id="nombre" class="form-control" value="{{old('nombre', $data->nombre ?? '')}}" required />
                </div>
</div> 
<div class="form-group">
    <label for="img_unidad" class="col-lg-3 control-label requerido">Img unidad</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="img_unidad" id="img_unidad" class="form-control" value="{{old('img_unidad', $data->img_unidad ?? '')}}"  />
                     <br>
                    <label ><img src="/storage/{{old('img2', $data->img_unidad ?? '')}}" style="width: 50%;"></label>
                </div>
</div> 
<div class="form-group">
    <label for="precio" class="col-lg-3 control-label requerido">Precio</label>
               <div class="col-lg-8">
                    <input type="number" name="precio" id="precio" class="form-control" value="{{old('precio', $data->precio ?? '')}}" required />
                </div>
</div>
<div class="form-group">
    <label for="superficie_util" class="col-lg-3 control-label requerido">Superficie Util</label>
               <div class="col-lg-8">
                    <input type="number" name="superficie_util" id="superficie_util" class="form-control" value="{{old('superficie_util', $data->superficie_util ?? '')}}" required />
                </div>
</div>
<div class="form-group">
    <label for="superf_areaverde" class="col-lg-3 control-label requerido">Superficie Area Verde</label>
               <div class="col-lg-8">
                    <input type="text" name="superf_areaverde" id="superf_areaverde" class="form-control" value="{{old('superf_areaverde', $data->superf_areaverde ?? '')}}" required />
                </div>
</div> 

<div class="form-group">
    <label for="modelo_id" class="col-lg-3 control-label requerido">Modelo</label>
               <div class="col-lg-8">
                   <select class="form-control" id="modelo_id" name="modelo_id" required>
                    <option value="" selected disabled hidden>Seleccionar</option>
                                        @foreach(\App\Models\Admin\Modelo::all() as $modelo)
                                            <option value="{{ $modelo->id }}" >{{ $modelo->nombre_modelo }}</option>
                                            @endforeach
                    </select>
               </div>
</div>

