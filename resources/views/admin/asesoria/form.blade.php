<div class="form-group">
    <label for="titulo" class="col-lg-3 control-label requerido">Titulo</label>
               <div class="col-lg-8">
                    <input type="text" name="titulo" id="titulo" class="form-control" value="{{old('titulo', $data->titulo ?? '')}}" required />
                </div>
</div> 
<!---->
<div class="form-group">
    <label for="img" class="col-lg-3 control-label requerido">Imagen 1</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="img" id="img" class="form-control"  /><br>
                    <label ><img src="/storage/{{old('img', $data->img ?? '')}}" style="width: 20%;"></label>
                </div>
</div>
<!---->
<div class="form-group">
    <label for="img1" class="col-lg-3 control-label requerido">Imagen 2</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="img1" id="img1" class="form-control"/><br>
                     <label ><img src="/storage/{{old('img1', $data->img1 ?? '')}}" style="width: 80%;"></label>
                </div>
</div>
<!---->
<div class="form-group">
    <label for="img2" class="col-lg-3 control-label requerido">Imagen 3</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="img2" id="img2" class="form-control"/><br>
                     <label ><img src="/storage/{{old('img2', $data->img2 ?? '')}}" style="width: 80%;"></label>
                </div>
</div>