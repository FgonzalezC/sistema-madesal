@extends("theme.$theme.layout")
@section('titulo')
Crear Ficha Proyecto
@endsection

@section("scripts")
<script src="{{asset("assets/pages/scripts/admin/index.js")}}" type="text/javascript"></script>
@endsection

@section('contenido')
<div class="row">
    <div class="col-lg-12">
        @include('includes.mensaje')
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Sección Proyecto</h3>
                <div class="box-tools pull-right">

                    <a href="{{route('crear_seccion-proyectos')}}" class="btn btn-block btn-success btn-sm">
                        <i class="fa fa-fw fa-plus-circle"></i> Nuevo registro
                    </a>

                </div>
            </div>
            <div class="box-body">
                <table class="table table-striped table-bordered table-hover" id="tabla-data" body style="overflow-x:hidden;">
                    <thead>
                        <tr>
                            <th>Titulo Minuscula</th>
                            <th>Titulo Mayuscula</th>
                            <th>img</th>
                           

                            <th class="width70"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($datas as $data)
                        <tr>
                             <td>
                                <a href="{{route('editar_seccion-proyectos', ['id' => $data->id])}}" class="btn-accion-tabla tooltipsC" title="Editar este registro">
                                    <i class="fa fa-fw fa-pencil"></i>
                                </a>
                                <form action="{{route('eliminar_seccion-proyectos', ['id' => $data->id])}}" class="d-inline form-eliminar" method="POST">
                                    @csrf @method("delete")
                                    <button type="submit" class="btn-accion-tabla eliminar tooltipsC" title="Eliminar este registro">
                                        <i class="fa fa-fw fa-trash text-danger"></i>
                                    </button>
                                </form>
                            </td>
                         
                            <td>{{$data->titulo}}</td>
                            <td>{{$data->titulo2}}</td>
                            <td>{{$data->img}}</td>
                          
                           
                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection