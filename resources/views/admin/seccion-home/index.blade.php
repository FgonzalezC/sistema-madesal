@extends("theme.$theme.layout")
@section('titulo')
Info Home
@endsection

@section("scripts")
<script src="{{asset("assets/pages/scripts/admin/index.js")}}" type="text/javascript"></script>
@endsection

@section('contenido')
<div class="row">
    <div class="col-lg-12">
        @include('includes.mensaje')
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Sección Home</h3>
                <div class="box-tools pull-right">

                    <a href="{{route('crear_seccion-home')}}" class="btn btn-block btn-success btn-sm">
                        <i class="fa fa-fw fa-plus-circle"></i> Nuevo registro
                    </a>

                </div>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover" id="tabla-data">
                    <thead>
                        <tr>
                            <th>Accion</th>
                            <th>Título Principal 1</th>
                            <th>Título Principal 2</th>
                            <th>Imagen Principal</th>
                            <th>Subtitulo Destacados</th>
                            <th>Subtitulo invierte</th>
                            <th>Subtitulo secc inferior</th>
                            <th>Texto</th>
                            <th>Top Banner</th>
                            <th>Bottom Banner</th>
                            <th>Bottom Banner</th>
                            <th>Titulo Movil</th>
                            <th>Subtitulo Movil</th>
                            <th>Subtitulo Movil</th>
                            <th>Subtitulo Movil</th>
                            <th>Subtitulo Movil</th>
                            <th>Imagen movil 1</th>
                            <th>Imagen movil 2</th>
                            <th>Imagen movil 3</th>
                            <th class="width70"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($datas as $data)
                        <tr>
                             <td>
                                <a href="{{route('editar_seccion-home', ['id' => $data->id])}}" class="btn-accion-tabla tooltipsC" title="Editar este registro">
                                    <i class="fa fa-fw fa-pencil"></i>
                                </a>
                                <form action="{{route('eliminar_seccion-home', ['id' => $data->id])}}" class="d-inline form-eliminar" method="POST">
                                    @csrf @method("delete")
                                    <button type="submit" class="btn-accion-tabla eliminar tooltipsC" title="Eliminar este registro">
                                        <i class="fa fa-fw fa-trash text-danger"></i>
                                    </button>
                                </form>
                            </td>
                            <td>{{$data->titulo_principal}}</td>
                            <td>{{$data->titulo_principal2}}</td>
                            <td>{{$data->img}}</td>
                            <td>{{$data->subtitulo}}</td>
                            <td>{{$data->subtitulo1}}</td>
                            <td>{{$data->subtitulo2}}</td>
                            <td>{{$data->texto}}</td>
                            <td>{{$data->img1}}</td>
                            <td>{{$data->img2}}</td>
                            <td>{{$data->img3}}</td>
                            <td>{{$data->titulo_movil}}</td>
                            <td>{{$data->msubtitulo1}}</td>
                            <td>{{$data->msubtitulo2}}</td>
                            <td>{{$data->msubtitulo3}}</td>
                            <td>{{$data->msubtitulo4}}</td>
                            <td>{{$data->movilimg1}}</td>
                            <td>{{$data->movilimg2}}</td>
                            <td>{{$data->movilimg3}}</td>
                           
                        </tr>
                        @endforeach

                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection