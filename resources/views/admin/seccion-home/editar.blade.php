@extends("theme.$theme.layout")
@section('titulo')
    Editar Sección Home
@endsection

@section("scripts")
<script src="{{asset("assets/pages/scripts/admin/crear.js")}}" type="text/javascript"></script>
<script src="{{asset("assets/pages/scripts/admin/customs/file-input.js")}}" type="text/javascript"></script> 
<script src="{{asset("assets/pages/scripts/admin/customs/bootstrap-4.3.1/js/bootstrap.min.js")}}" type="text/javascript"></script>
@endsection

@section('contenido')
<div class="row">
    <div class="col-lg-12">
        @include('includes.form-error')
        @include('includes.mensaje')
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">Editar Sección Home</h3>
                <div class="box-tools pull-right">
                    <a href="{{route('seccion-home')}}" class="btn btn-block btn-info btn-sm">
                        <i class="fa fa-fw fa-reply-all"></i> Volver
                    </a>
                </div>
            </div>
            <!-- ACTUALIZAR -->
            <form action="{{route('actualizar_seccion-home', ['id' => $data->id])}}" id= "form-general" class="form-horizontal" method="POST" autocomplete="off" enctype="multipart/form-data">

                {{ csrf_field() }}
                @csrf @method("put")
                <div class="box-body">
                    @include('admin.seccion-home.form')
                </div>
                <div class="box-footer">
                    <div class="col-lg-3"></div>
                    <div class="col-lg-6" align="center">
                        @include('includes.boton-form-editar')
                    </div>
                </div>
            </form>
            <!-- ACTUALIZAR -->
        </div>
    </div>
</div> 
@endsection