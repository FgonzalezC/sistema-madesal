<!-- ------------------Titulo------------------ -->
<div class="form-group">
    <label for="titulo_principal" class="col-lg-3 control-label requerido">Título Primera parte</label>
               <div class="col-lg-8">
                    <input type="text" name="titulo_principal" id="titulo_principal" class="form-control" value="{{old('titulo_principal', $data->titulo_principal ?? '')}}" required />
                </div>
</div> 
<div class="form-group">
    <label for="titulo_principal2" class="col-lg-3 control-label requerido">Título Segunda Parte</label>
               <div class="col-lg-8">
                    <input type="text" name="titulo_principal2" id="titulo_principal2" class="form-control" value="{{old('titulo_principal2', $data->titulo_principal2 ?? '')}}" required />
                </div>
</div> 
<!-- ------------------Texto------------------ -->
<div class="form-group">
    <label for="img" class="col-lg-3 control-label requerido">Foto Principal</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="img" id="img" value="{{old('img', $data->img ?? '')}}"  />
                     <br>
                    <label ><img src="/storage/{{old('img2', $data->img ?? '')}}" style="width: 50%;"></label>
                </div>
</div>
<div class="form-group">
    <label for="subtitulo" class="col-lg-3 control-label requerido">Subtitulo destacados</label>
               <div class="col-lg-8">
                    <input type="text" name="subtitulo" id="subtitulo" class="form-control" value="{{old('subtitulo', $data->subtitulo ?? '')}}" required />
                </div>
</div> 
<!-- </form> -->
<div class="form-group">
    <label for="subtitulo1" class="col-lg-3 control-label requerido">Subtitulo invierte</label>
               <div class="col-lg-8">
                    <input type="text" name="subtitulo1" id="subtitulo1" class="form-control" value="{{old('subtitulo1', $data->subtitulo1 ?? '')}}" required />
                </div>
</div> 
<div class="form-group">
    <label for="subtitulo2" class="col-lg-3 control-label requerido">Subtitulo secc inferior</label>
               <div class="col-lg-8">
                    <input type="text" name="subtitulo2" id="subtitulo2" class="form-control" value="{{old('subtitulo2', $data->subtitulo2 ?? '')}}" required />
                </div>
</div> 

<div class="form-group">
    <label for="texto" class="col-lg-3 control-label requerido">Texto</label>
               <div class="col-lg-8">
                    <textarea type="text" class="form-control" style="resize:none" name="texto" id="texto" rows="5" cols="33" wrap="hard" required>{{old('texto', $data->texto ?? '')}}</textarea>
                </div>
</div> 


<!-- ------------------Imagen 1------------------ -->
<!-- <form action="upload" method="post" enctype="multipart/form-data"> -->
<div class="form-group">
    <label for="img1" class="col-lg-3 control-label requerido">Top Banner</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="img1" id="img1" value="{{old('img1', $data->img1 ?? '')}}"  />
                     <br>
                    <label ><img src="/storage/{{old('img2', $data->img1 ?? '')}}" style="width: 50%;"></label>
                </div>
</div>
<!-- </form> -->


<!-- ------------------Imagen 2------------------ -->
<div class="form-group">
    <label for="img2" class="col-lg-3 control-label requerido">Bottom banner</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="img2" id="img2" class="form-control" value="{{old('img2', $data->img2 ?? '')}}"  /> <br>
                    <label ><img src="/storage/{{old('img2', $data->img2 ?? '')}}" style="width: 50%;"></label>
                </div>
</div> 
<!---->


<!-- ------------------Imagen 2------------------ -->
<div class="form-group">
    <label for="img3" class="col-lg-3 control-label requerido">Bottom banner</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="img3" id="img3" class="form-control" value="{{old('img3', $data->img3 ?? '')}}"  /> <br>
                    <label ><img src="/storage/{{old('img2', $data->img3 ?? '')}}" style="width: 50%;"></label>
                </div>
</div> 
<!-- ------------------Titulo------------------ -->
<div class="form-group">
    <label for="titulo_movil" class="col-lg-3 control-label requerido">Título Movil</label>
               <div class="col-lg-8">
                    <input type="text" name="titulo_movil" id="titulo_movil" class="form-control" value="{{old('titulo_movil', $data->titulo_movil ?? '')}}" required />
                </div>
</div> 
<!-- </form> -->
<div class="form-group">
    <label for="msubtitulo1" class="col-lg-3 control-label requerido">Subtitulo Movil</label>
               <div class="col-lg-8">
                    <input type="text" name="msubtitulo1" id="msubtitulo1" class="form-control" value="{{old('msubtitulo1', $data->msubtitulo1 ?? '')}}" required />
                </div>
</div> 
<div class="form-group">
    <label for="msubtitulo2" class="col-lg-3 control-label requerido">Subtitulo Movil</label>
               <div class="col-lg-8">
                    <input type="text" name="msubtitulo2" id="msubtitulo2" class="form-control" value="{{old('msubtitulo2', $data->msubtitulo2 ?? '')}}" required />
                </div>
</div> 
<div class="form-group">
    <label for="msubtitulo3" class="col-lg-3 control-label requerido">Subtitulo Movil</label>
               <div class="col-lg-8">
                    <input type="text" name="msubtitulo3" id="msubtitulo3" class="form-control" value="{{old('msubtitulo3', $data->msubtitulo3 ?? '')}}" required />
                </div>
</div> 
<div class="form-group">
    <label for="msubtitulo4" class="col-lg-3 control-label requerido">Subtitulo Movil</label>
               <div class="col-lg-8">
                    <input type="text" name="msubtitulo4" id="msubtitulo4" class="form-control" value="{{old('msubtitulo4', $data->msubtitulo4 ?? '')}}" required />
                </div>
</div> 
<div class="form-group">
    <label for="movilimg1" class="col-lg-3 control-label requerido">Foto Movil</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="movilimg1" id="movilimg1" value="{{old('movilimg1', $data->movilimg1 ?? '')}}"  />
                     <br>
                    <label ><img src="/storage/{{old('img2', $data->movilimg1 ?? '')}}" style="width: 50%;"></label>
                </div>
</div>
<!-- </form> -->


<!-- ------------------Imagen 2------------------ -->
<div class="form-group">
    <label for="movilimg2" class="col-lg-3 control-label requerido">Foto Movil</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="movilimg2" id="movilimg2" class="form-control" value="{{old('movilimg2', $data->movilimg2 ?? '')}}"  />
                     <br>
                    <label ><img src="/storage/{{old('img2', $data->movilimg2 ?? '')}}" style="width: 50%;"></label>
                </div>
</div> 
<!---->


<!-- ------------------Imagen 2------------------ -->
<div class="form-group">
    <label for="movilimg3" class="col-lg-3 control-label requerido">Foto Movil</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="movilimg3" id="movilimg3" class="form-control" value="{{old('movilimg3', $data->movilimg3 ?? '')}}"  />
                     <br>
                    <label ><img src="/storage/{{old('img2', $data->movilimg3 ?? '')}}" style="width: 50%;"></label>
                </div>
</div> 

