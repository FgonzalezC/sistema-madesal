@extends("theme.$theme.layout")
@section('titulo')
Modelos
@endsection

@section("scripts")
<script src="{{asset("assets/pages/scripts/admin/index.js")}}" type="text/javascript"></script>
@endsection

@section('contenido')
<div class="row">
    <div class="col-lg-12">
        @include('includes.mensaje')
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Modelos</h3>
                        <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                            class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                        <form method="post" action="{{ route('modelo_import') }}" enctype="multipart/form-data">
                            {{csrf_field()}}

                            <input type="file" name="excel">

                            <br>
                            <button type="submit" class="btn btn-primary">
                            Enviar
                        </button>
                            <br><br>
                            <a href="{{route('crear_modelo')}}" class="btn btn-block btn-success btn-sm">
                                <i class="fa fa-fw fa-plus-circle"></i> Nuevo registro
                            </a>
                        </form>
                  <div class="box-body">
                <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover" id="tabla-data">
                    <thead>
                        <tr>
                            <th>Accion</th>
                            <th>Nombre</th>
                            <th>Proyecto</th>
                            <th>Precio Desde</th>
                            <th>Foto Home Ficha Proyecto</th>
                            <th>Archivo Ficha Modelo</th>
                            <th>Foto slider 1</th>
                            <th>Foto slider 2</th>
                            <th>Foto slider 3</th>
                            <th>Foto slider 4</th>
                            <th>Foto slider 5</th>
                            <th>Foto slider movil 1</th>
                            <th>Foto slider movil 2</th>
                            <th>Foto slider movil 3</th>
                            <th>Foto slider movil 4</th>
                            <th>Foto slider movil 5</th>
                            <th>Bajada</th>
                            <th>Metros Cuadrados</th>
                            <th>Tipo Inmueble</th>
                            <th>Baños</th>
                            <th>Dormitorios</th>
                            <th>Tipo Cocina</th>
                            <th>Estacionamiento</th>
                            <th>Terminaciones</th>
                            <th>Equipamiento</th>
                            <th>Ubicacion y entorno</th>
                            <th>Foto t1</th>
                            <th>Foto t2</th>
                            <th>Foto t3</th>
                            <th>Foto t4</th>
                            <th>Foto t5</th>
                            <th>Foto t6</th>
                            <th>Foto t7</th>
                            <th>Foto t8</th>
                            <th>Foto t9</th>
                            <th>Foto t10</th>
                            <th>Foto e1</th>
                            <th>Foto e2</th>
                            <th>Foto e3</th>
                            <th>Foto e4</th>
                            <th>Foto e5</th>
                            <th>Foto e6</th>
                            <th>Foto e7</th>
                            <th>Foto e8</th>
                            <th>Foto e9</th>
                            <th>Foto e10</th>
                            <th>Foto u1</th>
                            <th>Foto u2</th>
                            <th>Foto u3</th>
                            <th>Foto u4</th>
                            <th>Foto u5</th>
                            <th>Foto u6</th>
                            <th>Foto u7</th>
                            <th>Foto u8</th>
                            <th>Foto u9</th>
                            <th>Foto u10</th>
                            <th>video Terminaciones</th>
                            <th>video Equipamiento</th>
                            <th>Video Ubicacion</th>
                            
                            
                            <th class="width70"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($datas as $data)
                        <tr>
                            <td> 
                                <a href="{{route('editar_modelo', ['id' => $data->id])}}" class="btn-accion-tabla tooltipsC" title="Editar este registro">
                                    <i class="fa fa-fw fa-pencil"></i>
                                </a>
                                <form action="{{route('eliminar_modelo', ['id' => $data->id])}}" class="d-inline form-eliminar" method="POST">
                                    @csrf @method("delete")
                                    <button type="submit" class="btn-accion-tabla eliminar tooltipsC" title="Eliminar este registro">
                                        <i class="fa fa-fw fa-trash text-danger"></i>
                                     </button>
                                </form>
                            </td>
                            
                            <td>{{$data->nombre_modelo}} {{$data->stock}} </td>
                             <?php
                            $proyecto = \DB::table('modelos')
                            ->select('nombre_proyecto','nombre_etapa')
                            ->where('modelos.id', $data->id)
                            ->join('proyectos','proyectos.id','modelos.proyecto_id')
                            ->join('tipo_etapas','proyectos.tipoetapa_id','tipo_etapas.id')
                            ->get();
                            ?>
                            @foreach($proyecto as $mod)
                            <td>{{$mod->nombre_proyecto}} / {{$mod->nombre_etapa}}</td>
                            @endforeach
                            <td>{{$data->precio_desde}}</td>
                            <td>{{$data->archivo}}</td>
                            <td>{{$data->slider2}}</td>
                            <td>{{$data->slider3}}</td>
                            <td>{{$data->slider4}}</td>
                            <td>{{$data->slider5}}</td>
                            <td>{{$data->slidermovil1}}</td>
                            <td>{{$data->slidermovil2}}</td>
                            <td>{{$data->slidermovil3}}</td>
                            <td>{{$data->slidermovil4}}</td>
                            <td>{{$data->slidermovil5}}</td>
                            <td>{{$data->bajada}}</td>
                            <td>{{$data->mtrs}}</td>
                            <td>{{\App\Models\Admin\TipoInmueble::find($data->tipoinmueble_id)->nombre_inmueble}}</td>
                            <td>{{$data->banos}}</td>
                            <td>{{$data->dormitorio}}</td>
                            <td>{{\App\Models\Admin\TipoCocina::find($data->tipococina_id)->nombre_cocina}}</td>
                            <td>{{$data->estacionamiento}}</td>
                            <td>{{$data->terminaciones}}</td>
                            <td>{{$data->equipamiento}}</td>
                            <td>{{$data->ubicacion}}</td>
                            <td>{{$data->timg1}}</td>
                            <td>{{$data->timg2}}</td>
                            <td>{{$data->timg3}}</td>
                            <td>{{$data->timg4}}</td>
                            <td>{{$data->timg5}}</td>
                            <td>{{$data->timg6}}</td>
                            <td>{{$data->timg7}}</td>
                            <td>{{$data->timg8}}</td>
                            <td>{{$data->timg9}}</td>
                            <td>{{$data->timg10}}</td>
                            <td>{{$data->eimg1}}</td>
                            <td>{{$data->eimg2}}</td>
                            <td>{{$data->eimg3}}</td>
                            <td>{{$data->eimg4}}</td>
                            <td>{{$data->eimg5}}</td>
                            <td>{{$data->eimg6}}</td>
                            <td>{{$data->eimg7}}</td>
                            <td>{{$data->eimg8}}</td>
                            <td>{{$data->eimg9}}</td>
                            <td>{{$data->eimg10}}</td>
                            <td>{{$data->uimg1}}</td>
                            <td>{{$data->uimg2}}</td>
                            <td>{{$data->uimg3}}</td>
                            <td>{{$data->uimg4}}</td>
                            <td>{{$data->uimg5}}</td>
                            <td>{{$data->uimg6}}</td>
                            <td>{{$data->uimg7}}</td>
                            <td>{{$data->uimg8}}</td>
                            <td>{{$data->uimg9}}</td>
                            <td>{{$data->uimg10}}</td>
                            <td>{{$data->vt1}}</td>
                            <td>{{$data->ve1}}</td>
                            <td>{{$data->vu1}}</td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
