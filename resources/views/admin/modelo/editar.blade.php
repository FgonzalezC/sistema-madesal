@extends("theme.$theme.layout")
@section('titulo')
    Modelos
@endsection

@section("scripts")
<script src="{{asset("assets/pages/scripts/admin/crear.js")}}" type="text/javascript"></script>
@endsection

@section('contenido')
<div class="row">
    <div class="col-lg-12">
        @include('includes.form-error')
        @include('includes.mensaje')
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">Editar Modelo</h3>
                <div class="box-tools pull-right">
                    <a href="{{route('editar_proyecto', ['id' => $data->proyecto_id])}}" class="btn btn-block btn-info btn-sm">
                        <i class="fa fa-fw fa-reply-all"></i> Volver a proyecto 
                    </a>
                </div>
            </div>
            <form action="{{route('actualizar_modelo', ['id' => $data->id])}}" id="form-general" class="form-horizontal" method="POST" autocomplete="off"action="upload" enctype="multipart/form-data"> 
                {{ csrf_field() }}
                @csrf @method("put")
                <div class="box-body">
                    @include('admin.modelo.form')
                </div>
                <div class="box-footer">
                    <div class="col-lg-3"></div>
                    <div class="col-lg-6">
                        @include('includes.boton-form-editar')
                    </div>
                </div>
            </form>
        </div>
                    @include('admin.planta.listado')

    </div>
</div> 
@endsection