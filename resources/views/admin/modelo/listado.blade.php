
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <div class="box-header with-border">
                            
                <h3 class="box-title">Modelos del proyecto   </h3>

                  
                            <br><br>
                            <a href="{{route('crear_modelo')}}" class="btn btn-block btn-success btn-sm">
                                <i class="fa fa-fw fa-plus-circle"></i> Nuevo registro
                            </a>
                        </form>
                  <div class="box-body">
                <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover" id="tabla-data">
                    <thead>
                        <tr>
                            <th>Accion</th> 
                            <th>Nombre</th>
                            <th>Proyecto</th>
                            <th>Precio Desde</th>
                            <th>Bajada</th>
                            <th>Metros Cuadrados</th>
                            <th>Tipo Inmueble</th>
                            <th>Baños</th>
                            <th>Dormitorios</th>
                            <th>Tipo Cocina</th>
                            <th>Estacionamiento</th>

                            <th class="width70"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($modelos as $data)
                        <tr>
                            <td> 
                                <a href="{{route('editar_modelo', ['id' => $data->id])}}" class="btn-accion-tabla tooltipsC" title="Editar este registro">
                                    <i class="fa fa-fw fa-pencil"></i>
                                </a>
                                <form action="{{route('eliminar_modelo', ['id' => $data->id])}}" class="d-inline form-eliminar" method="POST">
                                    @csrf @method("delete")
                                    <button type="submit" class="btn-accion-tabla eliminar tooltipsC" title="Eliminar este registro">
                                        <i class="fa fa-fw fa-trash text-danger"></i>
                                     </button>
                                </form>
                            </td>
                            
                            <td>{{$data->nombre_modelo}} {{$data->stock}} </td>
                            <td>{{$data->nombre_proyecto}}/ {{ $data->nombre_etapa }} </td>
                            <td>{{$data->precio_desde}}</td>
                            <td>{{$data->bajada}}</td>
                            <td>{{$data->mtrs}}</td>
                            <td>{{\App\Models\Admin\TipoInmueble::find($data->tipoinmueble_id)->nombre_inmueble}}</td>
                            <td>{{$data->banos}}</td>
                            <td>{{$data->dormitorio}}</td>
                            <td>{{\App\Models\Admin\TipoCocina::find($data->tipococina_id)->nombre_cocina}}</td>
                            <td>{{$data->estacionamiento}}</td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

