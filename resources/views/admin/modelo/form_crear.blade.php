<div class="form-group">
    <label for="nombre_modelo" class="col-lg-3 control-label requerido">Nombre Modelo</label>
               <div class="col-lg-8">
                    <input type="text" name="nombre_modelo" id="nombre_modelo" class="form-control" value="{{old('nombre_modelo', $data->nombre_modelo ?? '')}}" required />
                </div>
</div>
<div class="form-group">
    <label for="precio_desde" class="col-lg-3 control-label requerido">Precio Desde</label>
               <div class="col-lg-8">
                    <input type="text" name="precio_desde" id="precio_desde" class="form-control" value="{{old('precio_desde', $data->precio_desde ?? '')}}"  />
                </div>
</div> 
<div class="form-group">
    <label for="img_fichaproyecto" class="col-lg-3 control-label requerido">Foto Home Ficha Proyecto</label>
      <div class="col-lg-8">
      <input type="file" class="custom-file-input" name="img_fichaproyecto" id="img_fichaproyecto" class="form-control" value="{{old('img_fichaproyecto', $data->img_fichaproyecto ?? '')}}"  /><br>
      <label ><img src="/storage/{{old('img_fichaproyecto', $data->img_fichaproyecto ?? '')}}" style="width: 50%;"></label>
    </div> 
</div> 
<div class="form-group">
    <label for="archivo" class="col-lg-3 control-label requerido">Archivo Ficha Modelo</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="archivo" id="archivo" class="form-control" value="{{old('archivo', $data->archivo ?? '')}}"  /><br>
                    <label ><img src="/storage/{{old('archivo', $data->archivo ?? '')}}" style="width: 50%;"></label>
                </div>
</div> 
<div class="form-group">
    <label for="slider1" class="col-lg-3 control-label requerido">Foto Slider 1</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="slider1" id="slider1" class="form-control" value="{{old('slider1', $data->slider1 ?? '')}}"  /><br>
                    <label ><img src="/storage/{{old('slider1', $data->slider1 ?? '')}}" style="width: 50%;"></label>
                </div>
</div> 
<!---->
<!-- ------------------Imagen terminacion 3 ------------------ -->
<div class="form-group">
    <label for="slider2" class="col-lg-3 control-label requerido">Foto Slider 2</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="slider2" id="slider2" class="form-control" value="{{old('slider2', $data->slider2 ?? '')}}"  /><br>
                    <label ><img src="/storage/{{old('slider2', $data->slider2 ?? '')}}" style="width: 50%;"></label>
                </div>
</div> 
<!---->
<!-- ------------------Imagen terminacion 3 ------------------ -->
<div class="form-group">
    <label for="slider3" class="col-lg-3 control-label requerido">Foto Slider 3</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="slider3" id="slider3" class="form-control" value="{{old('slider3', $data->slider3 ?? '')}}"  /><br>
                    <label ><img src="/storage/{{old('slider3', $data->slider3 ?? '')}}" style="width: 50%;"></label>
                </div>
</div> 
<!---->
<!-- ------------------Imagen terminacion 3 ------------------ -->
<div class="form-group">
    <label for="slider4" class="col-lg-3 control-label requerido">Foto Slider 4</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="slider4" id="slider4" class="form-control" value="{{old('slider4', $data->slider4 ?? '')}}"  /><br>
                    <label ><img src="/storage/{{old('slider4', $data->slider4 ?? '')}}" style="width: 50%;"></label>
                </div>
</div> 
<!---->
<!-- ------------------Imagen terminacion 3 ------------------ -->
<div class="form-group">
    <label for="slider5" class="col-lg-3 control-label requerido">Foto Slider 5</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="slider5" id="slider5" class="form-control" value="{{old('slider5', $data->slider5 ?? '')}}"  /><br>
                    <label ><img src="/storage/{{old('slider5', $data->slider5 ?? '')}}" style="width: 50%;"></label>
                </div>
</div> 
<!---->
<div class="form-group">
    <label for="slidermovil1" class="col-lg-3 control-label requerido">Foto Slider movil 1</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="slidermovil1" id="slidermovil1" class="form-control" value="{{old('slidermovil1', $data->slidermovil1 ?? '')}}"  /><br>
                    <label ><img src="/storage/{{old('slidermovil1', $data->slidermovil1 ?? '')}}" style="width: 50%;"></label>
                </div>
</div> 
<!---->
<div class="form-group">
    <label for="slidermovil2" class="col-lg-3 control-label requerido">Foto Slider movil 2</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="slidermovil2" id="slidermovil2" class="form-control" value="{{old('slidermovil2', $data->slidermovil2 ?? '')}}"  /><br>
                    <label ><img src="/storage/{{old('slidermovil2', $data->slidermovil2 ?? '')}}" style="width: 50%;"></label>
                </div>
</div> 
<!---->
<div class="form-group">
    <label for="slidermovil3" class="col-lg-3 control-label requerido">Foto Slider movil 3</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="slidermovil3" id="slidermovil3" class="form-control" value="{{old('slidermovil3', $data->slidermovil3 ?? '')}}"  /><br>
                    <label ><img src="/storage/{{old('slidermovil3', $data->slidermovil3 ?? '')}}" style="width: 50%;"></label>
                </div>
</div> 
<!---->
<div class="form-group">
    <label for="slidermovil4" class="col-lg-3 control-label requerido">Foto Slider movil 4</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="slidermovil4" id="slidermovil4" class="form-control" value="{{old('slidermovil4', $data->slidermovil4 ?? '')}}"  /><br>
                    <label ><img src="/storage/{{old('slidermovil4', $data->slidermovil4 ?? '')}}" style="width: 50%;"></label>
                </div>
</div> 
<!---->
<div class="form-group">
    <label for="slidermovil5" class="col-lg-3 control-label requerido">Foto Slider movil 5</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="slidermovil5" id="slidermovil5" class="form-control" value="{{old('slidermovil5', $data->slidermovil5 ?? '')}}"  /><br>
                    <label ><img src="/storage/{{old('slidermovil5', $data->slidermovil5 ?? '')}}" style="width: 50%;"></label>
                </div>
</div> 




<div class="form-group">
    <label for="stock" class="col-lg-3 control-label requerido">Stock</label>
               <div class="col-lg-8">
                    <input type="text" name="stock" id="stock" class="form-control" value="{{old('stock', $data->stock ?? '')}}"  />
                </div>
</div> 
<div class="form-group">
    <label for="check_stock" class="col-lg-3 control-label" >¿Tiene Stock?</label>
      <div class="col-lg-8">
        <input type="checkbox" name="check_stock"  id="check_stock"  value="1" />
      </div>
</div> 
<div class="form-group">
    <label for="check_tw" class="col-lg-3 control-label" >Tipo Townhouse</label>
      <div class="col-lg-8">
        <input type="checkbox" name="check_tw"  id="check_tw"  value="1" />
      </div>
</div> 

<!---->
<div class="form-group">
    <label for="bajada" class="col-lg-3 control-label requerido">Bajada Modelo</label>
               <div class="col-lg-8">
                    <textarea type="text" class="form-control" style="resize:none" name="bajada" id="bajada" rows="5" cols="33" wrap="hard" required>{{old('bajada', $data->bajada ?? '')}}</textarea>
                </div>
</div> 
<!---->
<div class="form-group">
    <label for="mtrs" class="col-lg-3 control-label requerido">Metros Cuadrados</label>
               <div class="col-lg-8">
                    <input type="text" name="mtrs" id="mtrs" class="form-control" value="{{old('mtrs', $data->mtrs ?? '')}}" required />
                </div>
</div> 
<!---->
<div class="form-group">
    <label for="tipoinmueble_id" class="col-lg-3 control-label requerido">Tipo Inmueble</label>
               <div class="col-lg-8">
                   <select class="form-control" id="tipoinmueble_id" name="tipoinmueble_id" required>
                    <option value="" selected disabled hidden>Seleccionar</option>
                                        @foreach(\App\Models\Admin\TipoInmueble::all() as $tipoinmueble)
                                            <option value="{{$tipoinmueble->id}}">{{$tipoinmueble->nombre_inmueble}}</option>
                                        @endforeach
                    </select>
               </div>
</div>
<!---->
<!---->
<div class="form-group">
    <label for="banos" class="col-lg-3 control-label requerido">Cantidad Baños</label>
               <div class="col-lg-8">
                    <input type="string" name="banos" id="banos" class="form-control" value="{{old('banos', $data->banos ?? '')}}" required />
                </div>
</div> 
<!---->
<!---->
<div class="form-group">
    <label for="dormitorio" class="col-lg-3 control-label requerido">Cantidad Dormitorios</label>
               <div class="col-lg-8">
                    <input type="string" name="dormitorio" id="dormitorio" class="form-control" value="{{old('dormitorio', $data->dormitorio ?? '')}}" required />
                </div>
</div>
<!---->
<div class="form-group">
    <label for="tipococina_id" class="col-lg-3 control-label requerido">Tipo Cocina</label>
               <div class="col-lg-8">
                   <select class="form-control" id="tipococina_id" name="tipococina_id" required>
                    <option value="" selected disabled hidden>Seleccionar</option>
                                        @foreach(\App\Models\Admin\TipoCocina::all() as $tipococina)
                                            <option value="{{$tipococina->id}}">{{$tipococina->nombre_cocina}}</option>
                                        @endforeach
                    </select>
               </div>
</div>
<!---->

<div class="form-group">
    <label for="estacionamiento" class="col-lg-3 control-label requerido">Cantidad Estacionamientos</label>
               <div class="col-lg-8">
                    <input type="string" name="estacionamiento" id="estacionamiento" class="form-control" value="{{old('estacionamiento', $data->estacionamiento ?? '')}}"  />
                </div>
</div>
<!---->
<!-- ------------------Terminaciones------------------ -->
<div class="form-group">
  <label for="galeria" class="col-lg-3 control-label requerido">Galeria</label>
  <div class="col-lg-8">
    <textarea type="text" class="form-control" style="resize:none" name="galeria" id="galeria" rows="5" cols="33" wrap="hard" required>{{old('galeria', $data->galeria ?? '')}}</textarea> 
  </div>
</div> 

<!-- ------------------Terminaciones------------------ -->
<div class="form-group">
  <label for="terminaciones" class="col-lg-3 control-label requerido">Terminaciones</label>
  <div class="col-lg-8">
    <textarea type="text" class="form-control" style="resize:none" name="terminaciones" id="terminaciones" rows="5" cols="33" wrap="hard" required>{{old('terminaciones', $data->terminaciones ?? '')}}</textarea> 
  </div>
</div> 

<!-- ------------------equipamiento------------------ -->
<div class="form-group">
  <label for="equipamiento" class="col-lg-3 control-label requerido">Equipamiento</label>
  <div class="col-lg-8">
    <textarea type="text" class="form-control" style="resize:none" name="equipamiento" id="equipamiento" rows="5" cols="33" wrap="hard" required>{{old('equipamiento', $data->equipamiento ?? '')}}</textarea> 
  </div>
</div> 

<!-- ------------------ubicacion_entorno------------------ -->
<div class="form-group">
  <label for="ubicacion" class="col-lg-3 control-label requerido">Ubicacion y entorno</label>
  <div class="col-lg-8">
    <textarea type="text" class="form-control" style="resize:none" name="ubicacion" id="ubicacion" rows="5" cols="33" wrap="hard" required>{{old('ubicacion', $data->ubicacion ?? '')}}</textarea> 
  </div>
</div> 

<!-- ------------------Imagen Financiamiento  ------------------ -->
<div class="form-group">
    <label for="gimg1" class="col-lg-3 control-label requerido">Foto Galeria</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="gimg1" id="gimg1" class="form-control" value="{{old('gimg1', $data->gimg1 ?? '')}}"  />
                </div>
</div> 
<!---->
<!-- ------------------Imagen terminacion 3 ------------------ -->
<div class="form-group">
    <label for="gimg2" class="col-lg-3 control-label requerido">Foto Galeria</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="gimg2" id="gimg2" class="form-control" value="{{old('gimg2', $data->gimg2 ?? '')}}"  />
                </div>
</div> 
<!---->
<!-- ------------------Imagen terminacion 4 ------------------ -->
<div class="form-group">
    <label for="gimg3" class="col-lg-3 control-label requerido">Foto Galeria</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="gimg3" id="gimg3" class="form-control" value="{{old('gimg3', $data->gimg3 ?? '')}}"  />
                </div>
</div> 
<!---->
<!-- ------------------Imagen terminacion 5 ------------------ -->
<div class="form-group">
    <label for="gimg4" class="col-lg-3 control-label requerido">Foto Galeria</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="gimg4" id="gimg4" class="form-control" value="{{old('gimg4', $data->gimg4 ?? '')}}"  />
                </div>
</div> 
<!---->
<!-- ------------------Opciones de financiamiento------------------ -->
<div class="form-group">
    <label for="gimg5" class="col-lg-3 control-label requerido">Foto Galeria</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="gimg5" id="gimg5" class="form-control" value="{{old('gimg5', $data->gimg5 ?? '')}}"  />
                </div>
</div> 
<!---->
<!-- ------------------Imagen 1------------------ -->
<div class="form-group">
    <label for="gimg6" class="col-lg-3 control-label requerido">Foto Galeria</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="gimg6" id="gimg6" class="form-control" value="{{old('gimg6', $data->gimg6 ?? '')}}"  />
                </div>
</div> 
<!---->
<!-- ------------------Imagen terminacion 3 ------------------ -->
<div class="form-group">
    <label for="gimg7" class="col-lg-3 control-label requerido">Foto Galeria</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="gimg7" id="gimg7" class="form-control" value="{{old('gimg7', $data->gimg7 ?? '')}}"  />
                </div>
</div> 
<!---->
<!-- ------------------Imagen terminacion 4 ------------------ -->
<div class="form-group">
    <label for="gimg8" class="col-lg-3 control-label requerido">Foto Galeria</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="gimg8" id="gimg8" class="form-control" value="{{old('gimg8', $data->gimg8 ?? '')}}"  />
                </div>
</div> 
<!---->
<!-- ------------------Imagen terminacion 5 ------------------ -->
<div class="form-group">
    <label for="gimg9" class="col-lg-3 control-label requerido">Foto Galeria</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="gimg9" id="gimg9" class="form-control" value="{{old('gimg9', $data->gimg9 ?? '')}}"  />
                </div>
</div> 
<!---->
<!-- ------------------Opciones de financiamiento------------------ -->
<div class="form-group">
    <label for="gimg10" class="col-lg-3 control-label requerido">Foto Galeria</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="gimg10" id="gimg10" class="form-control" value="{{old('gimg10', $data->gimg10 ?? '')}}"  />
                </div>
</div> 
<!-- ------------------Imagen 1------------------ -->
<div class="form-group">
    <label for="timg1" class="col-lg-3 control-label requerido">Foto Terminaciones</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="timg1" id="timg1" class="form-control" value="{{old('timg1', $data->timg1 ?? '')}}"  />
                </div>
</div> 
<!---->
<!-- ------------------Imagen terminacion 3 ------------------ -->
<div class="form-group">
    <label for="timg2" class="col-lg-3 control-label requerido">Foto Terminaciones</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="timg2" id="timg2" class="form-control" value="{{old('timg2', $data->timg2 ?? '')}}"  />
                </div>
</div> 
<!---->
<!-- ------------------Imagen terminacion 4 ------------------ -->
<div class="form-group">
    <label for="timg3" class="col-lg-3 control-label requerido">Foto Terminaciones</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="timg3" id="timg3" class="form-control" value="{{old('timg3', $data->timg3 ?? '')}}"  />
                </div>
</div> 
<!---->
<!-- ------------------Imagen terminacion 5 ------------------ -->
<div class="form-group">
    <label for="timg4" class="col-lg-3 control-label requerido">Foto Terminaciones</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="timg4" id="timg4" class="form-control" value="{{old('timg4', $data->timg4 ?? '')}}"  />
                </div>
</div> 
<!---->
<!-- ------------------Opciones de financiamiento------------------ -->
<div class="form-group">
    <label for="tmig5" class="col-lg-3 control-label requerido">Foto Terminaciones</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="tmig5" id="tmig5" class="form-control" value="{{old('tmig5', $data->tmig5 ?? '')}}"  />
                </div>
</div> 
<!---->
<!-- ------------------Imagen 1------------------ -->
<div class="form-group">
    <label for="timg6" class="col-lg-3 control-label requerido">Foto Terminaciones</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="timg6" id="timg6" class="form-control" value="{{old('timg6', $data->timg6 ?? '')}}"  />
                </div>
</div> 
<!---->
<!-- ------------------Imagen terminacion 3 ------------------ -->
<div class="form-group">
    <label for="timg7" class="col-lg-3 control-label requerido">Foto Terminaciones</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="timg7" id="timg7" class="form-control" value="{{old('timg7', $data->timg7 ?? '')}}"  />
                </div>
</div> 
<!---->
<!-- ------------------Imagen terminacion 4 ------------------ -->
<div class="form-group">
    <label for="timg8" class="col-lg-3 control-label requerido">Foto Terminaciones</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="timg8" id="timg8" class="form-control" value="{{old('timg8', $data->timg8 ?? '')}}"  />
                </div>
</div> 
<!---->
<!-- ------------------Imagen terminacion 5 ------------------ -->
<div class="form-group">
    <label for="timg9" class="col-lg-3 control-label requerido">Foto Terminaciones</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="timg9" id="timg9" class="form-control" value="{{old('timg9', $data->timg9 ?? '')}}"  />
                </div>
</div> 
<!---->
<!-- ------------------Opciones de financiamiento------------------ -->
<div class="form-group">
    <label for="timg10" class="col-lg-3 control-label requerido">Foto Terminaciones</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="timg10" id="timg10" class="form-control" value="{{old('timg10', $data->timg10 ?? '')}}"  />
                </div>
</div> 
<!---->
<div class="form-group">
    <label for="eimg1" class="col-lg-3 control-label requerido">Foto Equipamiento</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="eimg1" id="eimg1" class="form-control" value="{{old('eimg1', $data->eimg1 ?? '')}}"  />
                </div>
</div> 
<!---->
<!-- ------------------Imagen terminacion 3 ------------------ -->
<div class="form-group">
    <label for="eimg2" class="col-lg-3 control-label requerido">Foto Equipamiento</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="eimg2" id="eimg2" class="form-control" value="{{old('eimg2', $data->eimg2 ?? '')}}"  />
                </div>
</div> 
<!---->
<!-- ------------------Imagen terminacion 4 ------------------ -->
<div class="form-group">
    <label for="eimg3" class="col-lg-3 control-label requerido">Foto Equipamiento</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="eimg3" id="eimg3" class="form-control" value="{{old('eimg3', $data->eimg3 ?? '')}}"  />
                </div>
</div> 
<!---->
<!-- ------------------Imagen terminacion 5 ------------------ -->
<div class="form-group">
    <label for="eimg4" class="col-lg-3 control-label requerido">Foto Equipamiento</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="eimg4" id="eimg4" class="form-control" value="{{old('eimg4', $data->eimg4 ?? '')}}"  />
                </div>
</div> 
<!---->
<!-- ------------------Opciones de financiamiento------------------ -->
<div class="form-group">
    <label for="eimg5" class="col-lg-3 control-label requerido">Foto Equipamiento</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="eimg5" id="eimg5" class="form-control" value="{{old('eimg5', $data->eimg5 ?? '')}}"  />
                </div>
</div> 
<!---->
<!---->
<div class="form-group">
    <label for="eimg6" class="col-lg-3 control-label requerido">Foto Equipamiento</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="eimg6" id="eimg6" class="form-control" value="{{old('eimg6', $data->eimg6 ?? '')}}"  />
                </div>
</div> 
<!---->
<!-- ------------------Imagen terminacion 3 ------------------ -->
<div class="form-group">
    <label for="eimg7" class="col-lg-3 control-label requerido">Foto Equipamiento</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="eimg7" id="eimg7" class="form-control" value="{{old('eimg7', $data->eimg7 ?? '')}}"  />
                </div>
</div> 
<!---->
<!-- ------------------Imagen terminacion 4 ------------------ -->
<div class="form-group">
    <label for="eimg8" class="col-lg-3 control-label requerido">Foto Equipamiento</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="eimg8" id="eimg8" class="form-control" value="{{old('eimg8', $data->eimg8 ?? '')}}"  />
                </div>
</div> 
<!---->
<!-- ------------------Imagen terminacion 5 ------------------ -->
<div class="form-group">
    <label for="eimg9" class="col-lg-3 control-label requerido">Foto Equipamiento</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="eimg9" id="eimg9" class="form-control" value="{{old('eimg9', $data->eimg9 ?? '')}}"  />
                </div>
</div> 
<!---->
<!-- ------------------Opciones de financiamiento------------------ -->
<div class="form-group">
    <label for="eimg10" class="col-lg-3 control-label requerido">Foto Equipamiento</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="eimg10" id="eimg10" class="form-control" value="{{old('eimg10', $data->eimg10 ?? '')}}"  />
                </div>
</div> 
<!---->
<div class="form-group">
    <label for="uimg1" class="col-lg-3 control-label requerido">Foto Ubicacion</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="uimg1" id="uimg1" class="form-control" value="{{old('uimg1', $data->uimg1 ?? '')}}"  />
                </div>
</div> 
<!---->
<!-- ------------------Imagen terminacion 3 ------------------ -->
<div class="form-group">
    <label for="uimg2" class="col-lg-3 control-label requerido">Foto Ubicacion</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="uimg2" id="uimg2" class="form-control" value="{{old('uimg2', $data->uimg2 ?? '')}}"  />
                </div>
</div> 
<!---->
<!-- ------------------Imagen terminacion 4 ------------------ -->
<div class="form-group">
    <label for="uimg3" class="col-lg-3 control-label requerido">Foto Ubicacion</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="uimg3" id="uimg3" class="form-control" value="{{old('uimg3', $data->uimg3 ?? '')}}"  />
                </div>
</div> 
<!---->
<!-- ------------------Imagen terminacion 5 ------------------ -->
<div class="form-group">
    <label for="uimg4" class="col-lg-3 control-label requerido">Foto Ubicacion</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="uimg4" id="uimg4" class="form-control" value="{{old('uimg4', $data->uimg4 ?? '')}}"  />
                </div>
</div> 
<!---->
<!-- ------------------Opciones de financiamiento------------------ -->
<div class="form-group">
    <label for="uimg5" class="col-lg-3 control-label requerido">Foto Ubicacion</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="uimg5" id="uimg5" class="form-control" value="{{old('uimg5', $data->uimg5 ?? '')}}"  />
                </div>
</div> 
<!---->
<!---->
<div class="form-group">
    <label for="uimg6" class="col-lg-3 control-label requerido">Foto Ubicacion</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="uimg6" id="uimg6" class="form-control" value="{{old('uimg6', $data->uimg6 ?? '')}}"  />
                </div>
</div> 
<!---->
<!-- ------------------Imagen terminacion 3 ------------------ -->
<div class="form-group">
    <label for="uimg7" class="col-lg-3 control-label requerido">Foto Ubicacion</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="uimg7" id="uimg7" class="form-control" value="{{old('uimg7', $data->uimg7 ?? '')}}"  />
                </div>
</div> 
<!---->
<!-- ------------------Imagen terminacion 4 ------------------ -->
<div class="form-group">
    <label for="uimg8" class="col-lg-3 control-label requerido">Foto Ubicacion</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="uimg8" id="uimg8" class="form-control" value="{{old('uimg8', $data->uimg8 ?? '')}}"  />
                </div>
</div> 
<!---->
<!-- ------------------Imagen terminacion 5 ------------------ -->
<div class="form-group">
    <label for="uimg9" class="col-lg-3 control-label requerido">Foto Ubicacion</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="uimg9" id="uimg9" class="form-control" value="{{old('uimg9', $data->uimg9 ?? '')}}"  />
                </div>
</div> 
<!---->
<!-- ------------------Opciones de financiamiento------------------ -->
<div class="form-group">
    <label for="uimg10" class="col-lg-3 control-label requerido">Foto Ubicacion</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="uimg10" id="uimg10" class="form-control" value="{{old('uimg10', $data->uimg10 ?? '')}}"  />
                </div>
</div> 
<!---->
<div class="form-group">
    <label for="vt1" class="col-lg-3 control-label requerido">Video Terminaciones</label>
               <div class="col-lg-8">
                    <input type="text" name="vt1" id="vt1" class="form-control" value="{{old('vt1', $data->vt1 ?? '')}}"  />
                </div>
</div> 
<div class="form-group">
    <label for="ve1" class="col-lg-3 control-label requerido">Video Equipamiento</label>
               <div class="col-lg-8">
                    <input type="text" name="ve1" id="ve1" class="form-control" value="{{old('ve1', $data->ve1 ?? '')}}"  />
                </div>
</div> 
<div class="form-group">
    <label for="vu1" class="col-lg-3 control-label requerido">Video Ubicacion</label>
               <div class="col-lg-8">
                    <input type="text" name="vu1" id="vu1" class="form-control" value="{{old('vu1', $data->vu1 ?? '')}}"  />
                </div>
</div>
<div class="form-group">
    <label for="tour360" class="col-lg-3 control-label requerido">Tour 360°</label>
               <div class="col-lg-8">
                    <input type="text" name="tour360" id="tour360" class="form-control" value="{{old('tour360', $data->tour360 ?? '')}}"  />
                </div>
</div>  
<!---->
<div class="form-group">
    <label for="proyecto_id" class="col-lg-3 control-label requerido">Proyecto</label>
               <div class="col-lg-8">
                   <select class="form-control" id="proyecto_id" name="proyecto_id" required>
                    <option value="" selected disabled hidden>Seleccionar</option>
                                        @foreach(\App\Models\Admin\Proyecto::all() as $proyecto)
                                            <option value="{{$proyecto->id}}">{{$proyecto->nombre_proyecto}}</option>
                                        @endforeach
                    </select>
               </div>
</div>
