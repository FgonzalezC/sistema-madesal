@extends("theme.$theme.layout")
@section('titulo')
Proyectos
@endsection

@section("scripts")
<script src="{{asset("assets/pages/scripts/admin/index.js")}}" type="text/javascript"></script>
@endsection

@section('contenido')
<div class="row">
    <div class="col-lg-12">
        @include('includes.mensaje')
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Proyectos</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                            class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <form method="post" action="{{ route('proyecto_import') }}" enctype="multipart/form-data">
                            {{csrf_field()}}

                            <input type="file" name="excel_proyecto">

                            <br>
                            <button type="submit" class="btn btn-primary">
                            Enviar
                        </button>
                            <br><br>
                            <a href="{{route('crear_proyecto')}}" class="btn btn-block btn-success btn-sm">
                                <i class="fa fa-fw fa-plus-circle"></i> Nuevo registro
                            </a>
                        </form>

                </div>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                <table style="font-size:small " class="table table-striped table-bordered table-hover" id="tabla-data">
                    <thead>
                        <tr>
                            <th >Accion</th> <!-- borrar cuando esté lista la tabla ajustada a la pantalla -->
                            <th >Nombre</th>
                            <th>Tipo Etapa</th>
                            <th>Tipo Inmueble</th>
                            <th>Dormitorio</th>
                            <th>Baños</th>
                            <th>Estacionamientos</th>
                            <th >Valor Estacionamiento</th>
                            <th>Destacado Home</th>
                            <th>Destacado Inversionista</th>
                            <th>Superficie (M2)</th>
                            <th>Precio (UF)</th>
                            <th>Comuna</th>
                            <th>Estado Proyecto</th>
                            <th>Subsidio</th>

                            <th>Sala de Venta</th>

                            <th class="width70"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($datas as $data)
                        <tr>
                            <td> <!-- Inicio botones -->
                                <a href="{{route('editar_proyecto', ['id' => $data->id])}}" class="btn-accion-tabla tooltipsC" title="Editar este registro">
                                    <i class="fa fa-fw fa-pencil"></i>
                                </a>
                                <form action="{{route('eliminar_proyecto', ['id' => $data->id])}}" class="d-inline form-eliminar" method="POST">
                                    @csrf @method("delete")
                                    <button type="submit" class="btn-accion-tabla eliminar tooltipsC" title="Eliminar este registro">
                                        <i class="fa fa-fw fa-trash text-danger"></i>
                                    </button>
                                </form>
                            </td> <!-- fin botones -->

                            <td>{{$data->nombre_proyecto}}</td>
                            <td>{{\App\Models\Admin\TipoEtapa::find($data->tipoetapa_id)->nombre_etapa}}</td>
                            <td>{{\App\Models\Admin\TipoInmueble::find($data->tipoinmueble_id)->nombre_inmueble}}</td>
                            <td>{{$data->dormitorios}}</td>
                            <td>{{$data->banos}}</td>
                            <td>{{$data->estacionamiento}}</td>
                            <td>{{$data->valor_estacionamiento}}</td>
                            <td>{{$data->destacado}}</td>
                            <td>{{$data->destacado2}}</td>
                            <td>{{$data->superficie}}</td>
                            <td>{{$data->precio}}</td>
                            <td>{{\App\Models\Admin\Comuna::find($data->comuna_id)->nombre_comuna}}</td>
                            <td>{{\App\Models\Admin\EstadoProyecto::find($data->estado_id)->nombre_estado}}</td>
                            <td>{{\App\Models\Admin\Subsidio::find($data->subsidio_id)->nombre_subsidio}}</td>

                            @if(\App\Models\Admin\SaladeVenta::find($data->salaventas_id)!=NULL)
                               <td >{{\App\Models\Admin\SaladeVenta::find($data->salaventas_id)->nombre}}</td>
                               @else
                               <td>Sin Dato</td>
                            @endif

                            
                         <!--  <td> 
                                <a href="{{route('editar_proyecto', ['id' => $data->id])}}" class="btn-accion-tabla tooltipsC" title="Editar este registro">
                                    <i class="fa fa-fw fa-pencil"></i>
                                </a>
                                <form action="{{route('eliminar_proyecto', ['id' => $data->id])}}" class="d-inline form-eliminar" method="POST">
                                    @csrf @method("delete")
                                    <button type="submit" class="btn-accion-tabla eliminar tooltipsC" title="Eliminar este registro">
                                        <i class="fa fa-fw fa-trash text-danger"></i>
                                    </button>
                                </form>
                            </td> -->
                            
                        </tr>
                        @endforeach
                        

                    </tbody>
                </table>
                </div>
            </div>
            <div class="col-sm-7">
                                <div class="dataTables_paginate paging_simple_numbers"
                                     id="example2_paginate">
                                    {{ $datas->links() }}

                                </div>
                            </div>
        </div>
    </div>
</div>
@endsection
