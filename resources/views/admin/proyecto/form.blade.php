<div class="form-group">
  <label for="nombre_proyecto" class="col-lg-3 control-label requerido">Nombre Proyecto</label>
  <div class="col-lg-8">
    <input type="text" name="nombre_proyecto" id="nombre_proyecto" class="form-control" value="{{old('nombre_proyecto', $data->nombre_proyecto ?? '')}}" required />
  </div>
</div> 
<!--<div class="form-group">
    <label for="archivo" class="col-lg-3 control-label requerido">Ficha Documento</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="archivo" id="archivo" class="form-control" value="{{old('archivo', $data->archivo ?? '')}}"  /><br>
                    <label ><img src="/storage/{{old('img2', $data->archivo ?? '')}}" style="width: 50%;"></label>
                </div>
</div> !---->

<div class="form-group">
  <label for="orden" class="col-lg-3 control-label">Orden Proyecto</label>
  <div class="col-lg-8">
    <input type="text" name="orden" id="orden" class="form-control" value="{{old('orden', $data->orden ?? '')}}" required />
  </div>
</div> 


<div class="form-group">
  <label for="tipoetapa_id" class="col-lg-3 control-label requerido">Tipo Etapa</label>
  <div class="col-lg-8">
   <select class="form-control" id="tipoetapa_id" name="tipoetapa_id" required>
    <option value="" selected disabled hidden>Seleccionar</option>
    @foreach(\App\Models\Admin\TipoEtapa::all() as $tipoetapa)
    <option value="{{ $tipoetapa->id }}"  @if($tipoetapa->id==$data->tipoetapa_id) selected='selected' @endif >{{ $tipoetapa->nombre_etapa }}</option>

    @endforeach
  </select>
</div>





</div>
<!---->

<div class="form-group">
  <label for="tipoinmueble_id" class="col-lg-3 control-label requerido">Tipo Inmueble</label>
  <div class="col-lg-8">
   <select class="form-control" id="tipoinmueble_id" name="tipoinmueble_id" required>
    <option value="" selected disabled hidden>Seleccionar</option>
    @foreach(\App\Models\Admin\TipoInmueble::all() as $tipoinmueble)
    <option value="{{ $tipoinmueble->id }}"  @if($tipoinmueble->id==$data->tipoinmueble_id) selected='selected' @endif >{{ $tipoinmueble->nombre_inmueble }}</option>

    @endforeach
  </select>
</div>
</div>
<!---->
<div class="form-group">
  <label for="dormitorios" class="col-lg-3 control-label requerido">Cantidad Dormitorios</label>
  <div class="col-lg-8">
    <input type="string" name="dormitorios" id="dormitorios" class="form-control" value="{{old('dormitorios', $data->dormitorios ?? '')}}" required />
  </div>
</div>
<!---->
<div class="form-group">
  <label for="banos" class="col-lg-3 control-label requerido">Cantidad Baños</label>
  <div class="col-lg-8">
    <input type="string" name="banos" id="banos" class="form-control" value="{{old('banos', $data->banos ?? '')}}" required />
  </div>
</div> 
<!---->
<div class="form-group">
  <label for="estacionamiento" class="col-lg-3 control-label requerido">Cantidad Estacionamientos</label>
  <div class="col-lg-8">
    <input type="string" name="estacionamiento" id="estacionamiento" class="form-control" value="{{old('estacionamiento', $data->estacionamiento ?? '')}}" required />
  </div>
</div>
<div class="form-group">
    <label for="valor_estacionamiento" class="col-lg-3 control-label requerido">Valor Estacionamiento</label>
               <div class="col-lg-8">
                    <input type="integer" name="valor_estacionamiento" id="valor_estacionamiento" class="form-control" value="{{old('valor_estacionamiento', $data->valor_estacionamiento ?? '')}}"  />
                </div>
</div>
<!---->
<div class="form-group">
  <label for="destacado" class="col-lg-3 control-label" >Destacar Home</label>
  <div class="col-lg-8">
    @switch($data->destacado)

    @case(0)
    <input type="checkbox" name="destacado"  id="destacado"  value="1" />

    @break

    @case(1)
    <input type="checkbox" name="destacado"  id="destacado" checked="1" value="1" />

    @break
    @endswitch


  </div>
</div> 
<!---->
<!---->
<div class="form-group">
  <label for="destacado2" class="col-lg-3 control-label" >Destacar Inversionista</label>
  <div class="col-lg-8">
    @switch($data->destacado2)

    @case(0)
    <input type="checkbox" name="destacado2"  id="destacado"  value="1" />

    @break

    @case(1)
    <input type="checkbox" name="destacado2"  id="destacado" checked="1" value="1" />

    @break
    @endswitch

  </div>
</div> 
<!---->
<div class="form-group">
  <label for="superficie" class="col-lg-3 control-label ">Superficie (m2)</label>
  <div class="col-lg-8">
    <input type="integer" name="superficie" id="superficie" class="form-control" value="{{old('superficie', $data->superficie ?? '')}}" required />
  </div>
</div>
<!---->
<div class="form-group">
  <label for="precio" class="col-lg-3 control-label requerido">Precio (UF)</label>
  <div class="col-lg-8">
    <input type="integer" name="precio" id="precio" class="form-control" value="{{old('precio', $data->precio ?? '')}}" required />
  </div>
</div>
<!-- ------------------Imagen Home------------------ -->
<div class="form-group">
  <label for="img_home" class="col-lg-3 control-label requerido">Img Home</label>
  <div class="col-lg-8">
    <input type="file" class="custom-file-input" name="img_home" id="img_home" class="form-control" value="{{old('img_home', $data->img_home ?? '')}}"  /><br>
    @if($data->img_home!=NULL)
    <label ><img src="/storage/{{old('img2', $data->img_home ?? '')}}" style="width: 50%;"></label>
       <a onclick="return confirm('Esta seguro que quiere eliminar la imagen')" href="{{route('eliminar_fotoproyecto',['id' => $data->id,'dato'=>$data->img_home])}}" class="btn-accion-tabla eliminar tooltipsC" >
        <i class="fa fa-fw fa-trash text-danger"></i>
      </a>
    @endif
  </div>
</div> 
<!-- ------------------Imagen MAster Plan------------------ -->
<div class="form-group">
  <label for="masterplan" class="col-lg-3 control-label requerido">Masterplan</label>
  <div class="col-lg-8">
    <input type="file" class="custom-file-input" name="masterplan" id="masterplan" class="form-control" value="{{old('masterplan', $data->masterplan ?? '')}}"  /><br>
    @if($data->masterplan!=NULL)
    <label ><img src="/storage/{{old('img2', $data->masterplan ?? '')}}" style="width: 50%;"></label>
       <a onclick="return confirm('Esta seguro que quiere eliminar la imagen')" href="{{route('eliminar_fotoproyecto',['id' => $data->id,'dato'=>$data->masterplan])}}" class="btn-accion-tabla eliminar tooltipsC" >
        <i class="fa fa-fw fa-trash text-danger"></i>
      </a>
    @endif
  </div>
</div> 

<!-- ------------------Imagen 1------------------ -->

<div class="form-group">
  <label for="desktopimg1" class="col-lg-3 control-label requerido">Slider 1</label>
  <div class="col-lg-8">
    <input type="file" class="custom-file-input" name="desktopimg1" id="desktopimg1" class="form-control" value="{{old('desktopimg1', $data->desktopimg1 ?? '')}}"  /><br>
   @if($data->desktopimg1!=NULL)
    <label ><img src="/storage/{{old('img2', $data->desktopimg1 ?? '')}}" style="width: 50%;"></label>
       <a onclick="return confirm('Esta seguro que quiere eliminar la imagen')" href="{{route('eliminar_fotoproyecto',['id' => $data->id,'dato'=>$data->desktopimg1])}}" class="btn-accion-tabla eliminar tooltipsC" >
        <i class="fa fa-fw fa-trash text-danger"></i>
      </a>
    @endif
  </div>
</div> 
<!-- ------------------Imagen 2------------------ -->
<div class="form-group">
  <label for="desktopimg2" class="col-lg-3 control-label requerido">Slider 2</label>
  <div class="col-lg-8">
    <input type="file" class="custom-file-input" name="desktopimg2" id="desktopimg2" class="form-control" value="{{old('desktopimg2', $data->desktopimg2 ?? '')}}"  /><br>
    @if($data->desktopimg2!=NULL)
    <label ><img src="/storage/{{old('img2', $data->desktopimg2 ?? '')}}" style="width: 50%;"></label>
       <a onclick="return confirm('Esta seguro que quiere eliminar la imagen')" href="{{route('eliminar_fotoproyecto',['id' => $data->id,'dato'=>$data->desktopimg2])}}" class="btn-accion-tabla eliminar tooltipsC" >
        <i class="fa fa-fw fa-trash text-danger"></i>
      </a>
    @endif
  </div>
</div> 
<!-- ------------------Imagen 3------------------ -->
<div class="form-group">
  <label for="desktopimg3" class="col-lg-3 control-label requerido">Slider 3</label>
  <div class="col-lg-8">
    <input type="file" class="custom-file-input" name="desktopimg3" id="desktopimg3" class="form-control" value="{{old('desktopimg3', $data->desktopimg3 ?? '')}}"  /><br>
   @if($data->desktopimg3!=NULL)
    <label ><img src="/storage/{{old('img2', $data->desktopimg3 ?? '')}}" style="width: 50%;"></label>
       <a onclick="return confirm('Esta seguro que quiere eliminar la imagen')" href="{{route('eliminar_fotoproyecto',['id' => $data->id,'dato'=>$data->desktopimg3])}}" class="btn-accion-tabla eliminar tooltipsC" >
        <i class="fa fa-fw fa-trash text-danger"></i>
      </a>
    @endif
  </div>
</div> 
<!-- ------------------Imagen 4------------------ -->
<div class="form-group">
  <label for="desktopimg4" class="col-lg-3 control-label requerido">Slider 4</label>
  <div class="col-lg-8">
    <input type="file" class="custom-file-input" name="desktopimg4" id="desktopimg4" class="form-control" value="{{old('desktopimg4', $data->desktopimg4 ?? '')}}"  /><br>
    @if($data->desktopimg4!=NULL)
    <label ><img src="/storage/{{old('img2', $data->desktopimg4 ?? '')}}" style="width: 50%;"></label>
       <a onclick="return confirm('Esta seguro que quiere eliminar la imagen')" href="{{route('eliminar_fotoproyecto',['id' => $data->id,'dato'=>$data->desktopimg4])}}" class="btn-accion-tabla eliminar tooltipsC" >
        <i class="fa fa-fw fa-trash text-danger"></i>
      </a>
    @endif
  </div>
</div> 
<!-- ------------------Imagen 5------------------ -->
<div class="form-group">
  <label for="desktopimg5" class="col-lg-3 control-label requerido">Slider 5</label>
  <div class="col-lg-8">
    <input type="file" class="custom-file-input" name="desktopimg5" id="desktopimg5" class="form-control" value="{{old('desktopimg5', $data->desktopimg5 ?? '')}}"  /><br>
    @if($data->desktopimg5!=NULL)
    <label ><img src="/storage/{{old('img2', $data->desktopimg5 ?? '')}}" style="width: 50%;"></label>
       <a onclick="return confirm('Esta seguro que quiere eliminar la imagen')" href="{{route('eliminar_fotoproyecto',['id' => $data->id,'dato'=>$data->desktopimg5])}}" class="btn-accion-tabla eliminar tooltipsC" >
        <i class="fa fa-fw fa-trash text-danger"></i>
      </a>
    @endif
  </div>
</div> 
<!-- ------------------Fin img -------------------- -->
<div class="form-group">
  <label for="movilimg1" class="col-lg-3 control-label requerido">Slider Movil 1</label>
  <div class="col-lg-8">
    <input type="file" class="custom-file-input" name="movilimg1" id="movilimg1" class="form-control" value="{{old('movilimg1', $data->movilimg1 ?? '')}}"  /><br>
    @if($data->movilimg1!=NULL)
    <label ><img src="/storage/{{old('img2', $data->movilimg1 ?? '')}}" style="width: 50%;"></label>
       <a onclick="return confirm('Esta seguro que quiere eliminar la imagen')" href="{{route('eliminar_fotoproyecto',['id' => $data->id,'dato'=>$data->movilimg1])}}" class="btn-accion-tabla eliminar tooltipsC" >
        <i class="fa fa-fw fa-trash text-danger"></i>
      </a>
    @endif
  </div>
</div> 
<!-- ------------------Imagen 2------------------ -->
<div class="form-group">
  <label for="movilimg2" class="col-lg-3 control-label requerido">Slider Movil 2</label>
  <div class="col-lg-8">
    <input type="file" class="custom-file-input" name="movilimg2" id="movilimg2" class="form-control" value="{{old('movilimg2', $data->movilimg2 ?? '')}}"  />
    <br>
    @if($data->movilimg2!=NULL)
    <label ><img src="/storage/{{old('img2', $data->movilimg2 ?? '')}}" style="width: 50%;"></label>
       <a onclick="return confirm('Esta seguro que quiere eliminar la imagen')" href="{{route('eliminar_fotoproyecto',['id' => $data->id,'dato'=>$data->movilimg2])}}" class="btn-accion-tabla eliminar tooltipsC" >
        <i class="fa fa-fw fa-trash text-danger"></i>
      </a>
    @endif
  </div>
</div> 
<!-- ------------------Imagen 3------------------ -->
<div class="form-group">
  <label for="movilimg3" class="col-lg-3 control-label requerido">Slider Movil 3</label>
  <div class="col-lg-8">
    <input type="file" class="custom-file-input" name="movilimg3" id="movilimg3" class="form-control" value="{{old('movilimg3', $data->movilimg3 ?? '')}}"  /><br>
    @if($data->movilimg3!=NULL)
    <label ><img src="/storage/{{old('img2', $data->movilimg3 ?? '')}}" style="width: 50%;"></label>
       <a onclick="return confirm('Esta seguro que quiere eliminar la imagen')" href="{{route('eliminar_fotoproyecto',['id' => $data->id,'dato'=>$data->movilimg3])}}" class="btn-accion-tabla eliminar tooltipsC" >
        <i class="fa fa-fw fa-trash text-danger"></i>
      </a>
    @endif
  </div>
</div> 
<!-- ------------------Imagen 4------------------ -->
<div class="form-group">
  <label for="movilimg4" class="col-lg-3 control-label requerido">Slider Movil  4</label>
  <div class="col-lg-8">
    <input type="file" class="custom-file-input" name="movilimg4" id="movilimg4" class="form-control" value="{{old('movilimg4', $data->movilimg4 ?? '')}}"  /><br>
    @if($data->movilimg4!=NULL)
    <label ><img src="/storage/{{old('img2', $data->movilimg4 ?? '')}}" style="width: 50%;"></label>
       <a onclick="return confirm('Esta seguro que quiere eliminar la imagen')" href="{{route('eliminar_fotoproyecto',['id' => $data->id,'dato'=>$data->movilimg4])}}" class="btn-accion-tabla eliminar tooltipsC" >
        <i class="fa fa-fw fa-trash text-danger"></i>
      </a>
    @endif
  </div>
</div> 
<!-- ------------------Imagen 5------------------ -->
<div class="form-group">
  <label for="movilimg5" class="col-lg-3 control-label requerido">Slider Movil 5</label>
  <div class="col-lg-8">
    <input type="file" class="custom-file-input" name="movilimg5" id="movilimg5" class="form-control" value="{{old('movilimg5', $data->movilimg5 ?? '')}}"  /><br>
   @if($data->movilimg5!=NULL)
    <label ><img src="/storage/{{old('img2', $data->movilimg5 ?? '')}}" style="width: 50%;"></label>
       <a onclick="return confirm('Esta seguro que quiere eliminar la imagen')" href="{{route('eliminar_fotoproyecto',['id' => $data->id,'dato'=>$data->movilimg5])}}" class="btn-accion-tabla eliminar tooltipsC" >
        <i class="fa fa-fw fa-trash text-danger"></i>
      </a>
    @endif
  </div>
</div> 

<div class="form-group">
  <label for="descripcion_slider" class="col-lg-3 control-label requerido">Descripcion Slider</label>
  <div class="col-lg-8">
    <textarea type="text" class="form-control" style="resize:none" name="descripcion_slider" id="descripcion_slider" rows="5" cols="33" wrap="hard" required>{{old('descripcion_slider', $data->descripcion_slider ?? '')}}</textarea required>
    </div>
  </div> 
  <!---->

  <div class="form-group">
    <label for="comuna_id" class="col-lg-3 control-label requerido">Comuna</label>
    <div class="col-lg-8">
     <select class="form-control" id="comuna_id" name="comuna_id" required>
      <option value="" selected disabled hidden>Seleccionar</option>
      @foreach(\App\Models\Admin\Comuna::all() as $comuna)
      <option value="{{ $comuna->id }}"  @if($comuna->id==$data->comuna_id) selected='selected' @endif >{{ $comuna->nombre_comuna }}</option>

      @endforeach

    </select>

  </div>
</div>
<!---->

<div class="form-group">
  <label for="estado_id" class="col-lg-3 control-label requerido">Estado Proyecto</label>
  <div class="col-lg-8">
   <select class="form-control" id="estado_id" name="estado_id" required>
    <option value="" selected disabled hidden>Seleccionar</option>
    @foreach(\App\Models\Admin\EstadoProyecto::all() as $estado)
    <option value="{{ $estado->id }}"  @if($estado->id==$data->estado_id) selected='selected' @endif >{{ $estado->nombre_estado }}</option>

    @endforeach

  </select>

</div>
</div>
<div class="form-group">
  <label for="subsidio_id" class="col-lg-3 control-label requerido">Subsidio</label>
  <div class="col-lg-8">
   <select class="form-control" id="subsidio_id" name="subsidio_id" required>
    <option value="" selected disabled hidden>Seleccionar</option>
    @foreach(\App\Models\Admin\Subsidio::all() as $subsidio)
    <option value="{{ $subsidio->id }}"  @if($subsidio->id==$data->subsidio_id) selected='selected' @endif >{{ $subsidio->nombre_subsidio }}</option>

    @endforeach

  </select>

</div>
</div>

<div class="form-group">
  <label for="salaventas_id" class="col-lg-3 control-label requerido">Sala de Venta</label>
  <div class="col-lg-8">
   <select class="form-control" id="salaventas_id" name="salaventas_id" required>
    <option value="" selected disabled hidden>Seleccionar</option>
    @foreach(\App\Models\Admin\SaladeVenta::all() as $sala)
    <option value="{{ $sala->id }}"  @if($sala->id==$data->salaventas_id) selected='selected' @endif >{{ $sala->nombre }}</option>

    @endforeach

  </select>

</div>
</div>





