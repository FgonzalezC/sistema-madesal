@extends("theme.$theme.layout")
@section('titulo')
Seccion Madesal
@endsection

@section("scripts")
<script src="{{asset("assets/pages/scripts/admin/index.js")}}" type="text/javascript"></script>
@endsection

@section('contenido')
<div class="row">
    <div class="col-lg-12">
        @include('includes.mensaje')
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Seccion Madesal</h3>
                <div class="box-tools pull-right">

                    <a href="{{route('crear_madesal')}}" class="btn btn-block btn-success btn-sm">
                        <i class="fa fa-fw fa-plus-circle"></i> Nuevo registro
                    </a>

                </div>
            </div>
            <div class="box-body">
                <table class="table table-striped table-bordered table-hover" id="tabla-data">
                    <thead>
                        <tr>
                            <th>Titulo</th>
                            <th>Imagen</th>
                            <th>Titulo 1</th>
                            <th>Bajada 1</th>
                            <th>Bajada 2</th>
                            <th>Bajada 3</th>
                            <th>Imagen 1</th>
                            <th>Titulo 2</th>
                            <th>Bajada 4</th>
                            <th>Imagen 2</th>
                            <th class="width70"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($datas as $data)
                        <tr>
                            <td>{{$data->titulo}}</td>
                            <td>{{$data->img}}</td>
                            <td>{{$data->titulo1}}</td>
                            <td>{{$data->bajada1}}</td>
                            <td>{{$data->bajada2}}</td>
                            <td>{{$data->bajada3}}</td>
                            <td>{{$data->img1}}</td>
                            <td>{{$data->titulo2}}</td>
                            <td>{{$data->bajada2}}</td>
                            <td>{{$data->img2}}</td>
                            <td>
                                <a href="{{route('editar_madesal', ['id' => $data->id])}}" class="btn-accion-tabla tooltipsC" title="Editar este registro">
                                    <i class="fa fa-fw fa-pencil"></i>
                                </a>
                                <form action="{{route('eliminar_madesal', ['id' => $data->id])}}" class="d-inline form-eliminar" method="POST">
                                    @csrf @method("delete")
                                    <button type="submit" class="btn-accion-tabla eliminar tooltipsC" title="Eliminar este registro">
                                        <i class="fa fa-fw fa-trash text-danger"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection