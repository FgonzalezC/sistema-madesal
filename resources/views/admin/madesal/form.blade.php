<div class="form-group">
    <label for="titulo" class="col-lg-3 control-label requerido">Titulo</label>
               <div class="col-lg-8">
                    <input type="text" name="titulo" id="titulo" class="form-control" value="{{old('titulo', $data->titulo ?? '')}}" required />
                </div>
</div> 
<!---->
<div class="form-group">
    <label for="img" class="col-lg-3 control-label requerido">Imagen</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="img" id="img" class="form-control" value="{{old('img', $data->img ?? '')}}"  /><br>
                    <label ><img src="/storage/{{old('img2', $data->img ?? '')}}" style="width: 50%;"></label>
                </div>
</div> 
<!---->
<div class="form-group">
    <label for="titulo1" class="col-lg-3 control-label requerido">Titulo 1</label>
               <div class="col-lg-8">
                    <input type="text" name="titulo1" id="titulo1" class="form-control" value="{{old('titulo1', $data->titulo1 ?? '')}}" required />
                </div>
</div> 
<!---->
<div class="form-group">
    <label for="bajada1" class="col-lg-3 control-label requerido">Bajada 1</label>
               <div class="col-lg-8">
                   <textarea type="text" class="form-control" style="resize:none" name="bajada1" id="bajada1" rows="5" cols="33" wrap="hard" required>{{old('bajada1', $data->bajada1 ?? '')}}</textarea>
                </div>
</div> 
<!---->
<!---->
<div class="form-group">
    <label for="bajada2" class="col-lg-3 control-label requerido">Bajada 2</label>
               <div class="col-lg-8">
                   <textarea type="text" class="form-control" style="resize:none" name="bajada2" id="bajada2" rows="5" cols="33" wrap="hard" required>{{old('bajada2', $data->bajada2 ?? '')}}</textarea>
                </div>
</div> 
<!---->
<!---->
<div class="form-group">
    <label for="bajada3" class="col-lg-3 control-label requerido">Bajada 3</label>
               <div class="col-lg-8">
                   <textarea type="text" class="form-control" style="resize:none" name="bajada3" id="bajada3" rows="5" cols="33" wrap="hard" >{{old('bajada3', $data->bajada1 ?? '')}}</textarea>
                </div>
</div> 
<!---->
<div class="form-group">
    <label for="img1" class="col-lg-3 control-label requerido">Imagen 1</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="img1" id="img1" class="form-control" value="{{old('img1', $data->img1 ?? '')}}"  /><br>
                    <label ><img src="/storage/{{old('img1', $data->img1 ?? '')}}" style="width: 50%;"></label>
                </div>
</div> 
<!---->
<div class="form-group">
    <label for="titulo2" class="col-lg-3 control-label requerido">Titulo 2</label>
               <div class="col-lg-8">
                    <input type="text" name="titulo2" id="titulo2" class="form-control" value="{{old('titulo2', $data->titulo2 ?? '')}}" required />
                </div>
</div> 
<!---->
<div class="form-group">
    <label for="bajada4" class="col-lg-3 control-label requerido">Bajada 4</label>
               <div class="col-lg-8">
                    <input type="text" name="bajada4" id="bajada4" class="form-control" value="{{old('bajada4', $data->bajada4 ?? '')}}" required />
                </div>
</div>
<!---->
<div class="form-group">
    <label for="img2" class="col-lg-3 control-label requerido">Imagen 2</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="img2" id="img2" class="form-control" value="{{old('img2', $data->img2 ?? '')}}"  /><br>
                    <label ><img src="/storage/{{old('img2', $data->img2 ?? '')}}" style="width: 50%;"></label>
                </div>
</div> 
