@extends("theme.$theme.layout")
@section('titulo')
Sucursal
@endsection

@section("scripts")
<script src="{{asset("assets/pages/scripts/admin/index.js")}}" type="text/javascript"></script>
@endsection

@section('contenido')
<div class="row">
   <div class="col-lg-12">
        @include('includes.mensaje')
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Seccion Sucursal</h3>
               
                @if(count($datas)==0)
                 <a href="{{route('crear_sucursal')}}" class="btn btn-success btn-sm pull-right">Crear Sucursal</a>
               @endif
                
            </div>
            <div class="box-body">
                <table class="table table-striped table-bordered table-hover" id="tabla-data">
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Direccion</th>
                            <th>Telefono</th>
                            <th>Celular</th>
                            <th>Correo</th>
                            <th>Correo2</th>
                            <th>Horario</th>
                            <th>Mapa</th>
                            <th class="width70"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($datas as $data)
                        <tr>
                            <td>{{$data->nombre}}</td>
                            <td>{{$data->direccion}}</td>
                            <td>{{$data->telefono}}</td>
                            <td>{{$data->telefono2}}</td>
                            <td>{{$data->correo}}</td>
                             <td>{{$data->correo2}}</td>
                              <td>{{$data->horario}}</td>
                            <td>{{$data->mapa}}</td>
                            <td>
                            <a href="{{route('editar_sucursal', ['id' => $data->id])}}" class="btn-accion-tabla tooltipsC" title="Editar este registro">
                                    <i class="fa fa-fw fa-pencil"></i>
                                </a>
                                <form action="{{route('eliminar_sucursal', ['id' => $data->id])}}" class="d-inline form-eliminar" method="POST">
                                    @csrf @method("delete")
                                    <button type="submit" class="btn-accion-tabla eliminar tooltipsC" title="Eliminar este registro">
                                        <i class="fa fa-fw fa-trash text-danger"></i>
                                    </button>
                                </form>
                            </td> <!-- fin botones -->
                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection