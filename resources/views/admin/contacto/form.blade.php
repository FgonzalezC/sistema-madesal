<div class="form-group">
        <label for="titulo" class="col-lg-3 control-label requerido">Titulo</label>
                   <div class="col-lg-8">
                        <input type="text" name="titulo" id="titulo" class="form-control" value="{{old('titulo', $data->titulo ?? '')}}" required />
                    </div>
    </div> 
    <!---->
  <!-- ------------------Imagen Home------------------ -->
<div class="form-group">
    <label for="imagen" class="col-lg-3 control-label requerido">Imagen Slider</label>
               <div class="col-lg-8">
                    <input type="file" class="custom-file-input" name="imagen" id="imagen" class="form-control" value="{{old('imagen', $data->imagen ?? '')}}"  />
                </div>
</div> 

<!---->
<div class="form-group">
    <label for="subtitulo" class="col-lg-3 control-label requerido">Subtitulo</label>
               <div class="col-lg-8">
                    <input type="text" name="subtitulo" id="subtitulo" class="form-control" value="{{old('subtitulo', $data->subtitulo ?? '')}}" required />
                </div>
</div> 
<!----> 
