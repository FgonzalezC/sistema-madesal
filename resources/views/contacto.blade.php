<!DOCTYPE html>
<html lang="es-Es">
@include('includes.head')
<body>
    <!-- Loading -->
    <div class="loading">
        <div class="loader"></div>
    </div>
    <!-- /Loading -->   
   <header class="header-int">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-2 brand-image">
                   <a href="{{ url('/home')}}">
                        <img data-src="images/logo-madesal.png" class="d-none d-lg-block lazy" alt="Madesal"s>
                        <img data-src="images/logo-madesal-xs.png" class="d-block d-lg-none lazy" alt="Madesal">
                    </a>
                </div>
                <div class="col-lg-9 col-10">
                    <nav class="navbar navbar-expand-lg">
                        <div class="top">
                            <ul class="d-flex">
                                @include('includes.menu-superior')
                            </ul>
                        </div>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span></span>
                            <span></span>
                            <span></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav">
                                @include('includes.menu')
                            </ul>
                            <div class="box-mobile d-block d-lg-none">
                                <ul class="box-mobile__btns">
                                    <li>
                                        <a  href="{{ url('/contacto')}}">Ayuda</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/postventas')}}">Contacto</a>
                                    </li>
                                </ul>
                                <?php 
                                $footer = \DB::table('footers')
                                                ->select('img_footer','direccion','telefono','link_face','link_instagram','link_twitter')
                                                ->get();
                                ?>

                                @foreach($footer as $footers)
                                <ul class="box-mobile__social">
                                    <li><a target="_blanck" href="{{$footers->link_face}}"><i class="icon-facebook"></i></a></li>
                                    <li><a target="_blanck" href="{{$footers->link_instagram}}"><i class="icon-instagram"></i></a></li>
                                    <li><a target="_blanck" href="{{$footers->link_twitter}}"><i class="icon-twitter"></i></a></li>
                                </ul>
                                @endforeach
                            </div>
                        </div>
                    </nav>
                </div>
            </div>  
        </div>
    </header>
 <?php  
            $contacto = \DB::table('contactos')
                        ->get();

            /*$seccionC = \DB::table('seccion_homes')->count();*/
    ?>
    
    
    <!-- MAIN HEAD -->
    <section class="main-head main-head--page">

   

        @foreach($contacto as $contactos)
        <div class="main-head__image">
            <img data-src="{{URL::asset('storage/'.$contactos->imagen)}}" class="d-none d-md-block lazy" alt="Madesal">      
        </div>
        <div class="main-head__title">
            <h1>{{$contactos->titulo}}</h1>
        </div>  
        @endforeach 
    </section>
    <!-- /MAIN HEAD DESKTOP -->
       @include('includes.msj')
       @include('includes.form-error')
    <!-- CONTACTANOS -->
    <section class="contactanos">
        <div class="container">

            <div class="row justify-content-center">
                <div class="col-12 col-lg-8 text-center">
                    <h3>{{$contactos->subtitulo}}</h3>
                </div>

                <div class="col-12 col-lg-10">
                     <form action="{{route('contacto_guardar')}}" id= "form-general" class="gnrl-form" method="POST" autocomplete="off">
                     {{ csrf_field() }}
                        <div class="row">
                            <div class="col-lg-6">
                                <input type="text" name="nombre" id="nombre" placeholder="Nombre" required>
                                <input type="text" name="apellido" id="apellido" placeholder="Apellido"required>
                                <input type="text" name="telefono" id="telefono" placeholder="Teléfono"required>
                                <input type="text" name="email" id="email" placeholder="Email"required>
                            </div>
                            <div class="col-lg-6">
                                <select name="asunto_id" id="asunto_id" required>
                                        <option value="">Agregar Asunto</option>
                                        @foreach(\App\Models\Admin\Asunto::all() as $asunto)
                                        <option value="{{$asunto->id}}" required>{{$asunto->nombre_asunto}}</option>
                                    @endforeach
                                            
                                </select>
                                <textarea type="text" name="mensaje" id="mensaje" placeholder="Mensaje" required></textarea>
                            </div>
                            <div class="col-lg-12 text-center">
                                <button class="btn-gnrl btn-gnrl--green">ENVIAR</button>
                            </div>
                        </div>                        
                    </form>
                </div>  
                <?php

              
				$empleados = DB::table('empleados')
                ->where('cargos.nombre',"Ejecutivo Contacto")
                ->join('cargos','cargos.id','empleados.cargo_id')			
				->get();




                ?>
                

                
                <div class="col-12">
                    <ul>
                        <li>
                            <h3>¿Necesitas ayuda de Administración?</h3>
                            <h4>Administración</h4>
                          	@foreach($empleados as $empleado1)
                                                      	
                           
                                    <p>{{$empleado1->nombre_empleado}}</p>
                                    <p>{{$empleado1->telefono}}</p>
                                    <p>{{$empleado1->correo}} </p>
                                    <br>
                             
                            @endforeach
                        </li>

                        <li>
                        	 <h3>¿Necesitas ayuda de ventas?</h3>
                            <h4>Ventas</h4>
                             @foreach($empleados as $empleado1)
                             
                            
                           
                                    <p>{{$empleado1->nombre_empleado}}</p>
                                    <p>{{$empleado1->telefono}}</p>
                                    <p>{{$empleado1->correo}} </p>
                                         <br>    
                            @endforeach
                        	
                        </li>
                        <li>
                            <h3>¿Necesitas ayuda de Financiamiento?</h3>
                            <h4>Financiamiento</h4>

                               @foreach($empleados as $empleado1)
                               
                            
                           
                                    <p>{{$empleado1->nombre_empleado}}</p>
                                    <p>{{$empleado1->telefono}}</p>
                                    <p>{{$empleado1->correo}} </p>
                                         <br>
                              
                            @endforeach
                        </li>
                        <li>
                            <?php 
                              $footer = \DB::table('footers')
                                ->select('img_footer','direccion','telefono','correo','link_face','link_instagram','link_twitter')
                                ->get();
                            ?>
                            @foreach($footer as $footers)
                            <h2>Nuestras oficinas</h2>
                            <ul>
                                <li>
                                    <i class="icon-mapa"></i>
                                    <p>{{$footers->direccion}}</p>
                                </li>
                                <li>
                                    <i class="icon-whatsapp"></i>
                                    <a target="_blanck" href="https://api.whatsapp.com/send?phone=+56957004821&text=hola">{{$footers->telefono}}</a>
                                </li>
                                <li>
                                    <i class="icon-mail"></i>
                                   <a target="_blanck" href="mailto:info@madesal.cl">{{$footers->correo}}</a>
                                </li>
                            </ul>
                                @endforeach
                        </li>
                    </ul>
                </div>              
            </div>
        </div>
    </section>
    <!-- /CONTACTANOS -->

        

    @include('includes.footer')
    
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/lazyload.min.js"></script>
    <script src="js/nouislider.js"></script>
    <script src="js/slick.min.js"></script>
    <script src="js/main.js"></script>    
    @include('includes.chat')
  
</body>
</html>