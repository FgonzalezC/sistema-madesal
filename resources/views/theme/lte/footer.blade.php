<!--inicio footer -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0.0
    </div>
    <strong>Madesal &copy; 2019 <a href="https://madesal.cl">Madesal</a>.</strong> 
  </footer>
  <!--fin footer -->