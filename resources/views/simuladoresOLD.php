<!DOCTYPE html>
<html lang="es-Es">
<head>
	<meta charset="UTF-8">
	<title>Madesal</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<!-- Bootstrap-->
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/nouislider.min.css">
	<!-- Estilos Generales-->
	<link rel="stylesheet" href="css/style.min.css">	
	<!-- Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Karla:400,700|Playfair+Display:400,700&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="css/icons-madesal.css">
	<link rel="icon" href="../../../public/favicon.ico" type="image/x-icon">
</head>

<body>
	<!-- Loading -->
	<div class="loading">
		<div class="loader"></div>
	</div>
	<!-- /Loading -->	
	<header class="header-int">
			<div class="container">
					<div class="row">
						<div class="col-3 brand-image">
							<a href="./home">
							<img data-src="images/logo-madesal.png" class="d-none d-lg-block lazy" alt="Madesal"s>
							<img data-src="images/logo-madesal-xs.png" class="d-block d-lg-none lazy" alt="Madesal">
							</a>
						</div>
						<div class="col-lg-9 col-10">
							<nav class="navbar navbar-expand-lg">
								<div class="top">
									<ul class="d-flex">
										<li><a href="./asesoria">Ayuda</a></li>
										<li><a href="./contacto">Contacto</a></li>
										<li><a href="./postventas">Postventa</a></li>
									</ul>
								</div>
							<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
								aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
								<span></span>
								<span></span>
								<span></span>
							</button>
							<div class="collapse navbar-collapse" id="navbarSupportedContent">
								<ul class="navbar-nav">
									<li class="nav-item">
									<a class="nav-link" href="./proyecto">Proyectos</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="./inversionista">Inversionistas</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="./financiamiento">Financiamiento</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="./simuladores.php">Simuladores</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="./asesoria">Te asesoramos</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="./madesal">Madesal</a>
								</li>
								</ul>
							</div>
						</nav>
					</div>
				</div>	
	</header>


	
	
	<!-- MAIN HEAD -->
	<section class="main-head main-head--page">
		<div class="main-head__image">
            <img data-src="images/contacto.jpg" class="d-none d-md-block lazy" alt="Madesal">      
		</div>
		<div class="main-head__title">
			<h1>Simuladores</h1>
		</div>		
	</section>
    <!-- /MAIN HEAD DESKTOP -->
    
    <div id="error"></div>
</div>
    <!-- POST VENTA -->
    <section class="simuladores">
        <div class="container">
            <div class="row">
               <div class="col-12">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="hipotecario-tab" data-toggle="tab" href="#hipotecario" role="tab" aria-controls="hipotecario"
                                aria-selected="true">Simulador de dividendo</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="dividendo-tab" data-toggle="tab" href="#dividendo" role="tab" aria-controls="dividendo"
                                aria-selected="false"> Simulador hipotecario </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="endeudamiento-tab" data-toggle="tab" href="#endeudamiento" role="tab" aria-controls="endeudamiento"
                                aria-selected="false">Capacidad de endeudamiento</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="hipotecario" role="tabpanel" aria-labelledby="hipotecario-tab">
							<!--<form  >-->
								<div class="row">
									<div class="col-lg-6">
										<ul>
											<li>Precio de la propiedad</li>
										<li><input type="number" id="input-with-keypress-0" name="precio_propiedad" value="{{$precio_propiedad}}" required></li>
										
											<li>
												<?php 
													if($valor=="uf"){
												?>
												<input type="radio" name="valor" id="uf" value="uf" checked>
												<label for="uf">UF</label>
												<input type="radio" name="valor" id="peso" value="peso">
												<label for="peso">$</label>

												<?php 
													}else if($valor=="peso"){
												?>

												<input type="radio" name="valor" id="uf" value="uf">
												<label for="uf">UF</label>
												<input type="radio" name="valor" id="peso" value="peso" checked>
												<label for="peso">$</label>

												<?php 
													}else{
												?>

												<input type="radio" name="valor" id="uf" value="uf">
												<label for="uf">UF</label>
												<input type="radio" name="valor" id="peso" value="peso">
												<label for="peso">$</label>
												<?php 
													}
												?>



											</li>
										</ul>									
										<div id="valor-propiedad" class="noUi-target noUi-ltr noUi-horizontal"></div>									
									</div>
									<div class="col-lg-6">
										<ul>
											<li>Pie (%)</li>
										<li><input type="number" id="input-with-keypress-1" name="pie" value="" required></li>
											<li>
												<input type="radio" name="valor_pie" id="porcent-pie"  value="porcent_pie" checked>
												<label for="porcent-pie">%</label>
											</li>
										</ul>									
										<div id="valor-pie" class="noUi-target noUi-ltr noUi-horizontal"></div>
									</div>
									<div class="col-lg-6">
										<ul>
											<li>Plazo</li>
											<li>
												<div id="plazo" class="noUi-target noUi-ltr noUi-horizontal"></div>
											</li>
											<li>
												<input type="number" id="input-with-keypress-2" id="valor" name="plazo" value="" step="any" required>
											</li>
										</ul>
									</div>
									<div class="col-lg-6">
										<ul>
											<li>Tasa de interés <span>(% anual)</span></li>
											<li>
												<div id="tasa" class="noUi-target noUi-ltr noUi-horizontal"></div>
											</li>
											<li>
											<input type="number" name="tasa" id="input-with-keypress-3" value=""  step="any" required>
											</li>
										</ul>
									</div>
									<div class="col-12 text-center">
										<input type="submit" onclick="calcular_dividendo();" id="btsubmit">Calcular</input>
									</div>
									<div id="capa-dividendo" class="col-12 text-center result" style="display: none;">
										<p>EL MONTO DEL DIVIDENDO DE TU CRÉDITO ES:</p>
									
										<div class="price" id="respuesta_dividendo"></div>
									
										<p class="small">*El valor del dividendo es referencial y no incluye los costos asociados de seguros</p>
									</div>
								</div>
							<!--</form>-->
						</div>
                        <div class="tab-pane fade" id="dividendo" role="tabpanel" aria-labelledby="dividendo-tab">
							<!--<form name="dividendo">-->
								<div class="row justify-content-center">
									<div class="col-lg-8 text-center">
										<h3>Simula tu dividendo si ya conoces el valor de la vivienda.</h3>
									
										<p>Valor UF al día: {{$valor_uf}} </p>
											
									
									</div>
									<div class="col-lg-8">
										<ul class="justify-content-center">
											<li>
												<h4>Monto a financiar</h4>
											</li>
											<li>
												<div id="monto" class="noUi-target noUi-ltr noUi-horizontal"></div>
											</li>
											<li>
											<input type="number" name="monto" id="input-with-keypress-4" value="{{$monto}}">
											</li>									
										</ul>
									</div>
									<div class="col-lg-8">
										<ul class="justify-content-center">
											<li>
												<h4>Pie(%)</h4>
											</li>
											<li>
												<div id="div-plazo" class="noUi-target noUi-ltr noUi-horizontal"></div>
											</li>
											<li>
											<input type="number" name="div_plazo" id="input-with-keypress-5"  step="any" value="{{$div_plazo}}">
											</li>
										</ul>
									</div>
									<div class="col-lg-8">
										<ul class="justify-content-center">
											<li>
												<h4>Tasa de interés</h4>
											</li>
											<li>
												<div id="div-tasa" class="noUi-target noUi-ltr noUi-horizontal"></div>
											</li>
											<li>
											<input type="number" name="div_tasa" id="input-with-keypress-6"  step="any" value="{{$div_tasa}}">
											</li>
										</ul>
									</div>
									<div class="col-12 text-center">
										<button id="btsubmit">Calcular</button>
									</div>
									<div class="col-12 text-center result">
										<p>EL MONTO DEL DIVIDENDO DE TU CRÉDITO ES:</p>
										<div class="price"> {{$respuesta4}} U.F - ${{$respuesta2}}</div>
										<p class="small">*El valor del dividendo es referencial y no incluye los costos asociados de seguros</p>
									</div>
								</div>
							<!--</form>-->
						</div>
                        <div class="tab-pane fade" id="endeudamiento" role="tabpanel" aria-labelledby="endeudamiento-tab">
							<!--<form class="gnrl-form">-->
								<div class="row align-items-end">
									<div class="col-lg-12 text-center">
										<h3>Responde las siguientes preguntas para revisar tu capacidad de endeudamiento.</h3>
										<p>Valor UF al día: {{$valor_uf}} </p>
									</div>
									<div class="col-lg-6">
										<div class="question">
											<label for="">¿Cuál es tu renta mensual líquida aproximada?</label>
										<input type="text" id="monto-renta" name ="monto_renta" value="{{$monto_renta}}" placeholder="ESCRIBE EL MONTO DE LA RENTA DEL TITULAR">
										</div>										
									</div>
									<div class="col-lg-6">
										<div class="question">											
										<input type="text" id="co-renta"  name="co_renta" value="{{$co_renta}}"  placeholder="¿COMPLEMENTAS? ESCRIBE LA RENTA DEL CODEUDOR">
										</div>	
									</div>
									<div class="col-lg-6">
										<div class="question">
											<label for="">¿Tienes otros ingresos?</label>
											
											<select  id="otro_ingreso" name="otro_ingreso">

												<option style="font-weight:bold"  value="{{  $otro_ingreso }}">{{  $otro_ingreso }}</option>
												<option  name="otro_ingreso"  value="Diarios">Diarios</option>
												<option  name="otro_ingreso" value="Trimestrales">Trimestrales</option>
												<option  name="otro_ingreso"  value="Mensuales">Mensuales</option>												
												
											</select>
											
										</div>										
									</div>
									<div class="col-lg-6">
										<div class="question">											
										<input type="text" id="extra-monto" name="extra_monto" value="{{$extra_monto}}" placeholder="ESCRIBE EL MONTO">
										</div>
									</div>
									<div class="col-lg-12">
										<div class="question">
											<label for="">¿Tienes deudas?</label>
										<input type="text" id="monto-renta" name="monto_renta1" value="{{$monto_renta1}}" placeholder="¿cuánto pagas Mensual por créditos (consumo/hipotecário)?">
											<input type="text" id="monto-renta" name="monto_renta2" value="{{$monto_renta2}}" placeholder="¿cual es el crédito mensual de tu tarjeta de crédito??">
										</div>
									</div>
									<div class="col-lg-12 text-center">
										<h3>DATO CRÉDITO HIPOTECARIO</h3>
										<ul class="justify-content-center">
											<li>
												<h4>Tasa de interés</h4>
											</li>
											<li>
												<div id="cap-tasa" class="noUi-target noUi-ltr noUi-horizontal"></div>
											</li>
											<li>
											<input type="number" name="cap_tasa" id="input-with-keypress-7" value="{{$cap_tasa}}">
											</li>
										</ul>
										<ul class="justify-content-center">
											<li>
												<h4>Plazo</h4>
											</li>
											<li>
												<div id="cap-plazo" class="noUi-target noUi-ltr noUi-horizontal"></div>
											</li>
											<li>
											<input type="number" name="cap_plazo" id="input-with-keypress-8" value="{{$cap_plazo}}">
											</li>
										</ul>
									</div>
									<div class="col-12 text-center">
										<button type="button"  id="btsubmit">Calcular</button>
										<p class="small">*El valor del dividendo es referencial y no incluye los costsos asociados de seguros</p>
									</div>
									<div class="col-12 text-center result">
										<p>EL MONTO DEL DIVIDENDO DE TU CRÉDITO ES:</p>
									
										<div class="price">${{$respuesta3}}</div>
									
										<p class="small">*El valor del dividendo es referencial y no incluye los costos asociados de seguros</p>
									</div>
								</div>		
							<!--</form>-->
						</div>
                    </div>
               </div>
            </div>
        </div>
	</section>
	
	<!-- COTIZA -->
	<section class="contactanos contactanos--sim">
		<div class="container">
			<div class="row">
				<div class="col-12 text-center primary-title">
					<h2>Cotiza tu proyecto inmobiliario</h2>
				</div>
				<div class="col-12">
						<form action="{{route('simulador_guardar')}}" id= "form-general" class="gnrl-form" method="POST" autocomplete="off">
								{{ csrf_field() }}

							<div class="row">
							<div class="col-lg-6">
									<input type="text" name="nombre" id="nombre" placeholder="Nombre" required>
								<input type="text" name="apellido" id="apellido" placeholder="Apellido" required>
								<input type="text" name="telefono" id="telefono" placeholder="Teléfono" required>
								<input type="text" name="email" id="email" placeholder="Email" required>
							</div>
							<div class="col-lg-6">
									
									 <select name="asuntocoti_id" id="asuntocoti_id" required>
                                                <option value="">Agregar Asunto</option>
                                                @foreach(\App\Models\Admin\AsuntoCotizacion::all() as $asunto)
                                                <option value="{{$asunto->id}}" >{{$asunto->nombre}}</option>
                                            @endforeach
                                            
                                        </select>
                                    <textarea name="mensaje_cotizacion" id="mensaje_cotizacion" placeholder="Mensaje" required></textarea>
								</div>
							<div class="col-lg-12 text-center">
								<button class="btn-gnrl btn-gnrl--green">ENVIAR</button>
							</div>
						</div>                        
					</form>
				</div>                
			</div>
		</div>
	</section>
	<!-- /COTIZA -->

	<footer>
		<div class="container">
			<div class="row d-flex align-items-center">
				<div class="col-lg-3">
					<img src="images/logo-footer.png">
				</div>
				<div class="col-lg-6">
					<ul class="address">
						<li>
							<h4>Dirección</h4>
							<p>Conchrane 635 Of. 1601 Edificio Centro Plaza Concepción, Chile</p>
						</li>
						<li>
							<h4>Whatsapp:</h4>
							<p>+569 9 4545 8465</p>
						</li>
					</ul>
				</div>
				<div class="col-lg-3">
					<ul class="nav-social">
						<li><a href="#"><i class="icon-facebook"></i></a></li>
						<li><a href="#"><i class="icon-instagram"></i></a></li>
						<li><a href="#"><i class="icon-twitter"></i></a></li>
					</ul>
				</div>
			</div>
		</div>
	</footer>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="js/bootstrap.bundle.min.js"></script>
	<script src="js/lazyload.min.js"></script>
	<script src="js/nouislider.js"></script>
	<script src="js/slick.min.js"></script>
	<script src="js/lity.js"></script>
	<script src="js/main.js"></script>
	<script type="text/javascript" language="javascript">
		$(document).ready(function() {u
		    $("input:submit").click(function() {return false;});
		}


		var stepsSlider = document.getElementById('valor-propiedad');
		var input0 = document.getElementById('input-with-keypress-0');		
		var inputs = [input0];
		var precio_propiedad = $('#input-with-keypress-0').val().trim();

		noUiSlider.create(stepsSlider, {
			start: [precio_propiedad],
			connect: true,			
			range: {
				'min': [0],
				'10%': [1000],
				'50%': [3000],
				'80%': 8000,
				'max': 1000000000000
			}
		});

		stepsSlider.noUiSlider.on('update', function (values, handle) {
			inputs[handle].value = values[handle];
		});

		// Listen to keydown events on the input field.
		inputs.forEach(function (input, handle) {

		input.addEventListener('change', function () {
			stepsSlider.noUiSlider.setHandle(handle, this.value);
		});

		input.addEventListener('keydown', function (e) {

			var values = stepsSlider.noUiSlider.get();
			var value = Number(values[handle]);

			// [[handle0_down, handle0_up], [handle1_down, handle1_up]]
			var steps = stepsSlider.noUiSlider.steps();

			// [down, up]
			var step = steps[handle];

			var position;

			// 13 is enter,
			// 38 is key up,
			// 40 is key down.
			switch (e.which) {

				case 13:
					stepsSlider.noUiSlider.setHandle(handle, this.value);
					break;

				case 38:

					// Get step to go increase slider value (up)
					position = step[1];

					// false = no step is set
					if (position === false) {
						position = 1;
					}

					// null = edge of slider
					if (position !== null) {
						stepsSlider.noUiSlider.setHandle(handle, value + position);
					}

					break;

				case 40:

					position = step[0];

					if (position === false) {
						position = 1;
					}

					if (position !== null) {
						stepsSlider.noUiSlider.setHandle(handle, value - position);
					}

					break;
				}
			});
		});

		var stepsSliderPie = document.getElementById('valor-pie');
		var input1 = document.getElementById('input-with-keypress-1');		
		var inputs2 = [input1];
		var pie = $('#input-with-keypress-1').val().trim();


		noUiSlider.create(stepsSliderPie, {
			start: [pie],
			connect: true,			
			range: {
				'min': [0],
				'10%': [1000],
				'50%': [3000],
				'80%': 8000,
				'max': 10000
			}
		});

		stepsSliderPie.noUiSlider.on('update', function (values, handle) {
			inputs2[handle].value = values[handle];
		});

		// Listen to keydown events on the input field.
		inputs.forEach(function (input, handle) {

		input.addEventListener('change', function () {
			stepsSliderPie.noUiSlider.setHandle(handle, this.value);
		});

		input.addEventListener('keydown', function (e) {

			var values = stepsSliderPie.noUiSlider.get();
			var value = Number(values[handle]);

			// [[handle0_down, handle0_up], [handle1_down, handle1_up]]
			var piesteps = stepsSliderPie.noUiSlider.steps();

			// [down, up]
			var piestep = piesteps[handle];

			var position;

			// 13 is enter,
			// 38 is key up,
			// 40 is key down.
			switch (e.which) {

				case 13:
					stepsSliderPie.noUiSlider.setHandle(handle, this.value);
					break;

				case 38:

					// Get step to go increase slider value (up)
					position = piestep[1];

					// false = no step is set
					if (position === false) {
						position = 1;
					}

					// null = edge of slider
					if (position !== null) {
						stepsSliderPie.noUiSlider.setHandle(handle, value + position);
					}

					break;

				case 40:

					position = piestep[0];

					if (position === false) {
						position = 1;
					}

					if (position !== null) {
						stepsSliderPie.noUiSlider.setHandle(handle, value - position);
					}

					break;
				}
			});
		});

		var stepsSliderPie = document.getElementById('plazo');
		var input2 = document.getElementById('input-with-keypress-2');		
		var inputs3 = [input2];
		var plazo = $('#input-with-keypress-2').val().trim();



		noUiSlider.create(stepsSliderPie, {
			start: [plazo],
			connect: true,			
			range: {
				'min': [0],
				'10%': [5],
				'50%': [10],
				'80%': 15,
				'90%': 20,
				'max': 30
			}
		});

		stepsSliderPie.noUiSlider.on('update', function (values, handle) {
			inputs3[handle].value = values[handle];
		});

		// Listen to keydown events on the input field.
		inputs.forEach(function (input, handle) {

		input.addEventListener('change', function () {
			stepsSliderPie.noUiSlider.setHandle(handle, this.value);
		});

		input.addEventListener('keydown', function (e) {

			var values = stepsSliderPie.noUiSlider.get();
			var value = Number(values[handle]);

			// [[handle0_down, handle0_up], [handle1_down, handle1_up]]
			var piesteps = stepsSliderPie.noUiSlider.steps();

			// [down, up]
			var piestep = piesteps[handle];

			var position;

			// 13 is enter,
			// 38 is key up,
			// 40 is key down.
			switch (e.which) {

				case 13:
					stepsSliderPie.noUiSlider.setHandle(handle, this.value);
					break;

				case 38:

					// Get step to go increase slider value (up)
					position = piestep[1];

					// false = no step is set
					if (position === false) {
						position = 1;
					}

					// null = edge of slider
					if (position !== null) {
						stepsSliderPie.noUiSlider.setHandle(handle, value + position);
					}

					break;

				case 40:

					position = piestep[0];

					if (position === false) {
						position = 1;
					}

					if (position !== null) {
						stepsSliderPie.noUiSlider.setHandle(handle, value - position);
					}

					break;
				}
			});
		});

		var stepsSliderPie = document.getElementById('tasa');
		var input3 = document.getElementById('input-with-keypress-3');		
		var inputs4 = [input3];
		var tasa = $('#input-with-keypress-3').val().trim();


		noUiSlider.create(stepsSliderPie, {
			start: [tasa],
			connect: true,			
			range: {
				'min': [0],
				'10%': [1],
				'50%': [2],
				'80%': 3,
				'max': 4
			}
		});

		stepsSliderPie.noUiSlider.on('update', function (values, handle) {
			inputs4[handle].value = values[handle];
		});

		// Listen to keydown events on the input field.
		inputs.forEach(function (input, handle) {

		input.addEventListener('change', function () {
			stepsSliderPie.noUiSlider.setHandle(handle, this.value);
		});

		input.addEventListener('keydown', function (e) {

			var values = stepsSliderPie.noUiSlider.get();
			var value = Number(values[handle]);

			// [[handle0_down, handle0_up], [handle1_down, handle1_up]]
			var piesteps = stepsSliderPie.noUiSlider.steps();

			// [down, up]
			var piestep = piesteps[handle];

			var position;

			// 13 is enter,
			// 38 is key up,
			// 40 is key down.
			switch (e.which) {

				case 13:
					stepsSliderPie.noUiSlider.setHandle(handle, this.value);
					break;

				case 38:

					// Get step to go increase slider value (up)
					position = piestep[1];

					// false = no step is set
					if (position === false) {
						position = 1;
					}

					// null = edge of slider
					if (position !== null) {
						stepsSliderPie.noUiSlider.setHandle(handle, value + position);
					}

					break;

				case 40:

					position = piestep[0];

					if (position === false) {
						position = 1;
					}

					if (position !== null) {
						stepsSliderPie.noUiSlider.setHandle(handle, value - position);
					}

					break;
				}
			});
		});

		var stepsSliderPie = document.getElementById('monto');
		var input4 = document.getElementById('input-with-keypress-4');		
		var inputs5 = [input4];
		var monto = $('#input-with-keypress-4').val().trim();
		

		noUiSlider.create(stepsSliderPie, {
			start: [monto],
			connect: true,			
			range: {
				'min': [0],
				'10%': [1000],
				'50%': [3000],
				'80%': 5000,
				'max': 10000
			}
		});

		stepsSliderPie.noUiSlider.on('update', function (values, handle) {
			inputs5[handle].value = values[handle];
		});

		// Listen to keydown events on the input field.
		inputs.forEach(function (input, handle) {

		input.addEventListener('change', function () {
			stepsSliderPie.noUiSlider.setHandle(handle, this.value);
		});

		input.addEventListener('keydown', function (e) {

			var values = stepsSliderPie.noUiSlider.get();
			var value = Number(values[handle]);

			// [[handle0_down, handle0_up], [handle1_down, handle1_up]]
			var piesteps = stepsSliderPie.noUiSlider.steps();

			// [down, up]
			var piestep = piesteps[handle];

			var position;

			// 13 is enter,
			// 38 is key up,
			// 40 is key down.
			switch (e.which) {

				case 13:
					stepsSliderPie.noUiSlider.setHandle(handle, this.value);
					break;

				case 38:

					// Get step to go increase slider value (up)
					position = piestep[1];

					// false = no step is set
					if (position === false) {
						position = 1;
					}

					// null = edge of slider
					if (position !== null) {
						stepsSliderPie.noUiSlider.setHandle(handle, value + position);
					}

					break;

				case 40:

					position = piestep[0];

					if (position === false) {
						position = 1;
					}

					if (position !== null) {
						stepsSliderPie.noUiSlider.setHandle(handle, value - position);
					}

					break;
				}
			});
		});

		var stepsSliderPie = document.getElementById('div-plazo');
		var input5 = document.getElementById('input-with-keypress-5');		
		var inputs6 = [input5];
		var div_plazo = $('#input-with-keypress-5').val().trim();
		noUiSlider.create(stepsSliderPie, {
			start: [div_plazo],
			connect: true,			
			range: {
				'min': [0],
				'10%': [5],
				'50%': [10],
				'80%': 15,
				'max': 30
			}
		});

		stepsSliderPie.noUiSlider.on('update', function (values, handle) {
			inputs6[handle].value = values[handle];
		});

		// Listen to keydown events on the input field.
		inputs.forEach(function (input, handle) {

		input.addEventListener('change', function () {
			stepsSliderPie.noUiSlider.setHandle(handle, this.value);
		});

		input.addEventListener('keydown', function (e) {

			var values = stepsSliderPie.noUiSlider.get();
			var value = Number(values[handle]);

			// [[handle0_down, handle0_up], [handle1_down, handle1_up]]
			var piesteps = stepsSliderPie.noUiSlider.steps();

			// [down, up]
			var piestep = piesteps[handle];

			var position;

			// 13 is enter,
			// 38 is key up,
			// 40 is key down.
			switch (e.which) {

				case 13:
					stepsSliderPie.noUiSlider.setHandle(handle, this.value);
					break;

				case 38:

					// Get step to go increase slider value (up)
					position = piestep[1];

					// false = no step is set
					if (position === false) {
						position = 1;
					}

					// null = edge of slider
					if (position !== null) {
						stepsSliderPie.noUiSlider.setHandle(handle, value + position);
					}

					break;

				case 40:

					position = piestep[0];

					if (position === false) {
						position = 1;
					}

					if (position !== null) {
						stepsSliderPie.noUiSlider.setHandle(handle, value - position);
					}

					break;
				}
			});
		});

		var stepsSliderPie = document.getElementById('div-tasa');
		var input6 = document.getElementById('input-with-keypress-6');		
		var inputs7 = [input6];
		var div_tasa = $('#input-with-keypress-6').val().trim();

		noUiSlider.create(stepsSliderPie, {
			start: [div_tasa],
			connect: true,			
			range: {
				'min': [0],
				'10%': [1],
				'50%': [2],
				'80%': 3,
				'max': 4
			}
		});

		stepsSliderPie.noUiSlider.on('update', function (values, handle) {
			inputs7[handle].value = values[handle];
		});

		// Listen to keydown events on the input field.
		inputs.forEach(function (input, handle) {

		input.addEventListener('change', function () {
			stepsSliderPie.noUiSlider.setHandle(handle, this.value);
		});

		input.addEventListener('keydown', function (e) {

			var values = stepsSliderPie.noUiSlider.get();
			var value = Number(values[handle]);

			// [[handle0_down, handle0_up], [handle1_down, handle1_up]]
			var piesteps = stepsSliderPie.noUiSlider.steps();

			// [down, up]
			var piestep = piesteps[handle];

			var position;

			// 13 is enter,
			// 38 is key up,
			// 40 is key down.
			switch (e.which) {

				case 13:
					stepsSliderPie.noUiSlider.setHandle(handle, this.value);
					break;

				case 38:

					// Get step to go increase slider value (up)
					position = piestep[1];

					// false = no step is set
					if (position === false) {
						position = 1;
					}

					// null = edge of slider
					if (position !== null) {
						stepsSliderPie.noUiSlider.setHandle(handle, value + position);
					}

					break;

				case 40:

					position = piestep[0];

					if (position === false) {
						position = 1;
					}

					if (position !== null) {
						stepsSliderPie.noUiSlider.setHandle(handle, value - position);
					}

					break;
				}
			});
		});
		

		var stepsSliderPie = document.getElementById('cap-tasa');
		var input7 = document.getElementById('input-with-keypress-7');		
		var inputs8 = [input7];
		var cap_tasa = $('#input-with-keypress-7').val().trim();

		noUiSlider.create(stepsSliderPie, {
			start: [cap_tasa],
			connect: true,			
			range: {
				'min': [0],
				'10%': [1],
				'50%': [2],
				'80%': 3,
				'max': 4
			}
		});

		stepsSliderPie.noUiSlider.on('update', function (values, handle) {
			inputs8[handle].value = values[handle];
		});

		// Listen to keydown events on the input field.
		inputs.forEach(function (input, handle) {

		input.addEventListener('change', function () {
			stepsSliderPie.noUiSlider.setHandle(handle, this.value);
		});

		input.addEventListener('keydown', function (e) {

			var values = stepsSliderPie.noUiSlider.get();
			var value = Number(values[handle]);

			// [[handle0_down, handle0_up], [handle1_down, handle1_up]]
			var piesteps = stepsSliderPie.noUiSlider.steps();

			// [down, up]
			var piestep = piesteps[handle];

			var position;

			// 13 is enter,
			// 38 is key up,
			// 40 is key down.
			switch (e.which) {

				case 13:
					stepsSliderPie.noUiSlider.setHandle(handle, this.value);
					break;

				case 38:

					// Get step to go increase slider value (up)
					position = piestep[1];

					// false = no step is set
					if (position === false) {
						position = 1;
					}

					// null = edge of slider
					if (position !== null) {
						stepsSliderPie.noUiSlider.setHandle(handle, value + position);
					}

					break;

				case 40:

					position = piestep[0];

					if (position === false) {
						position = 1;
					}

					if (position !== null) {
						stepsSliderPie.noUiSlider.setHandle(handle, value - position);
					}

					break;
				}
			});
		});

		var stepsSliderPie = document.getElementById('cap-plazo');
		var input8 = document.getElementById('input-with-keypress-8');		
		var inputs9 = [input8];
		var cap_plazo = $('#input-with-keypress-8').val().trim();

		noUiSlider.create(stepsSliderPie, {
			start: [cap_plazo],
			connect: true,			
			range: {
				'min': [5],
				'10%': [10],
				'50%': [15],
				'80%': 25,
				'max': 30
			}
		});

		stepsSliderPie.noUiSlider.on('update', function (values, handle) {
			inputs9[handle].value = values[handle];
		});

		// Listen to keydown events on the input field.
		inputs.forEach(function (input, handle) {

		input.addEventListener('change', function () {
			stepsSliderPie.noUiSlider.setHandle(handle, this.value);
		});

		input.addEventListener('keydown', function (e) {

			var values = stepsSliderPie.noUiSlider.get();
			var value = Number(values[handle]);

			// [[handle0_down, handle0_up], [handle1_down, handle1_up]]
			var piesteps = stepsSliderPie.noUiSlider.steps();

			// [down, up]
			var piestep = piesteps[handle];

			var position;

			// 13 is enter,
			// 38 is key up,
			// 40 is key down.
			switch (e.which) {

				case 13:
					stepsSliderPie.noUiSlider.setHandle(handle, this.value);
					break;

				case 38:

					// Get step to go increase slider value (up)
					position = piestep[1];

					// false = no step is set
					if (position === false) {
						position = 1;
					}

					// null = edge of slider
					if (position !== null) {
						stepsSliderPie.noUiSlider.setHandle(handle, value + position);
					}

					break;

				case 40:

					position = piestep[0];

					if (position === false) {
						position = 1;
					}

					if (position !== null) {
						stepsSliderPie.noUiSlider.setHandle(handle, value - position);
					}

					break;
				}
			});
		});
		

		function calcular_dividendo(){
			event.preventDefault();
			alert('funcion');
			/*
			var parametros = $('#form_dividendo').serialize();
			var msj_error = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button><center><label style="color:#4c4c4c"><strong>Error en Enviar Datos</strong>    </label></center></div>';

			$.ajax({
		        data: parametros,
		        url: 'calcular_dividendo.php',
		        type: 'post',
		        async: false,
		        success: function(response) {
		        	response = 300;
		        	alert(response);
		            if (response){
		                $('#capa-dividendo').show();
		                $('#respuesta_dividendo').html(response);
		            }else{
		            	$('#capa-dividendo').hide();
		                $('#error').html(msj_error);
		            }
		        },
		        error: showError
		    });
		    */
		 } 

		 var btn = document.getElementById('btsubmit');

			btn.addEventListener('click',function(e){

			e.preventDefault();

			})
					
	</script>		
</body>
</html>