<!DOCTYPE html> 
<html lang="es-Es">
@include('includes.head2')
<body>
	<!-- Loading -->
	<div class="loading">
		<div class="loader"></div>
	</div>
	<!-- /Loading -->
	<header class="header-int">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-2 brand-image">
					<a href="{{ url('/home')}}">
						<img data-src="../../images/logo-madesal.png" class="d-none d-lg-block lazy" alt="Madesal"s>
						<img data-src="../../images/logo-madesal-xs.png" class="d-block d-lg-none lazy" alt="Madesal">
						</a>
				</div>
				<div class="col-lg-9 col-10">
					<nav class="navbar navbar-expand-lg">
						<div class="top">
							<ul class="d-flex">
								@include('includes.menu-superior')
							</ul>
						</div>
						<button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
							aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
							<span></span>
							<span></span>
							<span></span>
						</button>
						<div class="collapse navbar-collapse" id="navbarSupportedContent">
							<ul class="navbar-nav">
								@include('includes.menu')
							</ul>
							<div class="box-mobile d-block d-lg-none">
								<ul class="box-mobile__btns">
									<li>
										<a  href="{{ url('/contacto')}}">Ayuda</a>
									</li>
									<li>
										<a href="{{ url('/postventas')}}">Contacto</a>
									</li>
								</ul>
								<?php
								$footer = \DB::table('footers')
												->select('img_footer','direccion','telefono','link_face','link_instagram','link_twitter')
												->get();
								?>

								@foreach($footer as $footers)
								<ul class="box-mobile__social">
									<li><a target="_blanck" href="{{$footers->link_face}}"><i class="icon-facebook"></i></a></li>
									<li><a target="_blanck" href="{{$footers->link_instagram}}"><i class="icon-instagram"></i></a></li>
									<li><a target="_blanck" href="{{$footers->link_twitter}}"><i class="icon-twitter"></i></a></li>
								</ul>
								@endforeach
							</div>
						</div>
					</nav>
				</div>
			</div>
		</div>
	</header>
    <!-- HEAD PROJECT -->

    
    <section class="head-project">
    	@foreach($resultado['proyecto'] as $modelo)
        <div class="slide-images">
           @if($modelo->slider1 != NULL or $modelo->slidermovil1 != NULL)
            <div class="slide-images__item">
            	<img data-src="{{URL::asset('storage/'.$modelo->slider1)}}"  class="d-none d-lg-block lazy" alt=""/>
            	<img data-src="{{URL::asset('storage/'.$modelo->slidermovil1)}}"  class="d-block d-lg-none lazy" alt=""/>
                <!-- <img data-src="images/thumb-project-item.jpg" class="d-none d-lg-block lazy" alt="">
                <img data-src="images/thumb-project-item-xs.jpg" class="d-block d-lg-none lazy" alt=""> -->
			</div>
			  @endif
			  @if($modelo->slider2 != NULL or $modelo->slidermovil2 != NULL)
			<div class="slide-images__item">
				<img data-src="{{URL::asset('storage/'.$modelo->slider2)}}"  class="d-none d-lg-block lazy" alt=""/>
            	<img data-src="{{URL::asset('storage/'.$modelo->slidermovil2)}}"  class="d-block d-lg-none lazy" alt=""/>
				<!-- <img data-src="images/thumb-project-item.jpg" class="d-none d-lg-block lazy" alt="">
				<img data-src="images/thumb-project-item-xs.jpg" class="d-block d-lg-none lazy" alt=""> -->
			</div>
			@endif
			@if($modelo->slider3 != NULL or $modelo->slidermovil3 != NULL)
			<div class="slide-images__item">
				<img data-src="{{URL::asset('storage/'.$modelo->slider3)}}"  class="d-none d-lg-block lazy" alt=""/>
				<img data-src="{{URL::asset('storage/'.$modelo->slidermovil3)}}"  class="d-block d-lg-none lazy" alt=""/>
				<!-- <img data-src="images/thumb-project-item.jpg" class="d-none d-lg-block lazy" alt="">
				<img data-src="images/thumb-project-item-xs.jpg" class="d-block d-lg-none lazy" alt=""> -->
			</div>
			@endif
			@if($modelo->slider4 != NULL or $modelo->slidermovil4 != NULL)
			<div class="slide-images__item">
				<img data-src="{{URL::asset('storage/'.$modelo->slider4)}}"  class="d-none d-lg-block lazy" alt=""/>
				<img data-src="{{URL::asset('storage/'.$modelo->slidermovil4)}}"  class="d-block d-lg-none lazy" alt=""/>
				<!-- <img data-src="images/thumb-project-item.jpg" class="d-none d-lg-block lazy" alt="">
				<img data-src="images/thumb-project-item-xs.jpg" class="d-block d-lg-none lazy" alt=""> -->
			</div>
			@endif
			@if($modelo->slider5 != NULL or $modelo->slidermovil5 != NULL)
			<div class="slide-images__item">
				<img data-src="{{URL::asset('storage/'.$modelo->slider5)}}"  class="d-none d-lg-block lazy" alt=""/>
				<img data-src="{{URL::asset('storage/'.$modelo->slidermovil5)}}"  class="d-block d-lg-none lazy" alt=""/>
				<!-- <img data-src="images/thumb-project-item.jpg" class="d-none d-lg-block lazy" alt="">
				<img data-src="images/thumb-project-item-xs.jpg" clas="d-block d-lg-none lazy" alt=""> -->
			</div>
			@endif
		</div>
		@endforeach
			@foreach($resultado['proyecto'] as $proy)
		<div class="head-project__desc">
			<h1>{{$proy->nombre_proyecto}}</h1>
			<h3>{{\App\Models\Admin\TipoEtapa::find($proy->tipoetapa_id)->nombre_etapa}}</h3>
			<p>{!! nl2br(e($proy->descripcion_slider)) !!}</p>
			<ul class="c-bttn">
				<!--<li>
					<a href="#">RESERVAR</a>
				</li>-->
				<li>
					<a href="#cotizar-form">COTIZAR</a>
				</li>
			</ul>
		</div>
    </section>
    @endforeach
	<!-- /HEAD PROJECT -->
	@include('includes.msj')
    @include('includes.form-error')
    <!-- DESCCRIPCION MODELO -->
   <section class="desc-mod">
        <div class="container">
            <div class="row">
            	@foreach($resultado['ficha'] as $fichas)
                <div class="download">
                	@if($fichas->archivo != NULL)
					<div class="desc-mod__ficha">
						<a href="../storage/FichasProyectoPDF/{{$fichas->archivo}} " download >Descargar Ficha</a>
					</div>
					@endif
					@endforeach
                    <nav>
                        <ul id="modelos">
                        	@foreach($resultado['modelo'] as $resul)
                              <li>
                                <a href="{{route('ficha-proyecto_modelo', ['id' => $resul->id,$resul->proyecto_id ])}}" class="active">{{$resul->nombre_modelo}}</a></li>
                              @endforeach
                        </ul>
                    </nav>
                    @foreach($resultado['proyecto'] as $mod)
                    <div class="desc-mod__detalle">

                        <ul>

                            <li>
                            	<h2>{{$mod->nombre_modelo}}</h2>
                            	<div class="price_mod">
									<h3>Desde:</h3>
									<p>{{$mod->precio_desde}} UF</p>
								</div>
                            </li>
                            <li>
                                <i class="icon-casa"></i>
                                <h4>{{\App\Models\Admin\TipoInmueble::find($mod->tipoinmueble_id)->nombre_inmueble}}</h4>
                            </li>
                            <li>
                                <i class="icon-plano"></i>
                                <h4>{{$mod->mtrs}} MTS2</h4>
                            </li>
                            <li>
                                <i class="icon-dormitorio"></i>
                                <h4>{{$mod->dormitorio}} DORMITORIOS</h4>
                            </li>
                            <li>
                                <i class="icon-bano"></i>
                                <h4>{{$mod->banos}} BAÑOS</h4>
                            </li>
                            <li>
                                <i class="icon-cocina"></i>
                                <h4>{{\App\Models\Admin\TipoCocina::find($mod->tipococina_id)->nombre_cocina}}</h4>
                            </li>
                            @foreach($resultado['valores'] as $valor)
							@if($mod->estacionamiento != NULL)
                            <li>
                                <i class="icon-estacionamiento"></i>
                                <h4>{{$mod->estacionamiento}} ESTACIONAMIENTOS @if($valor->valor_estacionamiento != NULL) , {{$valor->valor_estacionamiento}} UF @endif</h4>
                            </li>
                            @endif
						@endforeach
                        </ul>

                    </div>
                    @endforeach

                   <?php

 				$planta = \DB::table('plantas')

      			->get();
                   ?>
                    <div class="form-unity">
                        <form action="">
                            <div class="image-detail">
                                <div class="image-detail__master">
                                    <h2>Master plan</h2>
                                      <div class="img-container d-block d-lg-none zoom" id="master-plan">										
										<img src="{{URL::asset('storage/'.$proy->masterplan)}}">										
									</div>
									<div class="img-container d-none d-lg-block">
										<a href="{{URL::asset('storage/'.$proy->masterplan)}}" data-lity>
											<img src="{{URL::asset('storage/'.$proy->masterplan)}}">
										</a>																														
                                    </div>
                                </div>
                                 @foreach($resultado['planta'] as $planta)

                                <div class="image-detail__plantas">
                                    <h2>Planta</h2>
                                    <nav>
                                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                        @if($planta->img_baja != NULL and strlen($planta->img_baja) > 0 )
                                            <a class="nav-item nav-link active" id="nav-baja-tab" data-toggle="tab" href="#nav-baja" role="tab"
                                                aria-controls="nav-baja" aria-selected="true">Baja</a>
                                        @endif
                                        @if( ($planta->img_alta != NULL and strlen($planta->img_alta) > 0) )
                                            <a class="nav-item nav-link" id="nav-alta-tab" data-toggle="tab" href="#nav-alta" role="tab"
                                                aria-controls="nav-alta" aria-selected="false">Alta</a>
                                        @endif
                                        @if($planta->img_3era != NULL and strlen($planta->img_3era) > 0 )
                                            <a class="nav-item nav-link" id="nav-3ra-tab" data-toggle="tab" href="#nav-3ra" role="tab"
                                                aria-controls="nav-3ra" aria-selected="false">3ra</a>
                                        @endif
                                        @if($planta->img_areaverde != NULL and strlen($planta->img_areaverde) > 0 )
                                            <a class="nav-item nav-link" id="nav-av-tab" data-toggle="tab" href="#nav-av" role="tab"
                                                aria-controls="nav-av" aria-selected="false">Áreas verdes</a>
                                        @endif
                                        </div>
                                    </nav>
                                    <div class="tab-content" id="nav-tabContent">
                                    	@if($planta->img_baja != NULL)
                                        <div class="tab-pane zoom fade show active" id="nav-baja" role="tabpanel" aria-labelledby="nav-baja-tab">
                                            <img src="{{URL::asset('storage/'.$planta->img_baja)}}" alt="">
                                        </div>
                                        @endif
                                        @if($planta->img_alta != NULL)
                                        <div class="tab-pane zoom fade" id="nav-alta" role="tabpanel" aria-labelledby="nav-alta-tab">
                                            <img src="{{URL::asset('storage/'.$planta->img_alta)}}" alt="">
                                        </div>
                                        @endif
                                        @if($planta->img_3era != NULL)
                                        <div class="tab-pane zoom fade" id="nav-3ra" role="tabpanel" aria-labelledby="nav-3ra-tab">
                                            <img src="{{URL::asset('storage/'.$planta->img_3era)}}" alt="">
                                        </div>
                                        @endif
                                        @if($planta->img_areaverde != NULL)
                                        <div class="tab-pane zoom fade" id="nav-av" role="tabpanel" aria-labelledby="nav-av-tab">
                                            <img src="{{URL::asset('storage/'.$planta->img_areaverde)}}" alt="">
                                        </div>
                                        @endif
                                    </div>
                                </div>
                                @endforeach
                            </div>


                    <!--       <div  class="select-date">
                                 <select id="id" name="id" onChange="mostrar(this.value);">
    								 <option value="0">Selecciona una unidad</option>
        								@foreach($resultado['unidad'] as $unidad)
             						 <option value="{{$unidad->id}}">{{$unidad->nombre}}</option>
       									 @endforeach
    							 </select>
   							 <div id="unidades"></div>
                            </div>-->
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /DESCCRIPCION MODELO -->

	<!-- TABS DESKTOP -->

	<section class="tabs-project">
		<nav>
			<div class="nav nav-tabs" id="nav-tab" role="tablist">
				<a class="nav-item nav-link " id="nav-gallery-tab" data-toggle="tab" href="#nav-gallery" role="tab"
					aria-controls="nav-gallery" aria-selected="true">Galería</a>
				<a class="nav-item nav-link active " id="nav-equipamiento-tab" data-toggle="tab" href="#nav-termin" role="tab"
					aria-controls="nav-termin" aria-selected="true">Terminaciones</a>
				<a class="nav-item nav-link" id="nav-equipamiento-tab" data-toggle="tab" href="#nav-equipamiento" role="tab"
					aria-controls="nav-equipamiento" aria-selected="false">Equipamiento</a>
				<a class="nav-item nav-link" id="nav-ubicacion-tab" data-toggle="tab" href="#nav-ubicacion" role="tab"

					aria-controls="ubicacion" aria-selected="false">Ubicacion y entorno</a>	
					 @if($modelo->tour360 != NULL and strlen($modelo->tour360) > 0 and $modelo->tour360 != NULL)
					<a class="nav-item nav-link" id="nav-360-tab" data-toggle="tab" href="#nav-360" role="tab"
				aria-controls="nav-360" aria-selected="false">Vista 360º</a>	
				@endif	

			</div>
		</nav>

		<div class="tab-content" id="nav-tabContent">
			<div class="tab-pane fade" id="nav-gallery" role="tabpanel" aria-labelledby="nav-gallery-tab">
				@foreach($resultado['proyecto'] as $fichas)
				<div class="slide-spot">
					<div class="slide-spot__item">
						@if($fichas->gimg1 != NULL)
						<img src="{{URL::asset('storage/'.$fichas->gimg1)}}" alt="">
						@endif
					</div>
					@if($fichas->gimg2 != NULL)
					<div class="slide-spot__item">
						<img src="{{URL::asset('storage/'.$fichas->gimg2)}}" alt="">
					</div>
					@endif
					@if($fichas->gimg3 != NULL)
					<div class="slide-spot__item">
						<img src="{{URL::asset('storage/'.$fichas->gimg3)}}" alt="">
					</div>
					@endif
					@if($fichas->gimg4 != NULL)
					<div class="slide-spot__item">
						<img src="{{URL::asset('storage/'.$fichas->gimg4)}}" alt="">
					</div>
					@endif
					@if($fichas->gimg5 != NULL)
					<div class="slide-spot__item">
						<img src="{{URL::asset('storage/'.$fichas->gimg5)}}" alt="">
					</div>
					@endif
					@if($fichas->gimg6 != NULL)
					<div class="slide-spot__item">
						<img src="{{URL::asset('storage/'.$fichas->gimg6)}}" alt="">
					</div>
					@endif
					@if($fichas->gimg7 != NULL)
					<div class="slide-spot__item">
						<img src="{{URL::asset('storage/'.$fichas->gimg7)}}" alt="">
					</div>
					@endif
					@if($fichas->gimg8 != NULL)
					<div class="slide-spot__item">
						<img src="{{URL::asset('storage/'.$fichas->gimg8)}}" alt="">
					</div>
					@endif
					@if($fichas->gimg9 != NULL)
					<div class="slide-spot__item">
						<img src="{{URL::asset('storage/'.$fichas->gimg9)}}" alt="">
					</div>
					@endif
					@if($fichas->gimg10 != NULL)
					<div class="slide-spot__item">
						<img src="{{URL::asset('storage/'.$fichas->gimg10)}}" alt="">
					</div>
					@endif
					@endforeach
				</div>
				@foreach($resultado['proyecto'] as $fichas)
				<div class="slide-spot--nav">
					@if($fichas->gimg1 != NULL)
					<div class="slide-spot--nav__item">
						<img src="{{URL::asset('storage/'.$fichas->gimg1)}}" alt="">
					</div>
					@endif
					@if($fichas->gimg2 != NULL)
					<div class="slide-spot--nav__item">
						<img src="{{URL::asset('storage/'.$fichas->gimg2)}}" alt="">
					</div>
					@endif
					@if($fichas->gimg3 != NULL)
					<div class="slide-spot--nav__item">
						<img src="{{URL::asset('storage/'.$fichas->gimg3)}}" alt="">
					</div>
					@endif
					@if($fichas->gimg4 != NULL)
					<div class="slide-spot--nav__item">
						<img src="{{URL::asset('storage/'.$fichas->gimg4)}}" alt="">
					</div>
					@endif
					@if($fichas->gimg5 != NULL)
					<div class="slide-spot--nav__item">
						<img src="{{URL::asset('storage/'.$fichas->gimg5)}}" alt="">
					</div>
					@endif
					@if($fichas->gimg6 != NULL)
					<div class="slide-spot--nav__item">
						<img src="{{URL::asset('storage/'.$fichas->gimg6)}}" alt="">
					</div>
					@endif
					@if($fichas->gimg7 != NULL)
					<div class="slide-spot--nav__item">
						<img src="{{URL::asset('storage/'.$fichas->gimg7)}}" alt="">
					</div>
					@endif
					@if($fichas->gimg8 != NULL)
					<div class="slide-spot--nav__item">
						<img src="{{URL::asset('storage/'.$fichas->gimg8)}}" alt="">
					</div>
					@endif
					@if($fichas->gimg9 != NULL)
					<div class="slide-spot--nav__item">
						<img src="{{URL::asset('storage/'.$fichas->gimg9)}}" alt="">
					</div>
					@endif
					@if($fichas->gimg10 != NULL)
					<div class="slide-spot--nav__item">
						<img src="{{URL::asset('storage/'.$fichas->gimg10)}}" alt="">
					</div>
					@endif
				</div>
				@endforeach
				<div class="tab-pane__box">
					<h2>Galeria de Fotos</h2>
					@foreach($resultado['proyecto'] as $modelo)
					<p>{{$modelo->galeria}}</p>
					@endforeach
				</div>
			</div>
			<div class="tab-pane fade show active" id="nav-termin" role="tabpanel" aria-labelledby="nav-termin-tab">
				<div class="slide-termin">
					@foreach($resultado['proyecto'] as $fichas)
					<div class="slide-termin__item">
						@if($fichas->timg1 != NULL)
						<img src="{{URL::asset('storage/'.$fichas->timg1)}}" alt="">
						@if($fichas->vt1 != "null")
						<a href="{{$fichas->vt1}}" data-lity>
							<i class="icon-play"></i>
						</a>
						@endif
					</div>
					@endif
					@if($fichas->timg2 != NULL)
					<div class="slide-termin__item">
						<img src="{{URL::asset('storage/'.$fichas->timg2)}}" alt="">
					</div>
					@endif
					@if($fichas->timg3 != NULL)
					<div class="slide-termin__item">
						<img src="{{URL::asset('storage/'.$fichas->timg3)}}" alt="">
					</div>
					@endif
					@if($fichas->timg4 != NULL)
					<div class="slide-termin__item">
						<img src="{{URL::asset('storage/'.$fichas->timg4)}}" alt="">
					</div>
					@endif
					@if($fichas->timg5 != NULL)
					<div class="slide-termin__item">
						<img src="{{URL::asset('storage/'.$fichas->timg5)}}" alt="">
					</div>
					@endif
					@if($fichas->timg6 != NULL)
					<div class="slide-termin__item">
						<img src="{{URL::asset('storage/'.$fichas->timg6)}}" alt="">
					</div>
					@endif
					@if($fichas->timg7 != NULL)
					<div class="slide-termin__item">
						<img src="{{URL::asset('storage/'.$fichas->timg7)}}" alt="">
					</div>
					@endif
					@if($fichas->timg8 != NULL)
					<div class="slide-termin__item">
						<img src="{{URL::asset('storage/'.$fichas->timg8)}}" alt="">
					</div>
					@endif
					@if($fichas->timg9 != NULL)
					<div class="slide-termin__item">
						<img src="{{URL::asset('storage/'.$fichas->timg9)}}" alt="">
					</div>
					@endif
					@if($fichas->timg10 != NULL)
					<div class="slide-termin__item">
						<img src="{{URL::asset('storage/'.$fichas->timg10)}}" alt="">
					</div>
					@endif
					@endforeach
				</div>
				@foreach($resultado['proyecto'] as $fichas)
				<div class="slide-termin--nav">
					@if($fichas->timg1 != NULL)
					<div class="slide-termin--nav__item">
						<img src="{{URL::asset('storage/'.$fichas->timg1)}}" alt="">
					</div>
					@endif
					@if($fichas->timg2 != NULL)
					<div class="slide-termin--nav__item">
						<img src="{{URL::asset('storage/'.$fichas->timg2)}}" alt="">
					</div>
					@endif
					@if($fichas->timg3 != NULL)
					<div class="slide-termin--nav__item">
						<img src="{{URL::asset('storage/'.$fichas->timg3)}}" alt="">
					</div>
					@endif
					@if($fichas->timg4 != NULL)
					<div class="slide-termin--nav__item">
						<img src="{{URL::asset('storage/'.$fichas->timg4)}}" alt="">
					</div>
					@endif
					@if($fichas->timg5 != NULL)
					<div class="slide-termin--nav__item">
						<img src="{{URL::asset('storage/'.$fichas->timg5)}}" alt="">
					</div>
					@endif
					@if($fichas->timg6 != NULL)
					<div class="slide-termin--nav__item">
						<img src="{{URL::asset('storage/'.$fichas->timg6)}}" alt="">
					</div>
					@endif
					@if($fichas->timg7 != NULL)
					<div class="slide-termin--nav__item">
						<img src="{{URL::asset('storage/'.$fichas->timg7)}}" alt="">
					</div>
					@endif
					@if($fichas->timg8 != NULL)
					<div class="slide-termin--nav__item">
						<img src="{{URL::asset('storage/'.$fichas->timg8)}}" alt="">
					</div>
					@endif
					@if($fichas->timg9 != NULL)
					<div class="slide-termin--nav__item">
						<img src="{{URL::asset('storage/'.$fichas->timg9)}}" alt="">
					</div>
					@endif
					@if($fichas->timg10 != NULL)
					<div class="slide-termin--nav__item">
						<img src="{{URL::asset('storage/'.$fichas->timg10)}}" alt="">
					</div>
					@endif
				</div>
				@endforeach
				

				<div class="tab-pane__box">
					<h2>Terminaciones</h2>
					@foreach($resultado['proyecto'] as $modelo)
					<p>{{$modelo->terminaciones}}</p>
					@endforeach
				</div>
			</div>	
			<div class="tab-pane fade" id="nav-equipamiento" role="tabpanel" aria-labelledby="nav-equipamiento-tab">
				@foreach($resultado['proyecto'] as $fichas)
				<div class="slide-equip">
					<div class="slide-equip__item">
						@if($fichas->eimg1 != NULL)
						<img src="{{URL::asset('storage/'.$fichas->eimg1)}}" alt="">
						@if($fichas->vt1 != "null")
						<a href="{{$fichas->ve1}}" data-lity>
							<i class="icon-play"></i>
						</a>
						@endif
					</div>
					@endif
					@if($fichas->eimg2 != NULL)
					<div class="slide-equip__item">
						<img src="{{URL::asset('storage/'.$fichas->eimg2)}}" alt="">
					</div>
					@endif
					@if($fichas->eimg3 != NULL)
					<div class="slide-equip__item">
						<img src="{{URL::asset('storage/'.$fichas->eimg3)}}" alt="">
					</div>
					@endif
					@if($fichas->eimg4 != NULL)
					<div class="slide-equip__item">
						<img src="{{URL::asset('storage/'.$fichas->eimg4)}}" alt="">
					</div>
					@endif
					@if($fichas->eimg5 != NULL)
					<div class="slide-equip__item">
						<img src="{{URL::asset('storage/'.$fichas->eimg5)}}" alt="">
					</div>
					@endif
					@if($fichas->eimg6 != NULL)
					<div class="slide-equip__item">
						<img src="{{URL::asset('storage/'.$fichas->eimg6)}}" alt="">
					</div>
					@endif
					@if($fichas->eimg7 != NULL)
					<div class="slide-equip__item">
						<img src="{{URL::asset('storage/'.$fichas->eimg7)}}" alt="">
					</div>
					@endif
					@if($fichas->eimg8 != NULL)
					<div class="slide-equip__item">
						<img src="{{URL::asset('storage/'.$fichas->eimg8)}}" alt="">
					</div>
					@endif
					@if($fichas->eimg9 != NULL)
					<div class="slide-equip__item">
						<img src="{{URL::asset('storage/'.$fichas->eimg9)}}" alt="">
					</div>
					@endif
					@if($fichas->eimg10 != NULL)
					<div class="slide-equip__item">
						<img src="{{URL::asset('storage/'.$fichas->eimg10)}}" alt="">
					</div>
					@endif
					@endforeach
				</div>
				@foreach($resultado['proyecto'] as $fichas)
				<div class="slide-equip--nav">
					@if($fichas->eimg1 != NULL)
					<div class="slide-equip--nav__item">
						<img src="{{URL::asset('storage/'.$fichas->eimg1)}}" alt="">
						
					</div>
					@endif
					@if($fichas->eimg2 != NULL)
					<div class="slide-equip--nav__item">
						<img src="{{URL::asset('storage/'.$fichas->eimg2)}}" alt="">
						
					</div>
					@endif
					@if($fichas->eimg3 != NULL)
					<div class="slide-equip--nav__item">
						<img src="{{URL::asset('storage/'.$fichas->eimg3)}}" alt="">
						
					</div>
					@endif
					@if($fichas->eimg4 != NULL)
					<div class="slide-equip--nav__item">
						<img src="{{URL::asset('storage/'.$fichas->eimg4)}}" alt="">
					</div>
					@endif
					@if($fichas->eimg5 != NULL)
					<div class="slide-equip--nav__item">
						<img src="{{URL::asset('storage/'.$fichas->eimg5)}}" alt="">
					</div>
					@endif
					@if($fichas->eimg6 != NULL)
					<div class="slide-equip--nav__item">
						<img src="{{URL::asset('storage/'.$fichas->eimg6)}}" alt="">
						
					</div>
					@endif
					@if($fichas->eimg7 != NULL)
					<div class="slide-equip--nav__item">
						<img src="{{URL::asset('storage/'.$fichas->eimg7)}}" alt="">
						
					</div>
					@endif
					@if($fichas->eimg8 != NULL)
					<div class="slide-equip--nav__item">
						<img src="{{URL::asset('storage/'.$fichas->eimg8)}}" alt="">
					</div>
					@endif
					@if($fichas->eimg9 != NULL)
					<div class="slide-equip--nav__item">
						<img src="{{URL::asset('storage/'.$fichas->eimg9)}}" alt="">
					</div>
					@endif
					@if($fichas->eimg10 != NULL)
					<div class="slide-equip--nav__item">
						<img src="{{URL::asset('storage/'.$fichas->eimg10)}}" alt="">
					</div>
					@endif
				</div>
				@endforeach
				<div class="tab-pane__box">
				<h2>Equipamiento</h2>
					@foreach($resultado['proyecto'] as $modelo)
					<p>{{$modelo->equipamiento}}</p>
					@endforeach
				</div>
			</div>
			<div class="tab-pane fade" id="nav-ubicacion" role="tabpanel" aria-labelledby="nav-ubicacion-tab">
				@foreach($resultado['proyecto'] as $fichas)
				<div class="slide-spot">
					<div class="slide-spot__item">
						@if($fichas->uimg1 != NULL)
						<img src="{{URL::asset('storage/'.$fichas->uimg1)}}" alt="">
						@if($fichas->vu1 != "null")
						<a href="{{$fichas->vu1}}" data-lity>
							<i class="icon-play"></i>
						</a>
						@endif
					</div>

					@endif
					@if($fichas->uimg2 != NULL)
					<div class="slide-spot__item">
						<img src="{{URL::asset('storage/'.$fichas->uimg2)}}" alt="">
					</div>
					@endif
					@if($fichas->uimg3 != NULL)
					<div class="slide-spot__item">
						<img src="{{URL::asset('storage/'.$fichas->uimg3)}}" alt="">
					</div>
					@endif
					@if($fichas->uimg4 != NULL)
					<div class="slide-spot__item">
						<img src="{{URL::asset('storage/'.$fichas->uimg4)}}" alt="">
					</div>
					@endif
					@if($fichas->uimg5 != NULL)
					<div class="slide-spot__item">
						<img src="{{URL::asset('storage/'.$fichas->uimg5)}}" alt="">
					</div>
					@endif
					@if($fichas->uimg6 != NULL)
					<div class="slide-spot__item">
						<img src="{{URL::asset('storage/'.$fichas->uimg6)}}" alt="">
					</div>
					@endif
					@if($fichas->uimg7 != NULL)
					<div class="slide-spot__item">
						<img src="{{URL::asset('storage/'.$fichas->uimg7)}}" alt="">
					</div>
					@endif
					@if($fichas->uimg8 != NULL)
					<div class="slide-spot__item">
						<img src="{{URL::asset('storage/'.$fichas->uimg8)}}" alt="">
					</div>
					@endif
					@if($fichas->uimg9 != NULL)
					<div class="slide-spot__item">
						<img src="{{URL::asset('storage/'.$fichas->uimg9)}}" alt="">
					</div>
					@endif
					@if($fichas->uimg10 != NULL)
					<div class="slide-spot__item">
						<img src="{{URL::asset('storage/'.$fichas->uimg10)}}" alt="">
					</div>
					@endif
					@endforeach
				</div>
				@foreach($resultado['proyecto'] as $fichas)
				<div class="slide-spot--nav">
					@if($fichas->uimg1 != NULL)
					<div class="slide-spot--nav__item">
						<img src="{{URL::asset('storage/'.$fichas->uimg1)}}" alt="">
					</div>
					@endif
					@if($fichas->uimg2 != NULL)
					<div class="slide-spot--nav__item">
						<img src="{{URL::asset('storage/'.$fichas->uimg2)}}" alt="">
					</div>
					@endif
					@if($fichas->uimg3 != NULL)
					<div class="slide-spot--nav__item">
						<img src="{{URL::asset('storage/'.$fichas->uimg3)}}" alt="">
					</div>
					@endif
					@if($fichas->uimg4 != NULL)
					<div class="slide-spot--nav__item">
						<img src="{{URL::asset('storage/'.$fichas->uimg4)}}" alt="">
					</div>
					@endif
					@if($fichas->uimg5 != NULL)
					<div class="slide-spot--nav__item">
						<img src="{{URL::asset('storage/'.$fichas->uimg5)}}" alt="">
					</div>
					@endif
					@if($fichas->uimg6 != NULL)
					<div class="slide-spot--nav__item">
						<img src="{{URL::asset('storage/'.$fichas->uimg6)}}" alt="">
					</div>
					@endif
					@if($fichas->uimg7 != NULL)
					<div class="slide-spot--nav__item">
						<img src="{{URL::asset('storage/'.$fichas->uimg7)}}" alt="">
					</div>
					@endif
					@if($fichas->uimg8 != NULL)
					<div class="slide-spot--nav__item">
						<img src="{{URL::asset('storage/'.$fichas->uimg8)}}" alt="">
					</div>
					@endif
					@if($fichas->uimg9 != NULL)
					<div class="slide-spot--nav__item">
						<img src="{{URL::asset('storage/'.$fichas->uimg9)}}" alt="">
					</div>
					@endif
					@if($fichas->uimg10 != NULL)
					<div class="slide-spot--nav__item">
						<img src="{{URL::asset('storage/'.$fichas->uimg10)}}" alt="">
					</div>
					@endif
				</div>
				@endforeach
				@foreach($resultado['proyecto'] as $modelo)
				<div class="tab-pane__box">
					<h2>Ubicacion y Entorno</h2>
					<p>{{$modelo->ubicacion}}</p>
				</div>
			</div>
			@if($modelo->tour360 != NULL)
			<div class="tab-pane fade" id="nav-360" role="tabpanel" aria-labelledby="nav-360-tab">
				<iframe src="{{$modelo->tour360}}" style="overflow:hidden;height:500px;width:100%" height="400px" width="100%"></iframe>
			</div>
			@endif
			@endforeach
		</div>
	
		
	</section>

	<!-- /TABS DESKTOP -->

	<!-- ACCORDEON MOBILE -->

	<!-- /ACCORDEON MOBILE -->

	<!-- BANNER INVERSIONISTAS -->
	<div class="container">
		<div class="row">
			@foreach($resultado['inversionista'] as $inversionistas)
			<div class="col-lg-12 d-none d-md-block banner-full text-center">
				<a href="{{ url('/financiamiento') }}">
				<img src="{{URL::asset('storage/'.$inversionistas->img3)}}" alt="">
			</a>
			</div>

			<div class = "col-lg-12 d-block d-md-none banner-full text-center ">
				<a href="{{ url('/financiamiento') }}">
				<img src ="{{URL::asset('storage/'.$inversionistas->imgmovil3)}}"alt =" ">
				</a>
 			</div>
			@endforeach
		</div>
	</div>
	<!-- /BANNER INVERSIONISTAS -->

	<!-- CONTACT PROJECT -->
	<section class="contact-project">
		<div class="container">

			<div class="row">
				<div class="col-lg-6">
					<div class="contact-project__form" id="cotizar-form">
							    <form action="{{route('inversionista_guardar')}}" id= "form-general" class="gnrl-form" method="POST" autocomplete="off">
										{{ csrf_field() }}

								<h2>Cotiza este proyecto</h2>
								<input type="text" name="nombre_cotizacion" id="nombre_cotizacion" placeholder="Nombre" required>
								<input type="text" name="apellido_cotizacion" id="apellido_cotizacion" placeholder="Apellido"required>
								<input type="text" name="telefono_cotizacion" id="telefono_cotizacion" placeholder="Teléfono"required>
								<input type="text" name="email_cotizacion" id="email_cotizacion" placeholder="Email" required>
								 <select name="asunto_id" id="asunto_id" required>
                                        <option value="">Agregar Asunto</option>
                                        @foreach(\App\Models\Admin\Asunto::all() as $asunto)
                                        <option value="{{$asunto->id}}" required>{{$asunto->nombre_asunto}}</option>
                                    @endforeach
                                            
                                </select>
								<textarea name="mensaje_cotizacion" id="mensaje_cotizacion" placeholder="Mensaje" required></textarea>
								<button type="submit" class="btn-send"  >ENVIAR</button>
							</form>
						</div>
				</div>



					@foreach($resultado['saladeventas'] as $saladeventa)
						<div class="col-lg-6">
						<div class="contact-project__dates">
						<!--	<div class="label">
								Cómo llegar
							</div> -->
							<div class="dates-text">
								<h2>Sala de ventas: {{$saladeventa->nombre}}</h2>
								<br>
								<p>Dirección: {{$saladeventa->direccion}} </p>
								<p>Teléfono: {{$saladeventa->telefono}}</p>

								@if($saladeventa->telefono2 !=NULL)
								<p>Celular: {{$saladeventa->telefono2}}</p>
								@endif

								<p>Mail: {{$saladeventa->mail}}</p>
								@if($saladeventa->mail2 !=NULL)
									<p>Mail 2: {{$saladeventa->mail2}}</p>
								@endif

								<p>Horario de Atención: {{$saladeventa->horario}}</p>
							</div>

								@if($saladeventa->mapa!=NULL)
								<div  style="width: 100%"><iframe width="100%" height="250" src="{{$saladeventa->mapa}}" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"><a href="https://www.maps.ie/coordinates.html">gps coordinates finder</a></iframe></div>
								@else
								<div  style="width: 100%"><iframe width="100%" height="250" src="https://maps.google.com/maps?width=100%&amp;height=600&amp;hl=en&amp;coord=-36.8294688,-73.0489664&amp;q=Cochcrane%20635%20concepci%C3%B3n+(Madesal%20Concepci%C3%B3n)&amp;ie=UTF8&amp;t=&amp;z=19&amp;iwloc=B&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"><a href="https://www.maps.ie/coordinates.html">gps coordinates finder</a></iframe></div>

								@endif



						</div>
					</div>
					@endforeach
				<div class="col-12 ejecutive-sale">
					@foreach($resultado['empleado'] as $empleados)
					<article class="ejecutive-sale__box">

						<figure>
							<div class="img-container">
								<img src="{{URL::asset('storage/'.$empleados->img)}}">
							</div>
							<figcaption>
								<div class="text">
									<h2>{{$empleados->nombre_empleado}}</h2>
									<h3>{{\App\Models\Admin\Cargo::find($empleados->cargo_id)->nombre}}</h3>
								</div>
								<div class="dates">
									<p>{{$empleados->telefono}}</p>
									<p>{{$empleados->correo}}</p>
								</div>
							</figcaption>
						</figure>

					</article>
					@endforeach
				</div>
			</div>
		</div>
	</section>
	<!-- /CONTACT PROJECT -->

	@include('includes.footer')

	<script src="../../js/jquery.min.js"></script>
	<script src="../../js/bootstrap.bundle.min.js"></script>
	<script src="../../js/lazyload.min.js"></script>
	<script src="../../js/nouislider.js"></script>
	<script src="../../js/lity.js"></script>
	<script src="../../js/slick.min.js"></script>
	<script src="../../js/jquery.zoom.min.js"></script>
	<script src="../../js/main.js"></script>
	<script type="text/javascript">
	function mostrar(id) {
		$('#unidades').html('');
	    $.ajax({
	                url: 'index2.php',
	                dataType: 'text',
	                type: 'post',
	                contentType: 'application/x-www-form-urlencoded',
	                data:{id:id},
	                success: function( data){
	                    $('#unidades').html( data );
	                }

	            });
	}
	</script>
	@include('includes.chat')
</body>
</html>
