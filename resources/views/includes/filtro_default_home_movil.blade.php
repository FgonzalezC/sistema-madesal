        <form name="form-mobile-home" id="form-mobile-home">
            <div id="menu-mobile-home" class="main-head-xs__slide">
                <div class="slide-item">
                    <img src="{{URL::asset('storage/'.$sechome->movilimg1)}}" alt="">
                    <div class="dropdown">
                        <h3>Dónde quieres buscar?</h3>
                        <button class="btn dropdown-toggle" type="button" id="dropdownComuna" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                            Comuna
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownComuna">
                        @foreach($comunas as $comuna)
                            <input type="hidden" name="exc-{{ $comuna->nombre_comuna  }}" id="exc-{{ $comuna->nombre_comuna  }}" value ="0">
                            <input type="checkbox" name="xs-comuna_{{$comuna->id}}" id="xs-comuna_{{$comuna->id}}" data-id="{{$comuna->id}}" data-nombre="{{$comuna->nombre_comuna}}">
                            <label for="xs-comuna_{{$comuna->id}}">{{$comuna->nombre_comuna}}</label>
                        @endforeach
                        </div>
                    </div>
                </div>
                <div class="slide-item">
                    <img src="{{URL::asset('storage/'.$sechome->movilimg2)}}" alt="">
                    <div class="dropdown">
                        <h3>Qué tipo de propiedad buscas?</h3>
                        <button class="btn dropdown-toggle" type="button" id="dropdownTipo" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">
                            Tipo de Proyecto
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownTipo">
                        @foreach($tipos_de_inmuebles as $inmueble)
                            <input type="hidden" name="exc-{{ $inmueble->nombre_inmueble }}" id="exc-{{ $inmueble->nombre_inmueble }}" value ="0">
                            <input type="checkbox" name="xs-type_{{$inmueble->id}}" id="xs-type_{{$inmueble->id}}" data-id="{{$inmueble->id}}" data-nombre="{{$inmueble->nombre_inmueble}}">
                            <label for="xs-type_{{$inmueble->id}}">{{$inmueble->nombre_inmueble}}</label>
                        @endforeach
                        </div>
                    </div>
                </div>
                <div class="slide-item">
                    <img src="{{URL::asset('storage/'.$sechome->movilimg3)}}" alt="">
                    <div class="dropdown">
                        <h3>Cuál es tu rango de precio?</h3>
                        <button class="btn dropdown-toggle" type="button" id="dropdownRange" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">
                            Rango
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownRange">
                            <div id="slider-xs" class="noUi-target noUi-ltr noUi-horizontal"></div>
                            <input type="hidden" name="margin_slider" id="margin_slider">
                            <span class="example-val" id="slider-margin-value-min"></span>
                            <span class="example-val" id="slider-margin-value-max"></span>
                            <input type="hidden" name="MinSlider" id="MinSlider">
                            <input type="hidden" name="MaxSlider" id="MaxSlider">
                            <button  type="submit" id="btn_descubrir_movil">Descubrir</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
