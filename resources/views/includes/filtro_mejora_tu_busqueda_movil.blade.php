                <form name="form-mobile" id="form-mobile">
                    <div id="menu-mobile" class="dropdown-menu" aria-labelledby="dropdownSearch">
                        <div class="spot">
                            <input type="hidden" name="exc-comuna2" id="exc-comuna2" value ="0">
                            <select name="comuna2" id="comuna2">
                                <option value="0">Selecciona lugar</option>
                                @foreach($comunas as $comuna)
                                <option value="{{$comuna->id}}">{{ $comuna->nombre_comuna  }}</option>
                                @endforeach
                            </select>
                        </div>
                        <ul>
                            <li>
                                <div class="label-date">
                                    <i class="icon-casa"></i>
                                    <span>Tipo Inmueble</span>
                                </div>
                                <input type="hidden" name="exc-inmueble2" id="exc-inmueble2" value ="0">
                                <select name="inmueble2" id="inmueble2">
                                    <option value="0">Selecciona inmueble</option>
                                    @foreach($tipos_de_inmuebles as $inmueble)
                                    <option value="{{$inmueble->id}}">{{ $inmueble->nombre_inmueble }}</option>
                                    @endforeach
                                </select>
                            </li>
                            <li>
                                <div class="label-date">
                                    <i class="icon-dormitorio"></i>
                                <span>DORMITORIO</span>
                                </div>
                                <input type="hidden" name="exc-dormitorio2" id="exc-dormitorio2" value ="0">
                                <select name="dormitorio2" id="dormitorio2">
                                    <option value="0">Selecciona dormi</option>
                                    <?php
                                        $query = 'SELECT DISTINCT(dormitorios) FROM proyectos ORDER BY dormitorios ASC';
                                        $resul = mysqli_query($conexion,$query);
                                        while($data = mysqli_fetch_assoc($resul)) {
                                            echo '<option value="'.$data['dormitorios'].'">'.$data['dormitorios'].'</option>';
                                        }
                                    ?>
                                </select>
                            </li>
                            <li>
                                <div class="label-date">
                                    <i class="icon-bano"></i>
                                    <span>BAÑOS</span>
                                </div>
                                <input type="hidden" name="exc-bano2" id="exc-bano2" value ="0">
                                <select name="bano2" id="bano2">
                                    <option value="0">Selecciona baño</option>
                                <?php
                                    $query = 'SELECT DISTINCT(banos) FROM proyectos ORDER BY banos ASC';
                                    $resul = mysqli_query($conexion,$query);
                                    while($data = mysqli_fetch_assoc($resul)) {
                                        echo '<option value="'.$data['banos'].'">'.$data['banos'].'</option>';
                                    }
                                ?>
                                </select>
                            </li>
                            <li>
                                <div class="label-date">
                                    <i class="icon-estacionamiento"></i>
                                    <span>ESTACIONAMIENTOS</span>
                                </div>
                                <input type="hidden" name="exc-estacionamiento2" id="exc-estacionamiento2" value ="0">
                                <select name="estacionamiento2" id="estacionamiento2">
                                    <option value="0">Selecciona estacio</option>
                                    <?php
                                        $query = 'SELECT DISTINCT(estacionamiento) FROM proyectos ORDER BY estacionamiento ASC';
                                        $resul = mysqli_query($conexion,$query);
                                        while($data = mysqli_fetch_assoc($resul)) {
                                            echo '<option value="'.$data['estacionamiento'].'">'.$data['estacionamiento'].'</option>';
                                        }
                                    ?>
                                </select>
                            </li>
                            <li>
                                <div class="label-date">
                                    <i class="icon-plano"></i>
                                    <span>ESTADO DEL PROYECTO</span>
                                </div>
                                <input type="hidden" name="exc-estado2" id="exc-estado2" value ="0">
                                <select name="estado2" id="estado2">
                                    <option value="0">Selecciona estado</option>
                                    <?php
                                        $qp = 'SELECT * FROM estado_proyectos';
                                        $rp = mysqli_query($conexion,$qp);
                                        while($dp = mysqli_fetch_assoc($rp)) {
                                            echo '<option value="'.$dp['id'].'">'.$dp['nombre_estado'].'</option>';
                                        }
                                    ?>
                                </select>
                            </li>
                            <li>
                                <div class="label-date">
                                    <i class="icon-valor"></i>
                                    <span>PROYECTO CON SUBSIDIO</span>
                                </div>
                                <input type="hidden" name="exc-subsidio2" id="exc-subsidio2" value ="0">
                                <select name="subsidio2" id="subsidio2">
                                    <option value="0">Selecciona subsidio</option>
                                    <?php
                                        $qs = 'SELECT * FROM subsidios';
                                        $rs = mysqli_query($conexion,$qs);
                                        while($ds = mysqli_fetch_assoc($rs)) {
                                            echo '<option value="'.$ds['id'].'">'.$ds['nombre_subsidio'].'</option>';
                                        }
                                    ?>
                                </select>
                            </li>
                            <li>
                            <div id="slider-xs-avanzado" class="noUi-target noUi-ltr noUi-horizontal"></div>

                                <input type="hidden" name="margin_slider_avanzado" id="margin_slider_avanzado">
                                <span class="example-val" id="slider-margin-value-min"></span>
                                <span class="example-val" id="slider-margin-value-max"></span>
                                <input type="hidden" name="MinSlider" id="MinSlider">
                                <input type="hidden" name="MaxSlider" id="MaxSlider">
                            </li>
                            <li>
                                <button  id="btn_mejora_tu_busqueda">Buscar</button>
                            </li>
                        </ul>
                    </div>
                </form>
