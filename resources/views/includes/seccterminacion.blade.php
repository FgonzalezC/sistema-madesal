<?php 
$footer = \DB::table('proyectos')
				
				->get();
?>

<section class="tabs-project">
		<nav>
			<div class="nav nav-tabs" id="nav-tab" role="tablist">
				<a class="nav-item nav-link active" id="nav-termin-tab" data-toggle="tab" href="#nav-termin" role="tab"
					aria-controls="nav-home" aria-selected="true">Terminaciones</a>
				<a class="nav-item nav-link" id="nav-equipamiento-tab" data-toggle="tab" href="#nav-equipamiento" role="tab"
					aria-controls="nav-profile" aria-selected="false">Equipamiento</a>
				<a class="nav-item nav-link" id="nav-ubicacion-tab" data-toggle="tab" href="#nav-ubicacion" role="tab"
					aria-controls="nav-contact" aria-selected="false">Ubicacion y entorno</a>
			</div>
		</nav>
	
		<div class="tab-content" id="nav-tabContent">
			<div class="tab-pane fade show active" id="nav-termin" role="tabpanel" aria-labelledby="nav-termin-tab">
				<div class="slide-termin">
					@foreach($resultado['proyecto'] as $fichas)
					<div class="slide-termin__item">
						<img src="{{URL::asset('storage/'.$fichas->timg1)}}" alt="">
						<a href="{{$fichas->vt1}}" data-lity>
							<i class="icon-play"></i>
						</a>
					</div>
					@if($fichas->timg1 != "null")					
					<div class="slide-termin__item">
						<img src="{{URL::asset('storage/'.$fichas->timg1)}}" alt="">
					</div>
					@endif
					@if($fichas->timg2 != "null")	
					<div class="slide-termin__item">
						<img src="{{URL::asset('storage/'.$fichas->timg2)}}" alt="">
					</div>
					@endif
					@if($fichas->timg3 != "null")	
					<div class="slide-termin__item">
						<img src="{{URL::asset('storage/'.$fichas->timg3)}}" alt="">
					</div>
					@endif
					@if($fichas->timg4 != "null")	
					<div class="slide-termin__item">
						<img src="{{URL::asset('storage/'.$fichas->timg4)}}" alt="">
					</div>
					@endif
					@if($fichas->timg5 != "null")	
					<div class="slide-termin__item">
						<img src="{{URL::asset('storage/'.$fichas->timg5)}}" alt="">
					</div>
					@endif
					@endforeach	
				</div>
				@foreach($resultado['proyecto'] as $fichas)
				<div class="slide-termin--nav">
					@if($fichas->timg1 != "null")
					<div class="slide-termin--nav__item">
						<img src="{{URL::asset('storage/'.$fichas->timg1)}}" alt="">
					</div>
					@endif
					@if($fichas->timg2 != "null")
					<div class="slide-termin--nav__item">
						<img src="{{URL::asset('storage/'.$fichas->timg2)}}" alt="">
					</div>
					@endif
					@if($fichas->timg3 != "null")
					<div class="slide-termin--nav__item">
						<img src="{{URL::asset('storage/'.$fichas->timg3)}}" alt="">
					</div>
					@endif
					@if($fichas->timg4 != "null")
					<div class="slide-termin--nav__item">
						<img src="{{URL::asset('storage/'.$fichas->timg4)}}" alt="">
					</div>
					@endif
					@if($fichas->timg5 != "null")
					<div class="slide-termin--nav__item">
						<img src="{{URL::asset('storage/'.$fichas->timg5)}}" alt="">
					</div>
					@endif
				</div>
				@endforeach
				<div class="tab-pane__link">
					<a href="{{route('ficha_modelo_exportarpdf',['id' => $mod->id,$mod->proyecto_id ])}}">Descargar ficha</a>
				</div>
				@foreach($resultado['modelo'] as $modelo)
				<div class="tab-pane__box">
					<h2>Terminaciones</h2>
					<p>{{$modelo->terminaciones}}</p>
				</div>
				@endforeach
			</div>
			<div class="tab-pane fade" id="nav-equipamiento" role="tabpanel" aria-labelledby="nav-equipamiento-tab">
				<div class="slide-equip">
					<div class="slide-equip__item">
						@foreach($resultado['proyecto'] as $fichas)
						<img src="{{URL::asset('storage/'.$fichas->eimg1)}}" alt="">
						<a href="{{$fichas->ve1}}" data-lity>
							<i class="icon-play"></i>
						</a>
					</div>
					@if($fichas->eimg1 != "null")
					<div class="slide-equip__item">
						<img src="{{URL::asset('storage/'.$fichas->eimg1)}}" alt="">
					</div>
					@endif
					@if($fichas->eimg2 != "null")
					<div class="slide-equip__item">
						<img src="{{URL::asset('storage/'.$fichas->eimg2)}}" alt="">
					</div>
					@endif
					@if($fichas->eimg3 != "null")
					<div class="slide-equip__item">
						<img src="{{URL::asset('storage/'.$fichas->eimg3)}}" alt="">
					</div>
					@endif
					@if($fichas->eimg4 != "null")
					<div class="slide-equip__item">
						<img src="{{URL::asset('storage/'.$fichas->eimg4)}}" alt="">
					</div>
					@endif
					@if($fichas->eimg5 != "null")
					<div class="slide-equip__item">
						<img src="{{URL::asset('storage/'.$fichas->eimg5)}}" alt="">
					</div>
					@endif
					@endforeach	
				</div>
				@foreach($resultado['proyecto'] as $fichas)
				<div class="slide-equip--nav">
					@if($fichas->eimg1 != "null")
					<div class="slide-equip--nav__item">
						<img src="{{URL::asset('storage/'.$fichas->eimg1)}}" alt="">
					</div>
					@endif
					@if($fichas->eimg2 != "null")
					<div class="slide-equip--nav__item">
						<img src="{{URL::asset('storage/'.$fichas->eimg2)}}" alt="">
					</div>
					@endif
					@if($fichas->timg3 != "null")
					<div class="slide-equip--nav__item">
						<img src="{{URL::asset('storage/'.$fichas->eimg3)}}" alt="">
					</div>
					@endif
					@if($fichas->timg4 != "null")
					<div class="slide-equip--nav__item">
						<img src="{{URL::asset('storage/'.$fichas->eimg4)}}" alt="">
					</div>
					@endif
					@if($fichas->timg5 != "null")
					<div class="slide-equip--nav__item">
						<img src="{{URL::asset('storage/'.$fichas->eimg5)}}" alt="">
					</div>
					@endif
				</div>
				@endforeach
				<div class="tab-pane__link">
					<a href="{{route('ficha_modelo_exportarpdf',['id' => $mod->id,$mod->proyecto_id ])}}">Descargar ficha</a>
				</div>
				<div class="tab-pane__box">
				<h2>Equipamiento</h2>
								@foreach($resultado['modelo'] as $modelo)

					<p>{{$modelo->equipamiento}}</p>
						@endforeach
				</div>
			
			</div>
			<div class="tab-pane fade" id="nav-ubicacion" role="tabpanel" aria-labelledby="nav-ubicacion-tab">
				<div class="slide-spot">
					<div class="slide-spot__item">
						@foreach($resultado['proyecto'] as $fichas)
						<img src="{{URL::asset('storage/'.$fichas->uimg1)}}" alt="">
						<a href="{{$fichas->vu1}}" data-lity>
							<i class="icon-play"></i>
						</a>
					</div>
					@if($fichas->uimg1 != "null")
					<div class="slide-spot__item">
						<img src="{{URL::asset('storage/'.$fichas->uimg1)}}" alt="">
					</div>
					@endif
					@if($fichas->uimg2 != "null")
					<div class="slide-spot__item">
						<img src="{{URL::asset('storage/'.$fichas->uimg2)}}" alt="">
					</div>
					@endif
					@if($fichas->uimg3 != "null")
					<div class="slide-spot__item">
						<img src="{{URL::asset('storage/'.$fichas->uimg3)}}" alt="">
					</div>
					@endif
					@if($fichas->uimg4 != "null")
					<div class="slide-spot__item">
						<img src="{{URL::asset('storage/'.$fichas->uimg4)}}" alt="">
					</div>
					@endif
					@if($fichas->uimg5 != "null")
					<div class="slide-spot__item">
						<img src="{{URL::asset('storage/'.$fichas->uimg5)}}" alt="">
					</div>
					@endif
					@endforeach
				</div>
				@foreach($resultado['proyecto'] as $fichas)
				<div class="slide-spot--nav">
					@if($fichas->uimg1 != "null")
					<div class="slide-spot--nav__item">
						<img src="{{URL::asset('storage/'.$fichas->uimg1)}}" alt="">
					</div>
					@endif
					@if($fichas->uimg2 != "null")
					<div class="slide-spot--nav__item">
						<img src="{{URL::asset('storage/'.$fichas->uimg2)}}" alt="">
					</div>
					@endif
					@if($fichas->uimg3 != "null")
					<div class="slide-spot--nav__item">
						<img src="{{URL::asset('storage/'.$fichas->uimg3)}}" alt="">
					</div>
					@endif
					@if($fichas->uimg4 != "null")
					<div class="slide-spot--nav__item">
						<img src="{{URL::asset('storage/'.$fichas->uimg4)}}" alt="">
					</div>
					@endif
					@if($fichas->uimg5 != "null")
					<div class="slide-spot--nav__item">
						<img src="{{URL::asset('storage/'.$fichas->uimg5)}}" alt="">
					</div>
					@endif
					@endforeach
				</div>
				<div class="tab-pane__link">
					<a href="{{route('ficha_modelo_exportarpdf',['id' => $mod->id,$mod->proyecto_id ])}}">Descargar ficha</a>
				</div>
				@foreach($resultado['proyecto'] as $modelo) 
				<div class="tab-pane__box">
					<h2>Ubicacion y Entorno</h2>
					<p>{{$modelo->ubicacion}}</p>
				</div>
				@endforeach
			</div>
		</div>
		@endforeach
	</section>