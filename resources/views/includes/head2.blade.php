<head>

<meta charset="UTF-8">
	<title>Inmobiliaria Madesal</title>
	<link rel="apple-touch-icon" sizes="57x57" href="../../images/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="../../images/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="../../images/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="../../images/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="../../images/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="../../images/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="../../images/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="../../images/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="../../images/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="../../images/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="../../images/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="../../images/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="../../images/favicon-16x16.png">		
	<meta name="msapplication-TileImage" content="../../images/ms-icon-144x144.png">	
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<!-- Bootstrap-->
	<link rel="stylesheet" href="../../css/bootstrap.min.css">
	<link rel="stylesheet" href="../../css/nouislider.min.css">
	<!-- Estilos Generales-->
	<link rel="stylesheet" href="../../css/style.min.css">	
	<!-- Fonts -->
	<link href="../../css/family.css" rel="stylesheet">
	<link rel="stylesheet" href="../../css/icons-madesal.css">
</head>