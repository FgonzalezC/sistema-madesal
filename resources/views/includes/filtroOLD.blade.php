				<li>
						<div class="dropdown">
							<button class="btn dropdown-toggle" type="button" id="dropdownComuna" data-toggle="dropdown"
							aria-haspopup="true" aria-expanded="false">
							<i class="icon-casa"></i>
							Comuna
						</button>
						<?php 	
						$comunas = \DB::table('comunas')
						->get();
						?>
						<div class="dropdown-menu" aria-labelledby="dropdownComuna">
							@foreach($comunas as $comuna)
							<input type="checkbox" name="Comuna[]"  id="{{ $comuna->id }}" value="{{ $comuna->nombre_comuna  }}"  {{ old('Comuna') == $comuna->nombre_comuna ? 'checked' : ''}} onclick="agregarFiltro(this.value)">

							<label for="{{$comuna->nombre_comuna }}">{{$comuna->nombre_comuna}}</label>
							@endforeach	
						</div>

					</div>
				</li>
				<li>
					<div class="dropdown">
						<button class="btn dropdown-toggle" type="button" id="dropdownSubsidio" data-toggle="dropdown" aria-haspopup="true"
						aria-expanded="false">
						<i class="icon-valor"></i>
						Proyecto con subsidio
					</button>
					<div class="dropdown-menu" aria-labelledby="dropdownSubsidio">

							@foreach(\App\Models\Admin\Subsidio::all() as $subsidio)
								<input type="checkbox" name="Subsidio[]"  id="{{ $subsidio->nombre_subsidio }}" value="{{ $subsidio->nombre_subsidio  }}"  {{ old('Subsidio') == $subsidio->nombre_subsidio ? 'checked' : ''}}  onclick="agregarFiltro(this.value)">
								<label for="{{$subsidio->nombre_subsidio }}">{{$subsidio->nombre_subsidio}}</label>

							@endforeach
					
					</div>
				</div>
			</li>
			<li>
				<div class="dropdown">
					<button class="btn dropdown-toggle" type="button" id="dropdownTipo" data-toggle="dropdown" aria-haspopup="true"
					aria-expanded="false">
					<i class="icon-pisos"></i>
					Tipo de Proyecto
				</button>
				<div class="dropdown-menu" aria-labelledby="dropdownTipo">
						@foreach(\App\Models\Admin\TipoInmueble::all() as $inmueble)
						<input type="checkbox" name="Inmueble[]"  id="{{ $inmueble->nombre_inmueble }}" value="{{ $inmueble->nombre_inmueble  }}"  {{ old('Inmueble') == $inmueble->nombre_inmueble ? 'checked' : ''}} onclick="agregarFiltro(this.value)">
						<label for="{{$inmueble->nombre_inmueble }}">{{$inmueble->nombre_inmueble}}</label>

					@endforeach
				</div>
			</div>
		</li>
		<li>
			<div class="dropdown">
				<button class="btn dropdown-toggle" type="button" id="dropdownEstado" data-toggle="dropdown" aria-haspopup="true"
				aria-expanded="false">
				<i class="icon-plano"></i>
				Estado del proyecto
			</button>
			<div class="dropdown-menu" aria-labelledby="dropdownEstado">
						@foreach(\App\Models\Admin\EstadoProyecto::all() as $estado)
						<input type="checkbox" name="Estado[]"  id="{{ $estado->nombre_estado }}" value="{{ $estado->nombre_estado  }}"  {{ old('Estado') == $estado->nombre_estado ? 'checked' : ''}} onclick="agregarFiltro(this.value)">
						<label for="{{$estado->nombre_estado }}">{{$estado->nombre_estado}}</label>
				
				@endforeach
			</div>
		</div>
	</li>
	
	<li>						
		<div id="slider" class="noUi-target noUi-ltr noUi-horizontal"></div>
		<span class="example-val" id="slider-margin-value-min"></span>
		<input type="hidden" name="MinSlider" id="MinSlider">
		<span class="example-val" id="slider-margin-value-max"></span>
		<input type="hidden" name="MaxSlider" id="MaxSlider">
	</li>
	<li>
		<button type="submit">Descubrir</button>
	</li> 

