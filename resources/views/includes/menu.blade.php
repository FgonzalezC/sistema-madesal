<li class="nav-item">
    <a class="nav-link" href="{{ url('/proyecto') }}">Proyectos</a>
</li>
<li class="nav-item">
    <a class="nav-link" href="{{ url('/inversionista') }}">Inversionistas</a>
</li>
<li class="nav-item">
    <a class="nav-link" href="{{ url('/financiamiento') }}">Financiamiento</a>
</li>
<li class="nav-item">
    <a class="nav-link" href="{{ url('/simuladores') }}">Simuladores</a>
</li>
<li class="nav-item">
    <a class="nav-link" href="{{ url('/asesoria') }}">Te asesoramos</a>
</li>
<li class="nav-item">
    <a class="nav-link" href="{{ url('/madesal') }}">Madesal</a>
</li>

