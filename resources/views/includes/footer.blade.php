<?php 
$footer = \DB::table('footers')
				->select('img_footer','direccion','telefono','link_face','link_instagram','link_twitter')
				->get();
?>

@foreach($footer as $footers)

<footer>
		<div class="container">
			<div class="row d-flex align-items-center">
				<div class="col-lg-3">
					 <a href="{{ url('/home')}}">
					<img src="{{URL::asset('storage/'.$footers->img_footer)}}">
					</a>
				</div>
				<div class="col-lg-6">
					<ul class="address">
						<!--<li>
							<h4>Dirección</h4>
							<p>{{$footers->direccion}}</p>
						</li>-->
						<li>
							<h4>Whatsapp:</h4>
							<a target="_blanck" href="https://api.whatsapp.com/send?phone=+56953782658&text=Hola, estoy interesado en contactar a un ejecutivo de Madesal">{{$footers->telefono}}</a>
						</li>
					</ul>
				</div>
				<div class="col-lg-3">
					<ul class="nav-social">
						<li><a target="_blanck" href="{{$footers->link_face}}"><i class="icon-facebook"></i></a></li>
						<li><a target="_blanck" href="{{$footers->link_instagram}}"><i class="icon-instagram"></i></a></li>
						<li><a target="_blanck" href="{{$footers->link_twitter}}"><i class="icon-twitter"></i></a></li>
					</ul>
				</div>
			</div>
		</div>
	</footer>
	@endforeach