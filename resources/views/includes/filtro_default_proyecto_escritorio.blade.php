            <form name="form-filtros" id="form-filtros" method="GET">
                <ul>
                    <li>
                        <div class="dropdown">
                            <button class="btn dropdown-toggle" type="button" id="dropdownComuna" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="icon-casa"></i>
                                Comuna
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownComuna">
                                @forelse($comunas as $comuna)
                                    <input type="checkbox" name="Comuna[]"  id="Comuna{{$comuna->id}}" value="{{$comuna->nombre_comuna}}"  {{ old('Comuna') == $comuna->nombre_comuna ? 'checked' : ''}} onclick="Madesal.actions.agregarFiltro('Comuna{{$comuna->id}}')">
                                    <label for="Comuna{{$comuna->id}}">{{$comuna->nombre_comuna}}</label>
                                @empty
                                    <label>No hay comunas</label>
                                @endforelse
                             </div>
                        </div>
                    </li>
                    <li>
                        <div class="dropdown">
                            <button class="btn dropdown-toggle" type="button" id="dropdownSubsidio" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="icon-valor"></i>
                                Proyecto con subsidio
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownSubsidio">
                                @forelse($subsidios as $subsidio)
                                <input type="checkbox" name="Subsidio[]"  id="{{ $subsidio->nombre_subsidio }}" value="{{ $subsidio->nombre_subsidio  }}"  {{ old('Subsidio') == $subsidio->nombre_subsidio ? 'checked' : ''}} >
                                    <input type="checkbox" name="Subsidio[]"  id="Subsidio{{$subsidio->id}}" value="{{$subsidio->nombre_subsidio}}"  onclick="Madesal.actions.agregarFiltro('Subsidio{{$subsidio->id}}')">
                                    <label for="Subsidio{{$subsidio->id}}">{{$subsidio->nombre_subsidio}}</label>
                                @empty
                                    <label>No hay subsidios</label>
                                @endforelse

                            </div>
                        </div>
                    </li>
                        <li>
                            <div class="dropdown">
                                <button class="btn dropdown-toggle" type="button" id="dropdownTipo" data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false">
                                <i class="icon-pisos"></i>
                                Tipo de Proyecto
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownTipo">
                                @forelse($tipos_de_inmuebles as $tp)
                                    <input type="checkbox" name="Inmueble[]"  id="Inmueble{{$tp->id}}" value="{{$tp->nombre_inmueble}}" {{ old('Inmueble') == $tp->nombre_inmueble ? 'checked' : ''}} onclick="Madesal.actions.agregarFiltro('Inmueble{{$tp->id}}')">
                                    <label for="Inmueble{{$tp->id}}">{{$tp->nombre_inmueble}}</label>
                                @empty
                                    <label>No hay tipo de proyecto</label>
                                @endforelse

                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="dropdown">
                            <button class="btn dropdown-toggle" type="button" id="dropdownEstado" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">
                            <i class="icon-plano"></i>
                            Estado del proyecto
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownEstado">
                                @forelse($estados_de_proyecto as $ep)
                                    <input type="checkbox" name="Estado[]"  id="Estado{{$ep->id}}" value="{{$ep->nombre_estado}}" {{ old('Estado') == $ep->nombre_estado ? 'checked' : ''}} onclick="Madesal.actions.agregarFiltro('Estado{{$ep->id}}')">
                                    <label for="Estado{{$ep->id}}">{{$ep->nombre_estado}}</label>
                                @empty
                                    <label>No hay estados de proyecto</label>
                                @endforelse
                        </div>
                    </div>
                    </li>
                    <li>
                        <div id="slider_proyecto" class="noUi-target noUi-ltr noUi-horizontal"></div>
                        <input type="hidden" name="margin_slider" id="margin_slider">
                        <span class="example-val" id="slider-margin-value-min"></span>
                        <input type="hidden" name="MinSlider" id="MinSlider">
                        <span class="example-val" id="slider-margin-value-max"></span>
                        <input type="hidden" name="MaxSlider" id="MaxSlider">
                        <input type="hidden" name="orden_precio" id="orden_precio" value="ASC">
                    </li>
                    <li>
                        <button class="btn-green" onclick="Madesal.actions.updateResultado(0,1);" id="bhsubmit">Descubrir</button>
                    </li>

                </ul>
            </form>
