
@if(session("error"))
	<div class="alert alert-danger alert-dismissible" data-auto-dismiss="3000">
     <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="fa fa-remove"></i> Mensaje Sistema Madesal.</h4>
        <ul>
        	
        	<li>{{session("error")}}</li>
        	
        </ul>       
	</div>
@endif