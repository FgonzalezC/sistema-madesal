@if(session()->has('msj'))
			
<div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <center>
            <label style='color:#4c4c4c'>
                    <strong>Sus Datos han sido ingresados correctamente nos comunicaremos con usted en la brevedad</strong>
            </label>
        </center>
    </div>	
    @elseif(session()->has('msj2'))	
    <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <center>
                <label style='color:#4c4c4c'>
                        <strong>Error en Enviar Datos</strong>
                </label>
            </center>
        </div>		
@endif

