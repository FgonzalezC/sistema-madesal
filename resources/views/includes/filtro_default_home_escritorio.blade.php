            <form action="" name="form-filtros-home" id="form-filtros-home" method="GET">
                <ul>
                    <li>
                        <div class="dropdown">
                            <button class="btn dropdown-toggle" type="button" id="dropdownComuna" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="icon-casa"></i>
                                Comuna
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownComuna">
                            @foreach($comunas as $comuna)
                                <input type="hidden" name="exc-{{ $comuna->nombre_comuna  }}" id="exc-{{ $comuna->nombre_comuna  }}" value ="0">
                                <input type="checkbox" name="Comuna[]"   id="{{ $comuna->nombre_comuna  }}" value="{{ $comuna->nombre_comuna  }}"  {{ old('Comuna') == $comuna->nombre_comuna ? 'checked' : ''}} >

                            <label for="{{$comuna->nombre_comuna }}">{{$comuna->nombre_comuna}}</label>
                            @endforeach
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="dropdown">
                            <button class="btn dropdown-toggle" type="button" id="dropdownSubsidio" data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false">
                                <i class="icon-valor"></i>
                                Proyecto con subsidio
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownSubsidio">
                                @foreach($subsidios as $subsidio)
                                <input type="hidden" name="exc-{{ $subsidio->nombre_subsidio  }}" id="exc-{{ $subsidio->nombre_subsidio  }}" value ="0">
                                <input type="checkbox" name="Subsidio[]"  id="{{ $subsidio->nombre_subsidio }}" value="{{ $subsidio->nombre_subsidio  }}"  {{ old('Subsidio') == $subsidio->nombre_subsidio ? 'checked' : ''}} >
                                <label for="{{$subsidio->nombre_subsidio }}">{{$subsidio->nombre_subsidio}}</label>

                            @endforeach
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="dropdown">
                            <button class="btn dropdown-toggle" type="button" id="dropdownTipo" data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false">
                                <i class="icon-pisos"></i>
                                Tipo de Proyecto
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownTipo">
                            @foreach($tipos_de_inmuebles as $inmueble)
                                <input type="hidden" name="exc-{{ $inmueble->nombre_inmueble }}" id="exc-{{ $inmueble->nombre_inmueble }}" value ="0">
                                <input type="checkbox" name="Inmueble[]"  id="{{ $inmueble->nombre_inmueble }}" value="{{ $inmueble->nombre_inmueble  }}"  {{ old('Inmueble') == $inmueble->nombre_inmueble ? 'checked' : ''}} >
                                <label for="{{$inmueble->nombre_inmueble }}">{{$inmueble->nombre_inmueble}}</label>
                            @endforeach
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="dropdown">
                            <button class="btn dropdown-toggle" type="button" id="dropdownEstado" data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false">
                                <i class="icon-plano"></i>
                                Estado del proyecto
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownEstado">
                            @foreach($estados_de_proyecto as $estado)
                                <input type="hidden" name="exc-{{ $estado->nombre_estado }}" id="exc-{{ $estado->nombre_estado }}" value ="0">
                                <input type="checkbox" name="Estado[]"  id="{{ $estado->nombre_estado }}" value="{{ $estado->nombre_estado  }}"  {{ old('Estado') == $estado->nombre_estado ? 'checked' : ''}} >
                                <label for="{{$estado->nombre_estado }}">{{$estado->nombre_estado}}</label>
                            @endforeach
                            </div>
                        </div>
                    </li>
                    <li>
                        <div id="slider" class="noUi-target noUi-ltr noUi-horizontal"></div>
                           <input type="hidden" name="margin_slider" id="margin_slider">
                           <span class="example-val" id="slider-margin-value-min"></span>
                           <span class="example-val" id="slider-margin-value-max"></span>
                           <input type="hidden" name="MinSlider" id="MinSlider">
                           <input type="hidden" name="MaxSlider" id="MaxSlider">
                    </li>
                    <li>
                        <button type="submit" id="btn_descubrir">Descubrir</button>
                    </li>
                </ul>
            </form>
