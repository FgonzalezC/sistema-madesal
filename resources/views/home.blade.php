<?php
    include '../public/includes/connect.php';

    $consulta = 'SELECT * FROM seccion_proyectos';
    $resultado= mysqli_query($conexion,$consulta);
    $r01 = mysqli_fetch_assoc($resultado);
?>

<!DOCTYPE html>
<html lang="es-Es">
@include('includes.head')
<body>

    <input type="hidden" id="rango_maximo" value="{{$rango_maximo}}">
    <input type="hidden" id="rango_minimo" value="{{$rango_minimo}}">
	<!-- Loading -->
	<div class="loading">
		<div class="loader"></div>
	</div>
	<!-- /Loading -->
	<header>
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-2 brand-image">
					<a href="{{ url('/home')}}">
						<img data-src="images/logo-madesal.png" class="d-none d-lg-block lazy" alt="Madesal"s>
						<img data-src="images/logo-madesal-xs.png" class="d-block d-lg-none lazy" alt="Madesal">
						</a>
				</div>
				<div class="col-lg-9 col-10">
					<nav class="navbar navbar-expand-lg">
						<div class="top">
							<ul class="d-flex">
								@include('includes.menu-superior')
							</ul>
						</div>
						<button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
							aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
							<span></span>
							<span></span>
							<span></span>
						</button>
						<div class="collapse navbar-collapse" id="navbarSupportedContent">
							<ul class="navbar-nav">
								@include('includes.menu')
							</ul>
							<div class="box-mobile d-block d-lg-none">
								<ul class="box-mobile__btns">
									<li>
										<a  href="{{ url('/asesoria')}}">Ayuda</a>
									</li>
									<li>
										<a href="{{ url('/contacto')}}">Contacto</a>
									</li>
								</ul>
								<?php
								$footer = \DB::table('footers')
								->select('img_footer','direccion','telefono','link_face','link_instagram','link_twitter')
								->get();
								?>

								@foreach($footer as $footers)
								<ul class="box-mobile__social">
									<li><a target="_blanck" href="{{$footers->link_face}}"><i class="icon-facebook"></i></a></li>
									<li><a target="_blanck" href="{{$footers->link_instagram}}"><i class="icon-instagram"></i></a></li>
									<li><a target="_blanck" href="{{$footers->link_twitter}}"><i class="icon-twitter"></i></a></li>
								</ul>
								@endforeach
							</div>
						</div>
					</nav>
				</div>
			</div>
		</div>
	</header>
	<?php
		$seccion = \DB::table('seccion_homes')
		->select('titulo_principal','titulo_principal2','img','subtitulo','subtitulo1','subtitulo2','texto','img1','img2','img3','titulo_movil','msubtitulo1','msubtitulo2','msubtitulo3','msubtitulo4','movilimg1','movilimg2','movilimg3')
		->get();
	?>
	<!-- MAIN HEAD -->
	<!-- MAIN HEAD DESKTOP -->
	<section class="main-head d-none d-lg-block" id="main_head_desktop" >
		@foreach ($seccion as $sechome)
		<div class="main-head__image">
			<img src="{{URL::asset('storage/'.$sechome->img)}}" alt="Madesal">
		</div>
		<div class="main-head__title">
			<h1>{{$sechome->titulo_principal}}<span>{{$sechome->titulo_principal2}}</span></h1>
		</div>
		<div class="main-head__form">
            <!-- formulario filtros home vista escrotorio por defecto -->
            @include('includes/filtro_default_home_escritorio')
            <!-- ./formulario filtros home vista escrotorio por defecto -->
		</div>
		@endforeach
	</section>
	<!-- /MAIN HEAD DESKTOP -->
<?php
	$seccion = \DB::table('seccion_homes')
	->select('titulo_principal','titulo_principal2','img','subtitulo','subtitulo1','subtitulo2','texto','img1','img2','img3','titulo_movil','msubtitulo1','msubtitulo2','msubtitulo3','msubtitulo4','movilimg1','movilimg2','movilimg3')
	->get();
?>

<!-- MAIN HEAD MOBILE  FILTRO AVANZADO-->
    <section class="main-head main-head--project" id="section_main_head_proyectos" style="display: none;">
        <div class="main-head__image">
            <img data-src="/storage/<?php echo $r01['img']; ?>" class="d-none d-md-block lazy" alt="Madesal">
        </div>
        <div class="main-head__title d-none d-lg-block">
            <?php echo "<h1>".$r01['titulo']."<span>".$r01['titulo2']."</span></h1>"; ?>
        </div>
        <div class="main-head__form d-none d-lg-block">
            <!-- formulario home vista escritorio cargado reemplaza primero -->
            @include('includes/filtro_reemplaza_home_escritorio')
            <!-- ./formulario home vista escritorio cargado reemplaza primero -->

        </div> 

        <div class="mobile-filter d-block d-lg-none lazy" id="section_mobile_filter_proyecto" style="display: none;">
            <div class="dropdown">
                <button class="btn dropdown-toggle" type="button" id="dropdownSearch" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false"> Mejorar tu busqueda</button>
                <!-- formulario mejora tu busqueda -->
                @include('includes/filtro_mejora_tu_busqueda_movil')
                <!-- formulario mejora tu busqueda -->
            </div>
        </div>
        <div class="form-result">
            <div class="form-result__title d-block d-lg-none">
                <h4>Resultados de busqueda</h4>
            </div>
            <ul id="lista">
                <li>
                    Filtros:
                </li>
            </ul>
            <div class="dropdown d-none d-lg-block">
                <select name="orden" onchange="Madesal.actions.updateResultado(this.value,1)">
                    <option value="ASC">Ordenar por</option>
                    <option value="DESC">Precio de mayor a menor</option>
                    <option value="ASC">Precio de menor a mayor</option>
                </select>
            </div>
        </div>
    </section>
    <div id="resultado_proyecto"></div>
<!-- .MAIN HEAD MOBILE  FILTRO AVANZADO-->

	<!-- MAIN HEAD MOBILE -->
	@foreach ($seccion as $sechome)
	<section class="main-head-xs d-block d-lg-none" id="filtro_mobile_home">
		<div class="main-head-xs__title">
			<h1>{{$sechome->titulo_movil}}</h1>
		</div>
        <!-- formulario mobil home default -->
            @include('includes/filtro_default_home_movil')
        <!-- ./formulario mobil home default -->
	</section>
	@endforeach
	<!-- /MAIN HEAD MOBILE -->
<?php
	$seccion = \DB::table('seccion_homes')
	->select('titulo_principal','titulo_principal2','subtitulo','subtitulo1','subtitulo2','texto','img1','img2','img3','movilimg1','movilimg2','movilimg3')
	->get();
?>
@foreach ($seccion as $sechome)
<section class="project" id="proyectos_destacados">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center">
				<h2 class="primary-title" >{{$sechome->subtitulo}}</h2>
			</div>
		</div>
		<div class="row d-flex justify-content-between">
			@foreach ($proyecto as $proyectos)
			<article class="project__box">
				<figure>
					<div class="label-type"> 
						@if ($proyectos->estado_stock === 'sin stock') 
							{{$proyectos->estado_stock}}
						@else
							{{ $proyectos->nombre_estado}} 
						@endif
					</div>
					<img data-src="{{URL::asset('storage/'.$proyectos->img_home)}}" class="lazy" alt="" >
					<figcaption>
						<figure>
							<h4>{{\App\Models\Admin\TipoInmueble::find($proyectos->tipoinmueble_id)->nombre_inmueble}}</h4>
							<h2>{{$proyectos->nombre_proyecto}}</h2>
							<h5>{{\App\Models\Admin\TipoEtapa::find($proyectos->tipoetapa_id)->nombre_etapa}}</h5>
						</figure>
					</figcaption>
				</figure>
				<ul class="box-features">
					<li>
						<i class="icon-dormitorio"></i>
						<span>{{$proyectos->dormitorios}}</span>
					</li>
					<li>
						<i class="icon-bano"></i>
						<span>{{$proyectos->banos}}</span>
					</li>
					<li>
						<i class="icon-estacionamiento"></i>
						<span>{{$proyectos->estacionamiento}}</span>
					</li>
				</ul>
				<div class="box-desc">
						<div class="col">
							<h5>Superficie:</h5>
							<p>{{$proyectos->superficie}} M2</p>
						</div>
						<div class="col">
							<h5>Precio desde:</h5>
							<p>{{$proyectos->precio}} UF</p>
						</div>
						@if($proyectos->valor_estacionamiento != NULL)
						<div class="col">
							<h5> Estacionamiento:</h5>
							<p>{{$proyectos->valor_estacionamiento}} UF</p>
						</div>
						@endif
				</div>
				
				<ul class="box-bt">
					<li><a href="{{route('ficha-proyecto', ['id' => $proyectos->id])}}">Ver más</a></li>
					<li><a href="{{route('ficha-proyecto', ['id' => $proyectos->id])}},#cotizar-form ">Cotizar</a></li>
				</ul>
			</article>
			@endforeach

		</div>
		<div class="row">
			<div class="col-12 text-center">
				<a href="{{url('/proyecto')}}" class="btn-primary">Ver todos</a>
			</div>
		</div>
	</div>
</section>
@endforeach
<?php
	$seccion = \DB::table('seccion_homes')
	->select('titulo_principal','titulo_principal2','subtitulo','subtitulo1','subtitulo2','texto','img1','img2','img3','movilimg1','movilimg2','movilimg3')
	->get();
?>
<!-- MAIN HEAD INVIERTE MADESAL-->
@foreach ($seccion as $sechome)
<section class="investment d-none d-lg-block" id="seccion_invierte_home">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center">
				<h2 class="primary-title">{{$sechome->subtitulo1}}</h2>
			</div>
		</div>
		<div class="row d-flex justify-content-center">
			<div class="col-lg-7">

				<?php
				$seccion = \DB::table('seccion_homes')
				->select('titulo_principal','titulo_principal2','img','subtitulo','subtitulo1','subtitulo2','texto','img1','img2','img3','titulo_movil','movilimg1','movilimg2','movilimg3')
				->get();

				$seccionC = \DB::table('seccion_homes')->count();
				?>

				<!-- IF SI TABLA seccion_homes ESTÁ VACÍA -->
				@if($seccionC != 0)	<!-- if -->
				@foreach ($seccion as $sechome)
				<article class="profitability">
					<figure>
						<!--	<img data-src="images/thumb-inv.jpg" class="lazy"> -->

						<img src="{{URL::asset('storage/'.$sechome->img1)}}" alt="" />
					<!--	<a href="" class="btn-go"></a>-->
						<figcaption>
							<!-- <h2>Rentabilidad de nuestros proyectos</h2> -->
							<h2>{{$sechome->subtitulo2}}</h2>
							<p>{{$sechome->texto}}</p>
								<!-- <p>Li Europan lingues es membres del sam familie. Lor separat existentie es un myth. Por scientie, musica, sport etc, litot
								Europa usa li sam vocabular. Li lingues differe solmen in li grammatica.</p> -->
								<a href="/inversionista" class="btn-primary">Quiero saber más</a>
							</figcaption>
						</figure>
					</article>
					@endforeach
					@else	<!-- else -->
					<article class="profitability">
						<figure>
							<img data-src="images/thumb-inv.jpg" class="lazy">
						<!--	<a href="" class="btn-go"></a>-->
							<figcaption>
								<h2>Rentabilidad de nuestros proyectos</h2>
								<p>Li Europan lingues es membres del sam familie. Lor separat existentie es un myth. Por scientie, musica, sport etc, litot
								Europa usa li sam vocabular. Li lingues differe solmen in li grammatica.</p>
								<a href="{{ url('/inversionista') }}" class="btn-primary">Quiero saber más</a>
							</figcaption>
						</figure>
					</article>
					@endif
				</div>
				<!-- END IF -->

				<!-- IF SI TABLA seccion_homes ESTÁ VACÍA -->
				@if($seccionC != 0) <!-- if -->
				<div class="col-lg-4">
					<div class="banner">
						<a href="{{ url('/reserva') }}">
							<!--	<img src="images/banner-1.jpg" alt=""> -->

							<img src="{{URL::asset('storage/'.$sechome->img2)}}" alt="" />
						</a>
					</div>
					<div class="banner">
						<a href="{{ url('/inversionista') }}">
							<!--	<img src="images/banner-2.jpg" alt=""> -->
							<img src="{{URL::asset('storage/'.$sechome->img3)}}" alt="" />
						</a>
					</div>
				</div>
				@else	<!-- else -->
				<div class="col-lg-4">
					<div class="banner">
						<a href="#">
							<img src="images/banner-1.jpg" alt="">
						</a>
					</div>
					<div class="banner">
						<a href="#">
							<img src="images/banner-2.jpg" alt="">
						</a>
					</div>
				</div>
				@endif
				<!-- END IF -->
			</div>
	</div>
</section>

	@endforeach

	@include('includes.footer')

	<script src="js/jquery.min.js"></script>
	<script src="js/bootstrap.bundle.min.js"></script>
	<script src="js/lazyload.min.js"></script>
	<script src="js/nouislider.js"></script>
	<script src="js/slick.min.js"></script>
	<script src="js/lity.js"></script>
	<script src="js/jquery.zoom.min.js"></script>
	<script src="js/wNumb.min.js"></script>
	<script src="js/main.js"></script>
    <script src="js/madesal.js"></script>
	<script>
		var slider = document.getElementById('slider');
        var rango_maximo = parseInt( document.getElementById('rango_maximo').value);
        var rango_minimo = parseInt( document.getElementById('rango_minimo').value);

		noUiSlider.create(slider, {
			start: [rango_minimo, rango_maximo],
			step: 100,
			connect: true,
			tooltips: [ wNumb({ decimals: 0 }), wNumb({ decimals: 0 }) ],
			range: {
				'min': rango_minimo,
				'max': rango_maximo
			},
		});

		var marginMin = document.getElementById('slider-margin-value-min'),
			marginMax = document.getElementById('slider-margin-value-max');

		slider.noUiSlider.on('update', function (values, handle) {
			if (handle) {
				marginMax.innerHTML = values[handle];
			} else {
				marginMin.innerHTML = values[handle];
			}
		});

        var margin_slider = document.getElementById('margin_slider');

        slider.noUiSlider.on('change', function( values, handle ) {
            margin_slider.value = values;
        });


	</script>
	<script>

        var sliderXs = document.getElementById('slider-xs');
        var rango_maximo = parseInt( document.getElementById('rango_maximo').value);
        var rango_minimo = parseInt( document.getElementById('rango_minimo').value);

        noUiSlider.create(sliderXs, {
            start: [rango_minimo, rango_maximo],
            step: 100,
            connect: true,
            tooltips: [ wNumb({ decimals: 0 }), wNumb({ decimals: 0 }) ],
            range: {
                'min': rango_minimo,
                'max': rango_maximo
            }
        });

        var marginMin = document.getElementById('slider-margin-value-min'),
            marginMax = document.getElementById('slider-margin-value-max');


        sliderXs.noUiSlider.on('update', function (values, handle) {
            if (handle) {
                marginMax.innerHTML = values[handle];
            } else {
                marginMin.innerHTML = values[handle];
            }
        });

        sliderXs.noUiSlider.on('change', function( values, handle ) {
            margin_slider.value = values;
        });
	</script>
@include('includes.chat')

</body>
</html>
