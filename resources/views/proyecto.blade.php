<?php
    include '../public/includes/connect.php';

    $consulta = 'SELECT * FROM seccion_proyectos';
    $resultado= mysqli_query($conexion,$consulta);
    $r01 = mysqli_fetch_assoc($resultado);
?>
<!DOCTYPE html>
<html lang="es-Es">
@include('includes.head')
<body>
    <input type="hidden" id="rango_maximo" value="{{$rango_maximo}}">
    <input type="hidden" id="rango_minimo" value="{{$rango_minimo}}">


	<!-- Loading -->
	<div class="loading">
		<div class="loader"></div>
	</div>
	<!-- /Loading -->
	<header class="header-int">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-2 brand-image">
					<a href="{{ url('/home')}}">
						<img data-src="images/logo-madesal.png" class="d-none d-lg-block lazy" alt="Madesal"s>
						<img data-src="images/logo-madesal-xs.png" class="d-block d-lg-none lazy" alt="Madesal">
						</a>
				</div>
				<div class="col-lg-9 col-10">
					<nav class="navbar navbar-expand-lg">
						<div class="top">
							<ul class="d-flex">
								@include('includes.menu-superior')
							<ul class="d-flex">
						</div>
						<button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
							aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
							<span></span>
							<span></span>
							<span></span>
						</button>
						<div class="collapse navbar-collapse" id="navbarSupportedContent">
							<ul class="navbar-nav">
								@include('includes.menu')
							</ul>
							<div class="box-mobile d-block d-lg-none">
								<ul class="box-mobile__btns">
									<li>
										<a  href="{{ url('/asesoria')}}">Ayuda</a>
									</li>
									<li>
										<a href="{{ url('/contacto')}}">Contacto</a>
									</li>
								</ul>
								<?php
								$footer = \DB::table('footers')
												->select('img_footer','direccion','telefono','link_face','link_instagram','link_twitter')
												->get();
								?>
								@foreach($footer as $footers)
								<ul class="box-mobile__social">
									<li><a target="_blanck" href="{{$footers->link_face}}"><i class="icon-facebook"></i></a></li>
									<li><a target="_blanck" href="{{$footers->link_instagram}}"><i class="icon-instagram"></i></a></li>
									<li><a target="_blanck" href="{{$footers->link_twitter}}"><i class="icon-twitter"></i></a></li>
								</ul>
								@endforeach
							</div>
						</div>
					</nav>
				</div>
			</div>
		</div>
	</header>

    <!-- MAIN HEAD -->
    <section class="main-head main-head--project" id="main_head_desktop">
        <div class="main-head__image">
            <img data-src="/storage/<?php echo $r01['img']; ?>" class="d-none d-md-block lazy" alt="Madesal">
        </div>
        <div class="main-head__title d-none d-lg-block">
            <?php echo "<h1>".$r01['titulo']."<span>".$r01['titulo2']."</span></h1>"; ?>
        </div>
        <div class="main-head__form d-none d-lg-block">
            <!-- formulario filtros home vista escrotorio por defecto -->
            @include('includes/filtro_default_proyecto_escritorio')
            <!-- ./formulario filtros home vista escrotorio por defecto -->
        </div>
        <div class="mobile-filter d-block d-lg-none lazy" id="section_mobile_filter_proyecto" style="display: none;">
            <div class="dropdown">
                <button class="btn dropdown-toggle" type="button" id="dropdownSearch" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false"> Mejorar tu busqueda</button>
                <!-- formulario mejora tu busqueda -->
                @include('includes/filtro_mejora_tu_busqueda_movil')
                <!-- formulario mejora tu busqueda -->
            </div>
        </div>
        <div class="form-result">
            <div class="form-result__title d-block d-lg-none">
                <h4>Resultados de busqueda</h4>
            </div>
            <ul id="lista">
                <li>
                    Filtros:
                </li>
            </ul>
            <div class="dropdown d-none d-lg-block">
                <select name="orden" onchange="Madesal.actions.updateResultado(this.value,1)">
                    <option value="ASC">Ordenar por</option>
                    <option value="DESC">Precio de mayor a menor</option>
                    <option value="ASC">Precio de menor a mayor</option>
                </select>
            </div>
        </div>
    </section>
    <!-- /MAIN HEAD DESKTOP -->
<div id="resultado_proyecto">
        <section class="project project--int">
            <div class="container">
                <div class="row d-flex justify-content-between">
                    <?php
                    $consulta = 'select distinct
                            p.id as id, 
                            p.orden as orden,
                            p.tipoinmueble_id as tipoinmueble_id,
                            p.tipoetapa_id as tipoetapa_id,
                            p.img_home as img_home,
                            p.nombre_proyecto as nombre_proyecto,
                            p.dormitorios as dormitorios, 
                            p.banos as banos,
                            p.estacionamiento as estacionamiento,
                            p.valor_estacionamiento as valor_estacionamiento,
                            p.superficie as superficie, 
                            p.precio,
                            ep.nombre_estado,
                            te.nombre_etapa,
                            (select sum(m2.check_stock) from modelos m2 where m2.proyecto_id=p.id) as stock,
                            (CASE
                                WHEN (select sum(m2.check_stock) from modelos m2 where m2.proyecto_id=p.id) > 0 THEN "stock"    
                                ELSE "sin stock"
                            END) as estado_stock
                        from proyectos p 
                        left join estado_proyectos ep on ep.id=p.estado_id 
                        left join tipo_etapas te on te.id=p.tipoetapa_id 
                        ORDER BY p.orden ASC,stock DESC
                    ';
                    $resultado= mysqli_query($conexion,$consulta);
                    while ($r01 = mysqli_fetch_assoc($resultado)){
                        $query = 'SELECT * FROM tipo_inmuebles WHERE id='.$r01['tipoinmueble_id'];
                        $resul= mysqli_query($conexion,$query);
                        $ri = mysqli_fetch_assoc($resul);
                        $ruta_proyecto = 'ficha-proyecto/'.$r01['id'];
                    ?>

                    <article class="project__box">
                        <figure>
                            <div class="label-type">
                            <?php
                                if ($r01['estado_stock'] == 'sin stock') {
                                    echo $r01['estado_stock'];
                                }
                                else{
                                    echo $r01['nombre_estado']; 
                                }                           
                            ?>
                            </div>
                            <img data-src="/storage/<?php echo $r01['img_home'];?>" class="lazy" alt="">
                            <figcaption>
                                <figure>
                                <h4><?php echo $ri['nombre_inmueble'];?></h4>
                                <h2><?php echo $r01['nombre_proyecto']?></h2>
                                <h5><?php echo $r01['nombre_etapa']?></h5>
                                </figure>
                            </figcaption>
                        </figure>



                        <ul class="box-features">
                            <li>
                                <i class="icon-dormitorio"></i>
                                <span><?php echo $r01['dormitorios']?></span>
                            </li>
                            <li>
                                <i class="icon-bano"></i>
                                <span><?php echo $r01['banos']?></span>
                            </li>
                            <li>
                                <i class="icon-estacionamiento"></i>
                                <span><?php echo $r01['estacionamiento']?></span>
                            </li>
                        </ul>

                        <div class="box-desc">
                                <div class="col">
                                    <h5>Superficie:</h5>
                                    <p><?php echo $r01['superficie']?> M2</p>
                                </div>
                                <div class="col">
                                    <h5>Precio desde:</h5>
                                    <p><?php echo $r01['precio']?> UF</p>
                                </div> 
                                <?php if($r01['valor_estacionamiento'] !=  NULL){?>
                                <div class="col">                                
                                    <h5> Estacionamiento:</h5>
                                    <p><?php echo $r01['valor_estacionamiento'].' UF'; ?> </p>                                
                                </div>
                                <?php
                                }
                                ?>
                        </div>
                        <ul class="box-bt">
                            <li><a href="<?php echo $ruta_proyecto ?>">Ver más</a></li>
                            <li><a href="<?php echo $ruta_proyecto ?>,#cotizar-form">Cotizar</a></li>
                        </ul>
                    </article>
                    <?php  } ?>

                </div>
            </div>
        </section>
</div>






@include('includes.footer')

    <script src="js/jquery.min.js"></script>
	<script src="js/bootstrap.bundle.min.js"></script>
	<script src="js/lazyload.min.js"></script>
	<script src="js/nouislider.js"></script>
	<script src="js/slick.min.js"></script>
	<script src="js/lity.js"></script>
	<script src="js/jquery.zoom.min.js"></script>
	<script src="js/wNumb.min.js"></script>
	<script src="js/main.js"></script>
    <script src="js/madesal.js"></script>
    <script>
        var sliderProyecto = document.getElementById('slider_proyecto');
        var rango_maximo = parseInt( document.getElementById('rango_maximo').value);
        var rango_minimo = parseInt( document.getElementById('rango_minimo').value);


        if (sliderProyecto.noUiSlider) {
            sliderProyecto.noUiSlider.destroy();
        }


         noUiSlider.create(sliderProyecto, {
            start: [rango_minimo, rango_maximo],
            step: 100,
            connect: true,
            tooltips: [ wNumb({ decimals: 0 }), wNumb({ decimals: 0 }) ],
            range: {
                'min': rango_minimo,
                'max': rango_maximo
            }
        });
        var marginMin = document.getElementById('slider-margin-value-min'),
            marginMax = document.getElementById('slider-margin-value-max');

        sliderProyecto.noUiSlider.on('update', function (values, handle) {
            if (handle) {
                marginMax.innerHTML = values[handle];
            } else {
                marginMin.innerHTML = values[handle];
            }
        });
        var margin_slider = document.getElementById('margin_slider');

        sliderProyecto.noUiSlider.on('change', function( values, handle ) {
            margin_slider.value = values;
        });
    </script>
    <script type="text/javascript">
        var rango_maximo = parseInt( document.getElementById('rango_maximo').value);
        var rango_minimo = parseInt( document.getElementById('rango_minimo').value);

        var sliderXsAvanzado = document.getElementById('slider-xs-avanzado');

        noUiSlider.create(sliderXsAvanzado, {
            start: [rango_minimo, rango_maximo],
            step: 100,
            connect: true,
            tooltips: [ wNumb({ decimals: 0 }), wNumb({ decimals: 0 }) ],
            range: {
                'min': rango_minimo,
                'max': rango_maximo
            }
        });
        var marginMin = document.getElementById('slider-margin-value-min'),
            marginMax = document.getElementById('slider-margin-value-max');

        sliderXsAvanzado.noUiSlider.on('update', function (values, handle) {
            if (handle) {
                marginMax.innerHTML = values[handle];
            } else {
                marginMin.innerHTML = values[handle];
            }
        });
        sliderXsAvanzado.noUiSlider.on('change', function( values, handle ) {
            margin_slider_avanzado.value = values;
        });

        var margin_slider_avanzado = document.getElementById('margin_slider_avanzado');

    </script>
    @include('includes.chat')


</body>
</html>
