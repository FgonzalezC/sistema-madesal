<?php
    include '../public/includes/connect.php';

    $consulta = 'SELECT * FROM seccion_proyectos';
    $py= mysqli_query($conexion,$consulta);
    $r01 = mysqli_fetch_assoc($py);

?>

<!DOCTYPE html>
<html lang="es-Es">
@include('includes.head')
<body>
    <input type="hidden" id="rango_maximo" value="{{$rango_maximo}}">
    <input type="hidden" id="rango_minimo" value="{{$rango_minimo}}">
	<!-- Loading -->
	<div class="loading">
		<div class="loader"></div>
	</div>
	<!-- /Loading -->
	<header class="header-int">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-2 brand-image">
					<a href="{{ url('/home')}}">
						<img data-src="images/logo-madesal.png" class="d-none d-lg-block lazy" alt="Madesal"s>
						<img data-src="images/logo-madesal-xs.png" class="d-block d-lg-none lazy" alt="Madesal">
						</a>
				</div>
				<div class="col-lg-9 col-10">
					<nav class="navbar navbar-expand-lg">
						<div class="top">
							<ul class="d-flex">
								@include('includes.menu-superior')
							</ul>
						</div>
						<button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
							aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
							<span></span>
							<span></span>
							<span></span>
						</button>
						<div class="collapse navbar-collapse" id="navbarSupportedContent">
							<ul class="navbar-nav">
								@include('includes.menu')
							</ul>
							<div class="box-mobile d-block d-lg-none">
								<ul class="box-mobile__btns">
									<li>
										<a  href="{{ url('/postventa')}}">Ayuda</a>
									</li>
									<li>
										<a href="{{ url('/contacto')}}">Contacto</a>
									</li>
								</ul>
								<?php
								$footer = \DB::table('footers')
												->select('img_footer','direccion','telefono','link_face','link_instagram','link_twitter')
												->get();
								?>

								@foreach($footer as $footers)
								<ul class="box-mobile__social">
									<li><a target="_blanck" href="{{$footers->link_face}}"><i class="icon-facebook"></i></a></li>
									<li><a target="_blanck" href="{{$footers->link_instagram}}"><i class="icon-instagram"></i></a></li>
									<li><a target="_blanck" href="{{$footers->link_twitter}}"><i class="icon-twitter"></i></a></li>
								</ul>
								@endforeach
							</div>
						</div>
					</nav>
				</div>
			</div>
		</div>
	</header>
<?php
        $proyecto = \DB::statement('select distinct
                            p.id as id, 
                            p.orden as orden,
                            p.tipoinmueble_id as tipoinmueble_id,
                            p.tipoetapa_id as tipoetapa_id,
                            p.img_home as img_home,
                            p.nombre_proyecto as nombre_proyecto,
                            p.dormitorios as dormitorios, 
                            p.banos as banos,
                            p.estacionamiento as estacionamiento,
                            p.valor_estacionamiento as valor_estacionamiento,
                            p.superficie as superficie, 
                            p.precio,
                            ep.nombre_estado,
                            te.nombre_etapa,
                            (select sum(m2.check_stock) from modelos m2 where m2.proyecto_id=p.id) as stock
                        from proyectos p 
                        left join estado_proyectos ep on ep.id=p.estado_id 
                        left join tipo_etapas te on te.id=p.tipoetapa_id 
                        ORDER BY p.orden ASC,stock DESC');

 		$inversionista = \DB::table('seccion_inversionistas')->get();
        $resul = array('proyecto' => $proyecto,'inversionista'=>$inversionista);
      	$resultado_inversionista=$resul;
?>
	<!-- MAIN HEAD DESKTOP -->
    <section class="main-head main-head--project"  id="main_head_desktop">
		@foreach($resultado_inversionista['inversionista'] as $inversionistas)
		<div class="main-head__image">
			<img data-src="{{URL::asset('storage/'.$inversionistas->img)}}" class="d-none d-md-block lazy" alt="Madesal">
		</div>
		<div class="main-head__title d-none d-lg-block">
			<h1>{{$inversionistas->titulo}}</h1>
		</div>
		<div class="main-head__form d-none d-lg-block">
            <!-- formulario filtros home vista escrotorio por defecto -->
            @include('includes/filtro_default_proyecto_escritorio')
            <!-- ./formulario filtros home vista escrotorio por defecto -->

		</div>
        <div class="mobile-filter d-block d-lg-none lazy" id="section_mobile_filter_proyecto" style="display: none;">
            <div class="dropdown">
                <button class="btn dropdown-toggle" type="button" id="dropdownSearch" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false"> Mejorar tu busqueda</button>
                <!-- formulario mejora tu busqueda -->
                @include('includes/filtro_mejora_tu_busqueda_movil')
                <!-- formulario mejora tu busqueda -->
            </div>
        </div>
        <div class="form-result">
            <div class="form-result__title d-block d-lg-none">
                <h4>Resultados de busqueda</h4>
            </div>
            <ul id="lista">
                <li>
                    Filtros:
                </li>
            </ul>
            <div class="dropdown d-none d-lg-block">
                <select name="orden" onchange="Madesal.actions.updateResultado(this.value,1)">
                    <option value="ASC">Ordenar por</option>
                    <option value="DESC">Precio de mayor a menor</option>
                    <option value="ASC">Precio de menor a mayor</option>
                </select>
            </div>
        </div>

		@endforeach
	</section>
    <!-- /MAIN HEAD DESKTOP -->


 <div id="resultado_proyecto">
	<!-- INVERTIR -->
	<section class="inversion" id="inversion">
		<br>
		<br>
		@include('includes.msj')
		@include('includes.form-error')

		@foreach($resultado_inversionista['inversionista'] as $inversionistas)
		<div class="container">
			<div class="row">
				<div class="col-12 text-center primary-title">
					<h2>{{$inversionistas->subtitulo}}</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-5 inversion__gray-box">
					<h3>{{$inversionistas->texto1}}<span>&nbsp;{{$inversionistas->texto2}}</span></h3>
				</div>
				<div class="inversion__white-box">
					<ul>
						<li>
							<div class="icon">
								<i class="icon-peso"></i>
							</div>
							<div class="text">
								<h3>{{$inversionistas->titulo1}}</h3>
								<p>{{$inversionistas->bajada1}}</p>
							</div>
						</li>
						<li>
							<div class="icon">
								<i class="icon-home"></i>
							</div>
							<div class="text">
								<h3>{{$inversionistas->titulo2}}</h3>
								<p>{{$inversionistas->bajada2}}</p>
							</div>
						</li>
						<li>
							<div class="icon">
								<i class="icon-inversion"></i>
							</div>
							<div class="text">
								<h3>{{$inversionistas->titulo3}}</h3>
								<p>{{$inversionistas->bajada3}}</p>
							</div>
						</li>
						<li>
							<div class="icon">
								<i class="icon-check"></i>
							</div>
							<div class="text">
								<h3>{{$inversionistas->titulo4}}</h3>
								<p>{{$inversionistas->bajada4}}</p>
							</div>
						</li>
					</ul>
				</div>
			</div>
			<div class="row beneficios">
				<div class="col-12">
					<ul>
						<li>
							<h3><span>{{$inversionistas->seccion1}}</span> {{$inversionistas->seccion2}}</h3>
						</li>
						<li>
							<ul>
								<li>
									<i class="icon-ok"></i>
									<p>{{$inversionistas->punto1}}</p>
								</li>
								<li>
									<i class="icon-ok"></i>
									<p>{{$inversionistas->punto2}}</p>
								</li>
								<li>
									<i class="icon-ok"></i>
									<p>{{$inversionistas->punto3}}</p>
								</li>
								<li>
									<i class="icon-ok"></i>
									<p>{{$inversionistas->punto4}}</p>
								</li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</div>
		@endforeach
	</section>
	<!-- INVERTIR -->
	<?php

        $resul = array('proyecto' => $lista_proyectos,'inversionista'=>$inversionista);
      	$resultado_inversionista=$resul;
      	//print_r($resultado_inversionista);
    ?>
	<section class="project project--int" id="destacados_inversion">
		<div class="container">
			<div class="row">
				<div class="col-12 text-center primary-title">
					<h2>Proyectos destacados para inversión</h2>
				</div>
			</div>
			<div class="row d-flex justify-content-between">
				@foreach($resultado_inversionista['proyecto'] as $proyectos)

			<article class="project__box">
				<figure>
					<div class="label-type"> 
						@if ($proyectos->estado_stock === 'sin stock') 
							{{$proyectos->estado_stock}}
						@else
							{{ $proyectos->nombre_estado}} 
						@endif
					</div>
					<img data-src="{{URL::asset('storage/'.$proyectos->img_home)}}" class="lazy" alt="" >
					<figcaption>
						<figure>
							<h4>{{\App\Models\Admin\TipoInmueble::find($proyectos->tipoinmueble_id)->nombre_inmueble}}</h4>
							<h2>{{$proyectos->nombre_proyecto}}</h2>
							<h5>{{\App\Models\Admin\TipoEtapa::find($proyectos->tipoetapa_id)->nombre_etapa}}</h5>
						</figure>
					</figcaption>
				</figure>
				<ul class="box-features">
					<li>
						<i class="icon-dormitorio"></i>
						<span>{{$proyectos->dormitorios}}</span>
					</li>
					<li>
						<i class="icon-bano"></i>
						<span>{{$proyectos->banos}}</span>
					</li>
					<li>
						<i class="icon-estacionamiento"></i>
						<span>{{$proyectos->estacionamiento}}</span>
					</li>
				</ul>
				<div class="box-desc">
						<div class="col">
							<h5>Superficie:</h5>
							<p>{{$proyectos->superficie}} M2</p>
						</div>
						<div class="col">
							<h5>Precio desde:</h5>
							<p>{{$proyectos->precio}} UF</p>
						</div>
						@if($proyectos->valor_estacionamiento != NULL)
						<div class="col">
							<h5> Estacionamiento:</h5>
							<p>{{$proyectos->valor_estacionamiento}} UF</p>
						</div>
						@endif
				</div>
				
				<ul class="box-bt">
					<li><a href="{{route('ficha-proyecto', ['id' => $proyectos->id])}}">Ver más</a></li>
					<li><a href="{{route('ficha-proyecto', ['id' => $proyectos->id])}},#cotizar-form ">Cotizar</a></li>
				</ul>
			</article>

				@endforeach

			</div>
		</div>

	</section>

	<!-- BANNER TWO COLUMN -->
    <div class="container" id="banner_inversion">
        @foreach($resultado_inversionista['inversionista'] as $inversionistas)
        <div class="row banner-50">
            <div class="col-lg-6 d-none d-md-block">
                <a href="{{ url('/simuladores')}}">
                    <img src="{{URL::asset('storage/'.$inversionistas->img1)}}" alt="">
                </a>
            </div>
            <div class="col-lg-6 d-none d-md-block">
                 <a href="{{ url('/asesoria')}}">
                    <img src="{{URL::asset('storage/'.$inversionistas->img2)}}" alt="">
                 </a>
            </div>
            <div class="col-lg-6 d-sm-block d-md-none">
               <a href="{{ url('/simuladores')}}">
                   <img src="{{URL::asset('storage/'.$inversionistas->imgmovil1)}}" alt="">
               </a>
           </div>

           <div class="col-lg-6 d-sm-block d-md-none">
               <a href="{{ url('/asesoria')}}">
                  <img src="{{URL::asset('storage/'.$inversionistas->imgmovil2)}}" alt="">
               </a>
            </div>
        </div>
        @endforeach
    </div>
	<!-- /BANNER TWO COLUMN -->

	<!-- CONTACT PROJECT -->
	<section class="contact-project" id="cotiza_inversion">
			<div class="container">

				<div class="row">
					<div class="col-lg-6">
					<div class="contact-project__form">
							    <form action="{{route('inversionista_guardar')}}" id= "form-general" class="gnrl-form" method="POST" autocomplete="off">
										{{ csrf_field() }}
								<h2>Cotiza este proyecto</h2>
								<input type="text" name="nombre_cotizacion" id="nombre_cotizacion" placeholder="Nombre" required>
								<input type="text" name="apellido_cotizacion" id="apellido_cotizacion" placeholder="Apellido"required>
								<input type="text" name="telefono_cotizacion" id="telefono_cotizacion" placeholder="Teléfono"required>
								<input type="text" name="email_cotizacion" id="email_cotizacion" placeholder="Email" required>
								 <select name="asunto_id" id="asunto_id" required>
                                        <option value="">Agregar Asunto</option>
                                        @foreach(\App\Models\Admin\Asunto::all() as $asunto)
                                        <option value="{{$asunto->id}}" required>{{$asunto->nombre_asunto}}</option>
                                    	@endforeach                                         
                                </select>
								<textarea name="mensaje_cotizacion" id="mensaje_cotizacion" placeholder="Mensaje" required></textarea>
								<button type="submit" class="btn-send"  >ENVIAR</button>
							</form>
						</div>
					</div>
					<?php
            		 $saladeventas = \DB::table('sucursales')
                         ->get();
                  	?>
					@foreach($saladeventas as $saladeventa)
					<div class="col-lg-6">
						<div class="contact-project__dates">
						<!--	<div class="label">
								Cómo llegar
							</div> -->
							<div class="dates-text">
							<h2>Sala de ventas: {{$saladeventa->nombre}}</h2>
                                <br>
                                <p>Dirección: {{$saladeventa->direccion}} </p>
                                <p>Teléfono: {{$saladeventa->telefono}}</p>

                                @if($saladeventa->telefono2 !=NULL)
                                <p>Celular: {{$saladeventa->telefono2}}</p>
                                @endif

                                <p>Mail: {{$saladeventa->correo}}</p>

                                @if($saladeventa->correo2 !=NULL)

                                    <p>Mail 2: {{$saladeventa->correo2}}</p>

                                @endif
                                <p>Horario de Atención: {{$saladeventa->horario}}</p>
                            </div>
                            @if($saladeventa->mapa!=NULL)
                                <div  style="width: 100%"><iframe width="100%" height="250" src="{{$saladeventa->mapa}}" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"><a href="https://www.maps.ie/coordinates.html">gps coordinates finder</a></iframe></div>
                                @else
                                <div  style="width: 100%"><iframe width="100%" height="250" src="https://maps.google.com/maps?width=100%&amp;height=600&amp;hl=en&amp;coord=-36.8294688,-73.0489664&amp;q=Cochcrane%20635%20concepci%C3%B3n+(Madesal%20Concepci%C3%B3n)&amp;ie=UTF8&amp;t=&amp;z=19&amp;iwloc=B&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"><a href="https://www.maps.ie/coordinates.html">gps coordinates finder</a></iframe></div>

                                @endif

						</div>

					</div>
					  @endforeach
					<?php
                   $empleado = \DB::table('proyecto_ejecutivos')
                ->where('cargos.nombre','=','Ejecutivo Inversionista')
             ->join('proyectos','proyectos.id','proyecto_ejecutivos.proyecto_id')
            ->join('empleados','empleados.id','proyecto_ejecutivos.empleado_id')
            ->join('cargos','empleados.cargo_id','cargos.id')
            ->get();
					?>

					<div class="col-12 ejecutive-sale">
						@foreach($empleado as $empleados)
						<article class="ejecutive-sale__box">
							<figure>
								<div class="img-container">
									<img src="{{URL::asset('storage/'.$empleados->img)}}">
								</div>
								<figcaption>
									<div class="text">
										<h2>{{$empleados->nombre_empleado}}</h2>
										<h3>{{\App\Models\Admin\Cargo::find($empleados->cargo_id)->nombre}}</h3>
									</div>
									<div class="dates">
										<p>{{$empleados->telefono}}</p>
										<p>{{$empleados->correo}}</p>
									</div>
								</figcaption>
							</figure>
						</article>
						@endforeach

					</div>
				</div>
			</div>
		</section>
		<!-- /CONTACT PROJECT -->
</div>
	@include('includes.footer')
	<script src="js/jquery.min.js"></script>
	<script src="js/bootstrap.bundle.min.js"></script>
	<script src="js/lazyload.min.js"></script>
	<script src="js/nouislider.js"></script>
	<script src="js/slick.min.js"></script>
	<script src="js/lity.js"></script>
	<script src="js/jquery.zoom.min.js"></script>
	<script src="js/wNumb.min.js"></script>
	<script src="js/main.js"></script>
    <script src="js/madesal.js"></script>
    <script>
        var sliderProyecto = document.getElementById('slider_proyecto');
        var rango_maximo = parseInt( document.getElementById('rango_maximo').value);
        var rango_minimo = parseInt( document.getElementById('rango_minimo').value);


        if (sliderProyecto.noUiSlider) {
            sliderProyecto.noUiSlider.destroy();
        }


         noUiSlider.create(sliderProyecto, {
            start: [rango_minimo, rango_maximo],
            step: 100,
            connect: true,
            tooltips: [ wNumb({ decimals: 0 }), wNumb({ decimals: 0 }) ],
            range: {
                'min': rango_minimo,
                'max': rango_maximo
            }
        });
        var marginMin = document.getElementById('slider-margin-value-min'),
            marginMax = document.getElementById('slider-margin-value-max');

        sliderProyecto.noUiSlider.on('update', function (values, handle) {
            if (handle) {
                marginMax.innerHTML = values[handle];
            } else {
                marginMin.innerHTML = values[handle];
            }
        });
        var margin_slider = document.getElementById('margin_slider');

        sliderProyecto.noUiSlider.on('change', function( values, handle ) {
            margin_slider.value = values;
        });
    </script>
    <script type="text/javascript">
        var rango_maximo = parseInt( document.getElementById('rango_maximo').value);
        var rango_minimo = parseInt( document.getElementById('rango_minimo').value);

        var sliderXsAvanzado = document.getElementById('slider-xs-avanzado');

        noUiSlider.create(sliderXsAvanzado, {
            start: [rango_minimo, rango_maximo],
            step: 100,
            connect: true,
            tooltips: [ wNumb({ decimals: 0 }), wNumb({ decimals: 0 }) ],
            range: {
                'min': rango_minimo,
                'max': rango_maximo
            }
        });
        var marginMin = document.getElementById('slider-margin-value-min'),
            marginMax = document.getElementById('slider-margin-value-max');

        sliderXsAvanzado.noUiSlider.on('update', function (values, handle) {
            if (handle) {
                marginMax.innerHTML = values[handle];
            } else {
                marginMin.innerHTML = values[handle];
            }
        });
        sliderXsAvanzado.noUiSlider.on('change', function( values, handle ) {
            margin_slider_avanzado.value = values;
        });

        var margin_slider_avanzado = document.getElementById('margin_slider_avanzado');

    </script>

    @include('includes.chat')

</body>
</html>
