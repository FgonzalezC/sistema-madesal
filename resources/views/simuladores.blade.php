<!DOCTYPE html>
<html lang="es-Es">
@include('includes.head')
<body>
	<!-- Loading -->
	<div class="loading">
		<div class="loader"></div>
	</div>
	<!-- /Loading -->	
	<header class="header-int">
			<div class="container">
					<div class="row">
						<div class="col-lg-3 col-2 brand-image">
							<a href="{{ url('/home')}}">
							<img data-src="images/logo-madesal.png" class="d-none d-lg-block lazy" alt="Madesal"s>
							<img data-src="images/logo-madesal-xs.png" class="d-block d-lg-none lazy" alt="Madesal">
							</a>
						</div>
						<div class="col-lg-9 col-10">
							<nav class="navbar navbar-expand-lg">
								<div class="top">
									<ul class="d-flex">
										@include('includes.menu-superior')
									</ul>
								</div>
							<button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
								aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
								<span></span>
								<span></span>
								<span></span>
							</button>
							<div class="collapse navbar-collapse" id="navbarSupportedContent">
								<ul class="navbar-nav">
									@include('includes.menu')
								</ul>
								<div class="box-mobile d-block d-lg-none">
								<ul class="box-mobile__btns">
									<li>
										<a  href="{{ url('/asesoria')}}">Ayuda</a>
									</li>
									<li>
										<a href="{{ url('/contacto')}}">Contacto</a>
									</li>
								</ul>
								<?php 
								$footer = \DB::table('footers')
												->select('img_footer','direccion','telefono','link_face','link_instagram','link_twitter')
												->get();
								?>

								@foreach($footer as $footers)
								<ul class="box-mobile__social">
									<li><a target="_blanck" href="{{$footers->link_face}}"><i class="icon-facebook"></i></a></li>
									<li><a target="_blanck" href="{{$footers->link_instagram}}"><i class="icon-instagram"></i></a></li>
									<li><a target="_blanck" href="{{$footers->link_twitter}}"><i class="icon-twitter"></i></a></li>
								</ul>
								@endforeach
							</div>
							</div>
						</nav>
					</div>
				</div>	
	</header>
 <?php  
            $simulador = \DB::table('simuladores')
                        
                        ->get();

            /*$seccionC = \DB::table('seccion_homes')->count();*/
    ?> 

	
	
	<!-- MAIN HEAD -->
	@foreach($simulador as $simuladores)
	<section class="main-head main-head--page">
		<div class="main-head__image">
            <img data-src="{{URL::asset('storage/'.$simuladores->img)}}" class="d-none d-md-block lazy" alt="Madesal">      
		</div>
		<div class="main-head__title">
			<h1>{{$simuladores->nombre}}</h1>
		</div>		
	</section>
	@endforeach
    <!-- /MAIN HEAD DESKTOP -->
	@include('includes.msj')
	@include('includes.msj3')


</div>
<?php 
	include '../public/simuladores.php';
?>
	<!-- COTIZA -->
	<section class="contactanos contactanos--sim">
		<div class="container">
			<div class="row">
				<div class="col-12 text-center primary-title">
					<h2>Cotiza tu proyecto inmobiliario</h2>
				</div>
				<div class="col-12">
						<form action="{{route('simulador_guardar')}}" id= "form-general" class="gnrl-form" method="POST" autocomplete="off">
								{{ csrf_field() }}

							<div class="row">
							<div class="col-lg-6">
									<input type="text" name="nombre_cotizacion" id="nombre_cotizacion" placeholder="Nombre" required>
								<input type="text" name="apellido_cotizacion" id="apellido_cotizacion" placeholder="Apellido"required>
								<input type="text" name="telefono_cotizacion" id="telefono_cotizacion" placeholder="Teléfono"required>
								<input type="text" name="email_cotizacion" id="email_cotizacion" placeholder="Email" required>
							</div>
							<div class="col-lg-6">
									
									<select name="asunto_id" id="asunto_id" required>
                                                    <option value="">Agregar Asunto</option>
                                                    @foreach(\App\Models\Admin\Asunto::all() as $asunto)
                                                    <option value="{{$asunto->id}}" required>{{$asunto->nombre_asunto}}</option>
                                                @endforeach
                                                
                                            </select>
                                    <textarea name="mensaje_cotizacion" id="mensaje_cotizacion" placeholder="Mensaje" required></textarea>
							<div class="col-lg-12 text-center">
								<button class="btn-gnrl btn-gnrl--green">ENVIAR</button>
							</div>
						</div>                        
					</form>
				</div>                
			</div>
		</div>
	</section>
			


	<!-- /COTIZA -->

	@include('includes.footer')

	<script src="js/jquery.min.js"></script>
	<script src="js/bootstrap.bundle.min.js"></script>
	<script src="js/lazyload.min.js"></script>
	<script src="js/nouislider.js"></script>
	<script src="js/wNumb.min.js"></script>
	<script src="js/slick.min.js"></script>
	<script src="js/lity.js"></script>
	<script src="js/main.js"></script>
	<script type="text/javascript" language="javascript">
		var stepsSlider = document.getElementById('valor-propiedad');
		var input0 = document.getElementById('input-with-keypress-0');		
		var inputs = [input0];
		var precio_propiedad = $('#input-with-keypress-0').val().trim();

		noUiSlider.create(stepsSlider, {
			start: [precio_propiedad],
			connect: true,			
			range: {
				'min': [0],
				'10%': [1000],
				'50%': [3000],
				'80%': 8000,
				'max': 10000
			}
		}); 

		stepsSlider.noUiSlider.on('update', function (values, handle) {
			inputs[handle].value = values[handle];
		});

		// Listen to keydown events on the input field.
		inputs.forEach(function (input, handle) {

		input.addEventListener('change', function () {
			stepsSlider.noUiSlider.setHandle(handle, this.value);
		});

		input.addEventListener('keydown', function (e) {

			var values = stepsSlider.noUiSlider.get();
			var value = Number(values[handle]);

			// [[handle0_down, handle0_up], [handle1_down, handle1_up]]
			var steps = stepsSlider.noUiSlider.steps();

			// [down, up]
			var step = steps[handle];

			var position;

			// 13 is enter,
			// 38 is key up,
			// 40 is key down.
			switch (e.which) {

				case 13:
					stepsSlider.noUiSlider.setHandle(handle, this.value);
					break;

				case 38:

					// Get step to go increase slider value (up)
					position = step[1];

					// false = no step is set
					if (position === false) {
						position = 1;
					}

					// null = edge of slider
					if (position !== null) {
						stepsSlider.noUiSlider.setHandle(handle, value + position);
					}

					break;

				case 40:

					position = step[0];

					if (position === false) {
						position = 1;
					}

					if (position !== null) {
						stepsSlider.noUiSlider.setHandle(handle, value - position);
					}

					break;
				}
			});
		});

		var stepsSliderPie = document.getElementById('valor-pie');
		var input1 = document.getElementById('input-with-keypress-1');		
		var inputs2 = [input1];
		var pie = $('#input-with-keypress-1').val().trim();


		noUiSlider.create(stepsSliderPie, {
			start: [pie],
			connect: true,			
			range: {
				'min': [0],
				'10%': [10],
				'50%': [50],
				'80%': 80,
				'max': 100
			},
			format: wNumb({
				decimals: 3,
				thousand: '.'
			})
		});

		stepsSliderPie.noUiSlider.on('update', function (values, handle) {
			inputs2[handle].value = values[handle];
		});

		// Listen to keydown events on the input field.
		inputs.forEach(function (input, handle) {

		input.addEventListener('change', function () {
			stepsSliderPie.noUiSlider.setHandle(handle, this.value);
		});

		input.addEventListener('keydown', function (e) {

			var values = stepsSliderPie.noUiSlider.get();
			var value = Number(values[handle]);

			// [[handle0_down, handle0_up], [handle1_down, handle1_up]]
			var piesteps = stepsSliderPie.noUiSlider.steps();

			// [down, up]
			var piestep = piesteps[handle];

			var position;

			// 13 is enter,
			// 38 is key up,
			// 40 is key down.
			switch (e.which) {

				case 13:
					stepsSliderPie.noUiSlider.setHandle(handle, this.value);
					break;

				case 38:

					// Get step to go increase slider value (up)
					position = piestep[1];

					// false = no step is set
					if (position === false) {
						position = 1;
					}

					// null = edge of slider
					if (position !== null) {
						stepsSliderPie.noUiSlider.setHandle(handle, value + position);
					}

					break;

				case 40:

					position = piestep[0];

					if (position === false) {
						position = 1;
					}

					if (position !== null) {
						stepsSliderPie.noUiSlider.setHandle(handle, value - position);
					}

					break;
				}
			});
		});

		var stepsSliderPie = document.getElementById('plazo');
		var input2 = document.getElementById('input-with-keypress-2');		
		var inputs3 = [input2];
		var plazo = $('#input-with-keypress-2').val().trim();



		noUiSlider.create(stepsSliderPie, {
			start: [plazo],
			connect: true,			
			range: {
				'min': [0],
				'10%': [5],
				'50%': [10],
				'80%': 15,
				'90%': 20,
				'max': 30
			},
			format: wNumb({
				decimals: 3,
				thousand: '.'
			})
		});

		/*
		var range = [
		    '1', '2', '3','4', '5', '6',
		    '7', '8', '9','10', '11','12',
		    '13','14','15'
		];

		bigValueSlider.noUiSlider.on('update', function (values, handle) {
		    bigValueSpan.innerHTML = range[values[handle]];
		});


		slider.noUiSlider.on("update", function(values, handle, unencoded ) {
		    //values: Current slider values;
		    //handle: Handle that caused the event;
		    //unencoded: Slider values without formatting;
		});

*/

		stepsSliderPie.noUiSlider.on('update', function (values, handle) {
			inputs3[handle].value = values[handle];
		});

		// Listen to keydown events on the input field.
		inputs.forEach(function (input, handle) {

		input.addEventListener('change', function () {
			stepsSliderPie.noUiSlider.setHandle(handle, this.value);
		});

		input.addEventListener('keydown', function (e) {

			var values = stepsSliderPie.noUiSlider.get();
			var value = Number(values[handle]);

			// [[handle0_down, handle0_up], [handle1_down, handle1_up]]
			var piesteps = stepsSliderPie.noUiSlider.steps();

			// [down, up]
			var piestep = piesteps[handle];

			var position;

			// 13 is enter,
			// 38 is key up,
			// 40 is key down.
			switch (e.which) {

				case 13:
					stepsSliderPie.noUiSlider.setHandle(handle, this.value);
					break;

				case 38:

					// Get step to go increase slider value (up)
					position = piestep[1];

					// false = no step is set
					if (position === false) {
						position = 1;
					}

					// null = edge of slider
					if (position !== null) {
						stepsSliderPie.noUiSlider.setHandle(handle, value + position);
					}

					break;

				case 40:

					position = piestep[0];

					if (position === false) {
						position = 1;
					}

					if (position !== null) {
						stepsSliderPie.noUiSlider.setHandle(handle, value - position);
					}

					break;
				}
			});
		});

		var stepsSliderPie = document.getElementById('tasa');
		var input3 = document.getElementById('input-with-keypress-3');		
		var inputs4 = [input3];
		var tasa = $('#input-with-keypress-3').val().trim();


		noUiSlider.create(stepsSliderPie, {
			start: [tasa],
			connect: true,			
			range: {
				'min': [0],
				'10%': [1],
				'50%': [2],
				'80%': 3,
				'max': 4
			}
		});

		stepsSliderPie.noUiSlider.on('update', function (values, handle) {
			inputs4[handle].value = values[handle];
		});

		// Listen to keydown events on the input field.
		inputs.forEach(function (input, handle) {

		input.addEventListener('change', function () {
			stepsSliderPie.noUiSlider.setHandle(handle, this.value);
		});

		input.addEventListener('keydown', function (e) {

			var values = stepsSliderPie.noUiSlider.get();
			var value = Number(values[handle]);

			// [[handle0_down, handle0_up], [handle1_down, handle1_up]]
			var piesteps = stepsSliderPie.noUiSlider.steps();

			// [down, up]
			var piestep = piesteps[handle];

			var position;

			// 13 is enter,
			// 38 is key up,
			// 40 is key down.
			switch (e.which) {

				case 13:
					stepsSliderPie.noUiSlider.setHandle(handle, this.value);
					break;

				case 38:

					// Get step to go increase slider value (up)
					position = piestep[1];

					// false = no step is set
					if (position === false) {
						position = 1;
					}

					// null = edge of slider
					if (position !== null) {
						stepsSliderPie.noUiSlider.setHandle(handle, value + position);
					}

					break;

				case 40:

					position = piestep[0];

					if (position === false) {
						position = 1;
					}

					if (position !== null) {
						stepsSliderPie.noUiSlider.setHandle(handle, value - position);
					}

					break;
				}
			});
		});

		var stepsSliderPie = document.getElementById('monto');
		var input4 = document.getElementById('input-with-keypress-4');		
		var inputs5 = [input4];
		var monto = $('#input-with-keypress-4').val().trim();
		

		noUiSlider.create(stepsSliderPie, {
			start: [monto],
			connect: true,			
			range: {
				'min': [0],
				'10%': [1000],
				'50%': [3000],
				'80%': 5000,
				'max': 10000
			}
		});

		stepsSliderPie.noUiSlider.on('update', function (values, handle) {
			inputs5[handle].value = values[handle];
		});

		// Listen to keydown events on the input field.
		inputs.forEach(function (input, handle) {

		input.addEventListener('change', function () {
			stepsSliderPie.noUiSlider.setHandle(handle, this.value);
		});

		input.addEventListener('keydown', function (e) {

			var values = stepsSliderPie.noUiSlider.get();
			var value = Number(values[handle]);

			// [[handle0_down, handle0_up], [handle1_down, handle1_up]]
			var piesteps = stepsSliderPie.noUiSlider.steps();

			// [down, up]
			var piestep = piesteps[handle];

			var position;

			// 13 is enter,
			// 38 is key up,
			// 40 is key down.
			switch (e.which) {

				case 13:
					stepsSliderPie.noUiSlider.setHandle(handle, this.value);
					break;

				case 38:

					// Get step to go increase slider value (up)
					position = piestep[1];

					// false = no step is set
					if (position === false) {
						position = 1;
					}

					// null = edge of slider
					if (position !== null) {
						stepsSliderPie.noUiSlider.setHandle(handle, value + position);
					}

					break;

				case 40:

					position = piestep[0];

					if (position === false) {
						position = 1;
					}

					if (position !== null) {
						stepsSliderPie.noUiSlider.setHandle(handle, value - position);
					}

					break;
				}
			});
		});

		var stepsSliderPie = document.getElementById('div-plazo');
		var input5 = document.getElementById('input-with-keypress-5');		
		var inputs6 = [input5];
		var div_plazo = $('#input-with-keypress-5').val().trim();
		noUiSlider.create(stepsSliderPie, {
			start: [div_plazo],
			connect: true,			
			range: {
				'min': [0],
				'10%': [10],
				'50%': [50],
				'80%': 80,
				'max': 100
			},
			format: wNumb({
				decimals: 3,
				thousand: '.'
			})
		});

		stepsSliderPie.noUiSlider.on('update', function (values, handle) {
			inputs6[handle].value = values[handle];
		});

		// Listen to keydown events on the input field.
		inputs.forEach(function (input, handle) {

		input.addEventListener('change', function () {
			stepsSliderPie.noUiSlider.setHandle(handle, this.value);
		});

		input.addEventListener('keydown', function (e) {

			var values = stepsSliderPie.noUiSlider.get();
			var value = Number(values[handle]);

			// [[handle0_down, handle0_up], [handle1_down, handle1_up]]
			var piesteps = stepsSliderPie.noUiSlider.steps();

			// [down, up]
			var piestep = piesteps[handle];

			var position;

			// 13 is enter,
			// 38 is key up,
			// 40 is key down.
			switch (e.which) {

				case 13:
					stepsSliderPie.noUiSlider.setHandle(handle, this.value);
					break;

				case 38:

					// Get step to go increase slider value (up)
					position = piestep[1];

					// false = no step is set
					if (position === false) {
						position = 1;
					}

					// null = edge of slider
					if (position !== null) {
						stepsSliderPie.noUiSlider.setHandle(handle, value + position);
					}

					break;

				case 40:

					position = piestep[0];

					if (position === false) {
						position = 1;
					}

					if (position !== null) {
						stepsSliderPie.noUiSlider.setHandle(handle, value - position);
					}

					break;
				}
			});
		});

		var stepsSliderPie = document.getElementById('div-tasa');
		var input6 = document.getElementById('input-with-keypress-6');		
		var inputs7 = [input6];
		var div_tasa = $('#input-with-keypress-6').val().trim();

		noUiSlider.create(stepsSliderPie, {
			start: [div_tasa],
			connect: true,			
			range: {
				'min': [0],
				'10%': [1],
				'50%': [2],
				'80%': 3,
				'max': 4
			}
		});

		stepsSliderPie.noUiSlider.on('update', function (values, handle) {
			inputs7[handle].value = values[handle];
		});

		// Listen to keydown events on the input field.
		inputs.forEach(function (input, handle) {

		input.addEventListener('change', function () {
			stepsSliderPie.noUiSlider.setHandle(handle, this.value);
		});

		input.addEventListener('keydown', function (e) {

			var values = stepsSliderPie.noUiSlider.get();
			var value = Number(values[handle]);

			// [[handle0_down, handle0_up], [handle1_down, handle1_up]]
			var piesteps = stepsSliderPie.noUiSlider.steps();

			// [down, up]
			var piestep = piesteps[handle];

			var position;

			// 13 is enter,
			// 38 is key up,
			// 40 is key down.
			switch (e.which) {

				case 13:
					stepsSliderPie.noUiSlider.setHandle(handle, this.value);
					break;

				case 38:

					// Get step to go increase slider value (up)
					position = piestep[1];

					// false = no step is set
					if (position === false) {
						position = 1;
					}

					// null = edge of slider
					if (position !== null) {
						stepsSliderPie.noUiSlider.setHandle(handle, value + position);
					}

					break;

				case 40:

					position = piestep[0];

					if (position === false) {
						position = 1;
					}

					if (position !== null) {
						stepsSliderPie.noUiSlider.setHandle(handle, value - position);
					}

					break;
				}
			});
		});
		

		var stepsSliderPie = document.getElementById('cap-tasa');
		var input7 = document.getElementById('input-with-keypress-7');		
		var inputs8 = [input7];
		var cap_tasa = $('#input-with-keypress-7').val().trim();

		noUiSlider.create(stepsSliderPie, {
			start: [cap_tasa],
			connect: true,			
			range: {
				'min': [0],
				'10%': [1],
				'50%': [2],
				'80%': 3,
				'max': 4
			}
		});

		stepsSliderPie.noUiSlider.on('update', function (values, handle) {
			inputs8[handle].value = values[handle];
		});

		// Listen to keydown events on the input field.
		inputs.forEach(function (input, handle) {

		input.addEventListener('change', function () {
			stepsSliderPie.noUiSlider.setHandle(handle, this.value);
		});

		input.addEventListener('keydown', function (e) {

			var values = stepsSliderPie.noUiSlider.get();
			var value = Number(values[handle]);

			// [[handle0_down, handle0_up], [handle1_down, handle1_up]]
			var piesteps = stepsSliderPie.noUiSlider.steps();

			// [down, up]
			var piestep = piesteps[handle];

			var position;

			// 13 is enter,
			// 38 is key up,
			// 40 is key down.
			switch (e.which) {

				case 13:
					stepsSliderPie.noUiSlider.setHandle(handle, this.value);
					break;

				case 38:

					// Get step to go increase slider value (up)
					position = piestep[1];

					// false = no step is set
					if (position === false) {
						position = 1;
					}

					// null = edge of slider
					if (position !== null) {
						stepsSliderPie.noUiSlider.setHandle(handle, value + position);
					}

					break;

				case 40:

					position = piestep[0];

					if (position === false) {
						position = 1;
					}

					if (position !== null) {
						stepsSliderPie.noUiSlider.setHandle(handle, value - position);
					}

					break;
				}
			});
		});

		var stepsSliderPie = document.getElementById('cap-plazo');
		var input8 = document.getElementById('input-with-keypress-8');		
		var inputs9 = [input8];
		var cap_plazo = $('#input-with-keypress-8').val().trim();

		noUiSlider.create(stepsSliderPie, {
			start: [cap_plazo],
			connect: true,			
			range: {
				'min': [5],
				'10%': [10],
				'50%': [15],
				'80%': 25,
				'max': 30
			}
		});

		stepsSliderPie.noUiSlider.on('update', function (values, handle) {
			inputs9[handle].value = values[handle];
		});

		// Listen to keydown events on the input field.
		inputs.forEach(function (input, handle) {

		input.addEventListener('change', function () {
			stepsSliderPie.noUiSlider.setHandle(handle, this.value);
		});

		input.addEventListener('keydown', function (e) {

			var values = stepsSliderPie.noUiSlider.get();
			var value = Number(values[handle]);

			// [[handle0_down, handle0_up], [handle1_down, handle1_up]]
			var piesteps = stepsSliderPie.noUiSlider.steps();

			// [down, up]
			var piestep = piesteps[handle];

			var position;

			// 13 is enter,
			// 38 is key up,
			// 40 is key down.
			switch (e.which) {

				case 13:
					stepsSliderPie.noUiSlider.setHandle(handle, this.value);
					break;

				case 38:

					// Get step to go increase slider value (up)
					position = piestep[1];

					// false = no step is set
					if (position === false) {
						position = 1;
					}

					// null = edge of slider
					if (position !== null) {
						stepsSliderPie.noUiSlider.setHandle(handle, value + position);
					}

					break;

				case 40:

					position = piestep[0];

					if (position === false) {
						position = 1;
					}

					if (position !== null) {
						stepsSliderPie.noUiSlider.setHandle(handle, value - position);
					}

					break;
				}
			});
		});
		

		function calcular_divi(){
			var parametros = $('#form-dividendo').serialize();

			var msj_error = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button><center><label style="color:#4c4c4c"><strong>Error al realizar cálculos. Ingrese todos los datos e intente Nuevamente</strong></label></center></div>';

			$.ajax({
		        data: parametros,
		        url: 'includes/calcular_dividendo.php',
		        type: 'post',
		        async: false,
		        success: function(response) {
		        	//alert(response);
		            if (response==0){
		            	$('#capa-dividendo').hide();
		                $('#error').html(msj_error);
		            }else{
		            	$('#capa-dividendo').show();
		                $('#respuesta_dividendo').html(response);
		            }
		        }
		    });
		 } 

		 function calcular_hipo(){
			var parametros = $('#form-hipotecario').serialize();

			var msj_error = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button><center><label style="color:#4c4c4c"><strong>Error al realizar cálculos. Ingrese todos los datos e intente Nuevamente</strong></label></center></div>';

			$.ajax({
		        data: parametros,
		        url: 'includes/calcular_hipotecario.php',
		        type: 'post',
		        async: false,
		        success: function(response) {
		        	//alert(response);
		            if (response==0){
		            	$('#capa-hipotecario').hide();
		                $('#error').html(msj_error);
		            }else{
		            	$('#capa-hipotecario').show();
		                $('#respuesta_hipotecario').html(response);
		            }
		        }
		    });
		 } 

		 function calcular_deuda(){
			var parametros = $('#form-deuda').serialize();

			var msj_error = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button><center><label style="color:#4c4c4c"><strong>Error al realizar cálculos. Ingrese todos los datos e intente Nuevamente</strong></label></center></div>';

			$.ajax({
		        data: parametros,
		        url: 'includes/calcular_endeudamiento.php',
		        type: 'post',
		        async: false,
		        success: function(response) {
		        //	alert(response);
		            if (response==0){
		            	$('#capa-deuda').hide();
		                $('#error').html(msj_error);
		            }else{
		            	$('#capa-deuda').show();
		                $('#respuesta_deuda').html(response);
		            }
		        }
		    });
		 } 

		var btn = document.getElementById('btsubmit');
			btn.addEventListener('click',function(e){
			e.preventDefault();
		})

		var bhn = document.getElementById('bhsubmit');
			bhn.addEventListener('click',function(e){
			e.preventDefault();
		})
	</script>	
	@include('includes.chat')

</body>
</html>