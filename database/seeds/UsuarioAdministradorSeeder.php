<?php

use Illuminate\Database\Seeder;
class UsuarioAdministradorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      
        DB::table('usuarios')->insert([
            'nombre_usuario'=> 'Camila',
            'apellido_usuario'=>'Iturra',
            'password'=>bcrypt('cami123'),
            'email'=>'citurra@gmail.com'
        ]);
        DB::table('usuarios')->insert([
            'nombre_usuario'=> 'Cesar',
            'apellido_usuario'=>'Muñoz',
            'password'=>bcrypt('cesar123'),
            'email'=>'ccastillo@gmail.com'
        ]);
        
        DB::table('usuariosroles')->insert([
            'rol_id'=>1,
            'usuario_id'=>3,
            'estado'=>1
        ]);
        DB::table('usuariosroles')->insert([
            'rol_id'=>1,
            'usuario_id'=>4,
            'estado'=>1
        ]);
    }
}
