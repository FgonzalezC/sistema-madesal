<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModelosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modelos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre_modelo');
            $table->string('bajada');
            $table->integer('mtrs');
            $table->foreign('tipoinmueble_id')->references('id')->on('tipo_inmuebles');
            $table->unsignedBigInteger('tipoinmueble_id');
            $table->integer('banos');
            $table->integer('dormitorio');
            $table->foreign('tipococina_id')->references('id')->on('tipococinas');
            $table->unsignedBigInteger('tipococina_id');
            $table->integer('estacionamiento');
            $table->text('terminaciones',300);
            $table->foreign('proyecto_id')->references('id')->on('proyectos');
            $table->unsignedBigInteger('proyecto_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('modelos');
    }
}
