<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCotizacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cotizaciones', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre_cotizacion');
            $table->string('apellido_cotizacion');
            $table->string('telefono_cotizacion');
            $table->string('email_cotizacion');
            $table->foreign('asunto_id')->references('id')->on('asuntos');
            $table->unsignedBigInteger('asunto_id');
            $table->string('mensaje_cotizacion');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cotizaciones');
    }
}
