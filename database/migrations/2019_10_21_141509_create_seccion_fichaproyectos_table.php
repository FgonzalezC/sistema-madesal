<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeccionFichaproyectosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seccion_fichaproyectos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreign('modelo_id')->references('id')->on('modelos');
            $table->unsignedBigInteger('modelo_id');
            $table->string('titulo');
            $table->string('img');
            $table->text('terminaciones');
            $table->text('equipamiento',300);
            $table->text('ubicacion_entorno',300);
            $table->string('imgfinanciamiento');
            $table->string('timg1');
            $table->string('timg2');
            $table->string('timg3');
            $table->string('timg4');
            $table->string('timg5');
            $table->string('eimg1');
            $table->string('eimg2');
            $table->string('eimg3');
            $table->string('eimg4');
            $table->string('eimg5');
            $table->string('uimg1');
            $table->string('uimg2');
            $table->string('uimg3');
            $table->string('uimg4');
            $table->string('uimg5');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seccion_fichaproyectos');
    }
}
