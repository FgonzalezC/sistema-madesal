<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeccionInversionistasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seccion_inversionistas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('titulo');
            $table->string('img');
            $table->string('subtitulo');
            $table->string('texto1');
            $table->string('texto2');
            $table->string('titulo1');
            $table->text('bajada1');
            $table->string('titulo2');
            $table->text('bajada2');
            $table->string('titulo3');
            $table->text('bajada3');
            $table->string('titulo4');
            $table->text('bajada4');
            $table->string('seccion1');
            $table->string('seccion2');
            $table->string('punto1');
            $table->string('punto2');
            $table->string('punto3');
            $table->string('punto4');
            $table->string('titulodestacado');
            $table->string('img1');
            $table->string('img2');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seccion_inversionistas');
    }
}
