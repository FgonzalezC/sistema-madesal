<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeccionHomesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seccion_homes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('titulo_principal',1000);
            $table->string('titulo_principal2',1000);
            $table->string('img');
            $table->text('subtitulo',1000);
            $table->string('subtitulo1',1000);
            $table->string('subtitulo2',1000);
            $table->text('texto',1000);
            $table->string('img1',100);
            $table->string('img2',100);
            $table->string('img3',100);
            $table->string('titulo_movil');
            $table->string('msubtitulo1',1000);
            $table->string('msubtitulo2',1000);
            $table->string('msubtitulo3',1000);
            $table->string('msubtitulo4',1000);
            $table->string('movilimg1',100);
            $table->string('movilimg2',100);
            $table->string('movilimg3',100);
            $table->timestamps();
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_spanish_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seccion_homes');
    }
}
