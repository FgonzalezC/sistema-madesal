<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeccionProcesocompasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seccion_procesocompas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('titulo');
            $table->string('titulo1');
            $table->string('bajada1');
            $table->string('img1');
            $table->string('titulo2');
            $table->string('bajada2');
            $table->string('img2');
            $table->string('titulo3');
            $table->string('bajada3');
            $table->string('img3');
            $table->string('titulo4');
            $table->string('bajada4');
            $table->string('img4');
            $table->string('titulo5');
            $table->string('bajada5');
            $table->string('img5');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seccion_procesocompas');
    }
}
