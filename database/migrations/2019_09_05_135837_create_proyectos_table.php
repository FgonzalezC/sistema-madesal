<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProyectosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proyectos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre_proyecto',50)->nullable();
            $table->foreign('tipoetapa_id')->references('id')->on('tipo_etapas');
            $table->unsignedBigInteger('tipoetapa_id');
            $table->foreign('tipoinmueble_id')->references('id')->on('tipo_inmuebles');
            $table->unsignedBigInteger('tipoinmueble_id');
            $table->string('dormitorios')->nullable();
            $table->string('banos')->nullable();
            $table->string('estacionamiento')->nullable();
            $table->boolean('destacado')->default(1);
            $table->boolean('destacado2')->default(1);
            $table->string('superficie')->nullable();
            $table->integer('precio')->nullable();
            $table->string('img_home',100);
            $table->string('masterplan',100);
            $table->string('desktopimg1',100);
            $table->string('desktopimg2',100);
            $table->string('desktopimg3',100);
            $table->string('desktopimg4',100);
            $table->string('desktopimg5',100);
            $table->string('movilimg1',100);
            $table->string('movilimg2',100);
            $table->string('movilimg3',100);
            $table->string('movilimg4',100);
            $table->string('movilimg5',100);
            $table->string('descripcion_slider',1000);
            $table->foreign('comuna_id')->references('id')->on('comunas');
            $table->unsignedBigInteger('comuna_id');
            $table->foreign('estado_id')->references('id')->on('estado_proyectos');
            $table->unsignedBigInteger('estado_id');
            $table->foreign('subsidio_id')->references('id')->on('subsidios');
            $table->unsignedBigInteger('subsidio_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proyectos');
    }
}
