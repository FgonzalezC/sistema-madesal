<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostventasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('postventas', function (Blueprint $table) {
            $table->bigIncrements('id');
             $table->string('titulo');
            $table->string('img1');
            $table->string('titulo1');
            $table->string('btn1');
            $table->string('btn2');
            $table->string('btn3');
            $table->string('btn4');
            $table->string('btn5');
            $table->string('btn6');
            $table->string('titulo2');      
            $table->string('btn7');
            $table->string('btn8');
            $table->string('btn9');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('postventas');
    }
}
